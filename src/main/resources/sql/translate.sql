/*
Navicat MySQL Data Transfer

Source Server         : 116.196.84.253
Source Server Version : 50721
Source Host           : 116.196.84.253:3306
Source Database       : translate

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-05-11 11:42:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for complaint
-- ----------------------------
DROP TABLE IF EXISTS `complaint`;
CREATE TABLE `complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `imageone` varchar(255) DEFAULT NULL,
  `imagetwo` varchar(255) DEFAULT NULL,
  `imagethree` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉表';

-- ----------------------------
-- Records of complaint
-- ----------------------------

-- ----------------------------
-- Table structure for t_authentication_cash
-- ----------------------------
DROP TABLE IF EXISTS `t_authentication_cash`;
CREATE TABLE `t_authentication_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authenticationid` int(11) DEFAULT NULL COMMENT '老师认证id',
  `money` float DEFAULT NULL COMMENT '保证金金额',
  `states` int(11) DEFAULT NULL COMMENT '缴纳状态 1缴纳 2未缴纳',
  `paytype` int(11) DEFAULT NULL COMMENT '支付方式',
  `isteacher` int(11) DEFAULT NULL COMMENT '是老师还是译员 1老师 2译员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='认证保证金缴纳表';

-- ----------------------------
-- Records of t_authentication_cash
-- ----------------------------

-- ----------------------------
-- Table structure for t_authentication_certificate
-- ----------------------------
DROP TABLE IF EXISTS `t_authentication_certificate`;
CREATE TABLE `t_authentication_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authenticationid` int(11) DEFAULT NULL COMMENT '老师认证id',
  `number` varchar(255) DEFAULT NULL COMMENT '证书编号',
  `twoid` int(255) DEFAULT NULL COMMENT '证书二级id',
  `time` varchar(255) DEFAULT NULL COMMENT '有效时间',
  `isteacher` int(11) DEFAULT NULL COMMENT '是否老师 1老师 2译员',
  `threeid` int(11) DEFAULT NULL COMMENT '证书三级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='认证证书表';

-- ----------------------------
-- Records of t_authentication_certificate
-- ----------------------------
INSERT INTO `t_authentication_certificate` VALUES ('106', '5', '453543', '2', '2018/04/27', '2', '2');
INSERT INTO `t_authentication_certificate` VALUES ('107', '9', '32432432', '1', '2018/04/27', '2', '1');
INSERT INTO `t_authentication_certificate` VALUES ('108', '7', '6576576576', '1', '2018/04/27', '2', '1');
INSERT INTO `t_authentication_certificate` VALUES ('109', null, '', '2', '2018/05/07', '2', '2');

-- ----------------------------
-- Table structure for t_authentication_language
-- ----------------------------
DROP TABLE IF EXISTS `t_authentication_language`;
CREATE TABLE `t_authentication_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `languageid` int(11) DEFAULT NULL COMMENT '语种id',
  `grade` varchar(255) DEFAULT NULL COMMENT '语言等级',
  `authenticationid` int(11) DEFAULT NULL COMMENT '老师认证id',
  `certificate` varchar(255) DEFAULT NULL COMMENT '证书编号',
  `isteacher` int(11) DEFAULT NULL COMMENT '是否老师 1老师 2译员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='认证语种关联表';

-- ----------------------------
-- Records of t_authentication_language
-- ----------------------------
INSERT INTO `t_authentication_language` VALUES ('11', '1', '2', '10', '2222222222222222', '2');
INSERT INTO `t_authentication_language` VALUES ('44', '2', '2', '9', '2222', '1');
INSERT INTO `t_authentication_language` VALUES ('45', '1', 'A1', '5', '145584', '2');
INSERT INTO `t_authentication_language` VALUES ('50', '1', 'A2', '5', '453543535', '2');
INSERT INTO `t_authentication_language` VALUES ('51', '1', 'A1', '9', '34324', '2');
INSERT INTO `t_authentication_language` VALUES ('52', '1', 'A1', null, '4543254354', '2');
INSERT INTO `t_authentication_language` VALUES ('53', '1', 'A1', null, '', '2');
INSERT INTO `t_authentication_language` VALUES ('54', '1', 'A1', '7', 'yr758967589675895', '2');
INSERT INTO `t_authentication_language` VALUES ('55', '1', 'A2', '7', '435353', '2');

-- ----------------------------
-- Table structure for t_authentication_school
-- ----------------------------
DROP TABLE IF EXISTS `t_authentication_school`;
CREATE TABLE `t_authentication_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authenticationid` int(11) DEFAULT NULL COMMENT '老师认证id',
  `graduationdate` datetime DEFAULT NULL COMMENT '毕业时间',
  `school` varchar(255) DEFAULT NULL COMMENT '毕业学校',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `major` varchar(255) DEFAULT NULL COMMENT '专业',
  `isteacher` int(11) DEFAULT NULL COMMENT '是否老师 1老师 2译员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 COMMENT='认证毕业学校关联表';

-- ----------------------------
-- Records of t_authentication_school
-- ----------------------------
INSERT INTO `t_authentication_school` VALUES ('30', '10', '2018-04-10 00:00:00', '聊', '本科', '计算机', '2');
INSERT INTO `t_authentication_school` VALUES ('39', '11', '2018-04-11 00:00:00', '32432', '本科', '234234', '2');
INSERT INTO `t_authentication_school` VALUES ('48', '6', '2018-04-13 00:00:00', '二院', '博士', '二院', '2');
INSERT INTO `t_authentication_school` VALUES ('58', '8', '2018-04-17 00:00:00', 'sdfgfd', '本科在读', 'fdsgds', '2');
INSERT INTO `t_authentication_school` VALUES ('59', '8', '2018-04-17 00:00:00', 'dfsgfds', '专科', 'fdgfsd', '2');
INSERT INTO `t_authentication_school` VALUES ('74', '10', '2018-04-23 00:00:00', '二院', '本科', '肉44646', '2');
INSERT INTO `t_authentication_school` VALUES ('80', '9', '2018-04-24 00:00:00', '二院', '本科', '热汤', '2');
INSERT INTO `t_authentication_school` VALUES ('83', '6', '2018-04-24 00:00:00', '二院', '研究生', '的热特', '2');
INSERT INTO `t_authentication_school` VALUES ('107', null, '2018-04-25 00:00:00', '不了', '本科', '吕他了', '2');
INSERT INTO `t_authentication_school` VALUES ('112', null, '2018-04-26 00:00:00', '二院', '本科', '明早起来', '2');
INSERT INTO `t_authentication_school` VALUES ('115', null, '2018-04-28 00:00:00', '分工会突然有人要', '研究生', '壵', '2');
INSERT INTO `t_authentication_school` VALUES ('117', null, '2018-04-26 00:00:00', '哦婆婆去', '研究生', '哦婆婆去', '2');
INSERT INTO `t_authentication_school` VALUES ('122', '5', '2018-04-28 00:00:00', '定日子去', '本科', '哦婆婆给我', '2');
INSERT INTO `t_authentication_school` VALUES ('124', null, '2012-06-01 00:00:00', '', '本科', '药学(英语)', '2');
INSERT INTO `t_authentication_school` VALUES ('125', null, '2018-04-27 00:00:00', '山东', '本科', '聊城', '2');
INSERT INTO `t_authentication_school` VALUES ('126', null, '2018-04-27 00:00:00', '山东', '本科在读', '聊城', '2');
INSERT INTO `t_authentication_school` VALUES ('127', null, '2018-04-27 00:00:00', '热同仁堂', '本科在读', '而台湾儿童', '2');
INSERT INTO `t_authentication_school` VALUES ('128', null, '2018-07-15 00:00:00', '华南师范大学', '研究生', '课程与教学论', '2');
INSERT INTO `t_authentication_school` VALUES ('129', null, '2012-07-01 00:00:00', '沈阳药科大学', '本科', '药学(英语)', '2');
INSERT INTO `t_authentication_school` VALUES ('130', null, '2020-03-04 00:00:00', '天津外国语大学', '研究生', '英语口译', '2');
INSERT INTO `t_authentication_school` VALUES ('131', null, '2020-03-07 00:00:00', '天津外国语大学', '研究生', '英语口译', '2');
INSERT INTO `t_authentication_school` VALUES ('132', null, '2020-03-07 00:00:00', '天津外国语大学', '研究生', '英语口译', '2');
INSERT INTO `t_authentication_school` VALUES ('133', null, '2020-03-07 00:00:00', '天津外国语大学', '研究生', '英语口译', '2');
INSERT INTO `t_authentication_school` VALUES ('134', null, '2018-05-07 00:00:00', '两层', '博士', '柯博士', '1');
INSERT INTO `t_authentication_school` VALUES ('135', null, '2018-05-09 00:00:00', '南开大学', '本科', '金融', '2');
INSERT INTO `t_authentication_school` VALUES ('136', null, '2008-05-08 00:00:00', '南开大学', '本科', '金融', '2');
INSERT INTO `t_authentication_school` VALUES ('137', null, '2017-06-09 00:00:00', '天津师范大学津沽学院', '本科', '俄语', '2');

-- ----------------------------
-- Table structure for t_authentication_studying
-- ----------------------------
DROP TABLE IF EXISTS `t_authentication_studying`;
CREATE TABLE `t_authentication_studying` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authenticationid` int(11) DEFAULT NULL COMMENT '老师认证id',
  `school` varchar(255) DEFAULT NULL COMMENT '留学学校',
  `country` varchar(255) DEFAULT NULL COMMENT '留学国家',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `major` varchar(255) DEFAULT NULL COMMENT '专业',
  `begintime` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `isteacher` int(11) DEFAULT NULL COMMENT '是否老师 1老师 2译员',
  `endtime` varchar(255) DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='认证留学经验关联表';

-- ----------------------------
-- Records of t_authentication_studying
-- ----------------------------
INSERT INTO `t_authentication_studying` VALUES ('59', '5', '二院', '中国', '本科在读', 'erererer', '2018-04-29', '2', '2018-04-26');
INSERT INTO `t_authentication_studying` VALUES ('60', '5', '二院', '中国', '本科在读', 'erererer', '2018-04-29', '2', '2018-04-26');
INSERT INTO `t_authentication_studying` VALUES ('61', null, 'Universidad de Burgos', 'null', '本科', 'Literatura', '2016-08-30', '2', '2017-06-30');
INSERT INTO `t_authentication_studying` VALUES ('62', null, 'Universidad de Burgos', 'null', '本科', 'Literatura', '2016-08-30', '2', '2017-06-30');
INSERT INTO `t_authentication_studying` VALUES ('63', null, '你婆婆9曲', '美国', '本科在读', '你没时间宿舍', '2018-04-27', '2', '2018-04-27');
INSERT INTO `t_authentication_studying` VALUES ('64', '9', '34324', '美国', '专科', '3243242', '2018-04-27', '2', '2018-04-27');
INSERT INTO `t_authentication_studying` VALUES ('65', null, '山东', '中国', '本科', '大丰港', '2018-04-27', '2', '2018-04-29');
INSERT INTO `t_authentication_studying` VALUES ('66', null, '华南师范大学', '中国', '研究生', '课程与教学论', '2015-08-15', '2', '2018-07-15');
INSERT INTO `t_authentication_studying` VALUES ('67', null, '', '中国', '', '', '', '2', '');

-- ----------------------------
-- Table structure for t_authorityfilter
-- ----------------------------
DROP TABLE IF EXISTS `t_authorityfilter`;
CREATE TABLE `t_authorityfilter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '认证权限名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='认证权限表';

-- ----------------------------
-- Records of t_authorityfilter
-- ----------------------------

-- ----------------------------
-- Table structure for t_certificate_three_type
-- ----------------------------
DROP TABLE IF EXISTS `t_certificate_three_type`;
CREATE TABLE `t_certificate_three_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twotypeid` int(11) DEFAULT NULL COMMENT '二级类别id',
  `name` varchar(255) DEFAULT NULL COMMENT '证书名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='证书三级类别表';

-- ----------------------------
-- Records of t_certificate_three_type
-- ----------------------------
INSERT INTO `t_certificate_three_type` VALUES ('1', '1', '法国专业二级证书');
INSERT INTO `t_certificate_three_type` VALUES ('2', '2', '专业英语二级证书');
INSERT INTO `t_certificate_three_type` VALUES ('3', '3', '专业德语二级证书');
INSERT INTO `t_certificate_three_type` VALUES ('4', '4', '专业汉语二级证书');

-- ----------------------------
-- Table structure for t_certificate_two_type
-- ----------------------------
DROP TABLE IF EXISTS `t_certificate_two_type`;
CREATE TABLE `t_certificate_two_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isyu` bit(1) DEFAULT NULL COMMENT '是否语言类',
  `twotype` varchar(255) DEFAULT NULL COMMENT '二级类别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='证书二级类别表';

-- ----------------------------
-- Records of t_certificate_two_type
-- ----------------------------
INSERT INTO `t_certificate_two_type` VALUES ('1', '', '法语');
INSERT INTO `t_certificate_two_type` VALUES ('2', '', '英语');
INSERT INTO `t_certificate_two_type` VALUES ('3', '', '德语');
INSERT INTO `t_certificate_two_type` VALUES ('4', '', '汉语');

-- ----------------------------
-- Table structure for t_collect
-- ----------------------------
DROP TABLE IF EXISTS `t_collect`;
CREATE TABLE `t_collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `collecttype` int(11) DEFAULT NULL COMMENT '收藏类型  1老师 2会议 3会展',
  `collectid` int(11) DEFAULT NULL COMMENT '收藏id',
  `time` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户收藏表';

-- ----------------------------
-- Records of t_collect
-- ----------------------------

-- ----------------------------
-- Table structure for t_complaint
-- ----------------------------
DROP TABLE IF EXISTS `t_complaint`;
CREATE TABLE `t_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) DEFAULT NULL COMMENT '订单号',
  `ordertype` int(11) DEFAULT NULL COMMENT '订单类别',
  `centont` varchar(255) DEFAULT NULL COMMENT '详细描述',
  `imageone` varchar(255) DEFAULT NULL COMMENT '图片1',
  `imagetwo` varchar(255) DEFAULT NULL COMMENT '图片2',
  `imagethree` varchar(255) DEFAULT NULL COMMENT '图片3',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户投诉表';

-- ----------------------------
-- Records of t_complaint
-- ----------------------------

-- ----------------------------
-- Table structure for t_conference
-- ----------------------------
DROP TABLE IF EXISTS `t_conference`;
CREATE TABLE `t_conference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `synopsis` varchar(255) DEFAULT NULL COMMENT '简介',
  `price` float DEFAULT NULL COMMENT '价格',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `host` varchar(255) DEFAULT NULL COMMENT '举办地',
  `sponsor` varchar(255) DEFAULT NULL COMMENT '主办方',
  `contractor` varchar(255) DEFAULT NULL COMMENT '承办方',
  `apply` varchar(255) DEFAULT NULL COMMENT '报名条件',
  `introduceimage` varchar(255) DEFAULT NULL COMMENT '会议介绍图片',
  `num` int(11) DEFAULT NULL COMMENT '参会人数',
  `isapply` bit(1) DEFAULT NULL COMMENT '是否同意报名参会',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `userid` int(11) DEFAULT NULL COMMENT '发布人id',
  `creattime` datetime DEFAULT NULL COMMENT '发布时间',
  `states` int(11) DEFAULT NULL COMMENT '订单状态  1待支付 2已支付未确认  3已支付已确认 4待退款 5已退款 6已取消 7已完成 8审核未通过 9审核通过 ',
  `ordernum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会议表';

-- ----------------------------
-- Records of t_conference
-- ----------------------------
INSERT INTO `t_conference` VALUES ('1', '/upload/1523848346066.jpg', 'erte w', 'ret retr', '546', '2018-04-18 00:00:00', 'FDSGFDGD', 'gfdsgdfg', '梵蒂冈梵蒂冈反对', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '/upload/1523848380298.jpg', '243', '\0', '565465464', '52', '2018-04-16 11:13:03', null, null);

-- ----------------------------
-- Table structure for t_conference_apply
-- ----------------------------
DROP TABLE IF EXISTS `t_conference_apply`;
CREATE TABLE `t_conference_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conferenceid` int(11) DEFAULT NULL COMMENT '会议id',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机',
  `paystates` int(11) DEFAULT NULL COMMENT '支付状态 1未支付 2已支付',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `pay` int(11) DEFAULT NULL COMMENT '支付方式 1微信支付 2支付宝 3余额支付',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `type` varchar(255) DEFAULT NULL COMMENT '身份类型 1个人 2企业',
  `company` varchar(255) DEFAULT NULL COMMENT '企业名称',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `num` int(11) DEFAULT NULL COMMENT '报名数量',
  `states` int(11) DEFAULT NULL COMMENT '会议报名状态（1同意 2拒绝 3未处理）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会议报名表';

-- ----------------------------
-- Records of t_conference_apply
-- ----------------------------
INSERT INTO `t_conference_apply` VALUES ('1', '1', '张三', '123445677', '1', '90', '1', '2018-05-03 11:39:22', '个人', '天津智联', '天津市河西区青林大厦', 'adfasf@qq.com', '2', '3');

-- ----------------------------
-- Table structure for t_consumption
-- ----------------------------
DROP TABLE IF EXISTS `t_consumption`;
CREATE TABLE `t_consumption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `money` float DEFAULT NULL COMMENT '操作金额',
  `ispay` bit(1) DEFAULT b'0' COMMENT '充值还是提现 false为提现',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户充值提现记录表';

-- ----------------------------
-- Records of t_consumption
-- ----------------------------
INSERT INTO `t_consumption` VALUES ('1', '3', '1000', '', '2018-04-19 15:07:51', '是打发斯蒂芬说道');
INSERT INTO `t_consumption` VALUES ('2', '3', '200', '\0', '2018-05-04 09:23:07', '阿什顿发斯蒂芬');
INSERT INTO `t_consumption` VALUES ('3', '3', '500', '', '2018-05-04 09:23:28', '额外若群翁热无群');
INSERT INTO `t_consumption` VALUES ('4', '3', '99', '\0', '2018-05-04 09:23:56', '打发斯蒂芬');
INSERT INTO `t_consumption` VALUES ('5', '3', '55', '\0', '2018-05-04 09:24:23', '的身份第三方');

-- ----------------------------
-- Table structure for t_contribute
-- ----------------------------
DROP TABLE IF EXISTS `t_contribute`;
CREATE TABLE `t_contribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户投稿表';

-- ----------------------------
-- Records of t_contribute
-- ----------------------------

-- ----------------------------
-- Table structure for t_convention
-- ----------------------------
DROP TABLE IF EXISTS `t_convention`;
CREATE TABLE `t_convention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `synopsis` varchar(255) DEFAULT NULL COMMENT '简介',
  `price` float DEFAULT NULL COMMENT '价格',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `host` varchar(255) DEFAULT NULL COMMENT '举办地',
  `sponsor` varchar(255) DEFAULT NULL COMMENT '主办方',
  `contractor` varchar(255) DEFAULT NULL COMMENT '承办方',
  `apply` varchar(255) DEFAULT NULL COMMENT '报名条件',
  `introduceimage` varchar(255) DEFAULT NULL COMMENT '会议介绍图片',
  `num` int(11) DEFAULT NULL COMMENT '人满数量',
  `isapply` bit(1) DEFAULT NULL COMMENT '是否同意报名参展',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `userid` int(11) DEFAULT NULL,
  `creattime` datetime DEFAULT NULL,
  `states` int(11) DEFAULT NULL COMMENT ' 1待支付 2已支付未确认  3已支付已确认 4待退款 5已退款 6已取消 7已完成 8未通过 9已通过',
  `ordernum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会展表';

-- ----------------------------
-- Records of t_convention
-- ----------------------------
INSERT INTO `t_convention` VALUES ('1', '/upload/1523859044182.jpg', 'dfdsafsa', 'dsaffdsaf', '4535', '2018-04-17 00:00:00', '讽德诵功富商大贾', '个发的是根深蒂固', '撒地方割发代首', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '/upload/1523859073988.jpg', '43', '', '3534', '50', '2018-04-16 14:11:16', null, null);

-- ----------------------------
-- Table structure for t_convention_apply
-- ----------------------------
DROP TABLE IF EXISTS `t_convention_apply`;
CREATE TABLE `t_convention_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conventionid` int(11) DEFAULT NULL COMMENT '会展id',
  `name` varchar(255) DEFAULT NULL COMMENT '报名人姓名',
  `phone` varchar(255) DEFAULT NULL COMMENT '报名人手机',
  `paystates` int(11) DEFAULT NULL COMMENT '支付状态',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `paytype` int(11) DEFAULT NULL COMMENT '支付方式',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `states` int(11) DEFAULT NULL COMMENT '会议报名状态（1同意 2拒绝 3未处理）',
  `type` int(11) DEFAULT NULL COMMENT '1公司 2个人',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `website` varchar(255) DEFAULT NULL COMMENT '官网',
  `company` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `duty` varchar(255) DEFAULT NULL COMMENT '报名人职务',
  `camera` varchar(255) DEFAULT NULL COMMENT '座机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会展报名表';

-- ----------------------------
-- Records of t_convention_apply
-- ----------------------------
INSERT INTO `t_convention_apply` VALUES ('1', '2', '张三', '231323', '1', '30', '1', '2018-05-03 15:13:29', '3', '1', '天津市青林大厦A座716', '165156565@qq.com', 'www.zlxn.com', '智联新农', '总经理', '666666');

-- ----------------------------
-- Table structure for t_country
-- ----------------------------
DROP TABLE IF EXISTS `t_country`;
CREATE TABLE `t_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '国家名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='国家表';

-- ----------------------------
-- Records of t_country
-- ----------------------------
INSERT INTO `t_country` VALUES ('1', '中国');
INSERT INTO `t_country` VALUES ('2', '美国');
INSERT INTO `t_country` VALUES ('3', '俄罗斯');
INSERT INTO `t_country` VALUES ('4', '法国');
INSERT INTO `t_country` VALUES ('5', '意大利');
INSERT INTO `t_country` VALUES ('6', '韩国');
INSERT INTO `t_country` VALUES ('7', '澳大利亚');
INSERT INTO `t_country` VALUES ('8', null);

-- ----------------------------
-- Table structure for t_country_area
-- ----------------------------
DROP TABLE IF EXISTS `t_country_area`;
CREATE TABLE `t_country_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryid` int(11) DEFAULT NULL COMMENT '国家id',
  `name` varchar(255) DEFAULT NULL COMMENT '地区名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='国家地区表';

-- ----------------------------
-- Records of t_country_area
-- ----------------------------
INSERT INTO `t_country_area` VALUES ('1', '1', '北京');
INSERT INTO `t_country_area` VALUES ('2', '1', '上海');
INSERT INTO `t_country_area` VALUES ('3', '1', '天津');
INSERT INTO `t_country_area` VALUES ('4', '1', '武汉');

-- ----------------------------
-- Table structure for t_course_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `t_course_evaluate`;
CREATE TABLE `t_course_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` int(11) DEFAULT NULL COMMENT '课程id',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `score` float DEFAULT NULL COMMENT '评价分数',
  `content` varchar(255) DEFAULT NULL COMMENT '评价内容',
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  `coursetype` int(11) DEFAULT NULL COMMENT '课程类型 1视听课 2直播课 3线下课',
  `courseorderid` int(11) DEFAULT NULL COMMENT '课程订单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程评价表';

-- ----------------------------
-- Records of t_course_evaluate
-- ----------------------------

-- ----------------------------
-- Table structure for t_course_offline
-- ----------------------------
DROP TABLE IF EXISTS `t_course_offline`;
CREATE TABLE `t_course_offline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '封面',
  `name` varchar(255) DEFAULT NULL COMMENT '课程名称',
  `languageid` int(11) DEFAULT NULL COMMENT '课程语种id',
  `stageid` int(11) DEFAULT NULL COMMENT '课程阶段id',
  `attendtime` datetime DEFAULT NULL COMMENT '上课时间',
  `address` varchar(255) DEFAULT NULL COMMENT '上课地点',
  `teacher` varchar(255) DEFAULT NULL COMMENT '授课教师',
  `totalprice` float DEFAULT NULL COMMENT '课程总价',
  `hour` int(11) DEFAULT NULL COMMENT '课时',
  `sources` varchar(255) DEFAULT NULL COMMENT '课程来源',
  `introduceimages` varchar(255) DEFAULT NULL COMMENT '课程介绍图片',
  `schoolid` int(11) DEFAULT NULL COMMENT '学校id',
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `userid` int(11) DEFAULT NULL,
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  `merchantid` int(11) DEFAULT NULL COMMENT '商家用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='发布线下课表';

-- ----------------------------
-- Records of t_course_offline
-- ----------------------------
INSERT INTO `t_course_offline` VALUES ('1', '/upload/1523945124862.jpg', '4rewr', '2', '1', null, null, null, '23432', '32432', 'ewrew', '/upload/1523945238752.jpg', null, '2018-04-17 14:07:23', '9', null, null);
INSERT INTO `t_course_offline` VALUES ('2', '/upload/1523945124862.jpg', '4rewr', '2', '1', null, null, null, '23432', '32432', 'ewrew', '', null, '2018-04-17 14:11:47', '9', null, null);
INSERT INTO `t_course_offline` VALUES ('3', '/upload/1523945124862.jpg', '4rewr', '2', '1', '2018-04-17 00:00:00', 'fgsdg', 'fgfsg', '23432', '32432', 'ewrew', '/upload/1523946038227.gif', null, '2018-04-17 14:20:40', '9', null, null);
INSERT INTO `t_course_offline` VALUES ('4', '/upload/1523945124862.jpg', '4rewr', '2', '1', '2018-04-17 00:00:00', 'fgsdg', 'fgfsg', '23432', '32432', 'ewrew', '/upload/1523946133315.gif', null, '2018-04-17 14:22:14', '9', null, null);
INSERT INTO `t_course_offline` VALUES ('5', '/upload/1524637229366.gif', '然他4w6', '1', '3', '2018-04-25 00:00:00', '4564', '5454 规范化股份', '5464', '546', '2', '/upload/1524637257460.jpg', null, '2018-04-25 14:20:59', '9', null, null);

-- ----------------------------
-- Table structure for t_course_order
-- ----------------------------
DROP TABLE IF EXISTS `t_course_order`;
CREATE TABLE `t_course_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '购买姓名',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `courseid` int(11) DEFAULT NULL COMMENT '课程id',
  `lessonsid` int(11) DEFAULT NULL COMMENT '分节id',
  `isall` bit(1) DEFAULT NULL COMMENT '是否全买',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `price` float DEFAULT NULL COMMENT '金额',
  `states` int(11) DEFAULT NULL COMMENT '订单状态 1待支付 2已支付未确认  3已支付已确认 4待退款 5已退款 6已取消 7已完成',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `paytype` int(11) DEFAULT NULL COMMENT '支付方式 1微信支付 2支付宝 3余额支付',
  `time` datetime DEFAULT NULL COMMENT '购买时间',
  `ordernumber` varchar(255) DEFAULT NULL COMMENT '订单号',
  `coursetype` int(11) DEFAULT NULL COMMENT '课程类型 1视听课 2直播课 3线下课',
  `merchantid` int(11) DEFAULT NULL COMMENT '商家userid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='课程购买表';

-- ----------------------------
-- Records of t_course_order
-- ----------------------------
INSERT INTO `t_course_order` VALUES ('1', '张三', '21321@qq.com', '16515', '465', '', '4651155', 'ahfaifbdasfa', '200', '1', '50', '1', '2018-05-03 16:48:51', '15615616516', '1', '50');

-- ----------------------------
-- Table structure for t_course_seeing
-- ----------------------------
DROP TABLE IF EXISTS `t_course_seeing`;
CREATE TABLE `t_course_seeing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '课程封面',
  `name` varchar(255) DEFAULT NULL COMMENT '课程名称',
  `languageid` int(11) DEFAULT NULL COMMENT '课程语种id',
  `stageid` int(11) DEFAULT NULL COMMENT '课程阶段id',
  `sources` varchar(255) DEFAULT NULL COMMENT '课程来源',
  `totalprice` float DEFAULT NULL COMMENT '课程总价',
  `hour` int(11) DEFAULT NULL COMMENT '课时',
  `isspelling` bit(1) DEFAULT NULL COMMENT '是否接受拼客',
  `num` int(11) DEFAULT NULL COMMENT '拼课人数',
  `spellingprice` float DEFAULT NULL COMMENT '拼课价',
  `introduceimage` varchar(255) DEFAULT NULL COMMENT '课程介绍图片',
  `schoolid` int(11) DEFAULT NULL COMMENT '学校id',
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `userid` int(11) DEFAULT NULL,
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  `merchantid` int(11) DEFAULT NULL COMMENT '商家用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='发布视听课表';

-- ----------------------------
-- Records of t_course_seeing
-- ----------------------------
INSERT INTO `t_course_seeing` VALUES ('1', '/upload/1523863109554.gif', '水电费是否', '2', '2', '3432', '3543240', '324', '\0', '32432', '324323', '/upload/1523863131587.jpg', null, '2018-04-16 15:18:53', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('2', '/upload/1523865681094.jpg', '电风扇', '1', '1', '的范德萨范德萨', '5343', '43543', '', '345', '4353', '/upload/1523865704129.gif', null, '2018-04-16 16:01:46', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('3', '/upload/1523871483968.gif', '额345435', '3', '2', '454他热汤', null, '5654', '', '243', '3244', '/upload/1523871567598.jpg', null, '2018-04-16 17:40:48', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('4', '/upload/1523937838992.jpg', '34543543', '2', '1', '43543543', '43543', '43543', '\0', '345', '43534', '/upload/1523937925442.gif', null, '2018-04-17 12:05:29', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('5', '/upload/1523944984775.jpg', '34543543', '2', '1', '43543543', '43543', '43543', '\0', '345', '43534', '/upload/1523945002660.gif', null, '2018-04-17 14:03:27', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('6', '/upload/1524634935133.jpg', '为UR一四', '1', '3', '2', null, '435', '', '4353', '4353', '/upload/1524634993278.jpg', null, '2018-04-25 13:43:17', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('7', '/upload/1524634935133.jpg', '为UR一四', '1', '3', '2', null, '435', '', '4353', '4353', '/upload/1524634993278.jpg', null, '2018-04-25 14:13:47', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('9', '/upload/1525671959507.jpg', '视听课', '1', '4', '1', '222', '100', '', null, null, '/upload/1525673030903.jpg', null, '2018-05-07 14:03:54', '3', '2', '3');
INSERT INTO `t_course_seeing` VALUES ('10', '/upload/1525829427841.jpg', 'shitingke', '1', '4', '1', '34324', '23', '\0', null, null, '/upload/1525829526259.gif', null, '2018-05-09 09:32:18', null, '2', null);

-- ----------------------------
-- Table structure for t_course_simple
-- ----------------------------
DROP TABLE IF EXISTS `t_course_simple`;
CREATE TABLE `t_course_simple` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '分节名称',
  `price` float DEFAULT NULL COMMENT '单节价格',
  `courseid` int(11) DEFAULT NULL COMMENT '课程id',
  `coursetype` int(11) DEFAULT NULL COMMENT '课程类型(1视听课、2线下课)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='课程单节表';

-- ----------------------------
-- Records of t_course_simple
-- ----------------------------
INSERT INTO `t_course_simple` VALUES ('1', null, null, null, '1');
INSERT INTO `t_course_simple` VALUES ('2', null, null, null, '1');
INSERT INTO `t_course_simple` VALUES ('3', null, null, null, '1');
INSERT INTO `t_course_simple` VALUES ('4', null, null, null, '1');
INSERT INTO `t_course_simple` VALUES ('5', null, null, null, '2');
INSERT INTO `t_course_simple` VALUES ('6', null, null, null, '2');
INSERT INTO `t_course_simple` VALUES ('7', null, null, null, '2');
INSERT INTO `t_course_simple` VALUES ('8', null, null, '5', '1');
INSERT INTO `t_course_simple` VALUES ('9', null, null, '5', '1');
INSERT INTO `t_course_simple` VALUES ('10', null, null, '5', '1');
INSERT INTO `t_course_simple` VALUES ('11', null, null, '4', '2');
INSERT INTO `t_course_simple` VALUES ('12', null, null, '4', '2');
INSERT INTO `t_course_simple` VALUES ('13', null, null, null, '1');
INSERT INTO `t_course_simple` VALUES ('14', null, null, null, '1');
INSERT INTO `t_course_simple` VALUES ('15', null, null, null, '2');
INSERT INTO `t_course_simple` VALUES ('16', null, null, null, '2');
INSERT INTO `t_course_simple` VALUES ('17', '二娃若', '43543', '9', '2');
INSERT INTO `t_course_simple` VALUES ('18', '43543', '34534', '9', '1');
INSERT INTO `t_course_simple` VALUES ('19', '43543', '34534', '9', '1');
INSERT INTO `t_course_simple` VALUES ('20', '儿童', '43', '7', '1');
INSERT INTO `t_course_simple` VALUES ('21', '54654', '54654', '5', '2');
INSERT INTO `t_course_simple` VALUES ('22', '第一节', '21', '8', '1');
INSERT INTO `t_course_simple` VALUES ('23', '第一金', '22', '9', '1');
INSERT INTO `t_course_simple` VALUES ('24', '第二节', '22', '9', '1');
INSERT INTO `t_course_simple` VALUES ('25', '1', '111', '10', '1');
INSERT INTO `t_course_simple` VALUES ('26', '2', '222', '10', '1');

-- ----------------------------
-- Table structure for t_course_streaming
-- ----------------------------
DROP TABLE IF EXISTS `t_course_streaming`;
CREATE TABLE `t_course_streaming` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '课程封面',
  `name` varchar(255) DEFAULT NULL COMMENT '课程名称',
  `languageid` int(11) DEFAULT NULL COMMENT '语种id',
  `stageid` int(11) DEFAULT NULL COMMENT '课程阶段id',
  `teacher` varchar(255) DEFAULT NULL COMMENT '主讲教师',
  `streamingtime` datetime DEFAULT NULL COMMENT '直播时间',
  `totalprice` float DEFAULT NULL COMMENT '课程总价格',
  `introduceimages` varchar(255) DEFAULT NULL COMMENT '课程介绍图片',
  `schoolid` int(11) DEFAULT NULL COMMENT '学校id',
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `userid` int(11) DEFAULT NULL,
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  `merchantid` int(11) DEFAULT NULL COMMENT '商家用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='发布直播课表';

-- ----------------------------
-- Records of t_course_streaming
-- ----------------------------
INSERT INTO `t_course_streaming` VALUES ('1', '/upload/1524205916116.jpg,123', '规范合格的,123', '2', '3', '规范化个梵蒂冈', '2018-04-20 00:00:00', '54654', '/upload/1524205932380.jpg', null, '2018-04-20 21:59:18', '2', '2', null);
INSERT INTO `t_course_streaming` VALUES ('2', '/upload/1524235268800.jpg', 'erqwr', '1', '2', 'ewrwq', '2018-04-20 00:00:00', null, '/upload/1524235282600.jpg', null, '2018-04-20 22:41:25', '2', '2', null);
INSERT INTO `t_course_streaming` VALUES ('3', '/upload/1524453259568.jpg', '急急急', '2', '4', '明敏', '2018-04-23 00:00:00', '646466', '/upload/1524453281616.jpg', null, '2018-04-23 11:14:45', '2', '2', null);
INSERT INTO `t_course_streaming` VALUES ('4', '/upload/1524453337971.gif', '34324', '2', '2', '3432424', '2018-04-27 00:00:00', '324324', '/upload/1524453349184.gif', null, '2018-04-23 11:15:52', '2', '2', null);
INSERT INTO `t_course_streaming` VALUES ('5', null, '而问题retreat特特热', '2', '2', '其他热熔器tertiary', '2018-05-07 11:51:35', '984498', null, null, '2018-05-07 11:51:43', '9', '2', null);
INSERT INTO `t_course_streaming` VALUES ('6', null, '暗示法改动时', '1', '3', '其他热熔器tertiary', '2018-05-07 11:52:23', '5845900', null, null, '2018-05-07 11:52:29', '9', '2', null);
INSERT INTO `t_course_streaming` VALUES ('7', '/upload/1525846879937.tencent.mm.png', '你名字', '2', '5', '而你您', '2018-05-09 14:20:00', '646', '/upload/1525846906973.tencent.mm.png', null, '2018-05-09 14:21:53', '3', '2', null);

-- ----------------------------
-- Table structure for t_coursetype
-- ----------------------------
DROP TABLE IF EXISTS `t_coursetype`;
CREATE TABLE `t_coursetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '课程类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程类型表';

-- ----------------------------
-- Records of t_coursetype
-- ----------------------------

-- ----------------------------
-- Table structure for t_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `t_dynamic`;
CREATE TABLE `t_dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `likenum` int(11) DEFAULT NULL COMMENT '点赞数',
  `image` varchar(255) DEFAULT NULL COMMENT '动态图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='社区动态表';

-- ----------------------------
-- Records of t_dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for t_dynamic_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_dynamic_comment`;
CREATE TABLE `t_dynamic_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `dynamicid` int(11) DEFAULT NULL COMMENT '动态id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='社区动态评论表';

-- ----------------------------
-- Records of t_dynamic_comment
-- ----------------------------

-- ----------------------------
-- Table structure for t_enterprise
-- ----------------------------
DROP TABLE IF EXISTS `t_enterprise`;
CREATE TABLE `t_enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL COMMENT '所在地',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `introduce` varchar(255) DEFAULT NULL COMMENT '企业介绍',
  `creditcode` varchar(255) DEFAULT NULL COMMENT '社会信用代码',
  `enhancement` varchar(255) DEFAULT NULL COMMENT '增信证件',
  `legalname` varchar(255) DEFAULT NULL COMMENT '法人姓名',
  `legalhome` varchar(255) DEFAULT NULL COMMENT '法人代表归属地',
  `writtenname` varchar(255) DEFAULT NULL COMMENT '填写人身份',
  `writtenphone` varchar(255) DEFAULT NULL COMMENT '填写人联系方式',
  `idcardfront` varchar(255) DEFAULT NULL COMMENT '法人手持身份证正面',
  `idcardreverse` varchar(255) DEFAULT NULL COMMENT '法人手持身份证反面',
  `states` int(11) DEFAULT NULL COMMENT '认证状态',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='企业表';

-- ----------------------------
-- Records of t_enterprise
-- ----------------------------
INSERT INTO `t_enterprise` VALUES ('7', null, null, null, '324', '/upload/1524895341478.gif', '3242', '234', '324', '342', '/upload/1524895341479.gif', '/upload/1524895341479.gif', '2', '3242', '2');

-- ----------------------------
-- Table structure for t_enterprise_renzheng
-- ----------------------------
DROP TABLE IF EXISTS `t_enterprise_renzheng`;
CREATE TABLE `t_enterprise_renzheng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL COMMENT '所在地',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `introduce` varchar(255) DEFAULT NULL COMMENT '企业介绍',
  `creditcode` varchar(255) DEFAULT NULL COMMENT '社会信用代码',
  `enhancement` varchar(255) DEFAULT NULL COMMENT '增信证件',
  `legalname` varchar(255) DEFAULT NULL COMMENT '法人姓名',
  `legalhome` varchar(255) DEFAULT NULL COMMENT '法人代表归属地',
  `writtenname` varchar(255) DEFAULT NULL COMMENT '填写人身份',
  `writtenphone` varchar(255) DEFAULT NULL COMMENT '填写人联系方式',
  `idcardfront` varchar(255) DEFAULT NULL COMMENT '法人手持身份证正面',
  `idcardreverse` varchar(255) DEFAULT NULL COMMENT '法人手持身份证反面',
  `states` int(11) DEFAULT NULL COMMENT '认证状态',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='企业认证表';

-- ----------------------------
-- Records of t_enterprise_renzheng
-- ----------------------------
INSERT INTO `t_enterprise_renzheng` VALUES ('7', null, null, null, '324', '/upload/1524895341478.gif', '3242', '234', '324', '342', '/upload/1524895341479.gif', '/upload/1524895341479.gif', '2', '3242', '2');

-- ----------------------------
-- Table structure for t_finance
-- ----------------------------
DROP TABLE IF EXISTS `t_finance`;
CREATE TABLE `t_finance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` int(11) DEFAULT NULL COMMENT '财务咨询类型',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='财务咨询表';

-- ----------------------------
-- Records of t_finance
-- ----------------------------
INSERT INTO `t_finance` VALUES ('1', '1', '可视对讲法律考试基本法拉克数据备份奥斯卡发射空间分布拉斯傻得可怜发卡量解放碑那是肯定');
INSERT INTO `t_finance` VALUES ('2', '2', '爱国会计还是看个几把撒打开链接爱上搞快点撒会计给你吧圣诞快乐价格爱是个肯定是哪个科技第三啊三跪九叩撒开了个拿到');
INSERT INTO `t_finance` VALUES ('3', '3', '昂吧司空见惯巴黎司空见惯搜嘎圣诞节快乐功能空间撒大哥大格拉苏蒂你赶快你说的阿萨德给你撒打开就两年公司打开啦');
INSERT INTO `t_finance` VALUES ('4', '4', '埃里克森见到过拉的屎看不惯少打了个那点事开了个 爱是快乐给你上课拉到那噶的时刻桑德萨科技功能');
INSERT INTO `t_finance` VALUES ('5', '5', '卡两个不是贷款基本啥可拉倒吧格拉苏蒂空格 那边的空间功能多撒观看时打开两个那是的的撒开了国内杀毒可乐公司');
INSERT INTO `t_finance` VALUES ('6', '6', '奥古斯丁给你啥都健康大哥SD敢达纳斯达克留给你撒大圣归来那点事弄了个第六课');
INSERT INTO `t_finance` VALUES ('7', '1', 'aksdjflakdsjbfnkjdsabfdsakljfbndsaljkfadsklj');

-- ----------------------------
-- Table structure for t_finance_type
-- ----------------------------
DROP TABLE IF EXISTS `t_finance_type`;
CREATE TABLE `t_finance_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '财务咨询类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='财务咨询类型表';

-- ----------------------------
-- Records of t_finance_type
-- ----------------------------
INSERT INTO `t_finance_type` VALUES ('1', '财税法咨询');
INSERT INTO `t_finance_type` VALUES ('2', '会计服务');
INSERT INTO `t_finance_type` VALUES ('3', '税务筹划');
INSERT INTO `t_finance_type` VALUES ('4', '国际税法');
INSERT INTO `t_finance_type` VALUES ('5', '审计');
INSERT INTO `t_finance_type` VALUES ('6', '签证');

-- ----------------------------
-- Table structure for t_hauptstudium
-- ----------------------------
DROP TABLE IF EXISTS `t_hauptstudium`;
CREATE TABLE `t_hauptstudium` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hauptstudium` varchar(255) DEFAULT NULL COMMENT '课程阶段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程阶段表';

-- ----------------------------
-- Records of t_hauptstudium
-- ----------------------------

-- ----------------------------
-- Table structure for t_integral_commodity
-- ----------------------------
DROP TABLE IF EXISTS `t_integral_commodity`;
CREATE TABLE `t_integral_commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `integral` int(11) DEFAULT NULL COMMENT '消耗积分',
  `money` float DEFAULT NULL COMMENT '消耗金钱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分商城商品表';

-- ----------------------------
-- Records of t_integral_commodity
-- ----------------------------

-- ----------------------------
-- Table structure for t_integral_consume
-- ----------------------------
DROP TABLE IF EXISTS `t_integral_consume`;
CREATE TABLE `t_integral_consume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `integral` int(11) DEFAULT NULL COMMENT '积分',
  `isconsume` bit(1) DEFAULT NULL COMMENT '消耗还是获得 true消耗',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='用户积分使用记录表';

-- ----------------------------
-- Records of t_integral_consume
-- ----------------------------
INSERT INTO `t_integral_consume` VALUES ('1', '3', '500', '\0', '2018-05-04 09:16:11', '第一次获得积分');
INSERT INTO `t_integral_consume` VALUES ('2', '3', '400', '\0', '2018-05-04 09:17:29', 'sad佛挡杀佛');
INSERT INTO `t_integral_consume` VALUES ('3', '3', '200', '', '2018-05-04 09:17:39', '佛挡杀佛胜多负少的');
INSERT INTO `t_integral_consume` VALUES ('4', '3', '100', '\0', '2018-05-04 09:17:59', '撒范德萨发的');
INSERT INTO `t_integral_consume` VALUES ('5', '3', '300', '', '2018-05-04 09:18:12', '热同仁堂');
INSERT INTO `t_integral_consume` VALUES ('6', '3', '300', '', '2018-05-04 09:18:23', '大幅度多撒爱上发起萨芬的');
INSERT INTO `t_integral_consume` VALUES ('7', '3', '200', '\0', '2018-05-04 09:18:36', '萨法涩费');

-- ----------------------------
-- Table structure for t_integral_conversion
-- ----------------------------
DROP TABLE IF EXISTS `t_integral_conversion`;
CREATE TABLE `t_integral_conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `integral` int(11) DEFAULT NULL COMMENT '消耗积分',
  `time` datetime DEFAULT NULL COMMENT '兑换时间',
  `commodityid` int(11) DEFAULT NULL COMMENT '商品id',
  `email` varchar(255) DEFAULT NULL COMMENT '收货邮箱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分兑换表';

-- ----------------------------
-- Records of t_integral_conversion
-- ----------------------------

-- ----------------------------
-- Table structure for t_interpreter
-- ----------------------------
DROP TABLE IF EXISTS `t_interpreter`;
CREATE TABLE `t_interpreter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `head` varchar(255) DEFAULT NULL COMMENT '头像',
  `translatelanguage` int(11) DEFAULT NULL COMMENT '翻译语言id',
  `intolanguage` int(11) DEFAULT NULL COMMENT '翻译成语言id',
  `translategrade` varchar(255) DEFAULT NULL COMMENT '翻译语言等级',
  `intograde` varchar(255) DEFAULT NULL COMMENT '翻译成语言等级',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `age` int(11) DEFAULT NULL COMMENT '口译年限',
  `territoryid` int(11) DEFAULT NULL COMMENT '擅长领域id',
  `hourprice` float DEFAULT NULL COMMENT '价格(元/小时)',
  `minuteprice` float DEFAULT NULL COMMENT '价格(元/分钟)',
  `experienceid` int(11) DEFAULT NULL COMMENT '留学经验id',
  `certificateid` int(11) DEFAULT NULL COMMENT '证书id',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `experience` varchar(255) DEFAULT NULL COMMENT '口译经验',
  `make` varchar(255) DEFAULT NULL COMMENT '自我介绍',
  `imageone` varchar(255) DEFAULT NULL COMMENT '风采展示图1',
  `imagetwo` varchar(255) DEFAULT NULL COMMENT '风采展示图2',
  `imagethree` varchar(255) DEFAULT NULL COMMENT '风采展示图3',
  `imageoneexplain` varchar(255) DEFAULT NULL COMMENT '风采展示图1说明',
  `imagetwoexplain` varchar(255) DEFAULT NULL COMMENT '风采展示图2说明',
  `imagethreeexplain` varchar(255) DEFAULT NULL COMMENT '风采展示图3说明',
  `interpretertype` int(11) DEFAULT NULL COMMENT '口译类型（1交传、2同传、3陪同、4笔译）',
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `time` datetime DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `wordprice` float DEFAULT NULL COMMENT '单价（字/元）',
  `freeage` int(11) DEFAULT NULL COMMENT '免费年限',
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='发布译员表';

-- ----------------------------
-- Records of t_interpreter
-- ----------------------------
INSERT INTO `t_interpreter` VALUES ('1', '123', '123', '1', '2', '123', '123', '123', '123', '123', '12', '1', '1', '1', '1', '1', '12345667', '1', '123', '123', '123', '123', '123', '132', '123', '1', '', '2', '2018-04-11 23:06:43', '118.14', '39.13', null, null, '2');
INSERT INTO `t_interpreter` VALUES ('2', '/upload/1523608389696.jpg', '/upload/1523608393533.gif', '1', '2', '2', '3', '发热热', '天津', '研究生', '3', '2', '45243', '4353', '2', '3', null, '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '/upload/1523608431903.gif', '/upload/1523608434365.jpg', '/upload/1523608438061.gif', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '1', '', '2', '2018-04-13 16:34:25', '118.14', '39.13', null, null, '2');
INSERT INTO `t_interpreter` VALUES ('3', '/upload/1524104734659.gif', '/upload/1524105012803.gif', '1', '1', '2', '1', 'gfdsgsd', '天津', '本科', '4', '1', null, null, '1', '1', '5443654654', 'rtgrewtrweterter gretgrryrt', 'tryterytryeterytry', '/upload/1524107047021.jpg', '/upload/1524107048701.gif', '/upload/1524107051888.jpg', 'treytetreytry', 'trytettrtr', 'ytetete', '4', '', '2', '2018-04-19 14:10:04', '117.207649', '39.145927', '4353', '1', '2');
INSERT INTO `t_interpreter` VALUES ('4', '/upload/1524118224292.jpg', '/upload/1524118228167.jpg', '1', '1', '1', '1', 'heng', '友爱东里', '本科', '3', '3', '43543', '435354', '6', '3', '34543', '43543543534', '35435435435', '/upload/1524118878908.jpg', '/upload/1524118880872.jpg', '/upload/1524118882980.jpg', '任务而特委托', '热特委托we', '热太热问题惹我特任务二', '2', '', '2', '2018-04-19 14:21:35', '117.269804', '39.109359', null, null, '2');
INSERT INTO `t_interpreter` VALUES ('5', '/upload/1524119291372.gif', '/upload/1524119294955.gif', '1', '1', '1', '1', '交传', '秀波园', '本科', '2', '2', '4535', '43543', '1', '1', '43543543', '发的个梵蒂冈梵蒂冈是的非官方的', '发生地感受到法国发生的国防生的感受到法国', '/upload/1524119365279.gif', '/upload/1524119367595.gif', '/upload/1524119369880.gif', '发的规范的身高多少个梵蒂冈梵蒂冈撒地方', '电风扇归属地感受到根深蒂固', '讽德诵功富商大贾的分公司电饭锅', '1', '', '2', '2018-04-19 14:29:37', '117.302686', '39.101895', null, null, '2');
INSERT INTO `t_interpreter` VALUES ('6', '/upload/1524119402728.jpg', '/upload/1524119405347.gif', '1', '1', '1', '1', '僵小鱼', '烛光小区', '研究生', '4', '3', '453453', '4354350', '6', '1', '4354354343', '4354特让他个梵蒂冈梵蒂冈', '第三个讽德诵功SD敢达分公司的非官方', '/upload/1524119493896.gif', '/upload/1524119498409.gif', '/upload/1524119501232.gif', '个丰东股份试的改讽德诵功地方广东省供电所十多个十多个', '是反对根深蒂固第三个第四个费功夫大使馆', '单方事故富商大贾第三个第四个的费功夫大使馆丰富的股份的股份范德萨', '3', '', '2', '2018-04-19 14:31:57', '116.70176', '39.996308', null, null, '3');
INSERT INTO `t_interpreter` VALUES ('7', '/upload/1524121635858.jpg', '/upload/1524121645809.jpg', '1', '1', null, null, '热太热太热', '集贤里', '本科', null, null, null, null, null, '1', null, '发生变化发生符合', '东方时尚发的规划的风格大方', '/upload/1524121984212.jpg', '/upload/1524121987148.jpg', '/upload/1524121989440.jpg', null, null, null, null, '', '2', '2018-04-19 15:16:17', '117.136937', '39.234543', null, null, '2');
INSERT INTO `t_interpreter` VALUES ('8', '/upload/1524121635858.jpg', '/upload/1524121645809.jpg', '1', '1', null, null, '热太热太热', '集贤里', '本科', null, null, null, null, null, '1', null, '发生变化发生符合', '东方时尚发的规划的风格大方', '/upload/1524121984212.jpg', '/upload/1524121987148.jpg', '/upload/1524121989440.jpg', null, null, null, null, '', '2', '2018-04-19 15:17:08', '117.136937', '39.234543', null, null, null);
INSERT INTO `t_interpreter` VALUES ('9', '/upload/1525416085461.jpg', '/upload/1525416091096.gif', '1', '1', 'A1', 'A2', '笔译译员', '货场大街', '本科在读', null, '4', null, null, '4', '1', '11111111111', '笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '/upload/1525416507085.gif', '/upload/1525416508644.gif', '/upload/1525416510764.gif', '图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明', '图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明', '图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明', '4', '', '3', '2018-05-04 14:48:47', '117.207649', '39.145927', '232', '3', '1');
INSERT INTO `t_interpreter` VALUES ('10', '/upload/1525416085461.jpg', '/upload/1525416091096.gif', '1', '1', 'A1', 'A2', '笔译译员', '货场大街', '本科在读', '3', '4', null, null, '4', '1', '11111111111', '笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验笔译经验', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '/upload/1525416507085.gif', '/upload/1525416508644.gif', '/upload/1525416510764.gif', '图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明', '图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明', '图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明图说明', '4', '', '3', '2018-05-04 14:48:58', '117.207649', '39.145927', '232', '3', '1');

-- ----------------------------
-- Table structure for t_interpreter_authentication
-- ----------------------------
DROP TABLE IF EXISTS `t_interpreter_authentication`;
CREATE TABLE `t_interpreter_authentication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `birthdate` datetime DEFAULT NULL COMMENT '出生日期',
  `nationality` varchar(255) DEFAULT NULL COMMENT '国籍',
  `education` varchar(255) DEFAULT NULL COMMENT '最高学历',
  `address` varchar(255) DEFAULT NULL COMMENT '家庭地址',
  `certificate` varchar(255) DEFAULT NULL COMMENT '证件类型',
  `idcard` varchar(255) DEFAULT NULL COMMENT '证件号',
  `idcardfront` varchar(255) DEFAULT NULL COMMENT '手持身份证照片正面',
  `idcardreverse` varchar(255) DEFAULT NULL COMMENT '手持身份证照片反面',
  `states` int(11) DEFAULT NULL COMMENT '状态 1审核中 2审核通过 3未提交审核',
  `time` datetime DEFAULT NULL COMMENT '认证时间',
  `interpretertype` int(11) DEFAULT NULL COMMENT '译员类型(2口译1笔译)',
  `certificatetype` varchar(255) DEFAULT NULL COMMENT '证件类型',
  `excellent` bit(1) DEFAULT NULL COMMENT '优秀译员 true表示优秀，在首页能够展示出来。false普通译员，在首页不显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='认证译员表';

-- ----------------------------
-- Records of t_interpreter_authentication
-- ----------------------------
INSERT INTO `t_interpreter_authentication` VALUES ('5', '2', '彩', '\0', '2018-04-23 00:00:00', '中国籍', '本科', '恶二二恶', '100,101,102', '山东省', '/upload/1524721086989.gif', '/upload/1524733718889.jpg', '1', '2018-04-10 14:38:33', '2', '1', '');
INSERT INTO `t_interpreter_authentication` VALUES ('7', '2', '彩彩', '\0', '2018-04-23 00:00:00', '中国籍', '本科', '6757657657', '108', '65765765765757', '/upload/1524821899009.gif', '/upload/1524821899009.gif', '1', '2018-04-27 17:38:19', '1', '2', '');

-- ----------------------------
-- Table structure for t_interpreter_authentication_territory
-- ----------------------------
DROP TABLE IF EXISTS `t_interpreter_authentication_territory`;
CREATE TABLE `t_interpreter_authentication_territory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interpreterid` int(11) DEFAULT NULL COMMENT '译员id',
  `territory` varchar(255) DEFAULT NULL COMMENT '领域名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='译员擅长领域表';

-- ----------------------------
-- Records of t_interpreter_authentication_territory
-- ----------------------------
INSERT INTO `t_interpreter_authentication_territory` VALUES ('17', '6', '行业1');
INSERT INTO `t_interpreter_authentication_territory` VALUES ('36', '5', '6');
INSERT INTO `t_interpreter_authentication_territory` VALUES ('37', null, '5');
INSERT INTO `t_interpreter_authentication_territory` VALUES ('38', '7', '6');
INSERT INTO `t_interpreter_authentication_territory` VALUES ('39', null, '6');
INSERT INTO `t_interpreter_authentication_territory` VALUES ('40', null, '1');

-- ----------------------------
-- Table structure for t_interpreter_order
-- ----------------------------
DROP TABLE IF EXISTS `t_interpreter_order`;
CREATE TABLE `t_interpreter_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `valuationtype` int(11) DEFAULT NULL COMMENT '计价方式',
  `address` varchar(255) DEFAULT NULL COMMENT '服务地点',
  `starttime` datetime DEFAULT NULL COMMENT '开始时间',
  `endtime` datetime DEFAULT NULL COMMENT '结束时间',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `totalprice` float DEFAULT NULL COMMENT '总价',
  `interpreterid` int(11) DEFAULT NULL COMMENT '译员id',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `travellingtype` int(11) DEFAULT NULL COMMENT '差旅费方式',
  `states` int(11) DEFAULT NULL COMMENT '订单状态 1待支付 2已支付未确认  3已支付已确认 4待退款 5已退款 6已取消 7已完成',
  `paytype` int(11) DEFAULT NULL COMMENT '支付方式',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `ordernumber` varchar(255) DEFAULT NULL COMMENT '订单号',
  `merchantid` int(11) DEFAULT NULL COMMENT '商家userid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='译员购买表';

-- ----------------------------
-- Records of t_interpreter_order
-- ----------------------------

-- ----------------------------
-- Table structure for t_interpreter_order_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `t_interpreter_order_evaluate`;
CREATE TABLE `t_interpreter_order_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` float DEFAULT NULL COMMENT '评论分',
  `interpreterid` int(11) DEFAULT NULL COMMENT '译员id',
  `content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='译员评论表';

-- ----------------------------
-- Records of t_interpreter_order_evaluate
-- ----------------------------

-- ----------------------------
-- Table structure for t_investment
-- ----------------------------
DROP TABLE IF EXISTS `t_investment`;
CREATE TABLE `t_investment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` int(11) DEFAULT NULL COMMENT '投资类型',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='投资咨询表';

-- ----------------------------
-- Records of t_investment
-- ----------------------------
INSERT INTO `t_investment` VALUES ('1', '1', 'alsdngsadnglskdngldsakngladskng;laksgn;dsngv;lsdaknglsdakn;san;gn;lksagsdng;laksn');
INSERT INTO `t_investment` VALUES ('2', '2', '感动死了看过你扫地阿萨德跟那谁给你给你打看过那算了都快过年 杆');
INSERT INTO `t_investment` VALUES ('3', '3', '都给你杀单联开关三大回复噶圣诞节快乐干哈谁都会该老师读后感拉克爱迪生嘎嘎是的嘎斯');
INSERT INTO `t_investment` VALUES ('4', '4', '挨个杀固定撒个是的嘎是的固定桑阿萨德固定萨嘎但是阿斯顿刚到萨嘎萨斯噶啥');
INSERT INTO `t_investment` VALUES ('5', '1', '都是gas的gas干撒公司大');

-- ----------------------------
-- Table structure for t_investment_type
-- ----------------------------
DROP TABLE IF EXISTS `t_investment_type`;
CREATE TABLE `t_investment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '投资类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='投资类型表';

-- ----------------------------
-- Records of t_investment_type
-- ----------------------------
INSERT INTO `t_investment_type` VALUES ('1', '投资政策');
INSERT INTO `t_investment_type` VALUES ('2', '投资环境');
INSERT INTO `t_investment_type` VALUES ('3', '投资法规');
INSERT INTO `t_investment_type` VALUES ('4', '工业园区');
INSERT INTO `t_investment_type` VALUES ('5', '并购收购');
INSERT INTO `t_investment_type` VALUES ('6', '中国上市');

-- ----------------------------
-- Table structure for t_job
-- ----------------------------
DROP TABLE IF EXISTS `t_job`;
CREATE TABLE `t_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '招聘标题',
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `company` varchar(255) DEFAULT NULL COMMENT '招聘公司',
  `mixpay` varchar(255) DEFAULT NULL COMMENT '最低薪资',
  `maxpay` varchar(255) DEFAULT NULL COMMENT '最高薪资',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `education` varchar(255) DEFAULT NULL COMMENT '要求学历',
  `experience` varchar(255) DEFAULT NULL COMMENT '经验',
  `num` int(11) DEFAULT NULL COMMENT '人数',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `description` varchar(255) DEFAULT NULL COMMENT '职位描述',
  `images` varchar(255) DEFAULT NULL COMMENT '企业照片',
  `describe` varchar(255) DEFAULT NULL COMMENT '招聘职位',
  `states` int(11) DEFAULT NULL COMMENT '发布状态  1未通过 2已通过 3已拒绝',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='招聘表';

-- ----------------------------
-- Records of t_job
-- ----------------------------
INSERT INTO `t_job` VALUES ('1', '招聘职员', '2018-04-23 13:48:56', '天津智联新农', '6', '7', '天津市河西区青林大厦', null, null, null, '123456789@qq.com', '这是一个很好的锻炼自己的职位，能够让你受益很深', null, '口译职员', '3', '1');
INSERT INTO `t_job` VALUES ('3', '招聘译员', '2018-04-23 18:32:10', '北京科技', '5', '6', '北京市朝阳区', null, null, null, '123456789@qq.com', 'dsfadsfasdf', null, '笔译职员', '3', '2');
INSERT INTO `t_job` VALUES ('4', '职员', '2018-04-23 18:37:07', '天津科技', '4', '5', '天津市河西区', null, null, null, '987654321@qq.com', 'adsfadsfdsafdsaf', null, '口译职员', '1', '3');
INSERT INTO `t_job` VALUES ('5', '职员', '2018-04-23 18:38:20', '北京互联网', '3', '5', '北京市大槐树', null, null, null, '132131332@qq.com', 'dsfasdfasdf', null, '笔译职员', '1', '4');
INSERT INTO `t_job` VALUES ('6', '译员', '2018-04-24 10:45:15', '天津科技', '7', '8', '天津市和平区', null, null, null, '123456789@qq.com', 'asdfadsgdsagswe', null, '口译译员', '1', '1');

-- ----------------------------
-- Table structure for t_language
-- ----------------------------
DROP TABLE IF EXISTS `t_language`;
CREATE TABLE `t_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) DEFAULT NULL COMMENT '语种',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='语种表';

-- ----------------------------
-- Records of t_language
-- ----------------------------
INSERT INTO `t_language` VALUES ('1', '英语');
INSERT INTO `t_language` VALUES ('2', '法语');
INSERT INTO `t_language` VALUES ('3', '德语');
INSERT INTO `t_language` VALUES ('4', '西班牙语');

-- ----------------------------
-- Table structure for t_language_grade
-- ----------------------------
DROP TABLE IF EXISTS `t_language_grade`;
CREATE TABLE `t_language_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `languageid` int(11) DEFAULT NULL COMMENT '语种id',
  `grade` varchar(255) DEFAULT NULL COMMENT '等级名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='语种等级表';

-- ----------------------------
-- Records of t_language_grade
-- ----------------------------
INSERT INTO `t_language_grade` VALUES ('1', '1', 'A1');
INSERT INTO `t_language_grade` VALUES ('2', '1', 'A2');

-- ----------------------------
-- Table structure for t_law
-- ----------------------------
DROP TABLE IF EXISTS `t_law`;
CREATE TABLE `t_law` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` int(11) DEFAULT NULL COMMENT '法律咨询类型',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='法律咨询表';

-- ----------------------------
-- Records of t_law
-- ----------------------------
INSERT INTO `t_law` VALUES ('1', '1', '阿莎十多个');
INSERT INTO `t_law` VALUES ('2', '2', '看看撒丹东港杰卡斯');
INSERT INTO `t_law` VALUES ('3', '3', '卡价格卡少年科技公司');
INSERT INTO `t_law` VALUES ('4', '4', '看见俺不是看的感觉那蓝思科技噶蓝思科技');
INSERT INTO `t_law` VALUES ('5', '5', '卡斯柯来得及噶困了就睡那个空间撒大哥吧');
INSERT INTO `t_law` VALUES ('6', '6', '离开家安保科技路过的萨科技了改变四大皆空');
INSERT INTO `t_law` VALUES ('7', '1', '卡锅克鲁赛德开关机');

-- ----------------------------
-- Table structure for t_law_type
-- ----------------------------
DROP TABLE IF EXISTS `t_law_type`;
CREATE TABLE `t_law_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '法律咨询类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='法律咨询类型表';

-- ----------------------------
-- Records of t_law_type
-- ----------------------------
INSERT INTO `t_law_type` VALUES ('1', '商事贸易');
INSERT INTO `t_law_type` VALUES ('2', '劳动法');
INSERT INTO `t_law_type` VALUES ('3', '金融');
INSERT INTO `t_law_type` VALUES ('4', '仲裁');
INSERT INTO `t_law_type` VALUES ('5', '诉讼');
INSERT INTO `t_law_type` VALUES ('6', '投资并购');

-- ----------------------------
-- Table structure for t_lease
-- ----------------------------
DROP TABLE IF EXISTS `t_lease`;
CREATE TABLE `t_lease` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `synopsis` varchar(255) DEFAULT NULL COMMENT '简介',
  `typeid` int(11) DEFAULT NULL COMMENT '租赁类型id',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `introduce` varchar(255) DEFAULT NULL COMMENT '介绍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='租赁表';

-- ----------------------------
-- Records of t_lease
-- ----------------------------
INSERT INTO `t_lease` VALUES ('1', 'sredsgd', '爱上都会发生法纳税', '阿卡记得发困了就睡方便看见爱上对方比较快拉上不发拉倒思考', '1', '天津', '阿喀琉斯电话费会计爱迪生卡拉胶斯蒂芬看见爱上封闭空间卡设计费达康书记部分接口苏打绿开发商快了几分');
INSERT INTO `t_lease` VALUES ('2', '大三顿饭', '大师傅', '撒的发顺丰', '2', '天津', '第三方阿发');
INSERT INTO `t_lease` VALUES ('3', '沙发舒服的', '发阿斯蒂芬', '阿发第三方', '3', '天津', '阿斯蒂芬阿发大幅度');
INSERT INTO `t_lease` VALUES ('4', '撒的发生的的发生', '阿凡达大幅', '阿什顿发斯蒂芬大', '4', '天津', '阿斯蒂芬噶说的是');

-- ----------------------------
-- Table structure for t_lease_type
-- ----------------------------
DROP TABLE IF EXISTS `t_lease_type`;
CREATE TABLE `t_lease_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '租赁类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='租赁类型表';

-- ----------------------------
-- Records of t_lease_type
-- ----------------------------
INSERT INTO `t_lease_type` VALUES ('1', '灯光设备');
INSERT INTO `t_lease_type` VALUES ('2', '同传设备');
INSERT INTO `t_lease_type` VALUES ('3', 'LED屏');
INSERT INTO `t_lease_type` VALUES ('4', '音响设备');

-- ----------------------------
-- Table structure for t_messages
-- ----------------------------
DROP TABLE IF EXISTS `t_messages`;
CREATE TABLE `t_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senduserid` int(11) DEFAULT NULL COMMENT '发送消息人id',
  `receptionuserid` int(11) DEFAULT NULL COMMENT '接收消息人id',
  `message` varchar(255) DEFAULT NULL COMMENT '消息',
  `time` datetime DEFAULT NULL COMMENT '发送时间',
  `isread` bit(1) DEFAULT NULL COMMENT '是否已读',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消息表';

-- ----------------------------
-- Records of t_messages
-- ----------------------------

-- ----------------------------
-- Table structure for t_opinion
-- ----------------------------
DROP TABLE IF EXISTS `t_opinion`;
CREATE TABLE `t_opinion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '意见标题',
  `centont` varchar(255) DEFAULT NULL COMMENT '意见内容',
  `imageone` varchar(255) DEFAULT NULL COMMENT '图片1',
  `imagetwo` varchar(255) DEFAULT NULL COMMENT '图片2',
  `imagethree` varchar(255) DEFAULT NULL COMMENT '图片3',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='意见问题表';

-- ----------------------------
-- Records of t_opinion
-- ----------------------------

-- ----------------------------
-- Table structure for t_refund
-- ----------------------------
DROP TABLE IF EXISTS `t_refund`;
CREATE TABLE `t_refund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordertype` int(11) DEFAULT NULL COMMENT '订单类型',
  `orderid` int(11) DEFAULT NULL COMMENT '订单id',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `states` int(11) DEFAULT NULL COMMENT '退款状态 1待确定 2已退款 3已拒绝',
  `time` datetime DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户退款表';

-- ----------------------------
-- Records of t_refund
-- ----------------------------

-- ----------------------------
-- Table structure for t_school
-- ----------------------------
DROP TABLE IF EXISTS `t_school`;
CREATE TABLE `t_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '学校封面',
  `name` varchar(255) DEFAULT NULL COMMENT '学校名称',
  `languageid` varchar(255) DEFAULT NULL COMMENT '开设语种id逗号分隔',
  `hauptstudiumid` varchar(255) DEFAULT NULL COMMENT '课程阶段id逗号分隔',
  `address` varchar(255) DEFAULT NULL COMMENT '地点',
  `introduce` varchar(255) DEFAULT NULL COMMENT '介绍',
  `userid` int(11) DEFAULT NULL COMMENT '发布人id',
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `longitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='发布学校表';

-- ----------------------------
-- Records of t_school
-- ----------------------------
INSERT INTO `t_school` VALUES ('1', '/upload/1523607149405.jpg', '天津', '3', '6', '天津', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '2', '2018-04-13 16:13:24', '32432', '3432', '2');
INSERT INTO `t_school` VALUES ('2', '/upload/1523607149405.jpg', '北京', '2', '6', '北京', '按键定时关机卡说不过卡萨丁改变', '3', '2018-05-03 14:04:42', '65561', '5616', '2');
INSERT INTO `t_school` VALUES ('3', '/upload/1523607149405.jpg', '南京', '1', '7', '南京', '阿打发海带丝烦恼丝', '5', '2018-05-03 14:05:40', '122254', '4544', '2');
INSERT INTO `t_school` VALUES ('4', '/upload/1523607149405.jpg', '西安', '5', '5', '西安', '大师傅那顿饭', '10', '2018-05-03 14:06:23', '1655615', '56465', '2');
INSERT INTO `t_school` VALUES ('5', '/upload/1523607149405.jpg', '天津科技大', '1', '10', '天津', '阿萨德哈佛阿萨德念佛鞍山', '15', '2018-05-03 14:07:04', '41665', '3213', '2');
INSERT INTO `t_school` VALUES ('6', '/upload/1523607149405.jpg', '天津大学', '3', '7', '天津', '昂达数据库的你发', '10', '2018-05-03 14:07:50', '564615', '455654', '2');
INSERT INTO `t_school` VALUES ('7', '/upload/1523607149405.jpg', '北京大学', '4', '15', '北京', 'ad师范生肯定发生', '3', '2018-05-03 14:08:34', '123132', '561656', '2');
INSERT INTO `t_school` VALUES ('8', '/upload/1523607149405.jpg', '北京航空', '1', '8', '北京', '搭理你发斯蒂芬理科', '12', '2018-05-03 14:09:26', '56516', '564656', '2');
INSERT INTO `t_school` VALUES ('9', '/upload/1523607149405.jpg', '北京航天', '3', '9', '北京', '安达街斯诺伐克静安寺', '20', '2018-05-03 14:09:58', '46516', '54165', '2');
INSERT INTO `t_school` VALUES ('10', '/upload/1523607149405.jpg', '天津航空', '4', '5', '天津', '案件的办法能看见爱上办法了', '13', '2018-05-03 14:10:39', '1516561', '5644651', '2');
INSERT INTO `t_school` VALUES ('11', '/upload/1525328870478.jpg', '454235', '2', '2', '4534', '435345435435435435', '3', '2018-05-03 14:28:42', '117.207222', '39.14617', '1');

-- ----------------------------
-- Table structure for t_slideshow
-- ----------------------------
DROP TABLE IF EXISTS `t_slideshow`;
CREATE TABLE `t_slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '图片名称',
  `url` varchar(255) DEFAULT NULL COMMENT '跳转路径',
  `image` varchar(255) DEFAULT NULL COMMENT '图片路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='轮播图表';

-- ----------------------------
-- Records of t_slideshow
-- ----------------------------

-- ----------------------------
-- Table structure for t_teacher
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher`;
CREATE TABLE `t_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `head` varchar(255) DEFAULT NULL COMMENT '头像',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `address` varchar(255) DEFAULT NULL COMMENT '所在地',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `teaching` varchar(255) DEFAULT NULL COMMENT '教学年限',
  `price` float DEFAULT NULL COMMENT '价格',
  `languageid` int(11) DEFAULT NULL COMMENT '语种id',
  `languagegrade` varchar(255) DEFAULT NULL COMMENT '语言等级',
  `hauptstudiumid` varchar(255) DEFAULT NULL COMMENT '课程阶段',
  `experience` varchar(255) DEFAULT NULL COMMENT '教学经验',
  `make` varchar(255) DEFAULT NULL COMMENT '自我介绍',
  `imageone` varchar(255) DEFAULT NULL COMMENT '风采展示1',
  `imagetwo` varchar(255) DEFAULT NULL COMMENT '风采展示2',
  `imagethree` varchar(255) DEFAULT NULL COMMENT '风采展示3',
  `imageoneexperience` varchar(255) DEFAULT NULL COMMENT '风采展示1介绍',
  `imagetwoexperience` varchar(255) DEFAULT NULL COMMENT '风采展示2介绍',
  `imagethreeexperience` varchar(255) DEFAULT NULL COMMENT '风采展示3介绍',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `longitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `certificate` varchar(255) DEFAULT NULL COMMENT '所持证书',
  `time` datetime DEFAULT NULL,
  `studying` int(11) DEFAULT NULL COMMENT '留学经验id',
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='发布老师表';

-- ----------------------------
-- Records of t_teacher
-- ----------------------------
INSERT INTO `t_teacher` VALUES ('1', '/upload/1524121635858.jpg', '3', '/upload/1524121645809.jpg', '热太热太热', '集贤里', '本科', '5', '45435', '1', '1', '4', '发生变化发生符合', '东方时尚发的规划的风格大方', '/upload/1524121984212.jpg', '/upload/1524121987148.jpg', '/upload/1524121989440.jpg', '热太热我特特天天热太热wretch', '热他热汤热汤认为他', '儿童惹我他热汤热汤', null, '117.136937', '39.234543', null, '2018-04-19 15:21:34', null, '', '2');
INSERT INTO `t_teacher` VALUES ('2', '/upload/1525311270837.jpg', '3', '/upload/1525311275571.gif', '452', '', '研究生', '32', '324', '1', 'A1', '初中', '34324324324324324324324', '3432432432432432432', '/upload/1525311352293.gif', '/upload/1525311355137.gif', '/upload/1525311357694.gif', '324324324324324', '324324324324', '324324324324324', null, '', '', null, '2018-05-03 09:36:07', '1', '', '1');
INSERT INTO `t_teacher` VALUES ('3', '/upload/1525311270837.jpg', '3', '/upload/1525311275571.gif', '爱看发生的', '青林大厦', '本科', '20', '4000', '1', '2', '高中', '案发地说哭就哭', '案发地说哭就哭', null, null, null, '案发地说哭就哭', '案发地说哭就哭', '案发地说哭就哭', null, '651651651', '16565165', null, '2018-05-03 15:20:16', '1', '', '1');
INSERT INTO `t_teacher` VALUES ('4', '/upload/1525311270837.jpg', '3', '/upload/1525311275571.gif', '昂大哥', '青林大厦1', '研究生', '15', '45656', '2', 'A2', '初中', '电费卡积分干', '电费卡积分干', null, null, null, '电费卡积分干', '电费卡积分干', '电费卡积分干', null, '15646515', '655411', null, '2018-05-03 15:21:54', '1', '', '2');
INSERT INTO `t_teacher` VALUES ('5', '/upload/1525311270837.jpg', '3', '/upload/1525311275571.gif', '爱迪生', '风之力', '博士', '30', '5000', '3', 'A1', '大学', '大数据客服部', '大数据客服部', null, null, null, '大数据客服部', '大数据客服部', '大数据客服部', null, '115615', '551651', null, '2018-05-03 15:23:13', '1', '', '1');
INSERT INTO `t_teacher` VALUES ('6', '/upload/1525311270837.jpg', '3', '/upload/1525311275571.gif', '错错错', '青林大厦B', '本科', '21', '2000', '1', 'A3', '初中', '阿大快速减肥不能', '阿大快速减肥不能', null, null, null, '阿大快速减肥不能', '阿大快速减肥不能', '阿大快速减肥不能', null, '121231', '51665156', null, '2018-05-03 15:24:49', '1', '', '2');
INSERT INTO `t_teacher` VALUES ('7', '/upload/1525415723599.jpg', '3', '/upload/1525415725745.gif', '语言老师', '货场大街', '研究生', '45', '324', '1', 'A1', '高中', '教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验教学经验', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '/upload/1525415817662.gif', '/upload/1525415819565.gif', '/upload/1525415821074.gif', '图1图1图1图1图1图1图1图1图1图1图1图1图1图1图1图1图1', '图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2图2', '图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3图3', null, '117.207222', '39.14617', null, '2018-05-04 14:37:58', '2', '', '1');

-- ----------------------------
-- Table structure for t_teacher_authentication
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher_authentication`;
CREATE TABLE `t_teacher_authentication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `nationality` varchar(255) DEFAULT NULL COMMENT '国籍',
  `birthdate` varchar(255) DEFAULT NULL COMMENT '出生日期',
  `address` varchar(255) DEFAULT NULL COMMENT '家庭住址',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `idcardfront` varchar(255) DEFAULT NULL COMMENT '手持证件正面照',
  `idcardreverse` varchar(255) DEFAULT NULL COMMENT '手持证件反面照',
  `certificatetype` varchar(255) DEFAULT NULL COMMENT '证件类型',
  `idnumber` varchar(255) DEFAULT NULL COMMENT '证件编号',
  `states` int(11) DEFAULT NULL COMMENT '认证状态 1审核中 2审核通过 3未提交审核',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `excellent` bit(1) DEFAULT NULL COMMENT '优秀译员 true表示优秀，在首页能够展示出来。false普通译员，在首页不显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='老师认证表';

-- ----------------------------
-- Records of t_teacher_authentication
-- ----------------------------
INSERT INTO `t_teacher_authentication` VALUES ('9', '\0', '中国籍', '2018/04/23', '尔特维特瑞天天热太热图文', '苏', '/upload/1525672934442.gif', '/upload/1525671599109.gif', '证件2', '222222222222222222222', '1', '3', '博士', '');
INSERT INTO `t_teacher_authentication` VALUES ('10', '\0', 'gmsfklmg', 'dfsgs', 'sgfsdgf', 'fgsdfg', null, null, 'fdgfdgfd', 'dfgfd', '1', '4', '本科', '\0');

-- ----------------------------
-- Table structure for t_teacher_authentication_credit
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher_authentication_credit`;
CREATE TABLE `t_teacher_authentication_credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authenticationid` int(11) DEFAULT NULL COMMENT '认证id',
  `images` varchar(255) DEFAULT NULL COMMENT '材料路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='老师认证信增材料关联表';

-- ----------------------------
-- Records of t_teacher_authentication_credit
-- ----------------------------
INSERT INTO `t_teacher_authentication_credit` VALUES ('5', '4', '/upload/1523458915040.jpg');
INSERT INTO `t_teacher_authentication_credit` VALUES ('6', '4', '/upload/1523458915069.jpg');
INSERT INTO `t_teacher_authentication_credit` VALUES ('7', '4', '/upload/1523458915097.jpg');
INSERT INTO `t_teacher_authentication_credit` VALUES ('8', '5', '/upload/1523603675037.jpg');
INSERT INTO `t_teacher_authentication_credit` VALUES ('9', '5', '/upload/1523603675077.jpg');
INSERT INTO `t_teacher_authentication_credit` VALUES ('10', '5', '/upload/1523603675116.jpg');
INSERT INTO `t_teacher_authentication_credit` VALUES ('11', '6', '/upload/1523605072714.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('12', '6', '/upload/1523605072727.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('13', '6', '/upload/1523605072739.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('14', '7', '/upload/1523947151534.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('15', '7', '/upload/1523947151546.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('16', '7', '/upload/1523947151557.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('17', '8', '/upload/1523958118109.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('18', '8', '/upload/1523958118126.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('19', '8', '/upload/1523958118139.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('26', '10', '/upload/1524449197406.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('27', '10', '/upload/1524449197418.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('28', '10', '/upload/1524449197430.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('92', '9', '/upload/1525671599186.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('93', '9', '/upload/1525671599198.gif');
INSERT INTO `t_teacher_authentication_credit` VALUES ('94', '9', '/upload/1525671599209.gif');

-- ----------------------------
-- Table structure for t_teacher_order
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher_order`;
CREATE TABLE `t_teacher_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordernum` varchar(255) DEFAULT NULL,
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `phone` varchar(255) DEFAULT NULL COMMENT '预约电话',
  `attendtime` datetime DEFAULT NULL COMMENT '上课时间',
  `chargetype` varchar(255) DEFAULT NULL COMMENT '收费类型',
  `address` varchar(255) DEFAULT NULL COMMENT '上课地点',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `states` int(11) DEFAULT NULL COMMENT '订单状态 1待支付 2已支付未确认  3已支付已确认 4待退款 5已退款 6已取消 7已完成  8已投诉',
  `totalprice` float DEFAULT NULL COMMENT '总价',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `paytype` int(11) DEFAULT NULL COMMENT '支付方式',
  `time` datetime DEFAULT NULL COMMENT '购买时间',
  `merchantid` int(11) DEFAULT NULL COMMENT '商家userid',
  `teacherid` int(11) DEFAULT NULL COMMENT '发布老师id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='老师购买表';

-- ----------------------------
-- Records of t_teacher_order
-- ----------------------------
INSERT INTO `t_teacher_order` VALUES ('1', '1', '1', '1', '2018-04-16 18:33:50', '1', '1', '1', '1', '1', '1', '1', '2018-04-16 18:34:02', '2', '1');
INSERT INTO `t_teacher_order` VALUES ('2', '1', '1', '1', '2018-04-23 15:03:02', '1', '1', '1', '1', '1', '2', '1', '2018-04-23 15:03:25', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('3', '1', '1', '1', '2018-04-23 16:11:30', '1', '1', '1', '2', '1', '2', '1', '2018-04-23 16:11:44', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('4', '1', '1', '1', '2018-04-23 16:13:54', '1', '1', '1', '2', '1', '2', '1', '2018-04-23 16:14:21', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('5', '2', '2', '2', '2018-04-23 16:13:58', '2', '2', '2', '4', '2', '2', '2', '2018-04-23 16:14:27', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('6', '2', '2', '2', '2018-04-23 16:14:02', '2', '2', '2', '4', '2', '2', '2', '2018-04-23 16:14:30', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('7', '2', '2', '2', '2018-04-23 16:16:25', '2', '2', '2', '7', '2', '2', '2', '2018-04-23 16:14:35', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('8', '2', '2', '2', '2018-04-23 16:14:09', '2', '2', '2', '7', '2', '2', '2', '2018-04-23 16:14:40', '2', '2');
INSERT INTO `t_teacher_order` VALUES ('9', '3', '3', '3', '2018-05-07 11:10:02', '2', '3', '2', '7', '20', '2', '2', '2018-05-07 11:10:25', '2', '6');

-- ----------------------------
-- Table structure for t_teacher_order_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher_order_evaluate`;
CREATE TABLE `t_teacher_order_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) DEFAULT NULL COMMENT '老师购买表id',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `score` float DEFAULT NULL COMMENT '评价分数',
  `content` varchar(255) DEFAULT NULL COMMENT '评价内容',
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='老师订单评价表';

-- ----------------------------
-- Records of t_teacher_order_evaluate
-- ----------------------------
INSERT INTO `t_teacher_order_evaluate` VALUES ('1', '9', '2', '5', '该老师讲课十分幽默，对学生态度良好，我很喜欢', '2018-05-07 11:12:03');
INSERT INTO `t_teacher_order_evaluate` VALUES ('2', '9', '2', '5', '案件是否看见爱上部分看见爱上会计法', '2018-05-07 11:12:25');
INSERT INTO `t_teacher_order_evaluate` VALUES ('3', '9', '2', '4', 'sadOK合法的赛欧不能放多久撒付款假把式', '2018-05-07 11:12:44');
INSERT INTO `t_teacher_order_evaluate` VALUES ('4', '9', '2', '5', '撒地方大是范德萨发斯蒂芬斯蒂芬发的打算但是', '2018-05-07 11:13:01');
INSERT INTO `t_teacher_order_evaluate` VALUES ('5', '9', '2', '4', '沙发斯蒂芬阿斯蒂芬顺丰到付的萨尔第三方大法师的 ', '2018-05-07 11:13:18');

-- ----------------------------
-- Table structure for t_territory_type
-- ----------------------------
DROP TABLE IF EXISTS `t_territory_type`;
CREATE TABLE `t_territory_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '行业领域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='行业领域';

-- ----------------------------
-- Records of t_territory_type
-- ----------------------------
INSERT INTO `t_territory_type` VALUES ('1', '1');
INSERT INTO `t_territory_type` VALUES ('2', '2');
INSERT INTO `t_territory_type` VALUES ('3', '3');
INSERT INTO `t_territory_type` VALUES ('4', '4');
INSERT INTO `t_territory_type` VALUES ('5', '5');
INSERT INTO `t_territory_type` VALUES ('6', '6');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `openid` varchar(255) DEFAULT NULL,
  `integral` int(11) DEFAULT NULL COMMENT '积分',
  `balance` float DEFAULT NULL COMMENT '余额',
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `portrait` varchar(255) DEFAULT NULL COMMENT '头像',
  `birthday` varchar(255) DEFAULT NULL COMMENT '生日',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `wechat` varchar(255) DEFAULT NULL COMMENT '微信号',
  `ismember` bit(1) DEFAULT b'0' COMMENT '是否会员',
  `identity` int(11) DEFAULT NULL COMMENT '身份 1用户 2翻译 3管理员',
  `nationality` varchar(255) DEFAULT NULL COMMENT '国籍',
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '123', '123', '10', '10', '', '/upload/1523363134931.jpg', '2018-04-04', '彩彩踩踩菜菜', '1', '1234@123', '123123', '\0', null, null, null);
INSERT INTO `t_user` VALUES ('2', '彩', 'oUaRu0V8zNW7imagfQcvOUGZW57k', null, null, '\0', '/upload/1524220350530.jpg', '1971-04-20', '不知道', '绑定手机', 'robertnxd@126.com', '18735443603', '\0', null, null, null);
INSERT INTO `t_user` VALUES ('3', '彩', 'oUaRu0QuEhZrHs5JGIXw7XFgP98g', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIzz6x6we8BY18KfMq0Bn2ubgwDdQKhdUgcshOJyn5U9iahyfXHrq6kvnO49KxT1iayu3ialAgSTXo3A/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('4', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('5', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('6', '李馨竹Urvashi', 'oUaRu0UygXHMY_9yEylSP9GLSYpQ', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/oBXmCBIu64UrAibzmX37iaHMXkxmaqM2iaPMDhbVCwDa14TDnwxnoQuMe8X6DhiaamZv83oryS1xEiaCqM1icjKWymPg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('7', '徐阿心', 'oUaRu0bM43m1AoNJlnH0JvuGbwHU', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKC5aSuFOab10IXfMOIZsjSEzkKN2UEN9ZGwUUib1v2ZjXsPE5PO39zMibFbRL9b9syfQB1ybictbYlA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('8', 'L', 'oUaRu0Vhm3ILli2u1fHkz5_-Fa1M', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLW4xGESQvNVlVIibn3HJGzZYzibfKF7w0Bmg8fUbzvibFnVQvpr5Jf7Td5y1SvY47gtn9iaAia1zw5Kmg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('9', 'Таня', 'oUaRu0ZyoDXQPpBCwMpv2KrGEo_8', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKwyPoUrHYibbs3wWJIeu7YGpzxjc4fJ0yVh1acFtaQYeY3ibpLdZFIoKxSPDSbfdBD9fapH3yCrJnw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('10', '信译天下', 'oUaRu0XCdM-QcAKNIlLIGSG_By0U', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpaqvnFvnzOXw2dC5A9c01E3FOSUFpRDmgIqujia9cy6UJNVc5F8khrzUxC1VbzvKxDAqbEFAE1fA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('11', '王美玲律师13821849821', 'oUaRu0VqTrkQ2QUirZJOG0m_5goM', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLIuzryYia3h8LVcaoQdaA2iaoHF1ZPswtXMZfyTOwv6c02QZa9DKibagqGXRCxXgQrYVEhUrc2krTww/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('12', 'Ctrl+S', 'oUaRu0UNCkDqepvxytStAe_H6-K4', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKXpMYTa0y9G2RwM86QHCm5I5hEFDDBGauPCfCo804TXnDdF1YLtuTCeLxBTY5GicYuv11cwk5mgyg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('13', 'Ophelia％足大亏', 'oUaRu0aoHc2A-qxIhM9NbTFrz_3k', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ4Lt3bQP6mjCl3S7VicLM0w6pic4lAbsphicCaakicrFXW7wWW5oQ87aib5FcBibGMwcoFrcS3bibicHD1HQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('14', '探寻', 'oUaRu0ZqvJit3iQuHsBWwotgFVBU', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKRngxr5TKg1MjicmGmn9dGQsngzXjKQbMHaJdxicwYpkibWkqJuy1cGsjz6geicyt3NeaUOhgTICOhRQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('15', 'biluo', 'oUaRu0ZsY4hNb5UykyNPpa88pDB8', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/HHPWm2UZdIsy0dniastcAfOzGwsUdNbhSwRuktJRibrJoznMrJSDM4Pt47lZXuUFAJrGR0MWjp1WIUJlD063Aibow/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('16', '洁哥儿', 'oUaRu0TuZMaCTtkkKHBKKrjONh38', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erFpA2gtR2iaxuODicJMcYFj5kegcC9M5TR3fCq9e2Kv9OKSRibyCSTichfXDQuHsXvzzy4b4R6R42X5g/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('17', '莉', 'oUaRu0QWjA7I_xjo32snntDBF2bc', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKiaRjuFX2cwtsn3XRuhgw2JKH3wrgPia2CxibK4waqjjpXkDPrMJVk5nlicpBBn4WJ9FMtIcYwMcKUCw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('18', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('19', 'Julien', 'oUaRu0TIhp4NOcpeXN86ewtWaOUY', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKS1ibm1zgO2iap2iaETED3WKas7J5GNNpREPzpn6meZpSxoH4mXBNk8jxmQIYkFiazqxhjJ1ENOS1nFw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('20', '王佳鑫', 'oUaRu0ex-PHhAkBAk0vlCg2X3gQg', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/mtN5IQ28oZ4zUZury5DAyY1G4krljVbuADIticYfQbicgiaicwwlT8AJiaLric29Qc6XTPrPzOev3ZoKiaAOy3WXDvHGA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('21', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('22', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('23', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('24', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('25', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('26', 'Judy', 'oUaRu0btP3JvfY22wWo1UxcOSCWs', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/h995ChicaLD1Un13HiapicByYiawr8TITuFNSTic5IeSibd4mBic0atangErhWyLEnbO5yRgcr9a2KTbqfFryb582EMYQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('27', '向左O_o向右', 'oUaRu0VzdsGm_aRNluA5lzbw-3_4', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI2p2U6CmrhX1LI8bW8gOtRPfcj4ibGrx0mJUxRfuibgk9iaslzjhTNUThcc1He14VVY32HAEoomLnuw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('28', '沐雨临风', 'oUaRu0YDv07P-EZU3i6Avs_-xl2U', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epLTiclfmSxickkUNo7gJJVN6bJlSMSia4P0k0CXRAc5mAIPmbFJia6nyVabGiby2M5UvuhITzVd9HmFQQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('29', 'Vago', 'oUaRu0Wt0OUOcHQymF99tzQ4vZJg', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eot29u0tswbTlwFnicge7r8xDjBL0nvs3lJWqjnQNTsqmBs5QlZl1CFrdznoqpLIiat9LQ5n8ZcuBag/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('30', '寒冰雪 ', 'oUaRu0TJUh3d178Vpj2gDaR9L3Cc', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/c6rBvXXDABI4Hq73DD8wKwsicicL5BWmcfbevVEpjI55XFL0cIre0icl9Ybt8nkLqZWZ6ru5H0wBNUZ1tGzATWadg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('31', '王 增 坤 HGR', 'oUaRu0bGW_MyLYVzAQ8Rh5KFgflk', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLr6R6LpqOwKytA3SpKoI4WEHpmjF4wKskbjTv43iapOsoUO3eMqQZ5tlquUWKlGBXlZl2mFHFbH9w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('32', '囧o(╯□╰)o张', 'oUaRu0XeFEc53IPCYMRFlV8oLolw', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/l4Omo3NmzqHmclmbib0E0KN857TsGIFaIhWN2ZAPVl6BtLBRthCcMtDvTkHyEmiaZZDice2RsA7kAZ4W2XrYBibQGg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('33', 'Bill Li', 'oUaRu0d4blQ0PdckAYUdrTG4_puY', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/QMicGzpxgwVWA6w5vqPzibACr7o6qqKghuic4zo38QB9g5vFJW6icQnaZxpXtFETyibs3vdGx7nc93ES8CuFlyppiaaA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('34', '漩漩', 'oUaRu0Ta2KSu_DsYAiW_wrFBXMpw', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKhDSfjEXX0xUQB9pes27mBYKfhicOGibiaia60kfYp9o5x1sfND8XkvjB7RHmCpe9bBOLC7wEYkgooAQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('35', '骁悍', 'oUaRu0cRQNWFqhJc3WFCrb33uE24', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/JFibNSUiaOdqoKhG4TV0U5kaWSVtFaFMDzHTrJ7Qy2GSKOSoS3RGmxgm9cr9E6AGEHSE033UYlMLTiciatTz4ar4xA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('36', '', 'oUaRu0QeHI6mV4xf7K-tOyp7CUnI', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJf2mBDrrJnfFOFB4Czc3KAnP4Q4ibRt6w8yqYronTjpdAjb60PR5WTribw518cMCNZva43IOc1MHSg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('37', '西浅', 'oUaRu0fz3C17Ety3ARMQasAYzLnQ', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLOBZ6mYhQn8pEDnqCbmB0Bo3eTPYUcUNCCicLnWIexNuVAzZklhb5euyY87DqlnAibgv7kQKZAcQlA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('38', '杉杉', 'oUaRu0THxz6FZKGfAC8dyTpvuUA0', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIicWhBAYZ1lKUb4329qn9HY6f24TasYZz7g5c1gulyg6rvkNLgEiaRaycSzGXGicnbZ7kEIS6FSkZ1w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('39', 'Jessie', 'oUaRu0RslXt4yNKDubGS-h3sxYEg', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/sXcfQTEvTpYJib5GCsoN7GzK8brib1nYUB1sZliaz7czhkJNmOXKhAFXQiaWf9nucUN2u7eTN0tJsNyrscYjMporHw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('40', '伊蓝', 'oUaRu0dgDOoBNcrVyB8QMY0rI3rQ', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/4d2bc7YKTKK66Agw9iaxfzdesPQs0oIWyib31lOx9jgImFzLzbYPp25nQxsibpFiaVHkKNiaDPbTICo1rEV7pJwnd3w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('41', 'La Triệu Thăng', 'oUaRu0U1O-CwIkeSVSAwQ43oJPs8', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/SVgSCHxRSO8C0dzoHC5VrXA0MkI7Kw0kVyWpTiaB75LEa8pHiamicq3HT0aFQjqGwcjLp6iaNfUxwnobjkNWxGWRcg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('42', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('43', '曹永权', 'oUaRu0St5oKEAWURK8DuDWobNg_c', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJu0buGdNa8yEHCzOILic7QV2661tzvGqrr1qsPLm5STGqdfB8eA6w606nR8IkHk6DFq40kWibibwnicg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('44', '付雪', 'oUaRu0aJ8lx9Hvx2pE7VSxymStNE', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLQdo1SBHhX2XFwibfjXtbAS0gl4KdADRS5EEU9BicevdkrhicwMvG2dQiawn7JXwk4uFuHV4VYAYYRMg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('45', '英语笔译+辅导', 'oUaRu0fnObYRWvCB02btdvttk5WU', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eopibicYcibaNGZqicFHtM46PpyevsZfWfSFGicPptyRbYTJSfPwQSthMFJwmicgpHruSjQTjDxgGozupXQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('46', 'cc', 'oUaRu0b-AugmtTdJ_Cad5jJz-BkU', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLh2ATHlLcPy2kgOeia09DQxZBtuHnicxH8tu70KLhOONnWE7cV791MB9236UgkMmY4iaboOibAKGZL1w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('47', '深海鱼', 'oUaRu0SEeeQzK3AkiYhhPM449k8o', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIu1n1DhUGGKU1y8MPaJjlv52ojiauVpW1rJY0iaIlPRl5NmpZnKTHTEiaq2CwMnT4UCgxNAESoqhTwQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('48', '不知道叫什么名字了', 'oUaRu0cBXiq6fOodfWPmIartZ58E', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoMR6Xtnom9GnyGqTmasDD0lPImJolN0cfibLewswhDAicQEPXKniajq6Trq1VT4eXnzhibArWCEPHMiaw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('49', 'Lee', 'oUaRu0YhRoqrZxNh5Vp_c7hkcml0', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIqW7uRtGUxbQkDTr2rKzK2WhsmQMDOKykiaEVUnjgEicFog5pMB3wbQOcRTciasHBTpibyD5ibwgQzFmw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('50', '空太', 'oUaRu0ckB1Q3XLKUjZaUgTNMjSVo', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL5XIlM0TbGliaex2GPF5SiaJL3sdyibhIGuokWKxibBE4hPKA1L8yzNmp0tGWicGMSLpwAicdNVFZ1DNjg/132', null, '张三', null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('51', 'Kent', 'oUaRu0RU5eZTDfZcJ8u_gGfF70Wk', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ergNK1vI32HjqfzBiciadhRk5yicsmNBs7tlW8CfQISG2NbhICfkLw7icdOrib4VsicoCriae5hXfpyfiadoA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('52', 'crystal', 'oUaRu0U73GIu7VUjoiw3Q1XJMYCs', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJccGD1Zk4ickb3PJ9gjibNb2MoWPnaaLfnHazduBRia8q4luA9VJfI5p4qvXnWV3Zyj1Q6otIriadibMQ/132', null, '李四', null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('53', '臻', 'oUaRu0SyTpfzyi9w1qNKWyRQoanE', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/E7FcKAiakbAQxvEW7dOzVCWKADrXnicSK0WCzHsFZclIk4TcyOzbqlq6AkicOAGSiac7C6cf6jf4oGAzgwkvN1mfpg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('54', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('55', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('56', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('57', 'jetuc', 'oUaRu0e7u8TRbJ8_pkuTH1gCd2ds', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/O81EY0hlmPEY2mxfQchGoPtzVlWAQBv0nUIC2iaibOs40JjVCic9ZnIwPo3vlIqOuLicHXLuMMia39fYdPqicUSQpMbw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('58', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('59', 'Lilixer', 'oUaRu0WXaQQ6seTa5RQcZnN496D0', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIdHNs6O71ibApkkF3MBxib4hg1R7cgEp7Bibva608B6ibZs8I8jsa2a9JX1rKMKJH1COIIoSgESpQzjQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('60', '天津银丰机械', 'oUaRu0Vwc9PrmQcqDwHboAWbLJI0', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/iaMuLqAtkNbojrs6ibs3tcYM6mibdZticFR9nHBngXdaiccLuNuWdyGxtp3SJ9sB9d3MsgfgJ37UUbw59e5wKgNI4NQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('61', 'Pamela', 'oUaRu0T_Vgs-zg7UcBd9LYKRHVyY', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKAPq3TRMRpcDj2xe3Zia04VKwNgSFG88jYPrxFdfrj9ic7iaNR3cguAVWCZGyoFBYtSqMK5MKIIlMCA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('62', '阳光的味道', 'oUaRu0VUBcNH8w4XSMfyRorLuFeA', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erlatnSzZSN3LUkyic9IibZMSRrRWdafvVYPzHhE5hWI7WEyo7j98McpzsGnQIjAkibNs6qUaljjD9eg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('63', '老实的大兔子', 'oUaRu0Tq_5AINHdkMed410SPm4O4', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqwfumCc5hXJrGAR4uxSexzXHWRVAb53uNW1stbS0vCicyspPJ1khvXiaYiclsJGqX9icoaNMvlzZEjvw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('64', '杨连生', 'oUaRu0fYCzBsKDZYFylSCfCFSTWY', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIhvibxHDOlc5ictqJPIEmF3vZcEN1eicjxkxToaIxKRicNZQKey2jKeCCw26l3RPNCT58XDpXI0exI1A/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('65', 'S', 'oUaRu0WtWyC_iE9B3OkA6usaVakw', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/5Wm8JMaxibmD7rsVLMEfCrCBpcytRqfjvx8qibFq9icxLX4jhzcyhIsmfoJ8h2yXcjOH5LEAwQOppR39wn6sVTHPg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('66', 'cancue', 'oUaRu0V2RV1V_yaCRH0naZN43XbA', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Mk5AYAfxDgF41vVyGzHkChMe5mhbpWxfQy8J3hqP09c35LgTPS2uicZ2Sqt3HhoPXuTdKJP0oibltOyIGN8ZFwnA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('67', '晴天霹雳小雨点', 'oUaRu0ZXzod6CWxAtKmpe2zt7K-0', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqiaPv4gpbstaMTibuca85PWjnias0p1qHdsUTXf73VricgN0JbUrMvXjelrlY6fXh4xnzdEnaSvzMDAQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('68', 'Anaer', 'oUaRu0ddjdyMl99eVMCz0EOq281Q', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKREdPmD9ZLvr4OGKcYMZoBqafVxQxySxBwlNFK7MXwCsm0yjO5Q7PtuMLyHkvfmZvXC6mAZGJM3g/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('69', '彻思叔叔', 'oUaRu0eAERw3SaIT-1IfrwWZGbHk', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epVZ6gnoHQnuQ3HXSzEKp0ej7MByZJ8tVibao7SvPlCUIHWegcw7bSXBu60FCYGApr5jMt4XcxMpibw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('70', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('71', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('72', '晓晓', 'oUaRu0ZQg0pXgDgAvwgahi2JE7gc', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/e6kCcibljjXlXnVZicrS6aJMeaZ7UYSqG9MG5lkMuoM1icDbI5rfMl41FaIjZOSZrnRgX2GgeHvCn7PuZ8qfj7meg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('73', '阿宝', 'oUaRu0RQf2Z0-j_KXaaxyUPiRSDg', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJNNAtHP4Y7vhwBpQgDbyZl9wLmTWI7XVubeJyF7Ksyrv1wA0PBPSl2y0pj2g7nLn8l5K7kCnlObg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('74', '对方正在输入中...', 'oUaRu0cTFkhuzgrV7wVY6D3fSn0g', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/qVyWEqek2YibdGuLic1XZ6dqLc16eGQTDoA4QREpNRhiaONYjhS97F0E6XzBfmCibzVuXmUgicmhgQCWDloorYBBOpA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('75', '人间失格', 'oUaRu0TyWYFV9JHkYlRsFEgchHaI', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/WlJYVGL3v80aEdkk1J5r72g6nMsPOZ1wHbicNyicnqlphoCDm7W9RA7UtP1Liahxs2mibHicegNsldiaBVvA8MgkUQEQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('76', '姚', 'oUaRu0fb06aCbUJowzEgrdtVe6gM', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIsRf8BtBSbQIiciaSkiceeLCHsWYGLpOsRSltzTXxWV1uHsAFpT5w9YA2x6BSWqUyMl3E3x2iaOwYCLA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('77', '菩提千年', 'oUaRu0QLV79-CLGSex9vyP1JinuE', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/L7cMsW2GHxHNvicGzyqlndImjNCGicIngUoicLGFedw0q72Zd4uKEUycYpWPLX3Ut9qzWm1iaytgtKPRxFP8EImygw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('78', '大玉子', 'oUaRu0ZhsLTNZI3Rj23_L8YKgBoo', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL7jOVvA3ek38EWSjlYbKQbCKUmPZhS10ND09EXogiadkfmp8tbD78NGLpTl4Cn0peyTuPedM1RicQA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('79', 'Лили', 'oUaRu0QdRX-dCMu8eTmpvpWUvwTs', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJiaOCnl2jzfGpKndQpUxGFo3IQuyI66lHO0WKKME9jeQRwXxDZPvqzr5x2znicrjk55ZDoIb6uPhnQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('80', '阿芬哥你瞅哪呢', 'oUaRu0Uk_EXogT2XDRbdCdsVzxCo', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ZEGTFjDibiaUIGibp4Gc1xALkhLR9N5iavcfpRJic8QCBupqOCOXNOVskQVk7Se1kxwicsfFPdKOzIbfo7ibgcmia4T0Jw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('81', '加亚', 'oUaRu0YZu-SdZT8I8UNF0PRxHExE', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/gk9fd05BbID60fFzbKfvCaSsws3JFUSNPNibmpdrqJeAqS8oCKTLJAdtHAyib9fh1rMiaRibVd1xvTx5Mt53aKSvcg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('82', 'Minna 高敏', 'oUaRu0UW_MNE5pNSbib7w4RgwCZg', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJDVia7VI5rNNBHywdvFq9DOt42cCicO8EHictTxKnLXgCpblicNleUo6q0OnibI0XwQBNs5nzvDxZcaKQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('83', '陈婷～平安保险', 'oUaRu0WB2J-_KMc7Gy9fUW34847g', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI2OjicePE1Vm07sDeHuxu2wt2P48kHHbpVLOiaOg1cZCnraQDcz2oY6ybgnsfO1KoaPByTgvp26P9w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('84', '哈哈吼吼嘿嘿嘻嘻', 'oUaRu0XT7Mx8ygD7V9xFUMWJFWyE', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/1ZXTXiavXkwWtibRuJuLN4gao7puVP2yMBjXwNV3icc1rBkbWP5xhNXiaLhg7B6YIJhW8nwYwIxaelfMlgmfuoFkXA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('85', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('86', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('87', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('88', '苗存立', 'oUaRu0aHJe6WZudBJyQJEHQU0LuM', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/blBmV7sSdXx2Xyn3QzMib492hVicFho5r2a8joEkvM2zBcFQuUZmzcK2JoKIozHEfeGjibM8Y2zjrreVtoyNEyicog/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('89', '禾刀', 'oUaRu0R-Wj1sA2m-FotqE2L1ME0g', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLjLkLnjZXzaOhZFoFRdlRTEDYfUUSqYKhOpUiaeJx4mvpFKicZggqTiced1YTPbbfGYfj1oK3jkEmug/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('90', '咿呀咿呀哟', 'oUaRu0fiuN1aSPW4pzCOQQrRYNH8', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/iaFlO9WuWgpbolfJxu7ibbH1cD9w6WXmZAtGh7vWK7kgQrcKUObfptKmozic3GOgrxib1IZZtqDsALiaTSkEgEUzj9Q/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('91', 'Сусанна', 'oUaRu0eijO8FWGFlV1ePlqcczGfQ', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKL1sfXpaGX0yMPdKEsb1ZOMa5VoHg0gicbCd8MZu5ich06MggTHdvxZ2NSm4OsbuicibPv91aA5UFOdg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('92', '韩丰丰', 'oUaRu0WWy-FFQDqiXfG1KYjltqXg', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Me9R3z0NcicTjEm3kwdCaevLbQjEwDyShxKF3HfaBoHFURsR0JJwA8gASephy4dA5mGDo0H4WQ8mn8s3cbLFQ5w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('93', '流云', 'oUaRu0e0pXCe75WSjUxwIZoMncdI', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJoOia8n2xdlIBm7Jh7cV5BBhqibDqbabm5xUOtCGsgGeXydmbMhTpouiagnmPw9081aSngBv4kMeRicg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('94', '糯米冰激凌', 'oUaRu0eC8G8fSQl_AygLibzgtZac', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/IKLN2Wl8eL5TOkh1GOrajg865k6GYejib7boSc9JWHmpEXSibwQFXgSLtMdVTKzbPWiaZIDLickM8rfuCoXUGZNEoA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('95', '不萌', 'oUaRu0XdBsL2KL5mkVuMo2cmXFoI', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ0PA7NZGiaUsQVic9MqlvENIibg5MDaacbTW1L68Ykicia2xnwWick94Mw4aAahjYZ6bzlcBuxfUE2Mhcg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('96', 'bayansumber', 'oUaRu0VgfivWw1_srR8Vkne9FEm0', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erpMmbsvjF1paFFNMiafWrv2Kxd7lTytYPawos5mtBDAXzbYVXfia306t0bPvWFKDY3RvnkRuicAof0w/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('97', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('98', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('99', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('100', 'Lucky bastard.', 'oUaRu0bjXxjDgwy52k67tc7FGmOk', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLmKF50zCdhQP906Asibiahk7KWqWynuNmIBFTeEXicA9XCY2ibnFCNAbTblJzeN2dWpmy54eib17aQVlg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('101', 'cccen', 'oUaRu0Z6rUBE7qnc-wSug5SuRud4', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/QicRsQg7tSz8IFSgQKfKsr7gic917MfgUtqHODmgdxg1WzmDROibWkYmsn91xvI3xm1oVo8LtFkkCQzy1nKaJnCaA/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('102', '杨萌', 'oUaRu0TqTUgtAoqE1f79LpPtv9TU', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLV8SL5uXMGoTUOQRjI875EROhRM2BBWibPbQTuqsOIB0WzlJmDqhZeVNuI79FFTBVlvagOG0fgH3A/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('103', '土豆片', 'oUaRu0S3NXK1_SeZ6hEXD2BccCo8', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/oqTXqkwrDagVWW2bLyAl0BYc8AlOpEKRzutN9kK5XKrkUkeo9LicKHX9oy76Yib5KMZ2k8fslfoeFUnATx2kU1TQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('104', 'seem', 'oUaRu0aqey1SDkTyLx6rRSb47ucM', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIR29iaYAOr8Q2jalvc2THAicuAvRKQpGoME46prq87QU3eZBj0pDJac94tiaYNDyjBlz8eBsNMMBHKw/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('105', '熙', 'oUaRu0c2FX97jbxDHPwKBZnKNJBs', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/wIf4fIUfCXZeP7S0QzEnwR9v0uliaLO0eGVLxQDNeHQ2dics2uVVMKm1NCQ70EDTFnGwV7FlYZaO3PXNz8BMZ5Qg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('106', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('107', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('108', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('109', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('110', 'Coffee', 'oUaRu0ccMCADVVn1AjQEU_NKGvUU', '0', '0', '\0', '', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('111', '西方烟白', 'oUaRu0drh5sVZrBhbLH-_T_XAlrc', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIFicK7Bn88LqjCT0r0HnZ2EQ8WVKdpsVgS3Y0hHyaKJBOyicLfXm0hp2kHhPLbThuS1zLHVsUsW4gQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('112', 'end', 'oUaRu0cDVwuCrg6Pn01YRcOic_Ic', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLq2qZOJ1u1cGpZzoZs0APBLaiax3MhG6FWspET4fhR0OKtniayxGn8nBia4icFuZXbvCkJACmwKOEPAQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('113', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('114', null, null, '0', '0', null, null, null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('115', '甘霖', 'oUaRu0Y59Dv_lnGNyjkq7beD5vOQ', '0', '0', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJzPyUgdxdz8pPspbgSogZ3EHILpiaZocSI08WMntuHFvsiaSSEgI8NJOicThjUaTEq97B0ickjNxHWGg/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('116', 'lavender', 'oUaRu0b_utGdmR6U3jV0Y4xoW9xs', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epFQQ7URKrRHYiaXPCZg6R4TUYQAjia2lMI014UFdEfBibM8ZCHfwDQc8Ow7el14u60jiaibClWLvX4yJQ/132', null, null, null, null, null, '\0', null, null, null);
INSERT INTO `t_user` VALUES ('117', '高多多', 'oUaRu0Xvxtr-DJ6gc_1j1YlHWyiU', '0', '0', '\0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKdiaSIlia1M3OcW03XuvcnbibZ4nfZpCqq70xPI7REPeKKw8YXfe9x3BPpiarjLhnl4qUmeJq6B6EMXg/132', null, null, null, null, null, '\0', null, null, null);

-- ----------------------------
-- Table structure for t_user_jurisdiction
-- ----------------------------
DROP TABLE IF EXISTS `t_user_jurisdiction`;
CREATE TABLE `t_user_jurisdiction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `jurisdictionid` int(11) DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户认证权限关联表';

-- ----------------------------
-- Records of t_user_jurisdiction
-- ----------------------------

-- ----------------------------
-- Table structure for t_visa
-- ----------------------------
DROP TABLE IF EXISTS `t_visa`;
CREATE TABLE `t_visa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(255) DEFAULT NULL COMMENT '区域',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `isinterview` bit(1) DEFAULT NULL COMMENT '是否面试',
  `visatype` varchar(255) DEFAULT NULL COMMENT '签证类型',
  `sendaddress` varchar(255) DEFAULT NULL COMMENT '送签地',
  `company` varchar(255) DEFAULT NULL COMMENT '签证公司',
  `images` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `price` float DEFAULT NULL COMMENT '价格',
  `num` varchar(255) DEFAULT NULL COMMENT '入境次数',
  `duration` varchar(255) DEFAULT NULL COMMENT '办理时长',
  `scope` varchar(255) DEFAULT NULL COMMENT '受理范围',
  `validity` varchar(255) DEFAULT NULL COMMENT '有效期',
  `remainum` int(11) DEFAULT NULL COMMENT '可停留天数',
  `material` varchar(255) DEFAULT NULL COMMENT '所需材料',
  `matters` varchar(255) DEFAULT NULL COMMENT '其他注意事项',
  `flow` varchar(255) DEFAULT NULL COMMENT '办理流程',
  `name` varchar(255) DEFAULT NULL COMMENT '服务名称',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `userid` int(11) DEFAULT NULL,
  `states` int(11) DEFAULT NULL COMMENT '状态  1未通过 2已通过 3已拒绝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='签证表';

-- ----------------------------
-- Records of t_visa
-- ----------------------------
INSERT INTO `t_visa` VALUES ('1', '4', '3', '', '3', '天津', '', '/upload/1524129731726.jpg', '34543', '345', '43543', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '5465465', '758', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍自我介绍', '签证', '5465465465464', '2', '2');

-- ----------------------------
-- Table structure for t_visa_order
-- ----------------------------
DROP TABLE IF EXISTS `t_visa_order`;
CREATE TABLE `t_visa_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visaid` int(11) DEFAULT NULL COMMENT '签证id',
  `departdate` datetime DEFAULT NULL COMMENT '出发日期',
  `name` varchar(255) DEFAULT NULL COMMENT '申请人姓名',
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `age` varchar(255) DEFAULT NULL COMMENT '年龄段',
  `clienttype` varchar(255) DEFAULT NULL COMMENT '客户类型',
  `passportnum` varchar(255) DEFAULT NULL COMMENT '护照号码',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `ordernumber` int(11) DEFAULT NULL COMMENT '订单号',
  `paytype` int(11) DEFAULT NULL COMMENT '支付方式',
  `paystates` int(11) DEFAULT NULL COMMENT '支付状态  1待支付 2已支付未确认  3已支付已确认 4待退款 5已退款 6已取消 7已完成',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `price` float DEFAULT NULL,
  `merchantid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='签证订单表';

-- ----------------------------
-- Records of t_visa_order
-- ----------------------------
INSERT INTO `t_visa_order` VALUES ('1', '1', '2018-05-04 10:29:56', '张三', '', '30', '1', '123456', '50', '15466', '1', '1', '2018-05-04 10:31:26', '200', '1');

-- ----------------------------
-- Table structure for t_visa_order_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `t_visa_order_evaluate`;
CREATE TABLE `t_visa_order_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) DEFAULT NULL COMMENT '签证订单id',
  `userid` int(11) DEFAULT NULL COMMENT '评价用户id',
  `score` float(11,0) DEFAULT NULL COMMENT '评价分数',
  `cotent` varchar(255) DEFAULT NULL COMMENT '评价内容',
  `time` datetime DEFAULT NULL COMMENT '评价时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签证订单评价表';

-- ----------------------------
-- Records of t_visa_order_evaluate
-- ----------------------------

-- ----------------------------
-- Table structure for t_visa_type
-- ----------------------------
DROP TABLE IF EXISTS `t_visa_type`;
CREATE TABLE `t_visa_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '签证类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='签证类型表';

-- ----------------------------
-- Records of t_visa_type
-- ----------------------------
INSERT INTO `t_visa_type` VALUES ('1', '类型1');
INSERT INTO `t_visa_type` VALUES ('2', '类型2');
INSERT INTO `t_visa_type` VALUES ('3', '类型3');
INSERT INTO `t_visa_type` VALUES ('4', '类型4');
INSERT INTO `t_visa_type` VALUES ('5', '类型5');
INSERT INTO `t_visa_type` VALUES ('6', '类型6');

-- ----------------------------
-- Table structure for t_wanted
-- ----------------------------
DROP TABLE IF EXISTS `t_wanted`;
CREATE TABLE `t_wanted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '求职标题',
  `name` varchar(255) DEFAULT NULL COMMENT '求职人',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `salary` varchar(255) DEFAULT NULL COMMENT '要求工资',
  `time` datetime DEFAULT NULL COMMENT '发布日期',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `school` varchar(255) DEFAULT NULL COMMENT '毕业学校',
  `work` varchar(255) DEFAULT NULL COMMENT '工作经验',
  `email` varchar(255) DEFAULT NULL COMMENT '联系邮箱',
  `description` varchar(255) DEFAULT NULL COMMENT '经验描述',
  `exhibition` varchar(255) DEFAULT NULL COMMENT '风采展示',
  `images` varchar(255) DEFAULT NULL COMMENT '本人照片',
  `position` varchar(255) DEFAULT NULL COMMENT '应聘职位',
  `sex` bit(1) DEFAULT NULL COMMENT '性别',
  `degree` varchar(255) DEFAULT NULL COMMENT '学位',
  `states` int(11) DEFAULT NULL COMMENT '发布状态 1未通过 2已通过 3已拒绝',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='求职表';

-- ----------------------------
-- Records of t_wanted
-- ----------------------------
INSERT INTO `t_wanted` VALUES ('1', '寻求工作', '姚', '天津', '6-7k', '2018-05-02 14:47:57', '本科', '20', '天津大学', '翻译两年', '2151313321@qq.com', 'hjgchgchjgchgc', 'tytdytytcytcyt', null, '笔译译员', '', '本科', '2', '76');

-- ----------------------------
-- Table structure for t_wanted_certificate
-- ----------------------------
DROP TABLE IF EXISTS `t_wanted_certificate`;
CREATE TABLE `t_wanted_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '证书名',
  `wantedid` int(11) DEFAULT NULL COMMENT '求职者id',
  `time` varchar(255) DEFAULT NULL COMMENT '有效日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='求职证书关联表';

-- ----------------------------
-- Records of t_wanted_certificate
-- ----------------------------

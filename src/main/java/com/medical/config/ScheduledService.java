package com.medical.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.medical.service.TeacherOrderEvaluateService;

@Component
public class ScheduledService {
	
	@Autowired
	private TeacherOrderEvaluateService biz;
	
	@Scheduled(cron = "0 0/10 * * * ?")
    public void scheduled(){
       biz.updateOrderStates();
    }
}

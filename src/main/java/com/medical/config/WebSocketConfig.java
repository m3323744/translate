package com.medical.config;

import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.medical.websocket.TextMessageHandler;


@Configuration
//@EnableWebMvc
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
//    	TextMessageHandler textMessageHandler=new TextMessageHandler();
    	System.out.println("websocket启动");
        registry.addHandler(textMessageHandler(), "/websocket/ChatNotice").setAllowedOrigins("*");
        registry.addHandler(textMessageHandler(), "/websocket/ChatNoticeJs").withSockJS();
    }

    @Bean
    public TextMessageHandler textMessageHandler() {
        return new TextMessageHandler();
    }
}

package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Conference;

public interface ConferenceMapper extends BaseMapper<Conference> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Conference record);

    Conference selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Conference record);

    int updateByPrimaryKey(Conference record);
    
    @Select("SELECT * FROM t_conference  WHERE id=#{id}")
    Map<String, Object> getOrderDateils(@Param("id")int id);
    
    @Update("UPDATE t_conference SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
    
    @Select("SELECT id,images,`name`,price as totalprice,`release`,7 as type FROM t_conference WHERE userid=#{userId} UNION ALL SELECT id,images,`name`,price as totalprice,`release`,8 as type FROM t_convention WHERE userid=#{userId}")
    public List<Map<String, Object>> getByUserId(@Param("userId")int userId);
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.TerritoryType;

public interface TerritoryTypeMapper extends BaseMapper<TerritoryType> {
    int deleteByPrimaryKey(Integer id);
    
    int insertSelective(TerritoryType record);

    TerritoryType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TerritoryType record);

    int updateByPrimaryKey(TerritoryType record);
}
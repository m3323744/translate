package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CertificateTwoType;

public interface CertificateTwoTypeMapper extends BaseMapper<CertificateTwoType> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CertificateTwoType record);

    CertificateTwoType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CertificateTwoType record);

    int updateByPrimaryKey(CertificateTwoType record);
}
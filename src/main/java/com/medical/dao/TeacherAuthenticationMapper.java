package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.TeacherAuthentication;


public interface TeacherAuthenticationMapper extends BaseMapper<TeacherAuthentication> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(TeacherAuthentication record);

    TeacherAuthentication selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TeacherAuthentication record);

    int updateByPrimaryKey(TeacherAuthentication record);
    
    @Select("SELECT * FROM t_teacher_authentication as ta WHERE userid=#{userid}")
    public TeacherAuthentication getByUserid(@Param("userid")int userid);
    
    @Select("SELECT ifnull(AVG(te.score),0.0) as score FROM t_teacher_authentication as ta LEFT JOIN t_teacher_order as t ON t.merchantid=ta.userid LEFT JOIN t_teacher_order_evaluate as te ON te.orderid=t.id WHERE ta.userid=#{userId}")
    public Double selectAvgScore(@Param("userId")int userId);
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.IntegralConversion;

public interface IntegralConversionMapper extends BaseMapper<IntegralConversion>{
    int deleteByPrimaryKey(Integer id);
    
    int insertSelective(IntegralConversion record);

    IntegralConversion selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IntegralConversion record);

    int updateByPrimaryKey(IntegralConversion record);
}
package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CourseOrder;


public interface CourseOrderMapper extends BaseMapper<CourseOrder> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CourseOrder record);

    CourseOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CourseOrder record);

    int updateByPrimaryKey(CourseOrder record);
    
    @Select("SELECT *,co.time as orderTime,co.paytype as paytype,co.`name` as inName,co.merchantid as shangId,co.userid as yongId FROM  t_course_seeing as cs LEFT JOIN t_course_order as co ON co.courseid=cs.id WHERE co.id=#{id}")
    public Map<String, Object> getSeeingOrderDateils(@Param("id")int id);
    
    @Select("SELECT * FROM t_course_simple as cs LEFT JOIN t_course_order,co.merchantid as shangId,co.userid as yongId  as co ON co.lessonsid=cs.id WHERE co.id=#{id}")
    public Map<String, Object> getSimpleOrderDateils(@Param("id")int id);
    
    @Select("SELECT *,co.time as orderTime,co.`name` as inName,co.merchantid as shangId,co.userid as yongId FROM t_course_streaming as cs LEFT JOIN  t_course_order as co ON co.courseid=cs.id WHERE co.id=#{id}")
    public Map<String, Object> getStreamingOrderDateils(@Param("id")int id);
    
    @Select("SELECT *,co.time as orderTime,co.`name` as inName,co.merchantid as shangId,co.userid as yongId FROM t_course_offline as cs LEFT JOIN t_course_order  as co ON co.courseid=cs.id WHERE co.id=#{id}")
    public Map<String, Object> getOfflineOrderDateils(@Param("id")int id);
    
    @Select("SELECT COUNT(1) FROM (SELECT COUNT(1) FROM t_course_order as co LEFT JOIN t_course_seeing as cs ON co.courseid=cs.id WHERE co.coursetype=1 AND cs.schoolid=#{schoolId} UNION ALL SELECT COUNT(1) FROM t_course_order as co LEFT JOIN t_course_streaming as cs ON co.courseid=cs.id WHERE co.coursetype=2 AND cs.schoolid=#{schoolId} UNION ALL SELECT COUNT(1) FROM t_course_order as co LEFT JOIN t_course_offline as cs ON co.courseid=cs.id WHERE co.coursetype=3 AND cs.schoolid=#{schoolId}) AS course")
    public Integer serviceCount(@Param("schoolId")int schoolId);
    
    @Select("SELECT COUNT(1) FROM t_course_order co WHERE co.coursetype=1 AND co.courseid=#{courseid}")
    public Integer getCountByCourseid(@Param("courseid")int courseid);
    
    @Update("UPDATE t_course_order SET states=#{states} WHERE id=#{id}")
    int updateStatesById(@Param("states")int states,@Param("id")int id);
}
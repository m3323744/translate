package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Hauptstudium;

public interface HauptstudiumMapper extends BaseMapper<Hauptstudium>{
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Hauptstudium record);

    Hauptstudium selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Hauptstudium record);

    int updateByPrimaryKey(Hauptstudium record);
}
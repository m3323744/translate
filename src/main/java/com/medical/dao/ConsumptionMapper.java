package com.medical.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Consumption;

public interface ConsumptionMapper extends BaseMapper<Consumption> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Consumption record);

    Consumption selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Consumption record);

    int updateByPrimaryKey(Consumption record);
    
    @Select("select * from  t_consumption where userId=#{userId} ${time} order by time desc limit #{pageIndex},10")
    List<Consumption> findAllByUserId(@Param("userId")int userId,@Param("time")String time,@Param("pageIndex")int pageIndex);
    
    @Select("select count(1) from  t_consumption where userId=#{userId} ${time}")
    public Integer findAllByUserIdCount(@Param("userId")int userId,@Param("time")String time);
}
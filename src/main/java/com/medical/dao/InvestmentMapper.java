package com.medical.dao;

import com.medical.entity.Investment;

public interface InvestmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Investment record);

    int insertSelective(Investment record);

    Investment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Investment record);

    int updateByPrimaryKey(Investment record);
}
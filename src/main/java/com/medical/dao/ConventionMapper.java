package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Convention;

public interface ConventionMapper extends BaseMapper<Convention> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Convention record);

    Convention selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Convention record);

    int updateByPrimaryKey(Convention record);
    
    @Select("SELECT * FROM t_convention as c LEFT JOIN t_convention_apply as ca ON c.id=ca.conventionid WHERE c.id=#{orderId}")
    public Map<String, Object>  getOrderDateils(@Param("orderId")int orderId);
    
    @Select("(SELECT c.id,c.`name`,c.images,c.time,c.states FROM t_convention as c WHERE id in (SELECT a.conventionid FROM t_convention_apply as a WHERE a.userid=#{userId})) UNION ALL (SELECT c.id,c.`name`,c.images,c.time,c.states FROM t_conference as c WHERE id in (SELECT a.conferenceid FROM t_conference_apply as a WHERE a.userid=#{userId})) limit #{pageIndex},#{size}")
    public List<Map<String, Object>> getByMyUserId(@Param("userId")int userId,@Param("pageIndex")int pageIndex,@Param("size")int size);
    
    @Update("UPDATE t_convention SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
}
package com.medical.dao;

import com.medical.entity.UserJurisdiction;

public interface UserJurisdictionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserJurisdiction record);

    int insertSelective(UserJurisdiction record);

    UserJurisdiction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserJurisdiction record);

    int updateByPrimaryKey(UserJurisdiction record);
}
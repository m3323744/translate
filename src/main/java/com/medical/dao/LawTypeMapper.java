package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.medical.entity.LawType;

public interface LawTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LawType record);

    int insertSelective(LawType record);

    LawType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LawType record);

    int updateByPrimaryKey(LawType record);
    
    @Select("select * from t_law_type")
    List<Map<String,Object>> findTypeList();
    @Select("select * from t_law where typeid=#{typeid}")
    Map<String,Object> findContentByTypeid(@Param("typeid")Integer typeId);
}
package com.medical.dao;

import com.medical.entity.Law;

public interface LawMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Law record);

    int insertSelective(Law record);

    Law selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Law record);

    int updateByPrimaryKey(Law record);
}
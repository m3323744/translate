package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Refund;

public interface RefundMapper extends BaseMapper<Refund> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Refund record);

    Refund selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Refund record);

    int updateByPrimaryKey(Refund record);
    
    @Update("UPDATE t_refund SET states=#{re.states} WHERE ordertype=#{re.ordertype} AND orderid=#{re.orderid} AND userid=#{re.userid}")
    public int updateStates(@Param("re")Refund re);
    
    @Select("SELECT * FROM t_refund WHERE ordertype=#{re.ordertype} AND orderid=#{re.orderid} AND userid=#{re.userid}")
    public Refund getByOrderId(@Param("re")Refund re);
}
package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Visa;

public interface VisaMapper extends BaseMapper<Visa> {
	
	@Select("SELECT * FROM t_visa WHERE 1=1 ${sql} ${order} limit #{pageIndex},#{size}")
    public List<Map<String, Object>> getList(@Param("sql")String sql,@Param("order") String order,@Param("pageIndex")int pageIndex,@Param("size")int size);
	
	@Update("UPDATE t_visa SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
	
	@Select("SELECT id,images,`name`,price as totalprice,`release`,6 as type FROM t_visa WHERE userid=#{userId}")
    public List<Map<String, Object>> getByUserId(@Param("userId")int userId);
	
	@Select("SELECT count(1) FROM t_visa WHERE 1=1 ${sql} ")
    public Integer getListCount(@Param("sql")String sql);
}
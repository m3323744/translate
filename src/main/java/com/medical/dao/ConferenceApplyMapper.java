package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.ConferenceApply;

public interface ConferenceApplyMapper extends BaseMapper<ConferenceApply> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(ConferenceApply record);

    ConferenceApply selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ConferenceApply record);

    int updateByPrimaryKey(ConferenceApply record);
}
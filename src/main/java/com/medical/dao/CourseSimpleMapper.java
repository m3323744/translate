package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CourseSimple;

public interface CourseSimpleMapper extends BaseMapper<CourseSimple> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CourseSimple record);

    CourseSimple selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CourseSimple record);

    int updateByPrimaryKey(CourseSimple record);
    
    @Update("UPDATE t_course_simple SET courseid=#{courseid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("courseid")int courseid,@Param("id")int id);
    
    @Select("select * from t_course_simple where id=#{id}")
    public Map<String, Object> getById(@Param("id")int id);
}
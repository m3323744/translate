package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Job;

public interface JobMapper extends BaseMapper<Job> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Job record);

    Job selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Job record);

    int updateByPrimaryKey(Job record);
    
    @Select("select j.id, j.title,j.time,j.company,j.address from t_job j where j.states=2 ORDER BY j.time desc limit 0,9")
    List<Map<String,Object>> findHotJobs();
    
    @Select("SELECT count(1) FROM t_job WHERE time >=#{startTime} AND time<=#{endTime} and userid=#{userId}")
    int addCountByTime(@Param("startTime")String startTime,@Param("endTime")String endTime,@Param("userId")int userId);
}
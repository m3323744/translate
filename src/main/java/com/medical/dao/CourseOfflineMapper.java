package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CourseOffline;

public interface CourseOfflineMapper extends BaseMapper<CourseOffline> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CourseOffline record);

    CourseOffline selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CourseOffline record);

    int updateByPrimaryKey(CourseOffline record);
    
    @Select("SELECT * FROM t_course_offline WHERE 1=1 ${sql} ${order} limit #{pageIndex},#{size}")
    public List<Map<String, Object>> getList(@Param("sql")String sql,@Param("order") String order,@Param("pageIndex")int pageIndex,@Param("size")int size);
    
    @Update("UPDATE t_course_offline SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
    
    @Select("SELECT count(1) FROM t_course_offline WHERE 1=1 ${sql}")
    public Integer getListCount(@Param("sql")String sql);
}
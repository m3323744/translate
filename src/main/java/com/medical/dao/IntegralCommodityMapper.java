package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.IntegralCommodity;

public interface IntegralCommodityMapper extends BaseMapper<IntegralCommodity> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(IntegralCommodity record);

    IntegralCommodity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IntegralCommodity record);

    int updateByPrimaryKey(IntegralCommodity record);
}
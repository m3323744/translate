package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Wantedcertificate;

public interface wantedcertificateMapper extends BaseMapper<Wantedcertificate> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Wantedcertificate record);

    Wantedcertificate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Wantedcertificate record);

    int updateByPrimaryKey(Wantedcertificate record);
    
    @Update("UPDATE t_wanted_certificate SET wantedid=#{wantedid} WHERE id=#{id}")
    public int updateWantedId(@Param("id")int id,@Param("wantedid")int wantedid);
    
    @Select("select * from t_wanted_certificate where id=#{id}")
    public Map<String, Object> getById(@Param("id")int id);
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Contribute;

public interface ContributeMapper extends BaseMapper<Contribute> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Contribute record);

    Contribute selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Contribute record);

    int updateByPrimaryKey(Contribute record);
}
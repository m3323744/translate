package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.VisaOrderEvaluate;

public interface VisaOrderEvaluateMapper extends BaseMapper<VisaOrderEvaluate> {
	@Select("SELECT IFNULL(AVG(ve.score),0) FROM t_visa_order_evaluate as ve LEFT JOIN t_visa_order as vo ON ve.orderid=vo.id WHERE vo.visaid=#{visaid}")
	public Double selectAvgScore(@Param("visaid")int visaid);
	
	@Select("SELECT * FROM t_visa_order_evaluate as vo LEFT JOIN t_visa_order as v ON vo.orderid=v.id WHERE v.id=#{orderId}")
	public Map<String, Object> selectByOrderId(@Param("orderId")int orderId);
}

package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.AuthenticationLanguage;

public interface AuthenticationLanguageMapper extends BaseMapper<AuthenticationLanguage> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(AuthenticationLanguage record);

    AuthenticationLanguage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AuthenticationLanguage record);

    int updateByPrimaryKey(AuthenticationLanguage record);
    
    @Update("UPDATE t_authentication_language SET authenticationid=#{authenticationid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("authenticationid")int authenticationid,@Param("id")int id);
    
    @Select("SELECT al.*,l.`language` FROM t_authentication_language as al LEFT JOIN t_language as l ON l.id=al.languageid WHERE al.authenticationid=#{authenticationid} and al.isteacher=#{isteacher}")
    public List<Map<String, Object>> selectByAuthenticationid(@Param("authenticationid")int authenticationid,@Param("isteacher")int isteacher);
    
    @Select("SELECT al.*,l.`language` FROM t_authentication_language as al LEFT JOIN t_language as l ON al.languageid=l.id WHERE al.id=#{id}")
    public Map<String, Object> getById(@Param("id")int id);
}
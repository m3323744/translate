package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.medical.entity.FinanceType;

public interface FinanceTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FinanceType record);

    int insertSelective(FinanceType record);

    FinanceType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FinanceType record);

    int updateByPrimaryKey(FinanceType record);
    
    @Select("select * from t_finance_type")
    List<Map<String,Object>> findTypeList();
   
    @Select("select * from t_finance where typeid=#{typeid}")
    Map<String,Object> findContentByTypeid(@Param("typeid")Integer typeId);
}
package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Enterprise;

public interface EnterpriseMapper extends BaseMapper<Enterprise> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Enterprise record);

    Enterprise selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Enterprise record);

    int updateByPrimaryKey(Enterprise record);
    
    @Select("SELECT * FROM t_enterprise as e WHERE e.userid=#{userId}")
    public Enterprise selectByUserId(@Param("userId")int userId);
}
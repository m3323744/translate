package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.LeaseType;

public interface LeaseTypeMapper extends BaseMapper<LeaseType> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(LeaseType record);

    LeaseType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LeaseType record);

    int updateByPrimaryKey(LeaseType record);
}
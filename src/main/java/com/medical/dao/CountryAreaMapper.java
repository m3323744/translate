package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CountryArea;

public interface CountryAreaMapper extends BaseMapper<CountryArea> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CountryArea record);

    CountryArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CountryArea record);

    int updateByPrimaryKey(CountryArea record);
}
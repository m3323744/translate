package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.InterpreterAuthentication;

public interface InterpreterAuthenticationMapper extends BaseMapper<InterpreterAuthentication> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(InterpreterAuthentication record);

    InterpreterAuthentication selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InterpreterAuthentication record);

    int updateByPrimaryKey(InterpreterAuthentication record);
    
    @Select("SELECT * FROM t_interpreter_authentication as ta WHERE userid=#{userid} and interpretertype=#{typeId}")
    public InterpreterAuthentication getByUserid(@Param("userid")int userid,@Param("typeId")int typeId);
}
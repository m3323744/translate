package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.User;

public interface UserMapper extends BaseMapper<User> {
	@Update("UPDATE t_user SET openid=#{openId} WHERE id=#{userId}")
	public int updateOpenId(@Param("userId")int userId,@Param("openId")String openId);
	
	@Select("SELECT * FROM t_user as u WHERE u.openid=#{openId}")
	public User selectByOpenId(@Param("openId")String openId);
	
	@Select("select IFNULL(balance,0) as balance,IFNULL(integral,0) as integral  from t_user where id=#{id}")
	public Map<String,Object> findBalance(@Param("id")String id);
	
	@Update("UPDATE t_user set integral=#{integral} WHERE id=#{userId}")
	public Integer updateIntegral(@Param("integral")int integral,@Param("userId")int userId);
	
	@Select("SELECT * FROM t_user WHERE phone=#{phone}")
	public User userDateilsByPhone(@Param("phone")String phone);
}
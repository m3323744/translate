package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.VisaOrder;

public interface VisaOrderMapper extends BaseMapper<VisaOrder> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(VisaOrder record);

    VisaOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(VisaOrder record);

    int updateByPrimaryKey(VisaOrder record);
    
    @Select("SELECT *,vo.time as orderTime,vo.`name` as inName,vo.userid as yongId  FROM t_visa as v LEFT JOIN  t_visa_order as vo ON vo.visaid=v.id WHERE vo.id=#{id}")
    Map<String, Object> getOrderDateils(@Param("id")int id);
    
    @Update("UPDATE t_visa_order SET paystates=#{states} WHERE id=#{id}")
    int updateStatesById(@Param("states")int states,@Param("id")int id);
}
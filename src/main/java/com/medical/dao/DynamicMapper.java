package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Dynamic;

public interface DynamicMapper extends BaseMapper<Dynamic>{
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Dynamic record);

    Dynamic selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Dynamic record);

    int updateByPrimaryKey(Dynamic record);
    
    @Select("SELECT d.*,u.nickname,u.portrait,(SELECT COUNT(1) FROM t_dynamic_comment WHERE t_dynamic_comment.dynamicid=d.id) as commentNum,(SELECT COUNT(1) FROM t_dynamic_like WHERE t_dynamic_like.dynamicid=d.id) as likeNum FROM t_dynamic as d LEFT JOIN t_user as u ON d.userid=u.id order by d.time desc limit #{pageIndex},#{size}")
    public List<Map<String, Object>> getList(@Param("pageIndex")int pageIndex,@Param("size")int size);
}
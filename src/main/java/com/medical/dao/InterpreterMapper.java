package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Interpreter;

public interface InterpreterMapper extends BaseMapper<Interpreter> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Interpreter record);

    Interpreter selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Interpreter record);

    int updateByPrimaryKey(Interpreter record);
    
    @Select("SELECT  i.* FROM t_interpreter as i LEFT JOIN t_user as u ON i.userid=u.id ${sql} ${order} limit #{pageIndex},#{size}")
    public List<Map<String, Object>>  selectByInter(@Param("sql")String sql,@Param("order") String order,@Param("pageIndex")int pageIndex,@Param("size")int size);
    
    @Select("SELECT count(1) FROM t_interpreter as i LEFT JOIN t_user as u ON i.userid=u.id ${sql}")
    public Integer  selectByInterCount(@Param("sql")String sql);
    
    @Select("select i.id as interpreterId,l.`language`,i.head,i.`name`,i.interpretertype from  t_interpreter i left join t_language l on l.id=i.intograde LEFT JOIN t_interpreter_authentication as ia ON ia.userid=i.userid WHERE i.`release`=true AND i.states=2 AND ia.excellent=true  order by i.id desc limit 0,9  ")
    public List<Map<String,Object>> selectByTop();
    
    @Update("UPDATE t_interpreter SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
    
    @Select("SELECT id,images,`name`,interpretertype,hourprice,minuteprice,wordprice,`release`,2 as type FROM t_interpreter  WHERE userid=#{userId}")
    public List<Map<String, Object>> getByUserId(@Param("userId")int userId);
}
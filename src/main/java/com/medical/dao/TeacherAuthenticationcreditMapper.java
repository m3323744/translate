package com.medical.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.TeacherAuthenticationcredit;

public interface TeacherAuthenticationcreditMapper extends BaseMapper<TeacherAuthenticationcredit> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(TeacherAuthenticationcredit record);

    TeacherAuthenticationcredit selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TeacherAuthenticationcredit record);

    int updateByPrimaryKey(TeacherAuthenticationcredit record);
    
    @Update("UPDATE t_teacher_authentication_credit SET authenticationid=#{authenticationid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("authenticationid")int authenticationid,@Param("id")int id);
    
    @Select("SELECT * FROM t_teacher_authentication_credit WHERE authenticationid=#{authenticationid}")
    public List<TeacherAuthenticationcredit> selectByAuthenticationid(@Param("authenticationid")int authenticationid);
}
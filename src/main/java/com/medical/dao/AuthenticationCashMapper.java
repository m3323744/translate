package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.AuthenticationCash;

public interface AuthenticationCashMapper extends BaseMapper<AuthenticationCash> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(AuthenticationCash record);

    AuthenticationCash selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AuthenticationCash record);

    int updateByPrimaryKey(AuthenticationCash record);
    
    @Select("SELECT IFNULL(states,0) as states FROM t_authentication_cash WHERE userid=#{userid}")
    public Integer selectStates(@Param("userid")int userid);
    
    @Select("SELECT * FROM t_authentication_cash ac WHERE userid=#{userId} LIMIT 0,1")
    public AuthenticationCash getByUserId(@Param("userId")int userId);
}
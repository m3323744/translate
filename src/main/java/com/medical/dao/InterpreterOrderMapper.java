package com.medical.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.InterpreterOrder;

public interface InterpreterOrderMapper extends BaseMapper<InterpreterOrder> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(InterpreterOrder record);

    InterpreterOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InterpreterOrder record);

    int updateByPrimaryKey(InterpreterOrder record);
    
    @Select("SELECT *,io.phone as telephone,io.address as orderAddress,io.time as orderTime,io.paytype as paytype,io.userid as yongId FROM  t_interpreter as i LEFT JOIN t_interpreter_order as io ON io.interpreterid=i.id WHERE io.id=#{id}")
    Map<String, Object> getOrderDateils(@Param("id")int id);
    
    @Select("SELECT COUNT(1) FROM t_interpreter_order as io where io.interpreterid=#{interpreterid}")
    public int fuCount(@Param("interpreterid")int interpreterid);
    
    @Update("UPDATE t_interpreter_order SET states=#{states} WHERE id=#{id}")
    int updateStatesById(@Param("states")int states,@Param("id")int id);
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.News;

public interface NewsMapper extends BaseMapper<News> {

}

package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Lease;

public interface LeaseMapper extends BaseMapper<Lease> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Lease record);

    Lease selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Lease record);

    int updateByPrimaryKey(Lease record);
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.DynamicLike;

public interface DynamicLikeMapper extends BaseMapper<DynamicLike> {

}

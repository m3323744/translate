package com.medical.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.AuthenticationSchool;

public interface AuthenticationSchoolMapper extends BaseMapper<AuthenticationSchool> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(AuthenticationSchool record);

    AuthenticationSchool selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AuthenticationSchool record);

    int updateByPrimaryKey(AuthenticationSchool record);
    
    @Update("UPDATE t_authentication_school SET authenticationid=#{authenticationid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("authenticationid")int authenticationid,@Param("id")int id);
    
    @Select("SELECT * FROM t_authentication_school WHERE authenticationid=#{authenticationid} and isteacher=#{isteacher}")
    public List<AuthenticationSchool> selectByAuthenticationid(@Param("authenticationid")int authenticationid,@Param("isteacher")int isteacher);
}
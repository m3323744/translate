package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Wanted;

public interface wantedMapper extends BaseMapper<Wanted> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Wanted record);

    Wanted selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Wanted record);

    int updateByPrimaryKey(Wanted record);
    
    @Select("SELECT count(1) FROM t_wanted WHERE time >=#{startTime} AND time<=#{endTime}  and userid=#{userId}")
    int addCountByTime(@Param("startTime")String startTime,@Param("endTime")String endTime,@Param("userId")int userId);
}
package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.School;

public interface SchoolMapper extends BaseMapper<School> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(School record);

    School selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(School record);

    int updateByPrimaryKey(School record);
    
    @Select("SELECT * FROM t_school AS s LEFT JOIN t_language as l ON s.languageid=l.id ${sql} ${order} limit #{pageIndex},#{size}")
    public List<Map<String, Object>> selectTeacherList(@Param("sql")String sql,@Param("order") String order,@Param("pageIndex")int pageIndex,@Param("size")int size);
    
    @Select("SELECT count(1) FROM t_school AS s LEFT JOIN t_language as l ON s.languageid=l.id ${sql}")
    public Integer selectTeacherListCount(@Param("sql")String sql);
    
    @Select("SELECT id,images,`name`,0 as totalprice,`release`,9 as type FROM t_school WHERE userid=#{userId}")
    public List<Map<String, Object>> getByUserId(@Param("userId")int userId);
    
    @Update("UPDATE t_school SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
}
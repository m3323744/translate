package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CertificateThreeType;

public interface CertificateThreeTypeMapper extends BaseMapper<CertificateThreeType> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CertificateThreeType record);

    CertificateThreeType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CertificateThreeType record);

    int updateByPrimaryKey(CertificateThreeType record);
    
    
}
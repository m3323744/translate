package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Teacher;

public interface TeacherMapper extends BaseMapper<Teacher> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Teacher record);

    Teacher selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Teacher record);

    int updateByPrimaryKey(Teacher record);
    
    @Select("SELECT t.*,l.`language` FROM t_teacher as t LEFT JOIN t_user as u ON t.userid=u.id LEFT JOIN t_language as l ON l.id=t.languageid LEFT JOIN t_teacher_authentication as ta ON ta.userid=u.id ${sql} ${order} limit #{pageIndex},#{size}")
    public List<Map<String, Object>> selectTeacherList(@Param("sql")String sql,@Param("order") String order,@Param("pageIndex")int pageIndex,@Param("size")int size);
    
    @Select("SELECT count(1) FROM t_teacher as t LEFT JOIN t_user as u ON t.userid=u.id LEFT JOIN t_language as l ON l.id=t.languageid ${sql}")
    public Integer selectTeacherListCount(@Param("sql")String sql);
    
    @Select("select t.id,t.head,l.`language`,t.`name` from t_teacher t left join t_language l on l.id=t.languageid LEFT JOIN t_teacher_authentication as ta ON ta.userid=t.userid where t.`release`=true and t.states=2 AND ta.excellent=TRUE order by t.id desc limit 0,9 ")
    public List<Map<String,Object>> selectByTop();
    
    @Update("UPDATE t_teacher SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
    
    @Select("SELECT id,images,`name`,price as totalprice,`release`,1 as type FROM t_teacher where userid=#{userId}")
    public List<Map<String, Object>> getByUserId(@Param("userId")int userId);
}
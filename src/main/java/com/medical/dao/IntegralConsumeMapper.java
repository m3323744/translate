package com.medical.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.entity.IntegralConsume;

public interface IntegralConsumeMapper extends BaseMapper<IntegralConsume> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(IntegralConsume record);

    IntegralConsume selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IntegralConsume record);

    int updateByPrimaryKey(IntegralConsume record);
    
    List<IntegralConsume> findIntegralConsumePage(Page<IntegralConsume> page,int userId);
    
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.LanguageGrade;

public interface LanguageGradeMapper extends BaseMapper<LanguageGrade> {

}

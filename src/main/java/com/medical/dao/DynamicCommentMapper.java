package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.DynamicComment;

public interface DynamicCommentMapper extends BaseMapper<DynamicComment> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(DynamicComment record);

    DynamicComment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DynamicComment record);

    int updateByPrimaryKey(DynamicComment record);
    
    @Select("SELECT dc.*,u.nickname,u.portrait FROM t_dynamic_comment as dc LEFT JOIN t_user as u ON dc.userid=u.id WHERE dc.dynamicid=#{dynamicid} order by dc.time")
    public List<Map<String, Object>> getList(@Param("dynamicid")int dynamicid);
}
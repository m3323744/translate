package com.medical.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Collect;

public interface CollectMapper extends BaseMapper<Collect> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Collect record);

    Collect selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Collect record);

    int updateByPrimaryKey(Collect record);
    
    @Select("SELECT * FROM t_collect WHERE userid=#{coll.userid} AND collecttype=#{coll.collecttype} limit #{page},#{size}")
    List<Collect> getList(@Param("coll")Collect coll,@Param("page")int page,@Param("size")int size);
    
    @Select("SELECT count(1) FROM t_collect WHERE userid=#{coll.userid} AND collecttype=#{coll.collecttype}")
    Integer getListCount(@Param("coll")Collect coll);
}
package com.medical.dao;

import com.medical.entity.Authorityfilter;

public interface AuthorityfilterMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Authorityfilter record);

    int insertSelective(Authorityfilter record);

    Authorityfilter selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Authorityfilter record);

    int updateByPrimaryKey(Authorityfilter record);
}
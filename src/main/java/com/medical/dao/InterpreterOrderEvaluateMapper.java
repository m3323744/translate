package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.InterpreterOrderEvaluate;

public interface InterpreterOrderEvaluateMapper extends BaseMapper<InterpreterOrderEvaluate> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(InterpreterOrderEvaluate record);

    InterpreterOrderEvaluate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InterpreterOrderEvaluate record);

    int updateByPrimaryKey(InterpreterOrderEvaluate record);
    
    @Select("SELECT oe.*,u.nickname FROM t_interpreter_order_evaluate as oe LEFT JOIN t_user as u ON oe.userid=u.id WHERE oe.interpreterid=#{orderId}")
    public Map<String, Object> selectByOrderId(@Param("orderId")int orderId);
    
    @Select("SELECT oe.content,oe.score,u.nickname,u.portrait FROM t_interpreter_order_evaluate as oe LEFT JOIN t_user as u ON oe.userid=u.id LEFT JOIN t_interpreter_order as te ON te.id=oe.interpreterid LEFT JOIN t_interpreter as t ON t.id=te.interpreterid WHERE oe.interpreterid=#{interpreterId}")
    public List<Map<String, Object>> selectByTeacherid(@Param("interpreterId")int interpreterId);
    
    @Select("SELECT avg(oe.score) as fen FROM t_interpreter_order_evaluate as oe LEFT JOIN t_interpreter_order as te ON te.id=oe.interpreterid LEFT JOIN t_interpreter as t ON t.id=te.interpreterid WHERE oe.interpreterid=#{interpreterId}")
    public String selectAvgScoreByeacherId(@Param("interpreterId")int interpreterId);
    
    @Select("SELECT ifnull(AVG(io.score),0.0) as score FROM t_interpreter_order_evaluate as io LEFT JOIN t_interpreter_authentication as ia ON ia.id=io.interpreterid WHERE io.interpreterid=#{interpreterid}")
    public Double selectAvgScore(@Param("interpreterid")int interpreterid);
}
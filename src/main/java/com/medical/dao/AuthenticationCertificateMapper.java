package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.AuthenticationCertificate;

public interface AuthenticationCertificateMapper extends BaseMapper<AuthenticationCertificate> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(AuthenticationCertificate record);

    AuthenticationCertificate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AuthenticationCertificate record);

    int updateByPrimaryKey(AuthenticationCertificate record);
    
    @Update("UPDATE t_authentication_certificate SET authenticationid=#{authenticationid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("authenticationid")int authenticationid,@Param("id")int id);
    
    @Select("SELECT ac.*,ctt.twotype as twoName,cte.`name` as threeName FROM t_authentication_certificate as ac LEFT JOIN t_certificate_two_type as ctt ON ctt.id=ac.twoid LEFT JOIN t_certificate_three_type as  cte ON cte.id=ac.threeid WHERE ac.authenticationid=#{authenticationid} AND ac.isteacher=#{isteacher}")
    public List<Map<String, Object>> selectByAuthenticationid(@Param("authenticationid")int authenticationid,@Param("isteacher")int isteacher);
}
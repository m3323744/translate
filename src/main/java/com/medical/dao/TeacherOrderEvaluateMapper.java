package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.TeacherOrderEvaluate;

public interface TeacherOrderEvaluateMapper extends BaseMapper<TeacherOrderEvaluate> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(TeacherOrderEvaluate record);

    TeacherOrderEvaluate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TeacherOrderEvaluate record);

    int updateByPrimaryKey(TeacherOrderEvaluate record);
    
    @Select("SELECT oe.*,u.nickname FROM t_teacher_order_evaluate as oe LEFT JOIN t_user as u ON oe.userid=u.id WHERE orderid=#{orderId}")
    public Map<String, Object> selectByOrderId(@Param("orderId")int orderId);
    
    @Select("SELECT oe.content,oe.score,u.nickname,u.portrait FROM t_teacher_order_evaluate as oe LEFT JOIN t_user as u ON oe.userid=u.id LEFT JOIN t_teacher_order as te ON te.id=oe.orderid LEFT JOIN t_teacher as t ON t.id=te.teacherid WHERE te.teacherid=#{teacherId}")
    public List<Map<String, Object>> selectByTeacherid(@Param("teacherId")int teacherId);
    
    @Select("SELECT avg(oe.score) as fen FROM t_teacher_order_evaluate as oe LEFT JOIN t_teacher_order as te ON te.id=oe.orderid LEFT JOIN t_teacher as t ON t.id=te.teacherid WHERE te.teacherid=#{teacherId}")
    public String selectAvgScoreByeacherId(@Param("teacherId")int teacherId);
}
package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.medical.entity.InvestmentType;

public interface InvestmentTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(InvestmentType record);

    int insertSelective(InvestmentType record);

    InvestmentType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InvestmentType record);

    int updateByPrimaryKey(InvestmentType record);
    
    @Select("select * from t_investment_type")
    List<Map<String,Object>> findTypeList();
    @Select("select * from t_investment where typeid=#{typeid}")
    Map<String,Object> findContentByTypeid(@Param("typeid")Integer typeId);
}
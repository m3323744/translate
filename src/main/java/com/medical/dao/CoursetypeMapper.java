package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Coursetype;

public interface CoursetypeMapper extends BaseMapper<Coursetype> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Coursetype record);

    Coursetype selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Coursetype record);

    int updateByPrimaryKey(Coursetype record);
}
package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.ConventionApply;

public interface ConventionApplyMapper extends BaseMapper<ConventionApply> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(ConventionApply record);

    ConventionApply selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ConventionApply record);

    int updateByPrimaryKey(ConventionApply record);
}
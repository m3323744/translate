package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CommonProblem;

public interface CommonProblemMapper extends BaseMapper<CommonProblem> {

}

package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.VisaType;

public interface VisaTypeMapper extends BaseMapper<VisaType> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(VisaType record);

    VisaType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(VisaType record);

    int updateByPrimaryKey(VisaType record);
}
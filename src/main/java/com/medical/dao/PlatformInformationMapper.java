package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.PlatformInformation;

public interface PlatformInformationMapper extends BaseMapper<PlatformInformation> {

}

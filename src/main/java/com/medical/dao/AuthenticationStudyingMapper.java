package com.medical.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.AuthenticationStudying;

public interface AuthenticationStudyingMapper extends BaseMapper<AuthenticationStudying> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(AuthenticationStudying record);

    AuthenticationStudying selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AuthenticationStudying record);

    int updateByPrimaryKey(AuthenticationStudying record);
    
    @Update("UPDATE t_authentication_studying SET authenticationid=#{authenticationid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("authenticationid")int authenticationid,@Param("id")int id);
    
    @Select("SELECT * FROM t_authentication_studying WHERE authenticationid=#{authenticationid} and isteacher=#{isteacher}")
    public List<AuthenticationStudying> selectByAuthenticationid(@Param("authenticationid")int authenticationid,@Param("isteacher")int isteacher);
    
    @Select("SELECT s.* FROM t_authentication_studying as s LEFT JOIN t_teacher_authentication as ta ON s.authenticationid=ta.id WHERE s.isteacher=#{type} AND ta.userid=#{userId}")
    public List<AuthenticationStudying> selectByUserId(@Param("type")int type,@Param("userId")int userId);
}
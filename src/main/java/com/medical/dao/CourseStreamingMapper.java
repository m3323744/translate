package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.CourseStreaming;

public interface CourseStreamingMapper extends BaseMapper<CourseStreaming> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(CourseStreaming record);

    CourseStreaming selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CourseStreaming record);

    int updateByPrimaryKey(CourseStreaming record);
    
    @Select("SELECT * FROM t_course_streaming WHERE 1=1 and streamingtime>=now() ${sql} ${order} limit #{pageIndex},#{size}")
    public List<Map<String, Object>> getList(@Param("sql")String sql,@Param("order") String order,@Param("pageIndex")int pageIndex,@Param("size")int size);
    
    @Select("SELECT count(1) FROM t_course_streaming WHERE 1=1 ${sql}")
    public Integer getListCount(@Param("sql")String sql);
    
    @Update("UPDATE t_course_streaming SET `release`=#{release} WHERE id=#{id}")
    public Integer updateRelease(@Param("release")Boolean release,@Param("id")int id);
}
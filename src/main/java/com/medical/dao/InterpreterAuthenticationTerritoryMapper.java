package com.medical.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.InterpreterAuthenticationTerritory;

public interface InterpreterAuthenticationTerritoryMapper extends BaseMapper<InterpreterAuthenticationTerritory>{
    int deleteByPrimaryKey(Integer id);

    int insertSelective(InterpreterAuthenticationTerritory record);

    InterpreterAuthenticationTerritory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InterpreterAuthenticationTerritory record);

    int updateByPrimaryKey(InterpreterAuthenticationTerritory record);
    
    @Update("UPDATE t_interpreter_authentication_territory SET interpreterid=#{authenticationid} WHERE id=#{id}")
    public int updateAuthenticationid(@Param("authenticationid")int authenticationid,@Param("id")int id);
    
    @Select("SELECT * FROM t_interpreter_authentication_territory WHERE interpreterid=#{authenticationid}")
    public List<InterpreterAuthenticationTerritory> selectByAuthenticationid(@Param("authenticationid")int authenticationid);
    
    @Select("SELECT iat.* FROM t_interpreter_authentication_territory as iat LEFT JOIN t_interpreter_authentication as ia ON ia.id=iat.interpreterid WHERE ia.userid=#{userId}")
    public List<InterpreterAuthenticationTerritory> getListByUserid(@Param("userId")int userId);
}
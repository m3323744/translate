package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Conference;
import com.medical.entity.ResultBean;

public interface ConferenceService {
	public ResultBean<Integer> addConference(Conference cf);
	public ResultBean<List<Conference>> getList(String keyWord,int pageIndex,int score,int size);
	public ResultBean<Map<String, Object>> selectById(int id,int userid);
	public ResultBean<Integer> delConference(int id);
	public ResultBean<Integer> getListCount(String keyWord);
}

package com.medical.service;

import java.util.List;

import com.medical.entity.Coursetype;
import com.medical.entity.ResultBean;

public interface CoursetypeService {
	public ResultBean<List<Coursetype>> getList();
}

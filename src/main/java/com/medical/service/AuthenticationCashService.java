package com.medical.service;

import com.medical.entity.AuthenticationCash;
import com.medical.entity.ResultBean;

public interface AuthenticationCashService {
	public ResultBean<Integer> addAuthenticationCash(AuthenticationCash cash);
	public ResultBean<Integer> refund(int userid);
}

package com.medical.service;

import java.util.List;


import com.medical.entity.AuthenticationStudying;
import com.medical.entity.ResultBean;

public interface AuthenticationStudyingService {
	public ResultBean<Integer> addStudying(AuthenticationStudying ats);
	public ResultBean<AuthenticationStudying> getDateils(int id);
	public ResultBean<List<AuthenticationStudying>> getList(int authenticationid,int isteacher);
	public ResultBean<List<AuthenticationStudying>> selectByIds(String ids);
	public ResultBean<List<AuthenticationStudying>> selectByUserId(int type,int userId);
	public ResultBean<Integer> delStudying(int id);
	public ResultBean<Integer> updateAuthenticationStudying(AuthenticationStudying ats);
}

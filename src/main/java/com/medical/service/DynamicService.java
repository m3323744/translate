package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Dynamic;
import com.medical.entity.DynamicComment;
import com.medical.entity.ResultBean;

public interface DynamicService {
	public ResultBean<List<Map<String, Object>>> getList(int pageIndex,int size,int userId);
	public ResultBean<Integer> getListByCount();
	public ResultBean<Map<String, Object>> getdetailById(int id);
	public ResultBean<Integer> delDynamic(int id);
	public ResultBean<Dynamic> addDynamic(Dynamic dy);
	public ResultBean<Integer> likeDynamic(int id,int userid);
	public ResultBean<Integer> evaluateDynamic(DynamicComment dc);
}

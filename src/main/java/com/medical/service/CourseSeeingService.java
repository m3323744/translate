package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.CourseSeeing;
import com.medical.entity.ResultBean;

public interface CourseSeeingService {
	public ResultBean<Integer> addCourseSeeing(CourseSeeing cs,String simple);
	public ResultBean<List<Map<String, Object>>> selectByTeacherId(int userId);
	public ResultBean<List<Map<String, Object>>> selectBySchoolId(int schoolId,int pageIndex,int size);
	public ResultBean<Integer> selectBySchoolIdCount(int schoolId);
	public ResultBean<Map<String, Object>> selectById(int id,int userid);
	public ResultBean<List<Map<String, Object>>> getList(CourseSeeing cs,String hours,String prices,Integer days,int score,String keyWord,int pageIndex,int size);
	public ResultBean<List<Map<String, Object>>> getSpellingList(int pageIndex,int size,String keyWord);
	public ResultBean<Integer> getSpellingListCount();
	public ResultBean<Integer> getListCount(CourseSeeing cs,String hours,String prices,Integer days,String keyWord);
}

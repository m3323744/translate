package com.medical.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.ModelAttribute;

import com.medical.entity.AuthenticationCertificate;
import com.medical.entity.ResultBean;

public interface AuthenticationCertificateService {
	public ResultBean<Integer> addAuthenticationCertificate(AuthenticationCertificate ace);
	public ResultBean<Map<String, Object>> getDateils(int id);
	public ResultBean<List<AuthenticationCertificate>> getList(int authenticationid,int isteacher);
	public ResultBean<List<Map<String, Object>>> selectByIds(String ids);
	public ResultBean<Integer> delCertificate(int id);
	public ResultBean<Integer> updateAuthenticationCertificate(@ModelAttribute AuthenticationCertificate ace);
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.CourseSimple;
import com.medical.entity.ResultBean;

public interface CourseSimpleService {
	public ResultBean<CourseSimple> addCourseSimple(CourseSimple cs);
	public ResultBean<List<CourseSimple>> selectByIds(String ids);
	public ResultBean<Map<String, Object>> selectById(int id);
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Collect;
import com.medical.entity.ResultBean;

public interface CollectService {
	public ResultBean<Integer> addCollect(Collect coll);
	public ResultBean<Integer> delCollect(Collect coll);
	public ResultBean<List<Map<String, Object>>> getList(Collect coll,int pageIndex,int size);
	public ResultBean<Integer> getListCount(Collect coll);
}

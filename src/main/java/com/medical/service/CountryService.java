package com.medical.service;

import java.util.List;

import com.medical.entity.Country;
import com.medical.entity.ResultBean;

public interface CountryService {
	public ResultBean<List<Country>> getList();
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Job;
import com.medical.entity.ResultBean;

public interface JobService {
	public ResultBean<Integer> addJob(Job job);
	public ResultBean<Map<String, Object>> getList(int pageIndex,String keyWord,int size);
	public ResultBean<Job> selectById(int id);
	public ResultBean<List<Job>> selectByUserId(int userId,int pageIndex,int size);
	public ResultBean<Integer> selectByUserIdCount(int userId);
	/**
	 * 首页查询推荐职位
	 * @param pageIndex
	 * @param size
	 * @return
	 */
	public ResultBean<List<Map<String,Object>>> findHotJobs();
	
	public ResultBean<Integer> updateJob(Job job);
	public ResultBean<Integer> getListCount(String keyWord);
	
	public  int addCountByTime(int userId);
}

package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthentication;

public interface TeacherAuthenticationService {
	public ResultBean<Integer> addTeacherAuthentication(TeacherAuthentication ta,String lanuage,String certificate,String school,String studying,String zengone,String zengtwo,String zengthree);
	public ResultBean<Map<String, Object>> TeacherAuthenticationDateils(int userId);
	public ResultBean<Integer> updateTeacherAuthentication(TeacherAuthentication ta,String lanuage,String certificate,String school,String studying,String zengoneurl,String zengtwourl,String zengthreeurl);
}

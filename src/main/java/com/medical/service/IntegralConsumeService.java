package com.medical.service;

import java.util.List;

import com.medical.entity.IntegralConsume;

public interface IntegralConsumeService {
	
	public List<IntegralConsume> findByPage(int page,int userId);

	public Integer  findByPageCount(int userId);
}

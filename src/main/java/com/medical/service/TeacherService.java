package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.Teacher;

public interface TeacherService {
	public ResultBean<Integer> addTeacher(Teacher tea);
	public ResultBean<List<Map<String, Object>>> selectByTeacher(Teacher tea,String ages,String teachings,Double minprice,Double maxprice,int score,String nationality,Integer pageIndex,Integer size,Double longitude,Double latitude,String keyword);
	public ResultBean<Map<String, Object>> selectByTeacherId(int id,int userid);
	/**
	 * 首页热门老师
	 * @return
	 */
	public ResultBean<List<Map<String,Object>>> findHotTeachers();
	public ResultBean<Integer> selectByTeacherCount(Teacher tea,String ages,String teachings,Double minprice,Double maxprice,String nationality,String keyword);
}

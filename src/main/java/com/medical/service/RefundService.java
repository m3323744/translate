package com.medical.service;

import com.medical.entity.Refund;

public interface RefundService {
	public int addRefund(Refund re);
	public int updateStates(Refund re);
	public Refund getByOrderId(Refund re);
}

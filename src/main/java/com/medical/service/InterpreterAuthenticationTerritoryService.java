package com.medical.service;

import java.util.List;

import com.medical.entity.InterpreterAuthenticationTerritory;
import com.medical.entity.ResultBean;

public interface InterpreterAuthenticationTerritoryService {
	public ResultBean<Integer> addInterpreterAuthenticationTerritory(InterpreterAuthenticationTerritory iat);
	public ResultBean<InterpreterAuthenticationTerritory> getDateils(int id);
	public ResultBean<List<InterpreterAuthenticationTerritory>> getList(int interpreterid);
	public ResultBean<List<InterpreterAuthenticationTerritory>> selectByIds(String ids);
	public ResultBean<List<InterpreterAuthenticationTerritory>> getListByUserid(int userId);
	public ResultBean<Integer> delTerritory(int id);
	public ResultBean<Integer> updateInterpreterAuthenticationTerritory(InterpreterAuthenticationTerritory iat);
}

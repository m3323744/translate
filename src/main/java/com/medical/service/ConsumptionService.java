package com.medical.service;

import java.util.List;

import com.medical.entity.Consumption;

public interface ConsumptionService {
	
	public List<Consumption> findConsumptions(int userId,String minTime,String maxTime,int pageIndex);

	public Integer findConsumptionsCount(int userId,String minTime,String maxTime);

}

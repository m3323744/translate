package com.medical.service;

import java.util.List;

import com.medical.entity.Language;
import com.medical.entity.ResultBean;

public interface LanguageService {
	public ResultBean<List<Language>> getList();
}

package com.medical.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.ComplaintMapper;
import com.medical.entity.Complaint;
import com.medical.entity.ResultBean;
import com.medical.service.ComplaintService;

@Service
@Transactional
public class ComplaintServiceImpl implements ComplaintService {
	@Resource
	private ComplaintMapper dao;
	
	@Override
	public ResultBean<Integer> addComplaint(Complaint com) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(com);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

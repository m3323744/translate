package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CoursetypeMapper;
import com.medical.entity.Coursetype;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.CoursetypeService;

@Service
@Transactional
public class CoursetypeServiceImpl implements CoursetypeService {
	@Resource
	private CoursetypeMapper dao;
	
	@Override
	public ResultBean<List<Coursetype>> getList() {
		// TODO Auto-generated method stub
		try {
			List<Coursetype> list=dao.selectList(new EntityWrapper<>());
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

}

package com.medical.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.OpinionMapper;
import com.medical.entity.Opinion;
import com.medical.entity.ResultBean;
import com.medical.service.OpinionService;

@Service
@Transactional
public class OpinionServiceImpl implements OpinionService {
	@Resource
	private OpinionMapper dao;
	
	@Override
	public ResultBean<Integer> addOpinion(Opinion op) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(op);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

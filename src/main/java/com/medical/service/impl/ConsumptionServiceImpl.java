package com.medical.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.medical.dao.ConsumptionMapper;
import com.medical.entity.Consumption;
import com.medical.service.ConsumptionService;

@Service
public class ConsumptionServiceImpl implements ConsumptionService {

	@Autowired
	private ConsumptionMapper consumptionDao;
	@Override
	public List<Consumption> findConsumptions(int userId,String minTime,String maxTime,int pageIndex) {
		// TODO Auto-generated method stub
		String time="";
		if(minTime!=null&&!minTime.equals("")) {
			time+=" and time>='"+minTime+"'";
		}
		if(maxTime!=null&&!maxTime.equals("")) {
			time+=" and time<='"+maxTime+"'";
		}
		return consumptionDao.findAllByUserId(userId,time,(pageIndex-1)*10);
	}
	
	@Override
	public Integer findConsumptionsCount(int userId, String minTime, String maxTime) {
		// TODO Auto-generated method stub
		String time="";
		if(minTime!=null&&!minTime.equals("")) {
			time+=" and time>='"+minTime+"'";
		}
		if(maxTime!=null&&!maxTime.equals("")) {
			time+=" and time<='"+maxTime+"'";
		}
		
		return consumptionDao.findAllByUserIdCount(userId, time);
	}


}

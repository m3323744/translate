package com.medical.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.medical.dao.CourseOrderMapper;
import com.medical.entity.CourseOrder;
import com.medical.service.OrderService;

@Service
public class OrderCourseStreamingServiceImpl implements OrderService{
	
	@Resource
	private CourseOrderMapper cDao;
	
	
	public OrderCourseStreamingServiceImpl(CourseOrderMapper cDao) {
		this.cDao=cDao;
	}
	
	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		return cDao.getStreamingOrderDateils(id);
	}

	@Override
	public int updateState(int orderId, int states,int paytype) {
		// TODO Auto-generated method stub
		CourseOrder courOrder=cDao.selectById(orderId);
		courOrder.setId(orderId);
		courOrder.setStates(states);
		courOrder.setCoursetype(2);
		if(paytype!=0) {
			courOrder.setPaytype(paytype);
		}
		return cDao.updateAllColumnById(courOrder);
	}

	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		CourseOrder courOrder=cDao.selectById(orderId);
		courOrder.setId(orderId);
		courOrder.setStates(states);
		courOrder.setCoursetype(2);
		return cDao.updateAllColumnById(courOrder);
	}
}

package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.SlideshowMapper;
import com.medical.entity.ResultBean;
import com.medical.entity.Slideshow;
import com.medical.service.SlideshowService;

@Service
@Transactional
public class SlideshowServiceImpl implements SlideshowService {
	@Resource
	private SlideshowMapper dao;
	
	@Override
	public ResultBean<List<Slideshow>> getList(int type) {
		// TODO Auto-generated method stub
		List<Slideshow> list=dao.selectPage(new Page<>(0, 4), new EntityWrapper<Slideshow>().eq("isfan", type));
		return new ResultBean<List<Slideshow>>(list);
	}

}

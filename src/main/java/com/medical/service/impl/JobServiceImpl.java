package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.JobMapper;
import com.medical.entity.Job;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.JobService;
import com.medical.util.DataUtil;

@Service
@Transactional
public class JobServiceImpl implements JobService {
	@Resource
	private JobMapper dao;
	
	@Override
	public ResultBean<Integer> addJob(Job job) {
		// TODO Auto-generated method stub
		int num=dao.addCountByTime(DataUtil.getDate()+" 00:00:00", DataUtil.getDate()+" 23:59:59", job.getUserid());
		if(num>=2) {
			return new ResultBean<>(new MyException("今日发布不能超过2条"));
		}
		job.setTime(new Date());
		job.setStates(1);
		try {
			int count=dao.insert(job);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getList(int pageIndex,String keyWord,int size) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			List<Job> list=dao.selectPage(new Page<Job>(pageIndex, size),new EntityWrapper<Job>().like("title", keyWord).or().like("address", keyWord).and().eq("states", 2));
			map.put("jobList", list);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Job> selectById(int id) {
		// TODO Auto-generated method stub
		try {
			Job job=dao.selectById(id);
			return new ResultBean<>(job);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Job>> selectByUserId(int userId,int pageIndex,int size) {
		// TODO Auto-generated method stub
		try {
			List<Job> list=dao.selectPage(new Page<Job>(pageIndex, size),new EntityWrapper<Job>().eq("userid", userId));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> findHotJobs() {
		// TODO Auto-generated method stub
		List<Map<String,Object>> list=dao.findHotJobs();
		return new ResultBean<>(list);
	}

	@Override
	public ResultBean<Integer> updateJob(Job job) {
		// TODO Auto-generated method stub
		int count=dao.updateAllColumnById(job);
		return new ResultBean<Integer>(count);
	}

	@Override
	public ResultBean<Integer> getListCount(String keyWord) {
		// TODO Auto-generated method stub
		int count=dao.selectCount(new EntityWrapper<Job>().like("title", keyWord).or().like("address", keyWord).and().eq("states", 2));
		return new ResultBean<Integer>(count);
	}

	@Override
	public int addCountByTime(int userId) {
		// TODO Auto-generated method stub
		
		return 0;
	}

	@Override
	public ResultBean<Integer> selectByUserIdCount(int userId) {
		// TODO Auto-generated method stub
		try {
			List<Job> list=dao.selectList(new EntityWrapper<Job>().eq("userid", userId));
			return new ResultBean<>(list.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

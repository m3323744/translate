package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.InterpreterOrderEvaluateMapper;
import com.medical.entity.InterpreterOrderEvaluate;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterOrderEvaluateService;

@Service
@Transactional
public class InterpreterOrderEvaluateServiceImpl implements InterpreterOrderEvaluateService {
	@Resource
	private InterpreterOrderEvaluateMapper dao;
	
	@Override
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> te=dao.selectByOrderId(orderId);
			return new ResultBean<>(te);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> addEvaluate(InterpreterOrderEvaluate eva) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(eva);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectByInterpreterId(int interpreterId) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			List<Map<String, Object>> list=dao.selectByTeacherid(interpreterId);
			String score=dao.selectAvgScoreByeacherId(interpreterId);
			map.put("evaluate", list);
			if(score==null) {
				map.put("score", 0);
			}else {
				map.put("score", score);
			}
			
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

package com.medical.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.medical.dao.InterpreterOrderMapper;
import com.medical.entity.InterpreterOrder;
import com.medical.service.OrderService;

@Service
public class OrderInterpreterServiceImpl implements OrderService {

	private InterpreterOrderMapper iDao;
	
	public OrderInterpreterServiceImpl(InterpreterOrderMapper iDao) {
		this.iDao=iDao;
	}

	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		return iDao.getOrderDateils(id);
	}

	@Override
	public int updateState(int orderId, int states,int paytype) {
		// TODO Auto-generated method stub
		InterpreterOrder intOrder=iDao.selectById(orderId);
		intOrder.setId(orderId);
		intOrder.setStates(states);
		if(paytype!=0) {
			intOrder.setPaytype(paytype);
		}
		return iDao.updateAllColumnById(intOrder);
	}

	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		InterpreterOrder intOrder=iDao.selectById(orderId);
		intOrder.setId(orderId);
		intOrder.setStates(states);
		return iDao.updateAllColumnById(intOrder);
	}
	

}

package com.medical.service.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CourseEvaluateMapper;
import com.medical.dao.CourseOrderMapper;
import com.medical.dao.InterpreterOrderEvaluateMapper;
import com.medical.dao.InterpreterOrderMapper;
import com.medical.dao.TeacherOrderEvaluateMapper;
import com.medical.dao.TeacherOrderMapper;
import com.medical.dao.VisaOrderEvaluateMapper;
import com.medical.dao.VisaOrderMapper;
import com.medical.entity.CourseEvaluate;
import com.medical.entity.CourseOrder;
import com.medical.entity.InterpreterOrder;
import com.medical.entity.InterpreterOrderEvaluate;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherOrder;
import com.medical.entity.TeacherOrderEvaluate;
import com.medical.entity.VisaOrder;
import com.medical.entity.VisaOrderEvaluate;
import com.medical.service.TeacherOrderEvaluateService;
import com.medical.util.DataUtil;

@Service
@Transactional
public class TeacherOrderEvaluateServiceImpl implements TeacherOrderEvaluateService {
	@Resource
	private TeacherOrderEvaluateMapper dao;
	
	@Resource
	private InterpreterOrderEvaluateMapper iDao;
	
	@Resource
	private CourseEvaluateMapper cDao;
	
	@Resource
	private VisaOrderEvaluateMapper vDao;
	
	@Resource
	private TeacherOrderMapper toDao;
	
	@Resource
	private InterpreterOrderMapper ioDao;
	
	@Resource
	private CourseOrderMapper coDao;
	
	@Resource
	private VisaOrderMapper voDao;
	
	@Override
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId,int type) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> te=new HashMap<>();
			if(type==1) {
				te=dao.selectByOrderId(orderId);
			}else if(type==2) {
				te=iDao.selectByOrderId(orderId);
			}else if(type==3) {
				te=cDao.selectByOrderId(orderId);
			}else if(type==4) {
				te=vDao.selectByOrderId(orderId);
			}
			return new ResultBean<>(te);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> addEvaluate(Float score,int orderid,int userid,String content,int courseType,int type,int courseid) {
		// TODO Auto-generated method stub
		try {
			int count=0;
			if(type==1) {
				TeacherOrderEvaluate to=new TeacherOrderEvaluate();
				to.setContent(content);
				to.setOrderid(orderid);
				to.setScore(score);
				to.setTime(new Date());
				to.setUserid(userid);
				toDao.updateStatesById(9, orderid);
				count=dao.insert(to);
			}else if(type==2) {
				InterpreterOrderEvaluate oe=new InterpreterOrderEvaluate();
				InterpreterOrder order=ioDao.selectById(orderid);
				oe.setContent(content);
				oe.setInterpreterid(order.getInterpreterid());
				oe.setScore(score);
				oe.setTime(new Date());
				oe.setUserid(userid);
				ioDao.updateStatesById(9, orderid);
				count=iDao.insert(oe);
			}else if(type==3) {
				CourseEvaluate ce=new CourseEvaluate();
				ce.setContent(content);
				ce.setCourseid(courseid);
				ce.setCourseorderid(orderid);
				ce.setCoursetype(courseType);
				ce.setScore(score);
				ce.setTime(new Date());
				ce.setUserid(userid);
				coDao.updateStatesById(9, orderid);
				count=cDao.insert(ce);
			}else if(type==4) {
				VisaOrderEvaluate vo=new VisaOrderEvaluate();
				vo.setCotent(content);
				vo.setOrderid(orderid);
				vo.setScore(Double.parseDouble(score.toString()));
				vo.setTime(new Date());
				vo.setUserid(userid);
				voDao.updateStatesById(9, orderid);
				count=vDao.insert(vo);
			}
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectByTeacherId(int teacherId) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			List<Map<String, Object>> list=dao.selectByTeacherid(teacherId);
			String score=dao.selectAvgScoreByeacherId(teacherId);
			map.put("evaluate", list);
			map.put("score", score);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public void updateOrderStates() {
		// TODO Auto-generated method stub
		try {
			List<InterpreterOrder> list=ioDao.selectList(new EntityWrapper<InterpreterOrder>().eq("states", 1));
			for(InterpreterOrder io:list) {
				int num=DataUtil.charTime(io.getTime(), new Date());
				if(num>=60) {
					io.setStates(6);
					ioDao.updateAllColumnById(io);
				}
			}
			List<TeacherOrder> toList=toDao.selectList(new EntityWrapper<TeacherOrder>().eq("states", 1));
			for(TeacherOrder to:toList) {
				int num=DataUtil.charTime(to.getTime(), new Date());
				if(num>=60) {
					to.setStates(6);
					toDao.updateAllColumnById(to);
				}
			}
			List<CourseOrder> coList=coDao.selectList(new EntityWrapper<CourseOrder>().eq("states", 1));
			for(CourseOrder co:coList) {
				int num=DataUtil.charTime(co.getTime(), new Date());
				if(num>=60) {
					co.setStates(6);
					coDao.updateAllColumnById(co);
				}
			}
			List<VisaOrder> voList=voDao.selectList(new EntityWrapper<VisaOrder>().eq("paystates", 1));
			for(VisaOrder vo:voList) {
				int num=DataUtil.charTime(vo.getTime(), new Date());
				if(num>=60) {
					vo.setPaystates(6);
					voDao.updateAllColumnById(vo);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}

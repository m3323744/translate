package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationLanguageMapper;
import com.medical.entity.AuthenticationLanguage;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationLanuageService;

@Service
@Transactional
public class AuthenticationLanuageServiceImpl implements AuthenticationLanuageService {

	@Resource
	private AuthenticationLanguageMapper dao;
	@Override
	public ResultBean<Integer> addLanuage(AuthenticationLanguage acl) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insertAllColumn(acl);
			if(count>0) {
				return new ResultBean<>(acl.getId());
			}else {
				return new ResultBean<>(new MyException("插入失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	@Override
	public ResultBean<AuthenticationLanguage> getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			AuthenticationLanguage ac=dao.selectById(id);
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	@Override
	public ResultBean<List<AuthenticationLanguage>> getList(int authenticationid, int isteacher) {
		// TODO Auto-generated method stub
		try {
			List<AuthenticationLanguage> ac=dao.selectList(new EntityWrapper<AuthenticationLanguage>().eq("authenticationid", authenticationid).and().eq("isteacher", isteacher));
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	@Override
	public ResultBean<List<Map<String, Object>>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				String[] data=ids.split(",");
				for(String da:data) {
					int id=Integer.parseInt(da);
					Map<String, Object> cs=dao.getById(id);
					list.add(cs);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	@Override
	public ResultBean<Integer> delLanguage(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.deleteById(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	@Override
	public ResultBean<Integer> updateAuthenticationLanuage(AuthenticationLanguage acl) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(acl);
			if(count>0) {
				return new ResultBean<>(acl.getId());
			}else {
				return new ResultBean<>(new MyException("更新失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

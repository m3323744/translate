package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.ConferenceApplyMapper;
import com.medical.dao.ConferenceMapper;
import com.medical.dao.ConsumptionMapper;
import com.medical.dao.ConventionApplyMapper;
import com.medical.dao.ConventionMapper;
import com.medical.dao.CourseOrderMapper;
import com.medical.dao.InterpreterOrderMapper;
import com.medical.dao.TeacherOrderMapper;
import com.medical.dao.UserMapper;
import com.medical.dao.VisaOrderMapper;
import com.medical.entity.Conference;
import com.medical.entity.Consumption;
import com.medical.entity.Convention;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherOrder;
import com.medical.entity.User;
import com.medical.service.OrderService;
import com.medical.service.TeacherOrderService;



@Service
@Transactional
public class OrderTeacherServiceImpl implements TeacherOrderService,OrderService {
	@Resource
	private TeacherOrderMapper dao;
	
	@Resource
	private InterpreterOrderMapper iDao;
	
	@Resource
	private CourseOrderMapper cDao;
	
	@Resource
	private VisaOrderMapper vDao;
	
	@Resource
	private ConferenceMapper cfDao;
	
	@Resource
	private ConferenceApplyMapper cfaDao;
	
	@Resource
	private ConventionMapper ctDao;
	
	@Resource
	private ConventionApplyMapper ctaDao;
	
	private OrderService orderService;
	
	@Resource
	private UserMapper uDao;
	
	@Resource
	private ConsumptionMapper cnDao;
	
	@Override
	public ResultBean<List<Map<String, Object>>> getOrderList(int merchantid, int states) {
		// TODO Auto-generated method stub
		try {
			
			String state="";
			if(states==1) {
				state="1";
			}else if(states==2) {
				state="2,3";
			}else if(states==3) {
				state="4";
			}else if(states==4){
				state="7,8,9,11";
			}else {
				state="5,6";
			}
			List<Map<String, Object>> list=dao.getOrderList(merchantid, state);
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}
	@Override
	public ResultBean<Map<String, Object>> getByOrderId(int typeId, int orderId) {
		// TODO Auto-generated method stub
		Map<String, Object> map=null;
		try {
			map=findOrderById(typeId,orderId);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}
	/**
	 * 按照id查找订单
	 * @param typeId
	 * @param orderId
	 * @return
	 */
	private  Map<String, Object> findOrderById(int typeId, int orderId){
		Map<String, Object> map=new HashMap<>();
		try {
			orderService=orderService(typeId);
			map=orderService.getOrderDateils(orderId);
			/*int userid=Integer.parseInt(map.get("userid").toString());
			User user=uDao.selectById(userid);
			map.put("user", user);*/
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return map;
	}
	
	@Override
	public ResultBean<List<Map<String, Object>>> findOrdersForUsers(int userid,int states){
		try {
			String state="";
			if(states==1) {
				state="2";
			}else if(states==2) {
				state="1";
			}else if(states==3) {
				state="3,10";
			}else  if(states==4) {
				state="7";
			}else if(states==5){
				state="9,12";
			}else if(states==6){
				state="4,5,6,8";
			}
			List<Map<String, Object>> orderList=dao.findOrderListForUser(userid, state);
			return new ResultBean<>(orderList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}
	
	@Override
	public ResultBean<Integer> findOrderForUsersCount(int userid,int states){
		try {
			String state="";
			if(states==1) {
				state="2";
			}else if(states==2) {
				state="1";
			}else if(states==3) {
				state="3,10";
			}else  if(states==4) {
				state="7";
			}else if(states==5){
				state="9";
			}else if(states==6){
				state="4,5,6,8";
			}
			List<Map<String, Object>> orderList=dao.findOrderListForUser(userid, state);
			return new ResultBean<>(orderList.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

	@Override
	@Transactional
	public ResultBean<Map<String, Object>> updateOrderState(int type, int orderId, int states,int paytype) {
		// TODO Auto-generated method stub
		Map<String, Object> map=new HashMap<>();
		try {
			//检查状态
			/*if(!checkState(type,orderId,states)) {
				map.put("message", "核对订单状态");
				return  new ResultBean<>(map);	
			}*/
			orderService=orderService(type);
			if(paytype!=0) {
				orderService.updateState(orderId,states,paytype);
			}else {
				orderService.updateState(orderId,states,0);
			}
			try {
				orderService=orderService(type);
				map=orderService.getOrderDateils(orderId);
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
		}catch (Exception e) {
				// TODO: handle exception
			e.printStackTrace();
			map.put("message", " 修改失败");
			return  new ResultBean<>(map);
		}
		return new ResultBean<>(map);
	}
	
	/**
	 * 验证状态
	 * @param orderId
	 * @param states
	 * @return
	 */
	public boolean checkState(int typeId,int orderId, int states) {
		 Map<String, Object> orderMap= findOrderById(typeId,orderId);
		 //判断是否可以取消订单
		 if(states==5&&3==Integer.parseInt(orderMap.get("states").toString())){
			 return false;
		 }
		 //判断是否可以确认收款
		 else if(states==6&&2!=Integer.parseInt(orderMap.get("states").toString())) {
			 return false;
		 }
		return true;
	}
	
	/**
	 * 获取service
	 * @param typeId 订单类型
	 * @return
	 */
	private OrderService orderService( int typeId){
		if(typeId==1) {
			orderService=new OrderTeacherServiceImpl(dao);
		}else if(typeId==2) {
			orderService=new OrderInterpreterServiceImpl(iDao);
		}else if(typeId==3) {
			orderService=new OrderCourseStreamingServiceImpl(cDao);
		}else if(typeId==4) {
			orderService=new OrderOfflineServiceImpl(cDao);
		}else if(typeId==5) {
			orderService=new OrderSeeingServiceImpl(cDao);
		}else if(typeId==6) {
			orderService=new OrderVisaServiceImpl(vDao);
		}else if(typeId==7) {
			orderService=new OrderConferenceServiceImpl(cfDao, cfaDao);
		}else if(typeId==8) {
			orderService =new OrderConventionServiceImpl(ctDao,ctaDao);
		}else if(typeId==9) {
			orderService =new OrderSeeingSimpleServiceImpl(cDao);
		}
		return orderService;
	}
	
	/**
	 * 查询详情
	 */
	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		return dao.getOrderDateils(id);
	}
	/**
	 * 修改状态
	 */
	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		TeacherOrder to=dao.selectById(orderId);
		to.setStates(states);
		return dao.updateAllColumnById(to);
	}
	
	public OrderTeacherServiceImpl(TeacherOrderMapper dao) {
		this.dao=dao;
	}
	@Override
	public ResultBean<TeacherOrder> addOrder(TeacherOrder to) {
		// TODO Auto-generated method stub
		try {
			to.setStates(1);
			to.setTime(new Date());
			int count=dao.insert(to);
			if(count>0) {
				return new ResultBean<>(to);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	@Override
	public int updateState(int orderId, int states, int paytype) {
		// TODO Auto-generated method stub
		TeacherOrder to=dao.selectById(orderId);
		to.setStates(states);
		if(paytype!=0) {
			to.setPaytype(paytype);
		}
		return dao.updateAllColumnById(to);
	}
	@Override
	public ResultBean<Integer> yuePay(int type, int orderId, int states,int userId,Double money,int merchantid,Double money1) {
		// TODO Auto-generated method stub
		User user=uDao.selectById(userId);
		if(user.getBalance()>=money) {
			Double qian=user.getBalance()-money;
			user.setBalance(qian);
			int count=uDao.updateAllColumnById(user);
			Consumption cm=new Consumption();
			cm.setContent("余额付款");
			cm.setIspay(false);
			cm.setMoney(money);
			cm.setTime(new Date());
			cm.setUserid(userId);
			cnDao.insert(cm);
			if(count>0) {
				Consumption cmn=new Consumption();
				cmn.setContent("余额收款");
				cmn.setIspay(false);
				cmn.setMoney(money1);
				cmn.setTime(new Date());
				cmn.setUserid(merchantid);
				cnDao.insert(cmn);
				updateOrderState(type, orderId, states, 3);
				return new ResultBean<>(count);
			}else {
				return new ResultBean<>(new MyException("提现失败"));
			}
		}else {
			return new ResultBean<>(new MyException("余额不足"));
		}
	}
	@Override
	public ResultBean<Map<String, Object>> getOrderListCount(int merchantid, int states,int pageIndex,int size) {
		// TODO Auto-generated method stub
try {
			Map<String, Object> map=new HashMap<>();
			String state="";
			if(states==1) {
				state="1";
			}else if(states==2) {
				state="2,3";
			}else if(states==3) {
				state="4";
			}else if(states==4){
				state="7,8,9";
			}else {
				state="5,6";
			}
			List<Map<String, Object>> list=dao.getOrderListPage(merchantid, state,(pageIndex-1)*size,size);
			map.put("orderList", list);
			int count=dao.getOrderList(merchantid, state).size();
			map.put("totalCount", count);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}
	@Override
	public ResultBean<Map<String, Object>> findOrdersForUsersPC(int userid, int states, int pageIndex, int size) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			String state="";
			if(states==1) {
				state="2";
			}else if(states==2) {
				state="1";
			}else if(states==3) {
				state="3,10";
			}else  if(states==4) {
				state="7";
			}else if(states==5){
				state="9";
			}else if(states==6){
				state="4,5,6,8";
			}
			List<Map<String, Object>> list=dao.findOrderListForUserCount(userid, state, (pageIndex-1)*size, size);
			map.put("orderList", list);
			int count=dao.findOrderListForUser(userid, state).size();
			map.put("totalCount", count);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}
	@Override
	public void addConsumption(int userId,Double money,String content) {
		// TODO Auto-generated method stub
		Consumption cm=new Consumption();
		cm.setContent(content);
		cm.setIspay(false);
		cm.setMoney(money);
		cm.setTime(new Date());
		cm.setUserid(userId);
		cnDao.insert(cm);
	}
	
	
	
}

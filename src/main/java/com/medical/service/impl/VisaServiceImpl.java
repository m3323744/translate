package com.medical.service.impl;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CollectMapper;
import com.medical.dao.EnterpriseMapper;
import com.medical.dao.VisaMapper;
import com.medical.dao.VisaOrderEvaluateMapper;
import com.medical.entity.Collect;
import com.medical.entity.Enterprise;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.Visa;
import com.medical.service.VisaService;

@Service
@Transactional
public class VisaServiceImpl implements VisaService {
	@Resource
	private VisaMapper dao;
	
	@Resource
	private VisaOrderEvaluateMapper tDao;
	
	@Resource
	private CollectMapper cDao;
	
	@Resource
	private EnterpriseMapper eDao;
	
	
	@Override
	public ResultBean<Integer> addVisa(Visa vi) {
		// TODO Auto-generated method stub
		try {
			vi.setStates(1);
			vi.setRelease(true);
			vi.setTime(new Date());
			int num=eDao.selectCount(new EntityWrapper<Enterprise>().eq("userid", vi.getUserid()).and().eq("states", 2));
			if(num<1) {
				return new ResultBean<>(new MyException("未认证企业"));
			}
			int count=dao.insert(vi);
			if(count>0) {
				return new ResultBean<>(vi.getId());
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getList(Visa vi, String keyWord, int score, int pageIndex,
			int size) {
		// TODO Auto-generated method stub
		StringBuffer whereSql=new StringBuffer(" and name like '%"+keyWord+"%' ");
		if(vi.getArea()!=null&&!vi.getArea().equals("")) {
			whereSql.append(" and area='"+vi.getArea()+"'");
		}
		if(vi.getCountry()!=null&&!vi.getCountry().equals("")) {
			whereSql.append(" and country='"+vi.getCountry()+"'");
		}
		if(vi.getIsinterview()!=null) {
			whereSql.append(" and isinterview="+vi.getIsinterview()+"");
		}
		if(vi.getVisatype()!=null&&!vi.getVisatype().equals("")) {
			whereSql.append(" and visatype='"+vi.getVisatype()+"'");
		}
		if(vi.getSendaddress()!=null&&!vi.getSendaddress().equals("")) {        //时间段怎么算
			whereSql.append(" and sendaddress='"+vi.getSendaddress()+"'");
		}
		whereSql.append(" and states=2 and `release`=true");
		String orderStr="";
		if(score==2) {    //价格
			orderStr=" order by price";
		}
		try {
			List<Map<String, Object>> list=dao.getList(whereSql.toString(), orderStr,(pageIndex-1)*size,size);
			List<Map<String, Object>> list1=new ArrayList<>();
			for(Map<String, Object> map:list) {
				Integer visaid=Integer.valueOf(map.get("id").toString());
				Double fen=tDao.selectAvgScore(visaid);
				map.put("fen", fen);
				list1.add(map);
			}
			
			if(score==1) {
				 Collections.sort(list1, new Comparator<Map<String, Object>>() {
			            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			                Double name1 = Double.valueOf(o1.get("fen").toString()) ;//name1是从你list里面拿出来的一个 
			                Double name2 = Double.valueOf(o2.get("fen").toString()) ; //name1是从你list里面拿出来的第二个name
			                return name2.compareTo(name1);
			            }
			        });
			}
			return new ResultBean<>(list1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id, int userId) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			Visa sc=dao.selectById(id);
			map.put("visa", sc);
			int count=cDao.selectCount(new EntityWrapper<Collect>().eq("collectid", id).and().eq("userid", userId).and().eq("collecttype", 6));
			if(count>0) {
				map.put("isCollect", true);
			}else {
				map.put("isCollect", false);
			}
			return new  ResultBean<Map<String,Object>>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> getListCount(Visa vi, String keyWord) {
		// TODO Auto-generated method stub
		StringBuffer whereSql=new StringBuffer(" and name like '%"+keyWord+"%' ");
		if(vi.getArea()!=null&&!vi.getArea().equals("")) {
			whereSql.append(" and area='"+vi.getArea()+"'");
		}
		if(vi.getCountry()!=null&&!vi.getCountry().equals("")) {
			whereSql.append(" and country='"+vi.getCountry()+"'");
		}
		if(vi.getIsinterview()!=null) {
			whereSql.append(" and isinterview='"+vi.getIsinterview()+"'");
		}
		if(vi.getVisatype()!=null&&!vi.getVisatype().equals("")) {
			whereSql.append(" and visatype='"+vi.getVisatype()+"'");
		}
		if(vi.getSendaddress()!=null&&!vi.getSendaddress().equals("")) {        //时间段怎么算
			whereSql.append(" and sendaddress='"+vi.getSendaddress()+"'");
		}
		whereSql.append(" and states=2 and `release`=true");
		int count=dao.getListCount(whereSql.toString());
		return new ResultBean<Integer>(count);
	}

}

package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.PlatformInformationMapper;
import com.medical.entity.MyException;
import com.medical.entity.PlatformInformation;
import com.medical.entity.ResultBean;
import com.medical.service.PlatformInformationService;

@Service
@Transactional
public class PlatformInformationServiceImpl implements PlatformInformationService {
	
	@Resource
	private PlatformInformationMapper dao;
	
	@Override
	public ResultBean<PlatformInformation> getByType(int type) {
		// TODO Auto-generated method stub
		List<PlatformInformation> pfList=dao.selectList(new EntityWrapper<PlatformInformation>().eq("type", type));
		if(pfList!=null&&pfList.size()>0) {
			PlatformInformation pf=pfList.get(0);
			return new ResultBean<PlatformInformation>(pf);
		}else {
			return new ResultBean<>(new MyException("没有数据"));
		}
		
	}

}

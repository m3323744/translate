package com.medical.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.wantedcertificateMapper;
import com.medical.entity.ResultBean;
import com.medical.entity.Wantedcertificate;
import com.medical.service.WantedcertificateService;

@Service
@Transactional
public class WantedcertificateServiceImpl implements WantedcertificateService {
	@Resource
	private wantedcertificateMapper dao;
	
	@Override
	public ResultBean<Integer> addWantedcertificate(Wantedcertificate wantedcertificate) {
		// TODO Auto-generated method stub
		
		try {
			dao.insert(wantedcertificate);
			return new ResultBean<>(wantedcertificate.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Wantedcertificate>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		
		try {
			if(ids!=null&&!ids.equals("")) {
				
				String[] data=ids.split(",");
				List<Wantedcertificate> list=new ArrayList<>();
				for(String da:data) {
					int id=Integer.parseInt(da);
					Wantedcertificate cs=dao.selectById(id);
					list.add(cs);
				}
				return new ResultBean<>(list);
			}else {
				return new ResultBean<>(null);
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delWantedcertificate(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.deleteById(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Wantedcertificate> update(Wantedcertificate wantedcertificate) {
		// TODO Auto-generated method stub
		try {
			dao.updateAllColumnById(wantedcertificate);
			return new ResultBean<>(wantedcertificate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id) {
		// TODO Auto-generated method stub
		Map<String, Object> map=dao.getById(id);
		return new ResultBean<>(map);
	}

}

package com.medical.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.ConventionApplyMapper;
import com.medical.dao.ConventionMapper;
import com.medical.entity.Convention;
import com.medical.entity.ConventionApply;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.ConventionApplyService;


@Service
@Transactional
public class ConventionApplyServiceImpl implements ConventionApplyService {
	@Resource
	private ConventionApplyMapper dao;
	
	@Resource
	private ConventionMapper cDao;
	
	@Override
	public ResultBean<ConventionApply> addConventionApply(ConventionApply ca) {
		// TODO Auto-generated method stub
		try {
			ca.setStates(1);
			ca.setTime(new Date());
			ca.setPaystates(1);
			List<ConventionApply> list=dao.selectList(new EntityWrapper<ConventionApply>().eq("conventionid", ca.getConventionid()));
			Convention con=cDao.selectById(ca.getConventionid());
			if(list.size()>=con.getNum()) {
				return new ResultBean<>(new MyException("人数已满")); 
			}
			int count=dao.insert(ca);
			if(count>0) {
				return new ResultBean<>(ca);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

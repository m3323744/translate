package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationStudyingMapper;
import com.medical.entity.AuthenticationStudying;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationStudyingService;

@Service
@Transactional
public class AuthenticationStudyingServiceImpl implements AuthenticationStudyingService {

	@Resource
	private AuthenticationStudyingMapper dao;
	
	@Override
	public ResultBean<Integer> addStudying(AuthenticationStudying ats) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insertAllColumn(ats);
			if(count>0) {
				return new ResultBean<>(ats.getId());
			}else {
				return new ResultBean<>(new MyException("插入失败"));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<AuthenticationStudying> getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			AuthenticationStudying ac=dao.selectById(id);
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<AuthenticationStudying>> getList(int authenticationid, int isteacher) {
		// TODO Auto-generated method stub
		try {
			List<AuthenticationStudying> ac=dao.selectList(new EntityWrapper<AuthenticationStudying>().eq("authenticationid", authenticationid).and().eq("isteacher", isteacher));
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<AuthenticationStudying>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<AuthenticationStudying> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				String[] data=ids.split(",");
				for(String da:data) {
					int id=Integer.parseInt(da);
					AuthenticationStudying cs=dao.selectById(id);
					list.add(cs);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<AuthenticationStudying>> selectByUserId(int type, int userId) {
		// TODO Auto-generated method stub
		try {
			List<AuthenticationStudying> ac=dao.selectByUserId(type, userId);
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delStudying(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.deleteById(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateAuthenticationStudying(AuthenticationStudying ats) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(ats);
			if(count>0) {
				return new ResultBean<>(ats.getId());
			}else {
				return new ResultBean<>(new MyException("更新失败"));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

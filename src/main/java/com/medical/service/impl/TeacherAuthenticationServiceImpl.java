package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationCertificateMapper;
import com.medical.dao.AuthenticationLanguageMapper;
import com.medical.dao.AuthenticationSchoolMapper;
import com.medical.dao.AuthenticationStudyingMapper;
import com.medical.dao.TeacherAuthenticationMapper;
import com.medical.dao.TeacherAuthenticationcreditMapper;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthentication;
import com.medical.entity.TeacherAuthenticationcredit;
import com.medical.service.TeacherAuthenticationService;

@Service
@Transactional
public class TeacherAuthenticationServiceImpl implements TeacherAuthenticationService {
	@Resource
	private TeacherAuthenticationMapper dao;
	
	@Resource
	private AuthenticationCertificateMapper acfDao;
	
	@Resource
	private AuthenticationLanguageMapper aclDao;
	
	@Resource
	private AuthenticationSchoolMapper acsDao;
	
	@Resource
	private AuthenticationStudyingMapper asDao;
	
	@Resource
	private TeacherAuthenticationcreditMapper tacDao;

	@Override
	public ResultBean<Integer> addTeacherAuthentication(TeacherAuthentication ta, String lanuage, String certificate,
			String school, String studying,String zengone,String zengtwo,String zengthree) {
		// TODO Auto-generated method stub
		try {
			List<TeacherAuthentication> list=dao.selectList(new EntityWrapper<TeacherAuthentication>().eq("userid", ta.getUserid()));
			if(list.size()>0) {
				return new ResultBean<>(new MyException("已经认证过"));
			}
			ta.setTime(new Date());
			ta.setStates(1);
			int count=dao.insert(ta);
			if(zengone!=null&&!zengone.equals("")) {
				TeacherAuthenticationcredit ta1=new TeacherAuthenticationcredit();
				ta1.setAuthenticationid(ta.getId());
				ta1.setImages(zengone);
				tacDao.insert(ta1);
			}
			if(zengtwo!=null&&!zengtwo.equals("")) {
				TeacherAuthenticationcredit ta1=new TeacherAuthenticationcredit();
				ta1.setAuthenticationid(ta.getId());
				ta1.setImages(zengtwo);
				tacDao.insert(ta1);
			}
			if(zengthree!=null&&!zengthree.equals("")) {
				TeacherAuthenticationcredit ta1=new TeacherAuthenticationcredit();
				ta1.setAuthenticationid(ta.getId());
				ta1.setImages(zengthree);
				tacDao.insert(ta1);
			}
			if(count>0) {
				if(lanuage!=null&&!lanuage.equals("")) {
					String[] data=lanuage.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						aclDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				if(certificate!=null&&!certificate.equals("")) {
					String[] data=certificate.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acfDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				if(school!=null&&!school.equals("")) {
					String[] data=school.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acsDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				if(studying!=null&&!studying.equals("")) {
					String[] data=studying.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						asDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				return new ResultBean<>(ta.getId());
			}else {
				return new ResultBean<>(new MyException("插入异常"));
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> TeacherAuthenticationDateils(int userId) {
		// TODO Auto-generated method stub
		try {
			TeacherAuthentication ta=dao.getByUserid(userId);
			if(ta!=null) {
				Map<String, Object> map=new HashMap<>();
				map.put("TeacherAuthentication", ta);
				map.put("lanuage", aclDao.selectByAuthenticationid(ta.getId(),1));
				map.put("certificate", acfDao.selectByAuthenticationid(ta.getId(),1));
				map.put("school", acsDao.selectByAuthenticationid(ta.getId(),1));
				map.put("studying", asDao.selectByAuthenticationid(ta.getId(),1));
				map.put("credit", tacDao.selectByAuthenticationid(ta.getId()));
				return new ResultBean<>(map);
			}else {
				return new ResultBean<>();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateTeacherAuthentication(TeacherAuthentication ta, String lanuage,
			String certificate, String school, String studying,String zengoneurl,String zengtwourl,String zengthreeurl) {
		// TODO Auto-generated method stub
		try {
			ta.setStates(1);
			ta.setTime(new Date());
			int count=dao.updateAllColumnById(ta);
			if(count>0) {
				tacDao.delete(new EntityWrapper<TeacherAuthenticationcredit>().eq("authenticationid", ta.getId()));
				if(lanuage!=null&&!lanuage.equals("")) {
					//aclDao.delete(new EntityWrapper<AuthenticationLanguage>().eq("authenticationid", ta.getId()));
					String[] data=lanuage.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						aclDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				if(certificate!=null&&!certificate.equals("")) {
					//acfDao.delete(new EntityWrapper<AuthenticationCertificate>().eq("authenticationid", ta.getId()));
					String[] data=certificate.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acfDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				if(school!=null&&!school.equals("")) {
					//acsDao.delete(new EntityWrapper<AuthenticationSchool>().eq("authenticationid", ta.getId()));
					String[] data=school.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acsDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				if(studying!=null&&!studying.equals("")) {
					//asDao.delete(new EntityWrapper<AuthenticationStudying>().eq("authenticationid", ta.getId()));
					String[] data=studying.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						asDao.updateAuthenticationid(ta.getId(), id);
					}
				}
				//tacDao.delete(new EntityWrapper<TeacherAuthenticationcredit>().eq("authenticationid", ta.getId()));
				TeacherAuthenticationcredit ta1=new TeacherAuthenticationcredit();
				ta1.setAuthenticationid(ta.getId());
				if(zengoneurl!=null&&!zengoneurl.equals("")){
					ta1.setImages(zengoneurl);
					tacDao.insert(ta1);
				}
				
				TeacherAuthenticationcredit ta2=new TeacherAuthenticationcredit();
				ta2.setAuthenticationid(ta.getId());
				if(zengtwourl!=null&&!zengtwourl.equals("")){
					ta2.setImages(zengtwourl);
					tacDao.insert(ta2);
				}
				TeacherAuthenticationcredit ta3=new TeacherAuthenticationcredit();
				ta3.setAuthenticationid(ta.getId());
				if(zengthreeurl!=null&&!zengthreeurl.equals("")){
					ta3.setImages(zengthreeurl);
					tacDao.insert(ta3);
				}
				return new ResultBean<>(count);
			}else {
				return new ResultBean<>(new MyException("插入异常"));
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	
	
}

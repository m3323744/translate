package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.CollectMapper;
import com.medical.dao.ConventionMapper;
import com.medical.dao.EnterpriseMapper;
import com.medical.entity.Collect;
import com.medical.entity.Convention;
import com.medical.entity.Enterprise;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.ConventionService;

@Service
@Transactional
public class ConventionServiceImpl implements ConventionService {
	@Resource
	private ConventionMapper dao;
	
	@Resource
	private CollectMapper cDao;
	
	@Resource
	private EnterpriseMapper eDao;
	
	@Override
	public ResultBean<Integer> addConvention(Convention co) {
		// TODO Auto-generated method stub
		try {
			List<Enterprise> list=eDao.selectList(new EntityWrapper<Enterprise>().eq("userid", co.getUserid()).and().eq("states", 2));
			if(list.size()==0) {
				return new ResultBean<>(new MyException("未认证过企业"));
			}
			co.setCreattime(new Date());
			co.setStates(1);
			co.setRelease(true);
			int count=dao.insert(co);
			if(count>0) {
				return new ResultBean<>(co.getId());
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<List<Convention>> getList(String keyWord, int pageIndex, int score, int size) {
		// TODO Auto-generated method stub
		//keyWord="%"+keyWord+"%";
		String scores="";
		if(score==1) {
			scores="price";
		}else if(score==2) {
			scores="time";
		}else if(score==3) {
			scores="time";
		}
		try {
			if(score==1) {
				List<Convention> list=dao.selectPage(new Page<Convention>(pageIndex, size),new EntityWrapper<Convention>().like("name", keyWord).and().eq("`release`", true).orderBy(scores, true));
				return new ResultBean<>(list);
			}else {
				List<Convention> list=dao.selectPage(new Page<Convention>(pageIndex, size),new EntityWrapper<Convention>().like("name", keyWord).and().eq("`release`", true).orderBy(scores, false));
				return new ResultBean<>(list);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id,int userid) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			Convention cf=dao.selectById(id);
			int count=cDao.selectCount(new EntityWrapper<Collect>().eq("collectid", id).and().eq("userid", userid).and().eq("collecttype", 8));
			map.put("Convention", cf);
			if(count>0) {
				map.put("isCollect", true);
			}else {
				map.put("isCollect", false);
			}
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getByMyUserId(int userId,int pageIndex,int size) {
		// TODO Auto-generated method stub
		try {
			List<Map<String, Object>> list=dao.getByMyUserId(userId,(pageIndex-1)*size,size);
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delConvention(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.delete(new EntityWrapper<Convention>().eq("id", id));
			if(count>0) {
				return new ResultBean<>(count);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<Integer> getListCount(String keyWord) {
		// TODO Auto-generated method stub
		int count=dao.selectCount(new EntityWrapper<Convention>().like("name", keyWord).and().eq("`release`", true));
		return new ResultBean<Integer>(count);
	}

}

package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.LanguageMapper;
import com.medical.entity.Language;
import com.medical.entity.ResultBean;
import com.medical.service.LanguageService;

@Service
@Transactional
public class LanguageServiceImpl implements LanguageService {

	@Resource
	private LanguageMapper dao;
	
	@Override
	public ResultBean<List<Language>> getList() {
		// TODO Auto-generated method stub
		try {
			List<Language> list=dao.selectList(new EntityWrapper<Language>().orderBy("convert(language using gbk) asc"));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

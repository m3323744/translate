package com.medical.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.ContributeMapper;
import com.medical.entity.Contribute;
import com.medical.entity.ResultBean;
import com.medical.service.ContributeService;

@Service
@Transactional
public class ContributeServiceImpl implements ContributeService {
	@Resource
	private ContributeMapper dao;

	@Override
	public ResultBean<Integer> addContribute(Contribute con) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(con);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	
}

package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CountryAreaMapper;
import com.medical.entity.CountryArea;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.CountryAreaService;

@Service
@Transactional
public class CountryAreaSerivceImpl implements CountryAreaService {
	@Resource
	private CountryAreaMapper dao;
	
	@Override
	public ResultBean<List<CountryArea>> getListByCountryId(int countryId) {
		// TODO Auto-generated method stub
		try {
			List<CountryArea> list=dao.selectList(new EntityWrapper<CountryArea>().eq("countryid", countryId));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

}

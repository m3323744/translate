package com.medical.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.ConferenceApplyMapper;
import com.medical.dao.ConferenceMapper;
import com.medical.entity.Conference;
import com.medical.entity.ConferenceApply;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.ConferenceApplyService;

@Service
@Transactional
public class ConferenceApplyServiceImpl implements ConferenceApplyService {
	@Resource
	private ConferenceApplyMapper dao;
	
	@Resource
	private ConferenceMapper cDao;
	
	@Override
	public ResultBean<ConferenceApply> addConferenceApply(ConferenceApply ca) {
		// TODO Auto-generated method stub
		try {
			ca.setStates(1);
			ca.setPaystates(1);
			ca.setTime(new Date());
			List<ConferenceApply> list=dao.selectList(new EntityWrapper<ConferenceApply>().eq("conferenceid", ca.getConferenceid()));
			Conference con=cDao.selectById(ca.getConferenceid());
			if(list.size()>=con.getNum()) {
				return new ResultBean<>(new MyException("人数已满")); 
			}
			int count=dao.insert(ca);
			if(count>0) {
				/*if((list.size()+1)==con.getNum()) {
					con.setStates(2);
				}*/
				return new ResultBean<>(ca);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

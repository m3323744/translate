package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.VisaTypeMapper;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.VisaType;
import com.medical.service.VisaTypeService;

@Service
@Transactional
public class VisaTypeServiceImpl implements VisaTypeService {
	@Resource
	private VisaTypeMapper dao;
	
	@Override
	public ResultBean<List<VisaType>> getList() {
		// TODO Auto-generated method stub
		try {
			List<VisaType> list=dao.selectList(new EntityWrapper<VisaType>());
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

}

package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.medical.dao.FinanceTypeMapper;
import com.medical.dao.InvestmentTypeMapper;
import com.medical.dao.LawTypeMapper;
import com.medical.enums.ConsultationTypeEnum;
import com.medical.service.ConsultationService;

@Service
public class ConsultationServiceImpl implements ConsultationService {

	@Autowired
	private InvestmentTypeMapper investmentTypeMapper;
	@Autowired
	private FinanceTypeMapper financeTypeMapper;
	@Autowired
	private LawTypeMapper lawTypeMapper;
	
	@Override
	public List<Map<String, Object>> findTypeList(Integer type) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> typeList=null;
		if(ConsultationTypeEnum.INVESTMENT.getCode()==type) {
			typeList=investmentTypeMapper.findTypeList();
		}else if(ConsultationTypeEnum.FINANCE.getCode()==type) {
			typeList=financeTypeMapper.findTypeList();
		}else if(ConsultationTypeEnum.LAW.getCode()==type) {
			typeList=lawTypeMapper.findTypeList();
		}
		return typeList;
	}
	@Override
	public Map<String,Object> findContentByType(Integer typeId,Integer twotypeId){
		Map<String, Object> typeList=null;
		if(ConsultationTypeEnum.INVESTMENT.getCode()==typeId) {
			typeList=investmentTypeMapper.findContentByTypeid(twotypeId);
		}else if(ConsultationTypeEnum.FINANCE.getCode()==typeId) {
			typeList=financeTypeMapper.findContentByTypeid(twotypeId);
		}else if(ConsultationTypeEnum.LAW.getCode()==typeId) {
			typeList=lawTypeMapper.findContentByTypeid(twotypeId);
		}
		return typeList;
	}

}

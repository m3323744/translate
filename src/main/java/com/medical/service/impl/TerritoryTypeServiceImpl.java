package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.TerritoryTypeMapper;
import com.medical.entity.ResultBean;
import com.medical.entity.TerritoryType;
import com.medical.service.TerritoryTypeService;

@Service
@Transactional
public class TerritoryTypeServiceImpl implements TerritoryTypeService {
	@Resource
	private TerritoryTypeMapper dao;
	
	@Override
	public ResultBean<List<TerritoryType>> getList() {
		// TODO Auto-generated method stub
		try {
			List<TerritoryType> list=dao.selectList(new EntityWrapper<>());
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

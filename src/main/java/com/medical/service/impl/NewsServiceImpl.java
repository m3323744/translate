package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.NewsMapper;
import com.medical.entity.MyException;
import com.medical.entity.News;
import com.medical.entity.ResultBean;
import com.medical.service.NewsService;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {
	@Resource
	private NewsMapper dao;
	
	@Override
	public ResultBean<News> getList(int type) {
		// TODO Auto-generated method stub
		List<News> ne=dao.selectList(new EntityWrapper<News>().eq("type", type).orderBy("id", false));
		if(ne.size()>0) {
			News news=ne.get(0);
			return new ResultBean<News>(news);
		}else {
			return new ResultBean<News>(new MyException("无新闻"));
		}
	}

	@Override
	public ResultBean<List<News>> getNews(int page, int size) {
		// TODO Auto-generated method stub
		List<News> ne=dao.selectPage(new Page<>(page, size), new EntityWrapper<News>().eq("type", 2).orderBy("id", false));
		return new ResultBean<List<News>>(ne);
	}

	@Override
	public ResultBean<News> getById(int id) {
		// TODO Auto-generated method stub
		News ne=dao.selectById(id);
		return new ResultBean<News>(ne);
	}

	@Override
	public ResultBean<List<News>> selectByKey(String key,int page, int size) {
		// TODO Auto-generated method stub
		List<News> ne=dao.selectPage(new Page<>(page, page), new EntityWrapper<News>().eq("type", 2).and().like("content", key).orderBy("id", false));
		return new ResultBean<List<News>>(ne);
	}

}

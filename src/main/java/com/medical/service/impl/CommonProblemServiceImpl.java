package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CommonProblemMapper;
import com.medical.entity.CommonProblem;
import com.medical.service.CommonProblemService;

@Service
@Transactional
public class CommonProblemServiceImpl implements CommonProblemService {
	
	@Resource
	private CommonProblemMapper dao;
	
	@Override
	public CommonProblem getOne() {
		// TODO Auto-generated method stub
		List<CommonProblem> cp=dao.selectList(new EntityWrapper<CommonProblem>());
		return cp.get(0);
	}

}

package com.medical.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.EnterpriseMapper;
import com.medical.entity.Enterprise;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.EnterpriseService;

@Service
@Transactional
public class EnterpriseServiceImpl implements EnterpriseService {
	@Resource
	private EnterpriseMapper dao;
	
	@Override
	public ResultBean<Integer> addEnterprise(Enterprise ent) {
		// TODO Auto-generated method stub
		try {
			ent.setStates(1);
			ent.setTime(new Date());
			int count=dao.insert(ent);
			if(count>0) {
				return new ResultBean<>(count);
			}else {
				return new ResultBean<>(new MyException("系统异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
		
	}

	@Override
	public ResultBean<Enterprise> getEnterpriseDateils(int userId) {
		// TODO Auto-generated method stub
		try {
			Enterprise ent=dao.selectByUserId(userId);
			return new ResultBean<>(ent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

	@Override
	public ResultBean<Integer> updateEnterprise(Enterprise ent) {
		// TODO Auto-generated method stub
		try {
			ent.setStates(1);
			int count=dao.updateAllColumnById(ent);
			if(count>0) {
				return new ResultBean<>(count);
			}else {
				return new ResultBean<>(new MyException("系统异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

}

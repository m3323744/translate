package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CourseSimpleMapper;
import com.medical.entity.CourseSimple;
import com.medical.entity.Enterprise;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthentication;
import com.medical.service.CourseSimpleService;

@Service
@Transactional
public class CourseSimpleServiceImpl implements CourseSimpleService {
	@Resource
	private CourseSimpleMapper dao;
	
	
	
	@Override
	public ResultBean<CourseSimple> addCourseSimple(CourseSimple cs) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(cs);
			
			if(count>0) {
				return new ResultBean<>(cs);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}
	
	@Override
	public ResultBean<List<CourseSimple>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<CourseSimple> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				String[] data=ids.split(",");
				for(String da:data) {
					int id=Integer.parseInt(da);
					CourseSimple cs=dao.selectById(id);
					list.add(cs);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id) {
		// TODO Auto-generated method stub
		return new ResultBean<>(dao.getById(id));
	}

}

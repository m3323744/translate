package com.medical.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CourseOrderMapper;
import com.medical.entity.CourseOrder;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.CourseOrderService;

@Service
@Transactional
public class CourseOrderServiceImpl implements CourseOrderService {
	@Resource
	private CourseOrderMapper dao;

	@Override
	public ResultBean<CourseOrder> addCourseOrder(CourseOrder co) {
		// TODO Auto-generated method stub
		try {
			if(co.getLessonsid()!=0) {
				int count=dao.selectCount(new EntityWrapper<CourseOrder>().eq("courseid", co.getCourseid()).and().eq("userid", co.getUserid()).and().eq("isall", false));
				if(count>0) {
					return new ResultBean<>(new MyException("拼课限制同一个人不能拼两次"));
				}
			}
			co.setStates(1);
			co.setTime(new Date());
			int count=dao.insert(co);
			if(count>0) {
				return new ResultBean<>(co);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	
}

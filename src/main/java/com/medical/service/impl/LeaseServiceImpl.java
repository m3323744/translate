package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.LeaseMapper;
import com.medical.dao.LeaseTypeMapper;
import com.medical.entity.Lease;
import com.medical.entity.LeaseType;
import com.medical.entity.ResultBean;
import com.medical.service.LeaseService;

@Service
@Transactional
public class LeaseServiceImpl implements LeaseService {
	@Resource
	private LeaseMapper dao;
	
	@Resource
	private LeaseTypeMapper lDao;
	
	@Override
	public ResultBean<List<Lease>> getList(int pageIndex, int type,int size) {
		// TODO Auto-generated method stub
		try {
			List<Lease> list=dao.selectPage(new Page<Lease>(pageIndex, size),new EntityWrapper<Lease>().eq("typeid", type));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			Lease le=dao.selectById(id);
			LeaseType lt=lDao.selectById(le.getTypeid());
			map.put("Lease", le);
			map.put("LeaseType", lt);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

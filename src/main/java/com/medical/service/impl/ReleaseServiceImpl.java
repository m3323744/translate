package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.ConferenceMapper;
import com.medical.dao.ConventionMapper;
import com.medical.dao.CourseOfflineMapper;
import com.medical.dao.CourseSeeingMapper;
import com.medical.dao.CourseStreamingMapper;
import com.medical.dao.InterpreterMapper;
import com.medical.dao.SchoolMapper;
import com.medical.dao.TeacherMapper;
import com.medical.dao.VisaMapper;
import com.medical.entity.ResultBean;
import com.medical.service.ReleaseService;

@Service
@Transactional
public class ReleaseServiceImpl implements ReleaseService {
	@Resource
	private TeacherMapper tDao;
	
	@Resource
	private InterpreterMapper iDao;
	
	@Resource
	private CourseStreamingMapper csDao;
	
	@Resource
	private CourseOfflineMapper coDao;
	
	@Resource
	private CourseSeeingMapper ceDao;
	
	@Resource
	private VisaMapper vDao;
	
	@Resource
	private ConferenceMapper cDao;
	
	@Resource
	private ConventionMapper cvDao;
	
	@Resource
	private SchoolMapper sDao;
	
	@Override
	public ResultBean<Integer> CancelRelease(int id, int type) {
		// TODO Auto-generated method stub
		try {
			int count=0;
			if(type==1) {
				count=tDao.updateRelease(false, id);
			}else if(type==2) {
				count=iDao.updateRelease(false, id);
			}else if(type==3) {
				count=csDao.updateRelease(false, id);
			}else if(type==4) {
				count=coDao.updateRelease(false, id);
			}else if(type==5) {
				count=ceDao.updateRelease(false, id);
			}else if(type==6) {
				count=vDao.updateRelease(false, id);
			}else if(type==7) {
				count=cDao.updateRelease(false, id);
			}else if(type==8) {
				count=cvDao.updateRelease(false, id);
			}else if(type==9) {
				count=sDao.updateRelease(false, id);
			}
			/*if(type==1) {
				count=tDao.deleteById(id);
			}else if(type==2) {
				count=iDao.deleteById(id);
			}else if(type==3) {
				count=csDao.deleteById(id);
			}else if(type==4) {
				count=coDao.deleteById(id);
			}else if(type==5) {
				count=ceDao.deleteById(id);
			}else if(type==6) {
				count=vDao.deleteById(id);
			}else if(type==7) {
				count=cDao.deleteById(id);
			}else if(type==8) {
				count=cvDao.deleteById(id);
			}*/
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getByUserId(int userId, int type) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> list=new ArrayList<>();
		if(type==1) {
			list=tDao.getByUserId(userId);
		}else if(type==2) {
			list=sDao.getByUserId(userId);
		}else if(type==3) {
			list=ceDao.getByUserId(userId);
		}else if(type==4) {
			list=iDao.getByUserId(userId);
		}else if(type==5) {
			list=cDao.getByUserId(userId);
		}else if(type==6) {
			list=vDao.getByUserId(userId);
		}
		return new ResultBean<List<Map<String,Object>>>(list);
	}
	
	

}

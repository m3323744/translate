package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.DynamicCommentMapper;
import com.medical.dao.DynamicLikeMapper;
import com.medical.dao.DynamicMapper;
import com.medical.dao.UserMapper;
import com.medical.entity.Dynamic;
import com.medical.entity.DynamicComment;
import com.medical.entity.DynamicLike;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.DynamicService;

@Service
@Transactional
public class DynamicServiceImpl implements DynamicService {
	@Resource
	private DynamicMapper dao;
	
	@Resource
	private DynamicLikeMapper dlDao;
	
	@Resource
	private DynamicCommentMapper dcDao;
	
	@Resource
	private UserMapper uDao;
	
	@Override
	public ResultBean<List<Map<String, Object>>> getList(int pageIndex, int size,int userId) {
		// TODO Auto-generated method stub
		try {
			List<Map<String, Object>> list=dao.getList((pageIndex-1)*size, size);
			for(Map<String, Object> ma:list) {
				Boolean isLike=false;
				int id=Integer.parseInt(ma.get("id").toString());
				int likeNum=dlDao.selectCount(new EntityWrapper<DynamicLike>().eq("dynamicid", id).and().eq("userid", userId));
				if(likeNum>0) {
					isLike=true;
				}
				ma.put("isLike", isLike);
				likeNum=dlDao.selectCount(new EntityWrapper<DynamicLike>().eq("dynamicid", id));
				ma.put("likeNum", likeNum);
				Boolean isMe=Integer.parseInt(ma.get("userid").toString())==userId;
				ma.put("isMe", isMe);
				Boolean isComment=false;
				int commentNum=dcDao.selectCount(new EntityWrapper<DynamicComment>().eq("dynamicid", id).and().eq("userid", userId));
				if(commentNum>0) {
					isComment=true;
				}
				ma.put("isComment", isComment);
			}
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getdetailById(int id) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			Dynamic dy=dao.selectById(id);
			User user=uDao.selectById(dy.getUserid());
			List<Map<String, Object>> list=dcDao.getList(id);
			int commentNum=dcDao.selectCount(new EntityWrapper<DynamicComment>().eq("dynamicid", id));
			int likeNum=dlDao.selectCount(new EntityWrapper<DynamicLike>().eq("dynamicid", id));
			map.put("dynamic", dy);
			map.put("commentList", list);
			map.put("commentNum", commentNum);
			map.put("likeNum", likeNum);
			map.put("user", user);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delDynamic(int id) {
		// TODO Auto-generated method stub
		try {
			dcDao.delete(new EntityWrapper<DynamicComment>().eq("dynamicid", id));
			dlDao.delete(new EntityWrapper<DynamicLike>().eq("dynamicid", id));
			int count=dao.delete(new EntityWrapper<Dynamic>().eq("id", id));
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Dynamic> addDynamic(Dynamic dy) {
		// TODO Auto-generated method stub
		try {
			dy.setTime(new Date());
			dao.insert(dy);
			return  new ResultBean<>(dy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> likeDynamic(int id, int userid) {
		// TODO Auto-generated method stub
		try {
			DynamicLike dl=new DynamicLike();
			dl.setDynamicid(id);
			dl.setUserid(userid);
			int count=dlDao.insert(dl);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> evaluateDynamic(DynamicComment dc) {
		// TODO Auto-generated method stub
		try {
			dc.setTime(new Date());
			int count=dcDao.insert(dc);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> getListByCount() {
		// TODO Auto-generated method stub
		int count=dao.selectCount(new EntityWrapper<Dynamic>());
		return new ResultBean<Integer>(count);
	}

}

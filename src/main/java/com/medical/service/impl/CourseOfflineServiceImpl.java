package com.medical.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CollectMapper;
import com.medical.dao.CourseEvaluateMapper;
import com.medical.dao.CourseOfflineMapper;
import com.medical.dao.CourseSimpleMapper;
import com.medical.dao.EnterpriseMapper;
import com.medical.dao.LanguageMapper;
import com.medical.dao.TeacherAuthenticationMapper;
import com.medical.dao.UserMapper;
import com.medical.entity.Collect;
import com.medical.entity.CourseOffline;
import com.medical.entity.CourseSimple;
import com.medical.entity.Enterprise;
import com.medical.entity.Language;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthentication;
import com.medical.entity.User;
import com.medical.service.CourseOfflineService;

@Service
@Transactional
public class CourseOfflineServiceImpl implements CourseOfflineService {
	@Resource
	private CourseOfflineMapper dao;
	
	@Resource
	private CourseSimpleMapper csDao;
	
	@Resource
	private CourseEvaluateMapper tDao;
	
	@Resource
	private LanguageMapper lDao;
	
	@Resource
	private CollectMapper cDao;
	
	@Resource
	private UserMapper uDao;
	
	@Resource
	private TeacherAuthenticationMapper teDao;
	
	@Resource
	private EnterpriseMapper eDao;
	
	@Override
	public ResultBean<Integer> addCourseSeeing(CourseOffline co, String simple) {
		// TODO Auto-generated method stub
		try {
			TeacherAuthentication ta=teDao.getByUserid(co.getUserid());
			if(ta==null) {
				return new ResultBean<>(new MyException("未认证"));
			}
			if(ta.getStates()!=2) {
				return new ResultBean<>(new MyException("认证未通过"));
			}
			/*int num=eDao.selectCount(new EntityWrapper<Enterprise>().eq("userid", co.getUserid()).and().eq("states", 2));
			if(num<1) {
				return new ResultBean<>(new MyException("未认证企业"));
			}*/
			co.setStates(1);
			co.setTime(new Date());
			co.setRelease(true);
			int count=dao.insert(co);
			if(count>0) {
				if(simple!=null&&!simple.equals("")) {
					String[] data=simple.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						csDao.updateAuthenticationid(co.getId(), id);
					}
				}
				return new ResultBean<>(co.getId());
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id,int userId) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			CourseOffline cs=dao.selectById(id);
			List<CourseSimple> list=csDao.selectList(new EntityWrapper<CourseSimple>().eq("courseid", id).and().eq("coursetype", 2));
			map.put("CourseOffline", cs);
			map.put("CourseSimple", list);
			int count=cDao.selectCount(new EntityWrapper<Collect>().eq("collectid", id).and().eq("userid", userId).and().eq("collecttype", 4));
			if(count>0) {
				map.put("isCollect", true);
			}else {
				map.put("isCollect", false);
			}
			Language lang=lDao.selectById(cs.getLanguageid());
			map.put("language", lang);
			User user=uDao.selectById(cs.getUserid());
			map.put("user", user);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getList(CourseOffline cs,String hours, String prices, String startTime,String endTime, int score,
			String keyWord, int pageIndex, int size) {
		// TODO Auto-generated method stub
		StringBuffer whereSql=new StringBuffer(" and name like '%"+keyWord+"%' ");
		if(cs.getLanguageid()!=null&&cs.getLanguageid()!=0) {
			whereSql.append(" and languageid="+cs.getLanguageid());
		}
		if(cs.getStageid()!=null&&cs.getStageid()!=0) {
			whereSql.append(" and stageid="+cs.getStageid());
		}
		if(cs.getHour()!=null&&cs.getHour()!=0) {
			whereSql.append(" and hour="+cs.getHour());
		}
		if(prices!=null&&!prices.equals("")) {
			String[] price=prices.split("-");
			whereSql.append(" and totalprice>="+price[0]+" and totalprice<="+price[1]);
		}
		if(hours!=null&&!hours.equals("")) {        //时间段怎么算
			String[] hour=hours.split("-");
			whereSql.append(" and hour>="+hour[0]+" and hour<="+hour[1]);
		}
		if(cs.getSources()!=null&&!cs.getSources().equals("")) {
			whereSql.append(" and sources ='"+cs.getSources()+"'");
		}
		if(startTime!=null&&!startTime.equals("")) {
			whereSql.append(" and attendtime between '"+startTime+" 00:00:00'  and '"+endTime+" 23:59:59'");
			/*if(days.equals("1")) {
				whereSql.append(" and time>='"+DataUtil.CalendarTime(-3)+"'");
			}else if(days.equals("2")) {
				whereSql.append(" and time between '"+DataUtil.CalendarTime(-3)+" 00:00:00'  and '"+DataUtil.CalendarTime(-7)+" 23:59:59'");
			}else if(days.equals("3")){
				whereSql.append(" and time between '"+DataUtil.CalendarTime(-7)+" 00:00:00'  and '"+DataUtil.CalendarTime(-15)+" 23:59:59'");
			}else if(days.equals("4")) {
				whereSql.append(" and time between '"+DataUtil.CalendarTime(-15)+" 00:00:00'  and '"+DataUtil.CalendarTime(-30)+" 23:59:59'");
			}else if(days.equals("5")) {
				whereSql.append(" and time>='"+DataUtil.CalendarTime(-30)+"'");
			}*/
		}
		if(cs.getAddress()!=null&&!cs.getAddress().equals("")) {
			whereSql.append(" and address ='"+cs.getAddress()+"'");
		}
		whereSql.append(" and states=2");
		String orderStr="";
		if(score==2) {    //价格
			orderStr=" order by totalprice";
		}else if(score==3) {    //时间
			orderStr=" order by attendtime desc";
		}else if(score==4) {
			orderStr=" order by attendtime desc";
		}
		try {
			List<Map<String, Object>> list=dao.getList(whereSql.toString(), orderStr,(pageIndex-1)*size,size);
			List<Map<String, Object>> list1=new ArrayList<>();
			for(Map<String, Object> map:list) {
				Integer courseId=Integer.valueOf(map.get("id").toString());
				Double fen=tDao.selectAvgScoreByeacherId(courseId, 3);
				map.put("fen", fen);
				list1.add(map);
			}
			if(score==1) {
				 Collections.sort(list1, new Comparator<Map<String, Object>>() {
			            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			                Double name1 = Double.valueOf(o1.get("fen").toString()) ;//name1是从你list里面拿出来的一个 
			                Double name2 = Double.valueOf(o2.get("fen").toString()) ; //name1是从你list里面拿出来的第二个name
			                return name1.compareTo(name2);
			            }
			        });
			}
			return new ResultBean<>(list1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> getListCount(CourseOffline cs, String hours, String prices, String startTime,
			String endTime, String keyWord) {
		// TODO Auto-generated method stub
		StringBuffer whereSql=new StringBuffer(" and name like '%"+keyWord+"%' ");
		if(cs.getLanguageid()!=null&&cs.getLanguageid()!=0) {
			whereSql.append(" and languageid="+cs.getLanguageid());
		}
		if(cs.getStageid()!=null&&cs.getStageid()!=0) {
			whereSql.append(" and stageid="+cs.getStageid());
		}
		if(cs.getHour()!=null&&cs.getHour()!=0) {
			whereSql.append(" and hour="+cs.getHour());
		}
		if(prices!=null&&!prices.equals("")) {
			String[] price=prices.split("-");
			whereSql.append(" and totalprice>="+price[0]+" and totalprice<="+price[1]);
		}
		if(hours!=null&&!hours.equals("")) {        //时间段怎么算
			String[] hour=hours.split("-");
			whereSql.append(" and hour>="+hour[0]+" and hour<="+hour[1]);
		}
		if(cs.getSources()!=null&&!cs.getSources().equals("")) {
			whereSql.append(" and sources ='"+cs.getSources()+"'");
		}
		if(startTime!=null&&!startTime.equals("")) {
			whereSql.append(" and time between '"+startTime+" 00:00:00'  and '"+endTime+" 23:59:59'");
		}
		if(cs.getAddress()!=null&&!cs.getAddress().equals("")) {
			whereSql.append(" and address ='"+cs.getAddress()+"'");
		}
		whereSql.append(" and states=2");
		int count=dao.getListCount(whereSql.toString());
		return new ResultBean<Integer>(count);
	}

}

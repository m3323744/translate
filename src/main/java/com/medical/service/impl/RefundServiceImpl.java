package com.medical.service.impl;

import java.util.Date;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.RefundMapper;
import com.medical.entity.Refund;
import com.medical.service.RefundService;

@Service
@Transactional
public class RefundServiceImpl implements RefundService {
	@Resource
	private RefundMapper dao;

	@Override
	public int addRefund(Refund re) {
		// TODO Auto-generated method stub
		try {
			re.setTime(new Date());
			re.setStates(1);
			int count=dao.insert(re);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int updateStates(Refund re) {
		// TODO Auto-generated method stub
		return dao.updateStates(re);
	}

	@Override
	public Refund getByOrderId(Refund re) {
		// TODO Auto-generated method stub
		return dao.getByOrderId(re);
	}
	
	
}

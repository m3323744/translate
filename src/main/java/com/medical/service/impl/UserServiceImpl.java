package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationCashMapper;
import com.medical.dao.ConsumptionMapper;
import com.medical.dao.IntegralConsumeMapper;
import com.medical.dao.UserMapper;
import com.medical.entity.AuthenticationCash;
import com.medical.entity.Consumption;
import com.medical.entity.IntegralConsume;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.UserService;
import com.medical.util.WxPayUtil;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Resource
	private UserMapper dao;
	
	@Resource
	private ConsumptionMapper cDao;
	
	@Resource
	private AuthenticationCashMapper aDao;
	
	@Resource
	private IntegralConsumeMapper iDao;

	@Override
	public ResultBean<User> wxRegister(String openid, String nickname, String portrait) {
		// TODO Auto-generated method stub
		User user=new User();
		user.setOpenid(openid);
		user.setNickname(nickname);
		user.setIntegral(0);
		user.setBalance(0.00);
		user.setPortrait(portrait);
		try {
			List<User> list=dao.selectList(new EntityWrapper<User>().eq("openid", openid));
			if(list.size()>0) {
				return new ResultBean<>(new MyException("该用户已存在"));
			}
			int count=dao.insert(user);
			if(count>0) {
				return new ResultBean<User>(user);
			}else {
				return new ResultBean<>(new MyException("数据库操作失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<User> updateUser(User user) {
		// TODO Auto-generated method stub
		try {
			User us=dao.selectById(user.getId());
			if(user.getName()!=null&&!user.getName().equals("")) {
				us.setName(user.getName());
			}
			if(user.getPortrait()!=null&&!user.getPortrait().equals("")) {
				us.setPortrait(user.getPortrait());
			}
			if(user.getSex()!=null) {
				us.setSex(user.getSex());
			}
			if(user.getBirthday()!=null&&!user.getBirthday().equals("")) {
				us.setBirthday(user.getBirthday());
			}
			if(user.getPhone()!=null&&!user.getPhone().equals("")) {
				us.setPhone(user.getPhone());
			}
			if(user.getEmail()!=null&&!user.getEmail().equals("")) {
				us.setEmail(user.getEmail());
			}
			if(user.getWechat()!=null&&!user.getWechat().equals("")) {
				us.setWechat(user.getWechat());
			}
			if(user.getAge()!=null&&user.getAge()!=0) {
				us.setAge(user.getAge());
			}
			int count=dao.updateById(us);
			if(count>0) {
				return new ResultBean<User>(us);
			}else {
				return new ResultBean<>(new MyException("数据库操作失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> userDateils(int id) {
		// TODO Auto-generated method stub
		Map<String, Object> map=new HashMap<>();
		User user=dao.selectById(id);
		List<AuthenticationCash> ac=aDao.selectList(new EntityWrapper<AuthenticationCash>().eq("userid", user.getId()));
		Boolean isjiao=false;
		if(ac!=null&&ac.size()>0) {
			AuthenticationCash auc=ac.get(0);
			if(auc.getStates()==1) {
				isjiao=true;
			}
			map.put("bao", auc.getMoney());
		}else {
			map.put("bao", 0);
		}
		Boolean isbang=false;
		if(user.getPhone()!=null&&!user.getPhone().equals("")) {
			isbang=true;
		}
		map.put("user", user);
		map.put("isjiao", isjiao);
		map.put("isbang", isbang);
		
		return new ResultBean<>(map);
	}

	@Override
	public int updateOpenId(int userId, String openId) {
		// TODO Auto-generated method stub
		return dao.updateOpenId(userId, openId);
	}

	@Override
	public ResultBean<User> getOpenId(String openId) {
		// TODO Auto-generated method stub
		try {
			User user=dao.selectList(new EntityWrapper<User>().eq("openid", openId)).get(0);
			return new ResultBean<>(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public User getById(String openId) {
		// TODO Auto-generated method stub
		return dao.selectByOpenId(openId);
	}

	@Override
	public int updateUsers(User user) {
		// TODO Auto-generated method stub
		return dao.updateAllColumnById(user);
	}

	@Override
	public int addUser(User user) {
		// TODO Auto-generated method stub
		user.setIntegral(0);
		user.setBalance(0.00);
		return dao.insert(user);
	}
	

	@Override
	public Map<String,Object> findBelance(String userId) {
		// TODO Auto-generated method stub
		return dao.findBalance(userId);
	}

	@Override
	public User getByUserId(int id) {
		// TODO Auto-generated method stub
		return dao.selectById(id);
	}

	@Override
	public ResultBean<User> updatePhone(int userId, String phone) {
		// TODO Auto-generated method stub
		try {
			User user=dao.selectById(userId);
			user.setPhone(phone);
			int count=dao.updateAllColumnById(user);
			if(count>0) {
				return new ResultBean<>(user);
			}else {
				return new ResultBean<>(new MyException("手机修改失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<User> recharge(int userId, Double money) {
		// TODO Auto-generated method stub
		try {
			User user=dao.selectById(userId);
			Double qian=user.getBalance()+money;
			user.setBalance(qian);
			int count=dao.updateAllColumnById(user);
			Consumption cm=new Consumption();
			cm.setContent("充值");
			cm.setIspay(true);
			cm.setMoney(money);
			cm.setTime(new Date());
			cm.setUserid(userId);
			cDao.insert(cm);
			if(count>0) {
				return new ResultBean<>(user);
			}else {
				return new ResultBean<>(new MyException("充值失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<User> withdrawals(HttpServletRequest request, HttpServletResponse response,int userId, Double money) {
		// TODO Auto-generated method stub
		try {
			User user=dao.selectById(userId);
			if(user.getBalance()>=money) {
				Boolean pan=WxPayUtil.transferPay(request, response, user.getOpenid(), money.toString());
				if(pan) {
					Double qian=user.getBalance()-money;
					user.setBalance(qian);
					int count=dao.updateAllColumnById(user);
					Consumption cm=new Consumption();
					cm.setContent("提现");
					cm.setIspay(false);
					cm.setMoney(money);
					cm.setTime(new Date());
					cm.setUserid(userId);
					cDao.insert(cm);
					if(count>0) {
						return new ResultBean<>(user);
					}else {
						return new ResultBean<>(new MyException("提现失败"));
					}
				}else {
					return new ResultBean<>(new MyException("提现失败"));
				}
			}else {
				return new ResultBean<>(new MyException("余额不足"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public int updateIntegral(int integral, int userId,Boolean isconsume) {
		// TODO Auto-generated method stub
		User user=dao.selectById(userId);
		int ji=integral+user.getIntegral();
		int count=dao.updateIntegral(ji, userId);
		IntegralConsume ic=new IntegralConsume();
		ic.setContent("积分变更");
		ic.setIntegral(integral);
		ic.setIsconsume(isconsume);
		ic.setTime(new Date());
		ic.setUserid(userId);
		iDao.insert(ic);
		return count;
	}

	@Override
	public ResultBean<User> userDateilsByPhone(String phone) {
		// TODO Auto-generated method stub
		try {
			User user=dao.userDateilsByPhone(phone);
			return new ResultBean<User>(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<User>(new MyException("帐号重复"));
		}
	}
	
	
}

package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.TeacherAuthenticationcreditMapper;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthenticationcredit;
import com.medical.service.TeacherAuthenticationcreditService;

@Service
@Transactional
public class TeacherAuthenticationcreditServiceImpl implements TeacherAuthenticationcreditService {
	@Resource
	private TeacherAuthenticationcreditMapper dao;
	
	@Override
	public ResultBean<TeacherAuthenticationcredit> addTeacherAuthenticationcredit(TeacherAuthenticationcredit tac) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insertAllColumn(tac);
			if(count>0) {
				return new ResultBean<>(tac);
			}else {
				return new ResultBean<>(new MyException("插入失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateTeacherAuthenticationcredit(TeacherAuthenticationcredit tac) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(tac);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<TeacherAuthenticationcredit>> getList(int authenticationid) {
		// TODO Auto-generated method stub
		try {
			List<TeacherAuthenticationcredit> ac=dao.selectList(new EntityWrapper<TeacherAuthenticationcredit>().eq("authenticationid", authenticationid));
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<TeacherAuthenticationcredit>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<TeacherAuthenticationcredit> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				String[] data=ids.split(",");
				for(String da:data) {
					int id=Integer.parseInt(da);
					TeacherAuthenticationcredit cs=dao.selectById(id);
					list.add(cs);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

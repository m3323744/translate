package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.LanguageGradeMapper;
import com.medical.entity.LanguageGrade;
import com.medical.entity.ResultBean;
import com.medical.service.LanguageGradeService;

@Service
@Transactional
public class LanguageGradeServiceImpl implements LanguageGradeService {
	@Resource
	private LanguageGradeMapper dao;
	
	@Override
	public ResultBean<List<LanguageGrade>> getByLanguageId(int languageId) {
		// TODO Auto-generated method stub
		try {
			List<LanguageGrade> list=dao.selectList(new EntityWrapper<LanguageGrade>().eq("languageid", languageId));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

package com.medical.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.InterpreterOrderMapper;
import com.medical.entity.InterpreterOrder;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterOrderService;

@Service
@Transactional
public class InterpreterOrderServiceImpl implements InterpreterOrderService {
	@Resource
	private InterpreterOrderMapper dao;

	@Override
	public ResultBean<InterpreterOrder> addInterpreterOrder(InterpreterOrder inter) {
		// TODO Auto-generated method stub
		try {
			inter.setStates(1);
			inter.setTime(new Date());
			int count=dao.insert(inter);
			if(count>0) {
				return new ResultBean<>(inter);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	
}

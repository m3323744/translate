package com.medical.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.IntegralConsumeMapper;
import com.medical.entity.IntegralConsume;
import com.medical.service.IntegralConsumeService;

@Service
public class IntegralConsumeServiceImpl implements IntegralConsumeService {

	@Autowired 
	private IntegralConsumeMapper intrgralConsumeDao;
	@Override
	public List<IntegralConsume> findByPage(int page,int userId) {
		// TODO Auto-generated method stub
		List<IntegralConsume> list=intrgralConsumeDao.selectPage(new Page<IntegralConsume>(page, 10), new EntityWrapper<IntegralConsume>().eq("userid", userId).orderBy("time", false));
		return list;
	}
	
	@Override
	public Integer findByPageCount(int userId) {
		// TODO Auto-generated method stub
		int count=intrgralConsumeDao.selectCount( new EntityWrapper<IntegralConsume>().eq("userid", userId).orderBy("time", false));
		return count;
	}

}

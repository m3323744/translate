package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.CourseEvaluateMapper;
import com.medical.entity.CourseEvaluate;
import com.medical.entity.ResultBean;
import com.medical.service.CourseEvaluateService;

@Service
@Transactional
public class CourseEvaluateServiceImpl implements CourseEvaluateService {
	@Resource
	private CourseEvaluateMapper dao;

	@Override
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> te=dao.selectByOrderId(orderId);
			return new ResultBean<>(te);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> addEvaluate(CourseEvaluate eva) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(eva);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectByInterpreterId(int interpreterId, int type) {
		// TODO Auto-generated method stub
		StringBuffer sb=new StringBuffer("");
		if(type==1) {
			sb.append(" LEFT JOIN t_course_offline as co ON co.id=te.courseid WHERE te.courseid="+interpreterId);
		}else if(type==2) {
			sb.append(" LEFT JOIN t_course_seeing as co ON co.id=te.courseid WHERE te.courseid="+interpreterId);
		}else if(type==3) {
			sb.append(" LEFT JOIN t_course_streaming as co ON co.id=te.courseid WHERE te.courseid="+interpreterId);
		}
		try {
			Map<String, Object> map=new HashMap<>();
			List<Map<String, Object>> list=dao.selectByTeacherid(sb.toString());
			Double score=dao.selectAvgScoreByeacherId(interpreterId,type);
			map.put("evaluate", list);
			map.put("score", score);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> selectBySchoolId(int schoolId) {
		// TODO Auto-generated method stub
		try {
			List<Map<String, Object>> te=dao.selectBySchoolId(schoolId);
			return new ResultBean<>(te);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	
}

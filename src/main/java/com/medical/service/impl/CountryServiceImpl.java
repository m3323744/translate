package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CountryMapper;
import com.medical.entity.Country;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.CountryService;

@Service
@Transactional
public class CountryServiceImpl implements CountryService {
	@Resource
	private CountryMapper dao;
	
	@Override
	public ResultBean<List<Country>> getList() {
		// TODO Auto-generated method stub
		try {
			List<Country> list=dao.selectList(new EntityWrapper<Country>().orderBy("convert(name using gbk) asc"));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

}

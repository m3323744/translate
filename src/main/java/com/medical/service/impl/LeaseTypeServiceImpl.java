package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.LeaseTypeMapper;
import com.medical.entity.LeaseType;
import com.medical.entity.ResultBean;
import com.medical.service.LeaseTypeService;

@Service
@Transactional
public class LeaseTypeServiceImpl implements LeaseTypeService {
	@Resource
	private LeaseTypeMapper dao;
	
	@Override
	public ResultBean<List<LeaseType>> getList() {
		// TODO Auto-generated method stub
		try {
			List<LeaseType> list=dao.selectList(new EntityWrapper<LeaseType>());
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

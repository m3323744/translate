package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationCertificateMapper;
import com.medical.dao.AuthenticationLanguageMapper;
import com.medical.dao.AuthenticationSchoolMapper;
import com.medical.dao.AuthenticationStudyingMapper;
import com.medical.dao.InterpreterAuthenticationMapper;
import com.medical.dao.InterpreterAuthenticationTerritoryMapper;
import com.medical.entity.InterpreterAuthentication;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterAuthenticationService;

@Service
@Transactional
public class InterpreterAuthenticationServiceImpl implements InterpreterAuthenticationService {

	@Resource
	private InterpreterAuthenticationMapper dao;
	
	@Resource
	private AuthenticationCertificateMapper acfDao;
	
	@Resource
	private AuthenticationLanguageMapper aclDao;
	
	@Resource
	private AuthenticationSchoolMapper acsDao;
	
	@Resource
	private AuthenticationStudyingMapper asDao;
	
	@Resource
	private InterpreterAuthenticationTerritoryMapper iatDao;
	
	@Override
	public ResultBean<Integer> addInterpreterAuthentication(InterpreterAuthentication ia, String lanuage,
			String certificate, String school, String studying, String territory) {
		// TODO Auto-generated method stub
		try {
			List<InterpreterAuthentication> inte=dao.selectList(new EntityWrapper<InterpreterAuthentication>().eq("userid", ia.getUserid()).and().eq("interpretertype", ia.getInterpretertype()));
			if(inte.size()>0) {
				return new ResultBean<>(new MyException("已经认证过"));
			}
			ia.setStates(1);
			ia.setTime(new Date());
			int count=dao.insert(ia);
			if(count>0) {
				if(lanuage!=null&&!lanuage.equals("")) {
					String[] data=lanuage.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						aclDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(certificate!=null&&!certificate.equals("")) {
					String[] data=certificate.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acfDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(school!=null&&!school.equals("")) {
					String[] data=school.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acsDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(studying!=null&&!studying.equals("")) {
					String[] data=studying.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						asDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(territory!=null&&!territory.equals("")) {
					String[] data=territory.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						iatDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				return new ResultBean<>(ia.getId());
			}else {
				return new ResultBean<>(new MyException("插入异常"));
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<Map<String, Object>> interpreterAuthenticationDateils(int userId,int typeId) {
		// TODO Auto-generated method stub
		try {
			InterpreterAuthentication ta=dao.getByUserid(userId,typeId);
			Map<String, Object> map=new HashMap<>();
			if(ta==null) {
				return new ResultBean<>(map);
			}else {
				
				map.put("InterpreterAuthenticationTerritory", ta);
				map.put("lanuage", aclDao.selectByAuthenticationid(ta.getId(),2));
				map.put("certificate", acfDao.selectByAuthenticationid(ta.getId(),2));
				map.put("school", acsDao.selectByAuthenticationid(ta.getId(),2));
				map.put("studying", asDao.selectByAuthenticationid(ta.getId(),2));
				map.put("territory", iatDao.selectByAuthenticationid(ta.getId()));
				return new ResultBean<>(map);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<Integer> updateInterpreterAuthentication(InterpreterAuthentication ia, String lanuage,
			String certificate, String school, String studying, String territory) {
		// TODO Auto-generated method stub
		try {
			ia.setStates(1);
			int count=dao.updateById(ia);
			if(count>0) {
				if(lanuage!=null&&!lanuage.equals("")) {
					//aclDao.delete(new EntityWrapper<AuthenticationLanguage>().eq("authenticationid", ia.getId()));
					String[] data=lanuage.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						aclDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(certificate!=null&&!certificate.equals("")) {
					//acfDao.delete(new EntityWrapper<AuthenticationCertificate>().eq("authenticationid", ia.getId()));
					String[] data=certificate.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acfDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(school!=null&&!school.equals("")) {
					//acsDao.delete(new EntityWrapper<AuthenticationSchool>().eq("authenticationid", ia.getId()));
					String[] data=school.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						acsDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(studying!=null&&!studying.equals("")) {
					//asDao.delete(new EntityWrapper<AuthenticationStudying>().eq("authenticationid", ia.getId()));
					String[] data=studying.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						asDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				if(territory!=null&&!territory.equals("")) {
					//iatDao.delete(new EntityWrapper<InterpreterAuthenticationTerritory>().eq("interpreterid", ia.getId()));
					String[] data=territory.split(",");
					for(String da:data) {
						int id=Integer.parseInt(da);
						iatDao.updateAuthenticationid(ia.getId(), id);
					}
				}
				return new ResultBean<>(ia.getId());
			}else {
				return new ResultBean<>(new MyException("更新异常"));
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

}

package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.wantedMapper;
import com.medical.dao.wantedcertificateMapper;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.Wanted;
import com.medical.entity.Wantedcertificate;
import com.medical.service.WantedService;
import com.medical.util.DataUtil;

@Service
@Transactional
public class WantedServiceImpl implements WantedService {
	@Resource
	private wantedMapper dao;
	
	@Resource
	private wantedcertificateMapper wDao;
	
	@Override
	public ResultBean<Integer> addWanted(Wanted wanted,String wantedcertificates) {
		// TODO Auto-generated method stub
		int num=dao.addCountByTime(DataUtil.getDate()+" 00:00:00", DataUtil.getDate()+" 23:59:59", wanted.getUserid());
		if(num>2) {
			return new ResultBean<>(new MyException("今日发布不能超过2条"));
		}
		wanted.setTime(new Date());
		wanted.setStates(1);
		try {
			dao.insert(wanted);
			if(wantedcertificates!=null&&!wantedcertificates.equals("")&&!wantedcertificates.equals(",")) {
				String[] wantedcertificate=wantedcertificates.split(",");
				for(String wan:wantedcertificate) {
					int id=Integer.parseInt(wan);
					wDao.updateWantedId(id, wanted.getId());
				}
			}
			return new ResultBean<>(wanted.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getList(int pageIndex, String keyWord,int size) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			List<Wanted> list=dao.selectPage(new Page<>(pageIndex, size), new EntityWrapper<Wanted>().like("title", keyWord).or().like("name", keyWord).and().eq("states", 2));
			int count=dao.selectCount(new EntityWrapper<Wanted>().like("title", keyWord).or().like("name", keyWord).and().eq("states", 2));
			map.put("wantedList", list);
			map.put("count", count);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map=new HashMap<>();
			Wanted wd=dao.selectById(id);
			List<Wantedcertificate> list=wDao.selectList(new EntityWrapper<Wantedcertificate>().eq("wantedid", id));
			map.put("Wanted", wd);
			map.put("Wantedcertificate", list);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Wanted>> selectByUserId(int userId, int pageIndex, int size) {
		// TODO Auto-generated method stub
		try {
			List<Wanted> list=dao.selectPage(new Page<Wanted>(pageIndex, size),new EntityWrapper<Wanted>().eq("userid", userId));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delWanted(int id) {
		// TODO Auto-generated method stub
		try {
			wDao.delete(new EntityWrapper<Wantedcertificate>().eq("wantedid", id));
			int count=dao.deleteByPrimaryKey(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateWanted(Wanted wanted, String wantedcertificates) {
		// TODO Auto-generated method stub
		//wDao.delete(new EntityWrapper<Wantedcertificate>().eq("wantedid", wanted.getId()));
		if(wantedcertificates!=null&&!wantedcertificates.equals("")&&!wantedcertificates.equals(",")) {
			String[] wantedcertificate=wantedcertificates.split(",");
			for(String wan:wantedcertificate) {
				int id=Integer.parseInt(wan);
				wDao.updateWantedId(id, wanted.getId());
			}
		}
		int count=dao.updateAllColumnById(wanted);
		return new ResultBean<>(count);
	}

	@Override
	public ResultBean<Integer> getListCount(String keyWord) {
		// TODO Auto-generated method stub
		int count=dao.selectCount(new EntityWrapper<Wanted>().like("title", keyWord).or().like("name", keyWord));
		return new ResultBean<Integer>(count);
	}

	@Override
	public ResultBean<Integer> selectByUserIdCount(int userId) {
		// TODO Auto-generated method stub
		try {
			List<Wanted> list=dao.selectList(new EntityWrapper<Wanted>().eq("userid", userId));
			return new ResultBean<>(list.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

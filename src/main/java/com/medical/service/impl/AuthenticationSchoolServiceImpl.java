package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationSchoolMapper;
import com.medical.entity.AuthenticationSchool;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationSchoolService;

@Service
@Transactional
public class AuthenticationSchoolServiceImpl implements AuthenticationSchoolService {
	@Resource
	private AuthenticationSchoolMapper dao;
	
	@Override
	public ResultBean<Integer> addSchool(AuthenticationSchool acs) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insertAllColumn(acs);
			if(count>0) {
				return new ResultBean<>(acs.getId());
			}else {
				return new ResultBean<>(new MyException("插入失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<AuthenticationSchool> getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			AuthenticationSchool ac=dao.selectById(id);
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<AuthenticationSchool>> getList(int authenticationid, int isteacher) {
		// TODO Auto-generated method stub
		try {
			List<AuthenticationSchool> ac=dao.selectList(new EntityWrapper<AuthenticationSchool>().eq("authenticationid", authenticationid).and().eq("isteacher", isteacher));
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<AuthenticationSchool>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<AuthenticationSchool> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				String[] data=ids.split(",");
				for(String da:data) {
					int id=Integer.parseInt(da);
					AuthenticationSchool cs=dao.selectById(id);
					list.add(cs);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delSchool(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.deleteById(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateAuthenticationSchool(AuthenticationSchool acs) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(acs);
			if(count>0) {
				return new ResultBean<>(acs.getId());
			}else {
				return new ResultBean<>(new MyException("更新失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

package com.medical.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.EnquaryMapper;
import com.medical.entity.Enquary;
import com.medical.entity.ResultBean;
import com.medical.service.EnquaryService;

@Service
@Transactional
public class EnquaryServiceImpl implements EnquaryService {
	@Resource
	private EnquaryMapper dao;

	@Override
	public ResultBean<Integer> addEnquary(Enquary enq) {
		// TODO Auto-generated method stub
		int count=dao.insert(enq);
		return new ResultBean<Integer>(count);
	}
	
}

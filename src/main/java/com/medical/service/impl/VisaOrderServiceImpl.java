package com.medical.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.VisaOrderMapper;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.VisaOrder;
import com.medical.service.VisaOrderSerivce;

@Service
@Transactional
public class VisaOrderServiceImpl implements VisaOrderSerivce {
	@Resource
	private VisaOrderMapper dao;

	@Override
	public ResultBean<VisaOrder> addVisaOrder(VisaOrder vo) {
		// TODO Auto-generated method stub
		try {
			vo.setPaystates(1);
			vo.setTime(new Date());
			int count=dao.insert(vo);
			if(count>0) {
				return new ResultBean<>(vo);
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
}

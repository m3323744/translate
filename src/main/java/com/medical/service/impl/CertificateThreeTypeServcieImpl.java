package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CertificateThreeTypeMapper;
import com.medical.entity.CertificateThreeType;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.CertificateThreeTypeService;

@Service
@Transactional
public class CertificateThreeTypeServcieImpl implements CertificateThreeTypeService {
	@Resource
	private CertificateThreeTypeMapper dao;
	
	@Override
	public ResultBean<List<CertificateThreeType>> getByTwoId(int twotypeId) {
		// TODO Auto-generated method stub
		try {
			List<CertificateThreeType> list=dao.selectList(new EntityWrapper<CertificateThreeType>().eq("twotypeid", twotypeId));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

}

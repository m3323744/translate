package com.medical.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.InterpreterAuthenticationTerritoryMapper;
import com.medical.entity.InterpreterAuthenticationTerritory;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterAuthenticationTerritoryService;

@Service
@Transactional
public class InterpreterAuthenticationTerritoryServiceImpl implements InterpreterAuthenticationTerritoryService {

	@Resource
	private InterpreterAuthenticationTerritoryMapper dao;
	
	@Override
	public ResultBean<Integer> addInterpreterAuthenticationTerritory(InterpreterAuthenticationTerritory iat) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insert(iat);
			if(count>0) {
				return new ResultBean<>(iat.getId());
			}else {
				return new ResultBean<>(new MyException("系统异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<InterpreterAuthenticationTerritory> getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			InterpreterAuthenticationTerritory ac=dao.selectById(id);
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<InterpreterAuthenticationTerritory>> getList(int interpreterid) {
		// TODO Auto-generated method stub
		try {
			List<InterpreterAuthenticationTerritory> ac=dao.selectList(new EntityWrapper<InterpreterAuthenticationTerritory>().eq("interpreterid", interpreterid));
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<InterpreterAuthenticationTerritory>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<InterpreterAuthenticationTerritory> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				String[] data=ids.split(",");
				for(String da:data) {
					int id=Integer.parseInt(da);
					InterpreterAuthenticationTerritory cs=dao.selectById(id);
					list.add(cs);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<InterpreterAuthenticationTerritory>> getListByUserid(int userId) {
		// TODO Auto-generated method stub
		try {
			List<InterpreterAuthenticationTerritory> ac=dao.getListByUserid(userId);
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delTerritory(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.deleteById(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateInterpreterAuthenticationTerritory(InterpreterAuthenticationTerritory iat) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(iat);
			if(count>0) {
				return new ResultBean<>(iat.getId());
			}else {
				return new ResultBean<>(new MyException("系统异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}
	
}

package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.ConventionApplyMapper;
import com.medical.dao.ConventionMapper;
import com.medical.entity.Convention;
import com.medical.entity.ConventionApply;
import com.medical.service.OrderService;

public class OrderConventionServiceImpl implements OrderService {

	@Resource
	private ConventionMapper ctDao;
	
	@Resource
	private ConventionApplyMapper ctaDao;
	
	public OrderConventionServiceImpl(ConventionMapper ctDao,ConventionApplyMapper ctaDao) {
		// TODO Auto-generated constructor stub
		this.ctDao=ctDao;
		this.ctaDao=ctaDao;
	}
	
	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		Map<String, Object> map=new HashMap<>();
		ConventionApply ca=ctaDao.selectById(id);
		Convention cf=ctDao.selectById(ca.getConventionid());
		List<ConventionApply> caList=ctaDao.selectList(new EntityWrapper<ConventionApply>().eq("conventionid", cf.getId()));
		map.put("convention", cf);
		map.put("conventionApplyList", caList);
		return map;
	}

	@Override
	public int updateState(int orderId, int states,int paytype) {
		// TODO Auto-generated method stub
		ConventionApply convention=ctaDao.selectById(orderId);
		convention.setId(orderId);
		convention.setStates(states);
		if(paytype!=0) {
			convention.setPaytype(paytype);
		}
		return ctaDao.updateAllColumnById(convention);
	}

	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		ConventionApply convention=ctaDao.selectById(orderId);
		convention.setId(orderId);
		convention.setStates(states);
		return ctaDao.updateAllColumnById(convention);
	}

}

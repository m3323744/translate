package com.medical.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import com.medical.dao.CourseOrderMapper;
import com.medical.entity.CourseOrder;
import com.medical.service.OrderService;

public class OrderCourseServiceImpl implements OrderService {

	@Resource
	private CourseOrderMapper cDao;
	
	
	public OrderCourseServiceImpl(CourseOrderMapper cDao) {
		this.cDao=cDao;
	}

	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		return cDao.getStreamingOrderDateils(id);
	}

	@Override
	public int updateState(int orderId, int states,int paytype) {
		// TODO Auto-generated method stub
		CourseOrder courOrder=cDao.selectById(orderId);
		courOrder.setId(orderId);
		courOrder.setStates(states);
		if(paytype!=0) {
			courOrder.setPaytype(paytype);
		}
		return cDao.updateAllColumnById(courOrder);
	}

	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		CourseOrder courOrder=cDao.selectById(orderId);
		courOrder.setId(orderId);
		courOrder.setStates(states);
		return cDao.updateAllColumnById(courOrder);
	}

	

}

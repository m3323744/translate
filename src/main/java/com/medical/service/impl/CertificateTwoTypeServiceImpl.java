package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CertificateTwoTypeMapper;
import com.medical.entity.CertificateTwoType;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.CertificateTwoTypeService;

@Service
@Transactional
public class CertificateTwoTypeServiceImpl implements CertificateTwoTypeService {
	@Resource
	private CertificateTwoTypeMapper dao;
	
	@Override
	public ResultBean<List<CertificateTwoType>> getList() {
		// TODO Auto-generated method stub
		try {
			List<CertificateTwoType> list=dao.selectList(new EntityWrapper<CertificateTwoType>().orderBy("isyu", false));
			return new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("查询异常"));
		}
	}

}

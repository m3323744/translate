package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.ConferenceApplyMapper;
import com.medical.dao.ConferenceMapper;
import com.medical.entity.Conference;
import com.medical.entity.ConferenceApply;
import com.medical.service.OrderService;

public class OrderConferenceServiceImpl implements OrderService {

	@Resource
	private ConferenceMapper cfDao;
	
	@Resource
	private ConferenceApplyMapper cfaDao;
	
	public OrderConferenceServiceImpl(ConferenceMapper cfDao,ConferenceApplyMapper cfaDao) {
		// TODO Auto-generated constructor stub
		this.cfDao=cfDao;
		this.cfaDao=cfaDao;
	}
	
	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		Map<String, Object> map=new HashMap<>();
		ConferenceApply ca=cfaDao.selectById(id);
		Conference cf=cfDao.selectById(ca.getConferenceid());
		List<ConferenceApply> caList=cfaDao.selectList(new EntityWrapper<ConferenceApply>().eq("conferenceid", cf.getId()));
		map.put("conference", cf);
		map.put("conferenceApplyList", caList);
		return map;
	}

	@Override
	public int updateState(int orderId, int states,int paytype) {
		// TODO Auto-generated method stub
		ConferenceApply cf=cfaDao.selectById(orderId);
		cf.setId(orderId);
		cf.setStates(states);
		if(paytype!=0) {
			cf.setPay(paytype);
		}
		return cfaDao.updateAllColumnById(cf);
	}

	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		ConferenceApply cf=cfaDao.selectById(orderId);
		cf.setId(orderId);
		cf.setStates(states);
		return cfaDao.updateAllColumnById(cf);
	}

}

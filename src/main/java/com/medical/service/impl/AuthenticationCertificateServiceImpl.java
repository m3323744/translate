package com.medical.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.AuthenticationCertificateMapper;
import com.medical.dao.CertificateThreeTypeMapper;
import com.medical.dao.CertificateTwoTypeMapper;
import com.medical.entity.AuthenticationCertificate;
import com.medical.entity.CertificateThreeType;
import com.medical.entity.CertificateTwoType;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationCertificateService;

@Service
@Transactional
public class AuthenticationCertificateServiceImpl implements AuthenticationCertificateService {

	@Resource
	private AuthenticationCertificateMapper dao;
	
	@Resource
	private CertificateTwoTypeMapper tDao;
	
	@Resource
	private CertificateThreeTypeMapper rDao;
	
	@Override
	public ResultBean<Integer> addAuthenticationCertificate(AuthenticationCertificate ace) {
		// TODO Auto-generated method stub
		try {
			int count=dao.insertAllColumn(ace);
			if(count>0) {
				return new ResultBean<>(ace.getId());
			}else {
				return new ResultBean<>(new MyException("插入失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			AuthenticationCertificate ac=dao.selectById(id);
			Map<String, Object> map=new HashMap<>();
			map.put("AuthenticationCertificate", ac);
			CertificateTwoType tt=tDao.selectById(ac.getTwoid());
			CertificateThreeType ct=rDao.selectById(ac.getThreeid());
			map.put("CertificateTwoType", tt);
			map.put("CertificateThreeType", ct);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<AuthenticationCertificate>> getList(int authenticationid,int isteacher) {
		// TODO Auto-generated method stub
		try {
			List<AuthenticationCertificate> ac=dao.selectList(new EntityWrapper<AuthenticationCertificate>().eq("authenticationid", authenticationid).and().eq("isteacher", isteacher));
			return new ResultBean<>(ac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> selectByIds(String ids) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> list=new ArrayList<>();
		try {
			if(ids!=null&&!ids.equals("")) {
				
				String[] data=ids.split(",");
				for(String da:data) {
					Map<String, Object> map=new HashMap<>();
					int id=Integer.parseInt(da);
					AuthenticationCertificate cs=dao.selectById(id);
					map.put("AuthenticationCertificate", cs);
					CertificateTwoType tt=tDao.selectById(cs.getTwoid());
					CertificateThreeType ct=rDao.selectById(cs.getThreeid());
					map.put("CertificateTwoType", tt);
					map.put("CertificateThreeType", ct);
					list.add(map);
				}
			}
			return new ResultBean<>(list);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delCertificate(int id) {
		// TODO Auto-generated method stub
		try {
			int count=dao.deleteById(id);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> updateAuthenticationCertificate(AuthenticationCertificate ace) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(ace);
			if(count>0) {
				return new ResultBean<>(ace.getId());
			}else {
				return new ResultBean<>(new MyException("更新失败"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

}

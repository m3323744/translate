package com.medical.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.IntegralCommodityMapper;
import com.medical.dao.IntegralConsumeMapper;
import com.medical.dao.IntegralConversionMapper;
import com.medical.dao.UserMapper;
import com.medical.entity.IntegralCommodity;
import com.medical.entity.IntegralConsume;
import com.medical.entity.IntegralConversion;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.IntegralCommodityService;

@Service
@Transactional
public class IntegralCommodityServiceImpl implements IntegralCommodityService {
	
	@Resource
	private IntegralCommodityMapper dao;
	
	@Resource
	private IntegralConversionMapper icDao;
	
	@Resource
	private UserMapper uDao;
	
	@Resource
	private IntegralConsumeMapper icmDao;
	
	@Override
	public ResultBean<List<IntegralCommodity>> getList() {
		// TODO Auto-generated method stub
		return new ResultBean<>(dao.selectList(new EntityWrapper<IntegralCommodity>()));
	}

	@Override
	public ResultBean<Integer> exchange(IntegralConversion ic) {
		// TODO Auto-generated method stub
		try {
			User user=uDao.selectById(ic.getUserid());
			if(user.getIntegral()>=ic.getIntegral()) {
				int ji=user.getIntegral()-ic.getIntegral();
				user.setIntegral(ji);
				uDao.updateAllColumnById(user);
				IntegralConsume icm=new IntegralConsume();
				icm.setContent("兑换商品");
				icm.setIntegral(ic.getIntegral());
				icm.setIsconsume(true);
				icm.setTime(new Date());
				icm.setUserid(ic.getUserid());
				icmDao.insert(icm);
				ic.setTime(new Date());
				int count=icDao.insert(ic);
				return new ResultBean<>(count);
			}else {
				return new ResultBean<>(new MyException("用户积分不足兑换"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> sign(IntegralConsume ic) {
		// TODO Auto-generated method stub
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
			List<IntegralConsume> list=icmDao.selectList(new EntityWrapper<IntegralConsume>().between("time", df.format(ic.getTime())+" 00:00:00", df.format(ic.getTime())+" 23:59:59").and().eq("userid", ic.getUserid()));
			if(list.size()>0) {
				return new ResultBean<>(new MyException("今日签到过"));
			}
			User user=uDao.selectById(ic.getUserid());
			int ji=user.getIntegral()+ic.getIntegral();
			user.setIntegral(ji);
			uDao.updateAllColumnById(user);
			ic.setIsconsume(false);
			ic.setContent("每日签到");
			int count=icmDao.insert(ic);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getListCount(int pageIndex,int size) {
		// TODO Auto-generated method stub
		List<IntegralCommodity> list=dao.selectPage(new Page<>(pageIndex, size),new EntityWrapper<IntegralCommodity>());
		int count=dao.selectCount(new EntityWrapper<IntegralCommodity>());
		Map<String, Object> map=new HashMap<>();
		map.put("commodityList", list);
		map.put("count", count);
		return new ResultBean<Map<String, Object>>(map);
	}

	@Override
	public ResultBean<IntegralCommodity> selectById(int id) {
		// TODO Auto-generated method stub
		IntegralCommodity il=dao.selectById(id);
		return new ResultBean<IntegralCommodity>(il);
	}

}

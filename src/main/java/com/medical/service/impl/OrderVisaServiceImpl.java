package com.medical.service.impl;

import java.util.Map;

import com.medical.dao.VisaOrderMapper;
import com.medical.entity.VisaOrder;
import com.medical.service.OrderService;

public class OrderVisaServiceImpl implements OrderService{
	
	private VisaOrderMapper vDao;
	public OrderVisaServiceImpl(VisaOrderMapper vDao) {
		// TODO Auto-generated constructor stub
		this.vDao=vDao;
	}

	@Override
	public Map<String, Object> getOrderDateils(int id) {
		// TODO Auto-generated method stub
		return vDao.getOrderDateils(id);
	}

	@Override
	public int updateState(int orderId, int states) {
		// TODO Auto-generated method stub
		VisaOrder visaOrder=vDao.selectById(orderId);
		visaOrder.setId(orderId);
		visaOrder.setPaystates(states);
		return vDao.updateAllColumnById(visaOrder);
	}

	@Override
	public int updateState(int orderId, int states, int paytype) {
		// TODO Auto-generated method stub
		VisaOrder visaOrder=vDao.selectById(orderId);
		visaOrder.setId(orderId);
		visaOrder.setPaystates(states);
		if(paytype!=0) {
			visaOrder.setPaytype(paytype);
		}
		return vDao.updateAllColumnById(visaOrder);
	}

	

}

package com.medical.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.ChatMapper;
import com.medical.dao.UserMapper;
import com.medical.entity.Chat;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.ChatService;
import com.medical.util.DateHandle;

@Service
@Transactional
public class ChatServiceImpl implements ChatService {

	@Resource
	private ChatMapper chatDao;
	
	@Resource
	private UserMapper uDao;
	
	@Override
	public ResultBean<List<Map<String, Object>>> sendChat(Integer toUserId, Integer fromUserId, String type,
			String content, Integer currentId) {
		// TODO Auto-generated method stub
		Integer showTime=0;
		Chat lastChat=chatDao.getLastShowTime(toUserId, fromUserId);
		Long currentTimeMillis=System.currentTimeMillis();
		if(lastChat==null||(currentTimeMillis-DateHandle.stringToTimestamp(DateHandle.MS, lastChat.getTime()))>5*60*1000) {
			showTime=1;
		}
		String time=DateHandle.getCurrent();
		Chat chat=new Chat();
		chat.setContent(content);
		chat.setType(type);
		chat.setFromUserId(fromUserId);
		chat.setToUserId(toUserId);
		chat.setTime(time);
		chat.setShowTime(showTime);
		chatDao.sendChat(chat);
		if(currentId==null) {
			currentId=0;
		}
		List<Map<String, Object>> list=chatDao.getNightChatList(toUserId, fromUserId,currentId);
		return new ResultBean<List<Map<String,Object>>>(list);
	}

	@Override
	public ResultBean<Map<String, Object>> getChatList(Integer toUserId, Integer fromUserId, String type,
			Integer currentId) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> list=null;
		if(type==null||type.length()==0) {
			list = chatDao.getChatList(toUserId, fromUserId);
		}else if(type.equals("early")) {
			list = chatDao.getEarlyChatList(toUserId, fromUserId,currentId);
		}else if(type.equals("night")){
			list = chatDao.getNightChatList(toUserId, fromUserId,currentId);
		}else {
			return new ResultBean<>(new MyException("参数错误"));
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", list.size());
		return new ResultBean<Map<String,Object>>(map);
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getByUserId(int userId, int page, int size) {
		// TODO Auto-generated method stub
		List<Chat> list=chatDao.getByUserId(userId, (page-1)*size, size);
		List<Map<String, Object>> listMap=new ArrayList<>();
		for(Chat ca:list) {
			Map<String, Object> map=new HashMap<>();
			map.put("chat", ca);
			int id=0;
			if(ca.getFromUserId()!=userId) {
				id=ca.getFromUserId();
			}else {
				id=ca.getToUserId();
			}
			User user=uDao.selectById(id);
			map.put("user", user);
			listMap.add(map);
		}
		return new ResultBean<List<Map<String,Object>>>(listMap);
	}

}

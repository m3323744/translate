package com.medical.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CollectMapper;
import com.medical.dao.CourseEvaluateMapper;
import com.medical.dao.CourseStreamingMapper;
import com.medical.dao.EnterpriseMapper;
import com.medical.dao.LanguageMapper;
import com.medical.dao.TeacherAuthenticationMapper;
import com.medical.dao.UserMapper;
import com.medical.entity.Collect;
import com.medical.entity.CourseStreaming;
import com.medical.entity.Enterprise;
import com.medical.entity.Language;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthentication;
import com.medical.entity.User;
import com.medical.service.CourseStreamingService;

@Service
@Transactional
public class CourseStreamingServiceImpl implements CourseStreamingService {
	@Resource
	private CourseStreamingMapper dao;
	
	@Resource
	private CourseEvaluateMapper tDao;
	
	@Resource
	private LanguageMapper lDao;
	
	@Resource
	private CollectMapper cDao;
	
	@Resource
	private UserMapper uDao;
	
	@Resource
	private TeacherAuthenticationMapper teDao;
	
	@Resource
	private EnterpriseMapper eDao;
	
	@Override
	public ResultBean<Integer> addCourseStreaming(CourseStreaming cs) {
		// TODO Auto-generated method stub
		try {
			TeacherAuthentication ta=teDao.getByUserid(cs.getUserid());
			if(ta==null) {
				return new ResultBean<>(new MyException("未认证"));
			}
			if(ta.getStates()!=2) {
				return new ResultBean<>(new MyException("认证未通过"));
			}
			/*int num=eDao.selectCount(new EntityWrapper<Enterprise>().eq("userid", cs.getUserid()).and().eq("states", 2));
			if(num<1) {
				return new ResultBean<>(new MyException("未认证企业"));
			}*/
			cs.setTime(new Date());
			cs.setStates(1);
			cs.setRelease(true);
			int count=dao.insert(cs);
			if(count>0) {
				return new ResultBean<>(cs.getId());
			}else {
				return new ResultBean<>(new MyException("添加异常"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<Map<String, Object>> selectById(int id,int userId) {
		// TODO Auto-generated method stub
		try {
			CourseStreaming cs=dao.selectById(id);
			Map<String, Object> map=new HashMap<>();
			map.put("CourseStreaming", cs);
			int count=cDao.selectCount(new EntityWrapper<Collect>().eq("collectid", id).and().eq("userid", userId).and().eq("collecttype", 3));
			if(count>0) {
				map.put("isCollect", true);
			}else {
				map.put("isCollect", false);
			}
			Language lang=lDao.selectById(cs.getLanguageid());
			map.put("language", lang);
			User user=uDao.selectById(cs.getUserid());
			map.put("user", user);
			return new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(new MyException("系统异常"));
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getList(Integer languageid, Integer stageid, String prices, int score,
			String keyWord, int pageIndex, int size) {
		// TODO Auto-generated method stub
		StringBuffer whereSql=new StringBuffer(" and name like '%"+keyWord+"%' ");
		if(languageid!=null&&languageid!=0) {
			whereSql.append(" and languageid="+languageid);
		}
		if(stageid!=null&&stageid!=0) {
			whereSql.append(" and stageid="+stageid);
		}
		if(prices!=null&&!prices.equals("")) {
			String[] price=prices.split("-");
			whereSql.append(" and totalprice>="+price[0]+" and totalprice<="+price[1]);
		}
		whereSql.append(" and states=2");
		String orderStr="";
		if(score==2) {    //价格
			orderStr=" order by totalprice";
		}else if(score==3) {
			orderStr=" order by streamingtime";
		}
		try {
			List<Map<String, Object>> list=dao.getList(whereSql.toString(), orderStr,(pageIndex-1)*size,size);
			List<Map<String, Object>> list1=new ArrayList<>();
			for(Map<String, Object> map:list) {
				Integer courseId=Integer.valueOf(map.get("id").toString());
				Double fen=tDao.selectAvgScoreByeacherId(courseId, 2);
				map.put("fen", fen);
				list1.add(map);
			}
			if(score==1) {
				 Collections.sort(list1, new Comparator<Map<String, Object>>() {
			            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			            	Double name1 = Double.valueOf(o1.get("fen").toString()) ;//name1是从你list里面拿出来的一个 
			            	Double name2 = Double.valueOf(o2.get("fen").toString()) ; //name1是从你list里面拿出来的第二个name
			                return name2.compareTo(name1);
			            }
			        });
			}
			return new ResultBean<>(list1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> getListCount(Integer languageid, Integer stageid, String prices, String keyWord) {
		// TODO Auto-generated method stub
		StringBuffer whereSql=new StringBuffer(" and name like '%"+keyWord+"%' ");
		if(languageid!=null&&languageid!=0) {
			whereSql.append(" and languageid="+languageid);
		}
		if(stageid!=null&&stageid!=0) {
			whereSql.append(" and stageid="+stageid);
		}
		if(prices!=null&&!prices.equals("")) {
			String[] price=prices.split("-");
			whereSql.append(" and totalprice>="+price[0]+" and totalprice<="+price[1]);
		}
		whereSql.append(" and states=2");
		int count=dao.getListCount(whereSql.toString());
		return new ResultBean<Integer>(count);
	}

}

package com.medical.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.CollectMapper;
import com.medical.dao.ConferenceMapper;
import com.medical.dao.ConventionMapper;
import com.medical.dao.CourseEvaluateMapper;
import com.medical.dao.CourseOfflineMapper;
import com.medical.dao.CourseSeeingMapper;
import com.medical.dao.CourseStreamingMapper;
import com.medical.dao.InterpreterMapper;
import com.medical.dao.InterpreterOrderEvaluateMapper;
import com.medical.dao.LanguageMapper;
import com.medical.dao.SchoolMapper;
import com.medical.dao.TeacherAuthenticationMapper;
import com.medical.dao.TeacherMapper;
import com.medical.dao.VisaMapper;
import com.medical.dao.VisaOrderEvaluateMapper;
import com.medical.entity.Collect;
import com.medical.entity.Conference;
import com.medical.entity.Convention;
import com.medical.entity.CourseOffline;
import com.medical.entity.CourseSeeing;
import com.medical.entity.CourseStreaming;
import com.medical.entity.Interpreter;
import com.medical.entity.Language;
import com.medical.entity.ResultBean;
import com.medical.entity.School;
import com.medical.entity.Teacher;
import com.medical.entity.Visa;
import com.medical.service.CollectService;

@Service
@Transactional
public class CollectServiceImpl implements CollectService {

	@Resource
	private CollectMapper dao;
	
	@Resource
	private TeacherMapper tDao;
	
	@Resource
	private InterpreterMapper iDao;
	
	@Resource
	private CourseStreamingMapper csDao;
	
	@Resource
	private CourseOfflineMapper coDao;
	
	@Resource
	private CourseSeeingMapper ceDao;
	
	@Resource
	private VisaMapper vDao;
	
	@Resource
	private ConferenceMapper cDao;
	
	@Resource
	private ConventionMapper cvDao;
	
	@Resource
	private SchoolMapper sDao;
	
	@Resource
	private LanguageMapper lDao;
	
	@Resource
	private TeacherAuthenticationMapper taDao;
	
	@Resource
	private VisaOrderEvaluateMapper viDao;
	
	@Resource
	private CourseEvaluateMapper cemDao;
	
	@Resource
	private InterpreterOrderEvaluateMapper ioDao;
	
	@Override
	public ResultBean<Integer> addCollect(Collect coll) {
		// TODO Auto-generated method stub
		try {
			coll.setTime(new Date());
			int count=dao.insert(coll);
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> delCollect(Collect coll) {
		// TODO Auto-generated method stub
		try {
			int count=dao.delete(new EntityWrapper<Collect>().eq("userid", coll.getUserid()).and().eq("collecttype", coll.getCollecttype()).and().eq("collectid", coll.getCollectid()));
			return new ResultBean<>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<List<Map<String, Object>>> getList(Collect coll,int pageIndex,int size) {
		// TODO Auto-generated method stub
		try {
			List<Collect> list=dao.getList(coll,(pageIndex-1)*size,size);
			List<Map<String, Object>> list1=new ArrayList<>();
			for(Collect li:list) {
				Map<String, Object> map=new HashMap<>();
				if(coll.getCollecttype()==1) {
					Teacher teacher=tDao.selectById(li.getCollectid());
					Language la=lDao.selectById(teacher.getLanguageid());
					Double fen=taDao.selectAvgScore(teacher.getUserid());
					map.put("fen", fen);
					map.put("teacher", teacher);
					map.put("language", la);
					list1.add(map);
				}else if(coll.getCollecttype()==2){
					Interpreter inte=iDao.selectById(li.getCollectid());
					if(inte!=null) {
						if(inte.getIntolanguage()!=null) {
							Language la=lDao.selectById(inte.getIntolanguage());
							map.put("intolanguage", la);
							
						}else {
							map.put("intolanguage", null);
						}
						if(inte.getTranslatelanguage()!=null) {
							Language la=lDao.selectById(inte.getTranslatelanguage());
							map.put("translatelanguage", la);
						}else {
							map.put("translatelanguage", null);
						}
						Double fen=ioDao.selectAvgScore(inte.getId());
						map.put("fen", fen);
						map.put("interpreter", inte);
						
						list1.add(map);
					}
				}else if(coll.getCollecttype()==3) {
					CourseStreaming cs=csDao.selectById(li.getCollectid());
					map.put("courseStreaming", cs);
					list1.add(map);
				}else if(coll.getCollecttype()==4) {
					CourseOffline cs=coDao.selectById(li.getCollectid());
					map.put("courseOffline", cs);
					list1.add(map);
				}else if(coll.getCollecttype()==5) {
					CourseSeeing cs=ceDao.selectById(li.getCollectid());
					map.put("courseSeeing", cs);
					list1.add(map);
				}else if(coll.getCollecttype()==6) {
					Visa vi=vDao.selectById(li.getCollectid());
					Double fen=viDao.selectAvgScore(vi.getId());
					map.put("fen", fen);
					map.put("visa", vi);
					list1.add(map);
				}else if(coll.getCollecttype()==7) {
					Conference co=cDao.selectById(li.getCollectid());
					map.put("conference", co);
					list1.add(map);
				}else if(coll.getCollecttype()==8) {
					Convention co=cvDao.selectById(li.getCollectid());
					map.put("convention", co);
					list1.add(map);
				}else if(coll.getCollecttype()==9) {
					School sc=sDao.selectById(li.getCollectid());
					String[] str=sc.getLanguageid().split(",");
					List<Language> lanList=new ArrayList<>();
					for(String ids:str) {
						Language la=lDao.selectById(Integer.parseInt(ids));
						lanList.add(la);
					}
					map.put("school", sc);
					map.put("language", lanList);
					Double fen=cemDao.selectAvgScoreBySchoolId(li.getCollectid());
					map.put("fen", fen);
					list1.add(map);
				}
			}
			return new ResultBean<>(list1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
	}

	@Override
	public ResultBean<Integer> getListCount(Collect coll) {
		// TODO Auto-generated method stub
		int count=dao.getListCount(coll);
		return new ResultBean<Integer>(count);
	}

}

package com.medical.service;

import com.medical.entity.Contribute;
import com.medical.entity.ResultBean;

public interface ContributeService {
	public ResultBean<Integer> addContribute(Contribute con);
}

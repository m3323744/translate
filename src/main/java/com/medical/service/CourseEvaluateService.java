package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.CourseEvaluate;
import com.medical.entity.ResultBean;

public interface CourseEvaluateService {
public ResultBean<Map<String, Object>> selectByOrderId(int orderId);
	
	public ResultBean<Integer> addEvaluate(CourseEvaluate eva);
	
	public ResultBean<Map<String, Object>> selectByInterpreterId(int interpreterId,int type);
	
	public ResultBean<List<Map<String, Object>>> selectBySchoolId(int schoolId);
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.School;

public interface SchoolService {
	public ResultBean<Integer> addSchool(School school);
	
	public ResultBean<List<Map<String, Object>>> getList(String address,Integer lanuageId,String hauptstudiumid,int score,double longitude,double latitude,int pageIndex,int size,String keyword);
	
	public ResultBean<School> getByUserid(int userId);
	
	public ResultBean<Map<String, Object>> selectById(int id,int userId);
	
	public ResultBean<Integer> getListCount(String address,Integer lanuageId,String hauptstudiumid,String keyword);
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.CourseOffline;
import com.medical.entity.ResultBean;

public interface CourseOfflineService {
	public ResultBean<Integer> addCourseSeeing(CourseOffline co,String simple);
	public ResultBean<Map<String, Object>> selectById(int id,int userId);
	public ResultBean<List<Map<String, Object>>> getList(CourseOffline cs,String hours,String prices,String startTime,String endTime,int score,String keyWord,int pageIndex,int size);
	public ResultBean<Integer> getListCount(CourseOffline cs,String hours,String prices,String startTime,String endTime,String keyWord);
}

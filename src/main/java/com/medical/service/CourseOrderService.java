package com.medical.service;

import com.medical.entity.CourseOrder;
import com.medical.entity.ResultBean;

public interface CourseOrderService {
	public ResultBean<CourseOrder> addCourseOrder(CourseOrder co);
}

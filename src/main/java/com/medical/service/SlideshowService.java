package com.medical.service;

import java.util.List;

import com.medical.entity.ResultBean;
import com.medical.entity.Slideshow;

public interface SlideshowService {
	public ResultBean<List<Slideshow>> getList(int type);
}

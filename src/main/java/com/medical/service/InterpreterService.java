package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Interpreter;
import com.medical.entity.ResultBean;

public interface InterpreterService {
	public ResultBean<Integer> addInterpreter(Interpreter ine);
	public ResultBean<List<Map<String, Object>>> selectByInter(Interpreter ite, Integer pricetype,String interpretAge,String ages,String prices,int score,String nationality,Integer pageIndex,Double longitude,Double latitude,int size,String keyword);
	public ResultBean<Map<String, Object>> selectById(int id,int userId);
	
	/**
	 * 首页热门翻译员
	 * @return
	 */
	public ResultBean<List<Map<String,Object>>> findHotInterpreters();
	
	public ResultBean<Integer> selectByInterCount(Interpreter ite, Integer pricetype, String ages,String prices,int score,String nationality,String keyword);
}

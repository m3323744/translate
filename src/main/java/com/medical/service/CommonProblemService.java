package com.medical.service;

import com.medical.entity.CommonProblem;

public interface CommonProblemService {
	public CommonProblem getOne();
}

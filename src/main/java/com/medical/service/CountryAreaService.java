package com.medical.service;

import java.util.List;

import com.medical.entity.CountryArea;
import com.medical.entity.ResultBean;

public interface CountryAreaService {
	public ResultBean<List<CountryArea>> getListByCountryId(int countryId);
}

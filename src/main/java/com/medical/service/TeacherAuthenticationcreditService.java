package com.medical.service;

import java.util.List;

import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthenticationcredit;

public interface TeacherAuthenticationcreditService {
	public ResultBean<TeacherAuthenticationcredit> addTeacherAuthenticationcredit(TeacherAuthenticationcredit tac);
	
	public ResultBean<Integer> updateTeacherAuthenticationcredit(TeacherAuthenticationcredit tac);
	
	public ResultBean<List<TeacherAuthenticationcredit>> getList(int authenticationid);
	public ResultBean<List<TeacherAuthenticationcredit>> selectByIds(String ids);
}

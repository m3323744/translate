package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.Visa;

public interface VisaService {
	public ResultBean<Integer> addVisa(Visa vi);
	public ResultBean<List<Map<String, Object>>> getList(Visa vi,String keyWord,int score,int pageIndex,int size);
	public ResultBean<Map<String, Object>> selectById(int id,int userId);
	public ResultBean<Integer> getListCount(Visa vi,String keyWord);
}

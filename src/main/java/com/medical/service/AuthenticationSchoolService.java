package com.medical.service;

import java.util.List;


import com.medical.entity.AuthenticationSchool;
import com.medical.entity.ResultBean;

public interface AuthenticationSchoolService {
	public ResultBean<Integer> addSchool(AuthenticationSchool acs);
	public ResultBean<AuthenticationSchool> getDateils(int id);
	public ResultBean<List<AuthenticationSchool>> getList(int authenticationid,int isteacher);
	public ResultBean<List<AuthenticationSchool>> selectByIds(String ids);
	public ResultBean<Integer> delSchool(int id);
	public ResultBean<Integer> updateAuthenticationSchool(AuthenticationSchool acs);
}

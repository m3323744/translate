package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.TeacherOrder;


public interface TeacherOrderService {
	public ResultBean<List<Map<String, Object>>> getOrderList(int merchantid,int states);
	public ResultBean<Map<String, Object>> getOrderListCount(int merchantid,int states,int pageIndex,int size);
	public ResultBean<Map<String, Object>> getByOrderId(int typeId,int orderId);
	public ResultBean<List<Map<String, Object>>> findOrdersForUsers(int userid,int states);
	public  ResultBean<Integer> findOrderForUsersCount(int userid,int states);
	public ResultBean<Map<String, Object>> findOrdersForUsersPC(int userid,int states,int pageIndex,int size);
	public ResultBean<Map<String, Object>> updateOrderState(int type,int orderId,int states,int paytype);
	
	public ResultBean<TeacherOrder> addOrder(TeacherOrder to);
	
	public int updateState(int orderId, int states);
	
	public ResultBean<Integer> yuePay(int type,int orderId,int states,int userId,Double money,int merchantid,Double money1);
	
	public void addConsumption(int userId,Double money,String content);
}

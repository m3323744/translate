package com.medical.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.ModelAttribute;

import com.medical.entity.AuthenticationLanguage;
import com.medical.entity.ResultBean;

public interface AuthenticationLanuageService {
	public ResultBean<Integer> addLanuage(AuthenticationLanguage acl);
	public ResultBean<AuthenticationLanguage> getDateils(int id);
	public ResultBean<List<AuthenticationLanguage>> getList(int authenticationid,int isteacher);
	public ResultBean<List<Map<String, Object>>> selectByIds(String ids);
	public ResultBean<Integer> delLanguage(int id);
	public ResultBean<Integer> updateAuthenticationLanuage(@ModelAttribute AuthenticationLanguage acl);
}

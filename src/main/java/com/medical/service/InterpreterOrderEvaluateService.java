package com.medical.service;

import java.util.Map;

import com.medical.entity.InterpreterOrderEvaluate;
import com.medical.entity.ResultBean;

public interface InterpreterOrderEvaluateService {
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId);
	
	public ResultBean<Integer> addEvaluate(InterpreterOrderEvaluate eva);
	
	public ResultBean<Map<String, Object>> selectByInterpreterId(int interpreterId);
}

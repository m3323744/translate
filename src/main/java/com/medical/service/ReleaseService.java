package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;

public interface ReleaseService {
	public ResultBean<Integer> CancelRelease(int id,int type);
	public ResultBean<List<Map<String, Object>>> getByUserId(int userId,int type);
}

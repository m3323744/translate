package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.IntegralCommodity;
import com.medical.entity.IntegralConsume;
import com.medical.entity.IntegralConversion;
import com.medical.entity.ResultBean;

public interface IntegralCommodityService {
	public ResultBean<List<IntegralCommodity>> getList();
	public ResultBean<Integer>  exchange(IntegralConversion ic);
	public ResultBean<Integer> sign(IntegralConsume ic);
	public ResultBean<Map<String, Object>> getListCount(int pageIndex,int size);
	public ResultBean<IntegralCommodity> selectById(int id);
}

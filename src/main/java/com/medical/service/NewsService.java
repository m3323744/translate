package com.medical.service;

import java.util.List;

import com.medical.entity.News;
import com.medical.entity.ResultBean;

public interface NewsService {
	public ResultBean<News> getList(int type);
	public ResultBean<List<News>> getNews(int page,int size);
	public ResultBean<News> getById(int id);
	public ResultBean<List<News>> selectByKey(String key,int page, int size);
}	

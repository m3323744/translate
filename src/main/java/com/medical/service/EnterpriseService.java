package com.medical.service;


import com.medical.entity.Enterprise;
import com.medical.entity.ResultBean;

public interface EnterpriseService {
	public ResultBean<Integer> addEnterprise(Enterprise ent);
	public ResultBean<Enterprise> getEnterpriseDateils(int userId);
	public ResultBean<Integer> updateEnterprise(Enterprise ent);
}

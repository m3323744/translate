package com.medical.service;

import java.util.List;

import com.medical.entity.CertificateThreeType;
import com.medical.entity.ResultBean;

public interface CertificateThreeTypeService {
	public ResultBean<List<CertificateThreeType>> getByTwoId(int twotypeId);
}

package com.medical.service;

import java.util.List;

import com.medical.entity.ResultBean;
import com.medical.entity.TerritoryType;

public interface TerritoryTypeService {
	public ResultBean<List<TerritoryType>> getList();
}

package com.medical.service;

import com.medical.entity.InterpreterOrder;
import com.medical.entity.ResultBean;

public interface InterpreterOrderService {
	public ResultBean<InterpreterOrder> addInterpreterOrder(InterpreterOrder inter);
}

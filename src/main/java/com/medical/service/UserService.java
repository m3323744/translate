package com.medical.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.medical.entity.ResultBean;
import com.medical.entity.User;

public interface UserService {
	public ResultBean<User> wxRegister(String openid,String nickname,String portrait);
	public ResultBean<User> updateUser(User user);
	public ResultBean<Map<String, Object>> userDateils(int id);
	public int updateOpenId(int userId,String openId);
	public ResultBean<User> getOpenId(String openId);
	public int updateUsers(User user);
	public User getById(String openId);
	public int addUser(User user);
	public User getByUserId(int id);
	public Map<String,Object>  findBelance(String userId);
	public ResultBean<User> updatePhone(int userId,String phone);
	public ResultBean<User> recharge(int userId,Double money);
	public ResultBean<User> withdrawals(HttpServletRequest request, HttpServletResponse response,int userId,Double money);
	public int updateIntegral(int integral,int userId,Boolean isconsume);
	public ResultBean<User> userDateilsByPhone(String phone);
}

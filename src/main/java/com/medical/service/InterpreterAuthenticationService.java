package com.medical.service;

import java.util.Map;

import com.medical.entity.InterpreterAuthentication;
import com.medical.entity.ResultBean;

public interface InterpreterAuthenticationService {
	public ResultBean<Integer> addInterpreterAuthentication(InterpreterAuthentication ia,String lanuage,String certificate,String school,String studying,String territory);
	public ResultBean<Map<String, Object>> interpreterAuthenticationDateils(int userId,int typeId);
	public ResultBean<Integer> updateInterpreterAuthentication(InterpreterAuthentication ia,String lanuage,String certificate,String school,String studying,String territory);
}

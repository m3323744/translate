package com.medical.service;

import java.util.List;

import com.medical.entity.LeaseType;
import com.medical.entity.ResultBean;

public interface LeaseTypeService {
	public ResultBean<List<LeaseType>> getList();
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Convention;
import com.medical.entity.ResultBean;

public interface ConventionService {
	public ResultBean<Integer> addConvention(Convention co);
	public ResultBean<List<Convention>> getList(String keyWord,int pageIndex,int score,int size);
	public ResultBean<Map<String, Object>> selectById(int id,int userid);
	public ResultBean<List<Map<String, Object>>> getByMyUserId(int userId,int pageIndex,int size);
	public ResultBean<Integer> delConvention(int id);
	public ResultBean<Integer> getListCount(String keyWord);
}

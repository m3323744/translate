package com.medical.service;

import com.medical.entity.ConferenceApply;
import com.medical.entity.ResultBean;

public interface ConferenceApplyService {
	public ResultBean<ConferenceApply> addConferenceApply(ConferenceApply ca);
}

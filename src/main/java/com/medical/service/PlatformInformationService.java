package com.medical.service;

import com.medical.entity.PlatformInformation;
import com.medical.entity.ResultBean;

public interface PlatformInformationService {
	public ResultBean<PlatformInformation> getByType(int type);
}

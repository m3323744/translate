package com.medical.service;

import java.util.Map;



public interface OrderService {
	
	Map<String, Object> getOrderDateils(int id);
	
	int updateState(int orderId, int states,int paytype);
	
	int updateState(int orderId, int states);
}

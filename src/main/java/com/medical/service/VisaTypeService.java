package com.medical.service;

import java.util.List;

import com.medical.entity.ResultBean;
import com.medical.entity.VisaType;

public interface VisaTypeService {
	public ResultBean<List<VisaType>> getList();
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Lease;
import com.medical.entity.ResultBean;

public interface LeaseService {
	public ResultBean<List<Lease>> getList(int pageIndex,int type,int size);
	public ResultBean<Map<String, Object>> selectById(int id);
}

package com.medical.service;

import java.util.List;

import com.medical.entity.CertificateTwoType;
import com.medical.entity.ResultBean;

public interface CertificateTwoTypeService {
	public ResultBean<List<CertificateTwoType>> getList();
}

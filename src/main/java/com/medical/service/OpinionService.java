package com.medical.service;

import com.medical.entity.Opinion;
import com.medical.entity.ResultBean;

public interface OpinionService {
	public ResultBean<Integer> addOpinion(Opinion op);
}

package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.Wanted;

public interface WantedService {
	public ResultBean<Integer> addWanted(Wanted wanted,String wantedcertificates);
	public ResultBean<Map<String, Object>> getList(int pageIndex,String keyWord,int size);
	public ResultBean<Map<String, Object>> selectById(int id);
	public ResultBean<List<Wanted>> selectByUserId(int userId,int pageIndex,int size);
	public ResultBean<Integer> selectByUserIdCount(int userId);
	public ResultBean<Integer> delWanted(int id);
	public ResultBean<Integer> updateWanted(Wanted wanted,String wantedcertificates);
	public ResultBean<Integer> getListCount(String keyWord);
}

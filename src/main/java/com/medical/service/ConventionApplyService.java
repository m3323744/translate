package com.medical.service;

import com.medical.entity.ConventionApply;
import com.medical.entity.ResultBean;

public interface ConventionApplyService {
	public ResultBean<ConventionApply> addConventionApply(ConventionApply ca);
}

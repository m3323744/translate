package com.medical.service;
/**
 * 咨询业务逻辑接口
 * @author 76449
 *
 */

import java.util.List;
import java.util.Map;

public interface ConsultationService {

	public List<Map<String,Object>> findTypeList(Integer type);
	public Map<String,Object> findContentByType(Integer typeId,Integer twotypeId);
}

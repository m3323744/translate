package com.medical.service;

import java.util.List;

import com.medical.entity.LanguageGrade;
import com.medical.entity.ResultBean;

public interface LanguageGradeService {
	public ResultBean<List<LanguageGrade>> getByLanguageId(int languageId);
}

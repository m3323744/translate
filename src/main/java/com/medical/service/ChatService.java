package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;

public interface ChatService {
	/**
	 * 发送及时消息
	 * @param toUserId 接收方
	 * @param fromUserid 发送方
	 * @param type 类型
	 * @param content 内容
	 * @param currentId 
	 * @return Result对象
	 */
	
	public ResultBean<List<Map<String, Object>>> sendChat(Integer toUserId, Integer fromUserId, String type, String content, Integer currentId);
	
	/**
	 * 获取消息列表
	 * @param request request
	 * @param response response
	 * @param toUserId 对方id
	 * @param type early:比当前id早的记录 night:比当前id晚的记录 不传则
	 * @param currentId 当前id
	 * @param strip 条数 
	 * @return list为消息列表(按时间顺序倒叙排列) total为总条数 其中：list中【id:消息id fromUserId:发送方 toUserId:接收方 content:消息内容 time:发送时间  type:消息类型】
	 */
	public ResultBean<Map<String, Object>> getChatList(Integer toUserId, Integer fromUserId, String type, Integer currentId);
	
	public ResultBean<List<Map<String, Object>>> getByUserId(int userId,int page,int size);
}

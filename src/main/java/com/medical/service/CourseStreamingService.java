package com.medical.service;

import java.util.List;
import java.util.Map;


import com.medical.entity.CourseStreaming;
import com.medical.entity.ResultBean;

public interface CourseStreamingService {
	public ResultBean<Integer> addCourseStreaming(CourseStreaming cs);
	public ResultBean<Map<String, Object>> selectById(int id,int userId);
	public ResultBean<List<Map<String, Object>>> getList(Integer languageid,Integer stageid,String prices,int score,String keyWord,int pageIndex,int size);
	public ResultBean<Integer> getListCount(Integer languageid,Integer stageid,String prices,String keyWord);
}

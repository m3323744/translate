package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.Wantedcertificate;

public interface WantedcertificateService {
	public ResultBean<Integer> addWantedcertificate(Wantedcertificate wantedcertificate);
	public ResultBean<List<Wantedcertificate>> selectByIds(String ids);
	public ResultBean<Integer> delWantedcertificate(int id);
	public ResultBean<Wantedcertificate> update(Wantedcertificate wantedcertificate);
	public ResultBean<Map<String, Object>> selectById(int id);
}

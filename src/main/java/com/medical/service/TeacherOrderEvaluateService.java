package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;

public interface TeacherOrderEvaluateService {
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId,int type);
	
	public ResultBean<Integer> addEvaluate(Float score,int orderid,int userid,String content,int courseType,int type,int courseid);
	
	public ResultBean<Map<String, Object>> selectByTeacherId(int teacherId);
	
	public void updateOrderStates();
}

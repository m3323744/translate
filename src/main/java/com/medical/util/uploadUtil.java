package com.medical.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

public class uploadUtil {
	public static String upload(HttpServletRequest request,MultipartFile file){
		String path =request.getSession().getServletContext().getRealPath("/upload");  
		System.out.println(path);
        String fileName = file.getOriginalFilename();  
        String type=fileName.substring(fileName.indexOf("."));
	    fileName = new Date().getTime()+type;
	    File dest = new File(path + "/" + fileName);
        if(!dest.getParentFile().exists()){ //判断文件父目录是否存?
            dest.getParentFile().mkdir();
        }
        try {
            file.transferTo(dest); //保存文件
            copyFile(path + "/" + fileName,"c:/upload/"+fileName);
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return "/upload/"+fileName;
	}
	
	public static String uploadYa(HttpServletRequest request,MultipartFile file){
		
		String path =request.getSession().getServletContext().getRealPath("/upload");  
		System.out.println(path);
        String fileName = file.getOriginalFilename();  
        String type=fileName.substring(fileName.indexOf("."));
	    fileName = new Date().getTime()+type;
	    File dest = new File(path + "/" + fileName);
        if(!dest.getParentFile().exists()){ //判断文件父目录是否存?
            dest.getParentFile().mkdir();
        }
        try {
            file.transferTo(dest); //保存文件
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
      return "/upload/"+fileName;
	}
	
	/** 
	* 复制单个文件 
	* @param oldPath String 原文件路? 如：c:/fqf.txt 
	* @param newPath String 复制后路? 如：f:/fqf.txt 
	* @return boolean 
	*/ 
	public static void copyFile(String oldPath, String newPath) { 
		try { 
			int bytesum = 0; 
			int byteread = 0; 
			File oldfile = new File(oldPath); 
			if (oldfile.exists()) { //文件存在? 
				InputStream inStream = new FileInputStream(oldPath); //读入原文? 
				FileOutputStream fs = new FileOutputStream(newPath); 
				byte[] buffer = new byte[1444]; 
				int length; 
				while ( (byteread = inStream.read(buffer)) != -1) { 
					bytesum += byteread; //字节? 文件大小 
					System.out.println(bytesum); 
					fs.write(buffer, 0, byteread); 
				} 
				inStream.close(); 
			} 
		} 
		catch (Exception e) { 
			System.out.println("复制单个文件操作出错"); 
			e.printStackTrace(); 
	
		} 

	} 

	/** 
	* 复制整个文件夹内? 
	* @param oldPath String 原文件路? 如：c:/fqf 
	* @param newPath String 复制后路? 如：f:/fqf/ff 
	* @return boolean 
	*/ 
	public static void copyFolder(String oldPath, String newPath) { 

		try { 
			(new File(newPath)).mkdirs(); //如果文件夹不存在 则建立新文件? 
			File a=new File(oldPath); 
			String[] file=a.list(); 
			File temp=null; 
			for (int i = 0; i < file.length; i++) { 
				if(oldPath.endsWith(File.separator)){ 
					temp=new File(oldPath+file[i]); 
				} 
				else{ 
					temp=new File(oldPath+File.separator+file[i]); 
				} 
		
				if(temp.isFile()){ 
					FileInputStream input = new FileInputStream(temp); 
					FileOutputStream output = new FileOutputStream(newPath + "/" +(temp.getName()).toString()); 
					byte[] b = new byte[1024 * 5]; 
					int len; 
					while ( (len = input.read(b)) != -1) { 
						output.write(b, 0, len); 
					} 
					output.flush(); 
					output.close(); 
					input.close(); 
				} 
				if(temp.isDirectory()){//如果是子文件? 
					copyFolder(oldPath+"/"+file[i],newPath+"/"+file[i]); 
				} 
			} 
		} 
		catch (Exception e) { 
			System.out.println("复制整个文件夹内容操作出?"); 
			e.printStackTrace(); 
	
		} 

	}
	
	/** 
	  * 删除单个文件 
	  * @param   sPath    被删除文件的文件? 
	  * @return 单个文件删除成功返回true，否则返回false 
	  */  
	 public static boolean deleteFile(String sPath,HttpServletRequest request) { 
		 String path = request.getSession().getServletContext().getRealPath("/");  
		 boolean flag = false;  
	     File  file = new File(path+"/"+sPath);  
	     // 路径为文件且不为空则进行删除  
	     if (file.isFile() && file.exists()) {  
	         file.delete();  
	         flag = true;  
	     }  
	     return flag;  
	 }  
}

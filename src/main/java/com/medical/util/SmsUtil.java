package com.medical.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

public class SmsUtil {
	private static HttpClient client;

	private final static String Url="https://dx.ipyy.net/ensms.ashx";
	/**
	 * 加密发送DEMO
	 * @param args
	 */
	public static void sms(String mobile,String content) {
		

		//用户ID。
		String userId="55965";
		
		//用户账号名
		String userName="8D00038";
		
		//接口密码
		String password="8D0003813";
		
		//目标手机号，多个以半角","分隔
		
		//信息内容
		
		//扩展号，没有请留空
		String ext="";
		
		//即时短信请留空，定时短信请指定，格式为：yyyy-MM-dd HH:mm:ss
		String sendTime="";
		String stamp =new SimpleDateFormat("MMddHHmmss").format(new Date());
		String secret=MD5.GetMD5Code(password+stamp).toUpperCase();
		
		try {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("UserName", userName);
			jsonObj.put("Stamp", stamp);
			jsonObj.put("Secret", secret);
			jsonObj.put("Moblie", mobile);
			jsonObj.put("Text", "【鸟语网】您的验证码为："+content);
			jsonObj.put("Ext", ext);
			jsonObj.put("SendTime", sendTime);
	
			//Des加密，base64转码
			String text64=DesHelper.Encrypt(jsonObj.toString(), password); 

			client = new SSLClient();
			HttpPost post=new HttpPost(Url);
			post.setHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("UserId",userId));
			nvps.add(new BasicNameValuePair("Text64",text64));
			post.setEntity(new UrlEncodedFormEntity(nvps));
			HttpResponse response = client.execute(post);
			System.out.println(response.getStatusLine());

			HttpEntity entity = response.getEntity();
			String returnString=EntityUtils.toString(entity, "UTF-8");
			System.out.println(returnString);
			EntityUtils.consume(entity);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}		
	}	
}

package com.medical.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;

import com.google.zxing.common.BitMatrix;

public class QRcodeUtil {
	private static final int WHITE = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;
	/** 
	    * 生成并下载二维码 
	    * @param url 二维码对于URL 
	    * @param width 二维码宽 
	    * @param height 二维码高 
	    * @param format  二维码格式 
	    * @return 
	    * @throws WriterException 
	    * @throws IOException 
	    */  
	   public static ResponseEntity<byte[]> getResponseEntity(String url,int width, int height,String format) throws WriterException, IOException {  
	       Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();  
	       hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");  
	       BitMatrix bitMatrix = new MultiFormatWriter().encode(url,  
	               BarcodeFormat.QR_CODE, width, height, hints);// 生成矩阵  
	       //将矩阵转为Image  
	      // BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix);  
	       ByteArrayOutputStream out = new ByteArrayOutputStream();  
	       //ImageIO.write(image, format, out);//将BufferedImage转成out输出流  
	       HttpHeaders headers = new HttpHeaders();  
	       headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);  
	       return new ResponseEntity<byte[]>(out.toByteArray(),  
	               headers, HttpStatus.CREATED);  
	   }  
	   
	   /**
	     * 生成二维码图片 不存储 直接以流的形式输出到页面
	     * @param content
	     * @param response
	     */
	    @SuppressWarnings({ "unchecked", "rawtypes" })
	    public static void encodeQrcode(String content,HttpServletResponse response){
	        if(BlankUtil.isBlank(content))
	            return;
	        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
	        Map hints = new HashMap();
	        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8"); //设置字符集编码类型
	        BitMatrix bitMatrix = null;
	        try {
	            bitMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, 300, 300,hints);
	            BufferedImage image = toBufferedImage(bitMatrix);
	            //输出二维码图片流
	            try {
	                ImageIO.write(image, "png", response.getOutputStream());
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        } catch (WriterException e1) {
	            // TODO Auto-generated catch block
	            e1.printStackTrace();
	        }         
	    }
	    
	    private static BufferedImage toBufferedImage(BitMatrix matrix) {
	    	          int width = matrix.getWidth();
	    	          int height = matrix.getHeight();
	    	           BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	    	           for (int x = 0; x < width; x++) {
	    	            for (int y = 0; y < height; y++) {
	    	               image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
	    	             }
	    	           }
	    	           return image;
	    	      }    
}

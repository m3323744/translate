package com.medical.util;

import com.medical.entity.AccessToken;
import com.medical.entity.Jsapi_ticket;

/**
 * 
 * @author ange
 * @time 2017年9月11日 上午9:42:05
 */

public class AccessTokenService {
	//private static Property property = new Property("system.configer.properties");
	private static String appId = "wx1a3f8421d472017b";
	private static String appsecret = "671884725628e6c7b69faefde2eb19e5";
	private static AccessToken accessToken = new AccessToken();
	//private static Jsapi_ticket jsapi_ticket = new Jsapi_ticket();

	public static AccessToken getAccessToken() {
		
			String strAccessToken=Http.sendGet("https://api.weixin.qq.com/cgi-bin/token","grant_type=client_credential&appid="
					+appId+"&secret="+appsecret);
			System.out.println(strAccessToken);
			accessToken = Json.parse(strAccessToken, AccessToken.class);
			accessToken.setStartTime(System.currentTimeMillis()/1000);
			return accessToken;
	}
	
	/**
	 * 获取jsapi_ticket
	 * 
	 */
	public static Jsapi_ticket getJsapi_ticket() {
		String param = "access_token=" + AccessTokenService.getAccessToken().getAccess_token().toString() + "&type=jsapi";
		Jsapi_ticket jsapi_ticket = Json.parse(
					Http.sendGet("https://api.weixin.qq.com/cgi-bin/ticket/getticket", param), Jsapi_ticket.class);
		jsapi_ticket.setStartTime(System.currentTimeMillis() / 1000);
		return jsapi_ticket;
	}
	
	
}

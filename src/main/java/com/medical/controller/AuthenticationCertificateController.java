package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.AuthenticationCertificate;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationCertificateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/authenCertificate")
@Api(description="认证证书管理")
public class AuthenticationCertificateController {
	@Autowired
	private AuthenticationCertificateService biz;
	
	@PostMapping("/addAuthenticationCertificate")
	@ApiOperation(value ="添加认证证书，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "number", value = "证书编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "twoid", value = "证书二级id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "threeid", value = "证书三级id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "time", value = "有效时间", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int"),
		@ApiImplicitParam(name = "perpetual", value = "是否永久有效", required = true, dataType = "boolean")
	})
	public ResultBean<Integer> addAuthenticationCertificate(@ModelAttribute AuthenticationCertificate ace){
		return biz.addAuthenticationCertificate(ace);
	}
	
	@PostMapping("/getDateils")
	@ApiOperation(value ="添加认证证书详情", notes="")
	@ApiImplicitParam(name = "id", value = "认证证书id", required = true, dataType = "int")
	public ResultBean<Map<String, Object>> getDateils(int id){
		return biz.getDateils(id);
	}
	
	@ApiOperation(value ="添加认证证书详情列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<List<AuthenticationCertificate>> getList(int authenticationid,int isteacher){
		return biz.getList(authenticationid,isteacher);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="添加认证证书详情列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "认证证书id,逗号分隔", required = true, dataType = "String")
	public ResultBean<List<Map<String, Object>>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
	
	@PostMapping("/delCertificate")
	@ApiOperation(value ="删除认证证书", notes="")
	@ApiImplicitParam(name = "id", value = "认证证书id", required = true, dataType = "int")
	public ResultBean<Integer> delCertificate(int id){
		return biz.delCertificate(id);
	}
	
	@PostMapping("/updateAuthenticationCertificate")
	@ApiOperation(value ="更新认证证书，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "证书id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "authenticationid", value = "老师认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "number", value = "证书编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "twoid", value = "证书二级id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "threeid", value = "证书三级id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "time", value = "有效时间", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int"),
		@ApiImplicitParam(name = "perpetual", value = "是否永久有效", required = true, dataType = "boolean")
	})
	public ResultBean<Integer> updateAuthenticationCertificate(@ModelAttribute AuthenticationCertificate ace){
		return biz.updateAuthenticationCertificate(ace);
	}
}

package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Complaint;
import com.medical.entity.ResultBean;
import com.medical.service.ComplaintService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/Complaint")
@Api(description="用户投诉")
public class ComplaintController {
	@Autowired
	private ComplaintService biz;
	
	@PostMapping("/addComplaint")
	@ApiOperation(value ="用户投诉", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "orderid", value = "订单号", required = false, dataType = "String"),
		@ApiImplicitParam(name = "ordertype", value = "订单类别 1 老师 2译员 3直播课 4线下课 5视听课 6签证 7会议 8会展", required = false, dataType = "int"),
		@ApiImplicitParam(name = "centont", value = "详细描述", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imageone", value = "图片1", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagetwo", value = "图片2", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagethree", value = "图片3", required = false, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addComplaint(Complaint com){
		return biz.addComplaint(com);
	}
}

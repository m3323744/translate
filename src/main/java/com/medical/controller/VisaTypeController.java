package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.VisaType;
import com.medical.service.VisaTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/visaType")
@Api(description="签证类型")
public class VisaTypeController {
	@Autowired
	private VisaTypeService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value ="签证类别列表", notes="")
	public ResultBean<List<VisaType>> getList(){
		return biz.getList();
	}
}

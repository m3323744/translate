package com.medical.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Enterprise;
import com.medical.entity.ResultBean;
import com.medical.service.EnterpriseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/enterprise")
@Api(description="认证企业")
public class EnterpriseController {
	@Autowired
	private EnterpriseService biz;
	
	@PostMapping("/addEnterprise")
	@ApiOperation(value ="添加企业认证信息", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "address", value = "所在地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "city", value = "所在城市", required = false, dataType = "String"),
		@ApiImplicitParam(name = "introduce", value = "企业介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "creditcode", value = "社会信用代码", required = false, dataType = "String"),
		@ApiImplicitParam(name = "enhancement", value = "增信证件", required = false, dataType = "String"),
		@ApiImplicitParam(name = "legalname", value = "法人姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "legalhome", value = "法人代表归属地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "writtenname", value = "填写人姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "writtenphone", value = "填写人联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "idcardfront", value = "法人手持身份证正面", required = false, dataType = "String"),
		@ApiImplicitParam(name = "idcardreverse", value = "法人手持身份证反面", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addEnterprise(Enterprise ent){
		return biz.addEnterprise(ent);
	}
	
	@PostMapping("/getEnterpriseDateils")
	@ApiOperation(value ="认证企业详情", notes="")
	@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int")
	public ResultBean<Enterprise> getEnterpriseDateils(int userId){
		return biz.getEnterpriseDateils(userId);
	}
	
	@PostMapping("/updateEnterprise")
	@ApiOperation(value ="修改企业认证信息(没有修改的话，原样传回来)", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "认证信息id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "address", value = "所在地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "city", value = "所在城市", required = false, dataType = "String"),
		@ApiImplicitParam(name = "introduce", value = "企业介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "creditcode", value = "社会信用代码", required = false, dataType = "String"),
		@ApiImplicitParam(name = "legalname", value = "法人姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "legalhome", value = "法人代表归属地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "writtenname", value = "填写人姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "writtenphone", value = "填写人联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "idcardfront", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardreverse", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "enhancement", value = "增信文件路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "time", value = "发布时间", required = true, dataType = "date")
	})
	public ResultBean<Integer> updateEnterprise(Enterprise ent){
		return biz.updateEnterprise(ent);
	}
}

package com.medical.controller;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthentication;
import com.medical.service.TeacherAuthenticationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/teacherAuthentication")
@Api(description="老师认证管理")
public class TeacherAuthenticationController {
	@Autowired
	private TeacherAuthenticationService biz;
	
	@PostMapping("/addTeacherAuthentication")
	@ApiOperation(value ="添加认证老师，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sex", value = "性别", required = true, dataType = "boolean"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "birthdate", value = "出生日期", required = true, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "家庭住址", required = true, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificatetype", value = "证件类型", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idnumber", value = "证件编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "lanuage", value = "语言id，用添加语种认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "认证证书id，用添加证书认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "school", value = "认证教育经历id，用添加教育经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "认证留学经历id，用添加留学经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardfront", value = "手持证件正面照", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardreverse", value = "手持证件反面照", required = true, dataType = "String"),
		@ApiImplicitParam(name = "zengone", value = "增信材料1", required = true, dataType = "String"),
		@ApiImplicitParam(name = "zengtwo", value = "增信材料2", required = true, dataType = "String"),
		@ApiImplicitParam(name = "zengthree", value = "增信材料3", required = true, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "电话", required = true, dataType = "String")
	})
	public ResultBean<Integer> addTeacherAuthentication(@ModelAttribute TeacherAuthentication ta,String lanuage,String certificate,String school,String studying,String zengone,String zengtwo,String zengthree){
		return biz.addTeacherAuthentication(ta, lanuage, certificate, school, studying,zengone,zengtwo,zengthree);
	}
	
	@PostMapping("/updateTeacherAuthentication")
	@ApiOperation(value ="更新认证老师，成功返回添加的id(没有修改的话，原样传回来)", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "sex", value = "性别", required = true, dataType = "boolean"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "birthdate", value = "出生日期", required = true, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "家庭住址", required = true, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificatetype", value = "证件类型", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idnumber", value = "证件编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "lanuage", value = "语言id，用添加语种认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "认证证书id，用添加证书认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "school", value = "认证教育经历id，用添加教育经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "认证留学经历id，用添加留学经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardfront", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardreverse", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "zengoneurl", value = "增信材料1路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "zengtwourl", value = "增信材料2路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "zengthreeurl", value = "增信材料3路径", required = true, dataType = "String")
	})
	public ResultBean<Integer> updateTeacherAuthentication(@ModelAttribute TeacherAuthentication ta,String lanuage,String certificate,String school,String studying,String zengoneurl,String zengtwourl,String zengthreeurl){
		return biz.updateTeacherAuthentication(ta, lanuage, certificate, school, studying,zengoneurl,zengtwourl,zengthreeurl);
	}
	
	@PostMapping("/teacherAuthenticationDateils")
	@ApiOperation(value ="老师认证详情", notes="")
	@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int")
	public ResultBean<Map<String, Object>> teacherAuthenticationDateils(int userId){
		return biz.TeacherAuthenticationDateils(userId);
	}
}

package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Coursetype;
import com.medical.entity.ResultBean;
import com.medical.service.CoursetypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/coursetype")
@Api(description="课程类别")
public class CoursetypeController {
	@Autowired
	private CoursetypeService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value ="课程类别列表", notes="")
	public ResultBean<List<Coursetype>> getList(){
		return biz.getList();
	}
}

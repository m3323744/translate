package com.medical.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;

import com.medical.entity.ResultBean;
import com.medical.service.ChatService;
import com.medical.util.AccessTokenService;
import com.medical.util.Sign;
import com.medical.websocket.TextMessageHandler;


/**
 * 跟聊天有关内容
 * @author ange
 *
 */
@RestController
@RequestMapping("/chat")
public class ChatController {
	@Autowired
	private ChatService chatService;
	
	@Autowired
	TextMessageHandler handler;
	
	/**
	 * 发送聊天信息<br>接口：chat/sendChat
	 * @param request request
	 * @param response response
	 * @param toUserId 接受方id
	 * @param type pic:图片 audio语音 text文字 file文件
	 * @param content 内容
	 * @param currentId 当前页面最大聊天id
	 * @param fromUserId 发送方 
	 * @return id:消息id fromUserId:发送方 toUserId:接收方 content:消息内容 time:发送时间  type:消息类型
	 */
	@PostMapping("/sendChat")
	public ResultBean<List<Map<String, Object>>> sendChat(HttpServletRequest request, HttpServletResponse response, Integer toUserId,String type,String content,Integer currentId,Integer fromUserId) {
		ResultBean<List<Map<String, Object>>> res=chatService.sendChat(toUserId,fromUserId,type,content,currentId);
		TextMessage message = new TextMessage("true");
		handler.sendMessageToUsers(message);
		return res;
	}
	/**
	 * 获取消息列表<br>接口：chat/getChatList
	 * 
	 * @param request request
	 * @param response response
	 * @param toUserId 对方id
	 * @param type 当前id之前或之后的记录：early比当前id早的记录 night比当前id晚的记录 不传则为最后strip条记录
	 * @param currentId 当前id
	 * @param strip 条数 
	 * @param fromUserId 发送方 
	 * @return list为消息列表(按时间顺序倒叙排列) total为总条数 其中：list中【id:消息id fromUserId:发送方 toUserId:接收方 content:消息内容 time:发送时间  type:消息类型 showTime:为1时显示时间 为0时不显示】
	 */
	@PostMapping("/getChatList")
	public ResultBean<Map<String, Object>> getChatList(HttpServletRequest request, HttpServletResponse response,Integer toUserId,String type,Integer currentId,Integer fromUserId) {
		ResultBean<Map<String, Object>> res=chatService.getChatList(toUserId,fromUserId,type,currentId);
		return res;
	}
	
	/**
	 * 根据url获取调用wxjssdk需要的信息
	 * @param url url
	 * @return wxjssdk需要的信息
	 */
	@PostMapping("/getWxConfig")
	public ResultBean<Map<String, String>> getWxConfig(String url) {
		Map<String, String> map=Sign.sign(AccessTokenService.getJsapi_ticket().getTicket(), url);
		return new ResultBean<Map<String,String>>(map);
	}
	
	@PostMapping("/getByUserId")
	public ResultBean<List<Map<String, Object>>> getByUserId(int userId,int page,int size){
		return chatService.getByUserId(userId, page, size);
	}
	
	@RequestMapping(value = "send")
	public void broadcast(@RequestParam("text") String text) throws IOException {
		TextMessage message = new TextMessage("true");
		handler.sendMessageToUsers(message);
	}
}

package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Job;
import com.medical.entity.ResultBean;
import com.medical.service.JobService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/job")
public class JobController {
	@Autowired
	private JobService biz;
	
	@PostMapping("/addJob")
	public ResultBean<Integer> addJob(Job job){
		return biz.addJob(job);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value="获取招聘列表")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
	})
	public ResultBean<Map<String, Object>> getList(@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,@RequestParam(value="keyWord",defaultValue="")String keyWord,int size){
		return biz.getList(pageIndex, keyWord,size);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value="获取招聘详情")
	@ApiImplicitParam(name = "id", value = "招聘id", required = false, dataType = "int")
	public ResultBean<Job> selectById(int id){
		return biz.selectById(id);
	}
	
	@PostMapping("/selectByUserId")
	@ApiOperation(value="用户端，获取我的招聘列表")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<List<Job>> selectByUserId(int userId,int pageIndex,int size){
		return biz.selectByUserId(userId,pageIndex,size);
	}
	
	@PostMapping("/updateJob")
	public ResultBean<Integer> updateJob(Job job){
		return biz.updateJob(job);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value="用户端PC，获取我的招聘列表总条数")
	@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String")
	public ResultBean<Integer> getListCount(@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(keyWord);
	}
	
	@PostMapping("/selectByUserIdCount")
	@ApiOperation(value="PC用户端，获取我的招聘列表总条数")
	@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int")
	public ResultBean<Integer> selectByUserIdCount(int userId){
		return biz.selectByUserIdCount(userId);
	}
}

package com.medical.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Interpreter;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/interpreter")
@Api(description="发布译员")
public class InterpreterController {
	@Autowired
	private InterpreterService biz;
	
	@PostMapping("/addInterpreter")
	@ApiOperation(value ="发布译员", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "展示图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "head", value = "头像", required = false, dataType = "String"),
		@ApiImplicitParam(name = "translatelanguage", value = "翻译成语言id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "intolanguage", value = "翻译成语言id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "translategrade", value = "翻译语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "intograde", value = "翻译成语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "地址", required = false, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "age", value = "口译年限", required = false, dataType = "int"),
		@ApiImplicitParam(name = "territoryid", value = "擅长领域id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "hourprice", value = "价格(元/小时)", required = false, dataType = "float"),
		@ApiImplicitParam(name = "minuteprice", value = "价格(元/分钟)", required = false, dataType = "float"),
		@ApiImplicitParam(name = "wordprice", value = "价格(元/字)", required = false, dataType = "float"),
		@ApiImplicitParam(name = "experienceid", value = "留学经验id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "freeage", value = "免费修改年限", required = false, dataType = "int"),
		@ApiImplicitParam(name = "certificateid", value = "证书id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "experience", value = "口译经验", required = false, dataType = "String"),
		@ApiImplicitParam(name = "make", value = "自我介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imageone", value = "风采展示图1", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagetwo", value = "风采展示图2", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagethree", value = "风采展示图3", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imageoneexplain", value = "风采展示图1说明", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagetwoexplain", value = "风采展示图2说明", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagethreeexplain", value = "风采展示图3说明", required = false, dataType = "String"),
		@ApiImplicitParam(name = "interpretertype", value = "口译类型（1交传、2同传、3陪同、4笔译）", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "bit"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "longitude", value = "经度", required = false, dataType = "String"),
		@ApiImplicitParam(name = "latitude", value = "纬度", required = false, dataType = "String")
	})
	public ResultBean<Integer> addInterpreter(Interpreter ine){
		ine.setTime(new Date());
		return biz.addInterpreter(ine);
	}
	
	@PostMapping("/selectByInter")
	@ApiOperation(value ="查询译员列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "translatelanguage", value = "翻译语言id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "intolanguage", value = "翻译成语言id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "translategrade", value = "翻译语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "intograde", value = "翻译成语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "interpretertype", value = "口译类型（1交传、2同传、3陪同、4笔译）", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pricetype", value = "1价格(元/小时)2价格(元/分钟)3价格(元/字)", required = false, dataType = "int"),
		@ApiImplicitParam(name = "address", value = "地址", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "bit"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "interpretAge", value = "口译年限(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "ages", value = "年龄段(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "prices", value = "价格段(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = false, dataType = "String"),          //年龄 价格
		@ApiImplicitParam(name = "experience", value = "口译经验", required = false, dataType = "String"),
		@ApiImplicitParam(name = "territoryid", value = "擅长领域id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "experienceid", value = "留学经验id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = true, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = true, dataType = "int"),
		@ApiImplicitParam(name = "score", value = "筛选方式", required = true, dataType = "int"),
		@ApiImplicitParam(name = "longitude", value = "经度", required = true, dataType = "double"),
		@ApiImplicitParam(name = "latitude", value = "纬度", required = true, dataType = "double"),
		@ApiImplicitParam(name = "keyword", value = "关键字", required = false, dataType = "String")
		
	})
	public ResultBean<List<Map<String, Object>>> selectByInter(Interpreter ite, Integer pricetype, String interpretAge, String ages,String prices,int score,String nationality,Integer pageIndex,Double longitude,Double latitude,int size,@RequestParam(value="keyword",defaultValue="")String keyword){
		return biz.selectByInter(ite,pricetype,interpretAge,ages, prices, score, nationality,pageIndex,longitude,latitude,size,keyword);
	}
	
	@PostMapping("/selectByInterCount")
	@ApiOperation(value ="PC查询译员列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "translatelanguage", value = "翻译语言id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "intolanguage", value = "翻译成语言id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "translategrade", value = "翻译语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "intograde", value = "翻译成语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "interpretertype", value = "口译类型（1交传、2同传、3陪同、4笔译）", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pricetype", value = "1价格(元/小时)2价格(元/分钟)3价格(元/字)", required = false, dataType = "int"),
		@ApiImplicitParam(name = "address", value = "地址", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "bit"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "age", value = "口译年限", required = false, dataType = "int"),
		@ApiImplicitParam(name = "ages", value = "年龄段(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "prices", value = "价格段(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = false, dataType = "String"),          //年龄 价格
		@ApiImplicitParam(name = "experience", value = "口译经验", required = false, dataType = "String"),
		@ApiImplicitParam(name = "territoryid", value = "擅长领域id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "experienceid", value = "留学经验id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "keyword", value = "关键字", required = false, dataType = "String")
		
	})
	public ResultBean<Integer> selectByInterCount(Interpreter ite, Integer pricetype, String ages,String prices,int score,String nationality,@RequestParam(value="keyWord",defaultValue="")String keyword){
		return biz.selectByInterCount(ite,pricetype,ages, prices, score, nationality,keyword);
	}
	
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端找翻译详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "用户找翻译id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "登录人用户id", required = false, dataType = "int"),
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userId){
		return biz.selectById(id, userId);
	}
	
	
	
	
	
}

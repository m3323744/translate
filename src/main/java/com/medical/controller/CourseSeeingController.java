package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CourseSeeing;
import com.medical.entity.ResultBean;
import com.medical.service.CourseSeeingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/courseSeeing")
@Api(description="视听课")
public class CourseSeeingController {
	@Autowired
	private CourseSeeingService biz;
	
	@PostMapping("/addCourseSeeing")
	@ApiOperation(value ="添加视听课", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "课程封面", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "name", value = "课程名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sources", value = "课程来源", required = false, dataType = "String"),
		@ApiImplicitParam(name = "totalprice", value = "课程总价", required = false, dataType = "double"),
		@ApiImplicitParam(name = "hour", value = "课时", required = false, dataType = "int"),
		@ApiImplicitParam(name = "isspelling", value = "是否接受拼客", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "num", value = "拼课人数", required = false, dataType = "int"),
		@ApiImplicitParam(name = "spellingprice", value = "拼课价", required = false, dataType = "double"),
		@ApiImplicitParam(name = "introduceimage", value = "课程介绍图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "schoolid", value = "学校id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "simple", value = "分节id逗号分隔", required = false, dataType = "String"),
		@ApiImplicitParam(name = "merchantid", value = "商家用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addCourseSeeing(CourseSeeing cs,String simple){
		return biz.addCourseSeeing(cs, simple);
	}
	
	@PostMapping("/selectByTeacherId")
	@ApiOperation(value ="查询老师的课程", notes="")
	@ApiImplicitParam(name = "userId", value = "教师用户ID", required = false, dataType = "int")
	public ResultBean<List<Map<String, Object>>> selectByTeacherId(int userId){
		return biz.selectByTeacherId(userId);
	}
	
	@PostMapping("/selectBySchoolId")
	@ApiOperation(value ="查询学校的课程", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "schoolId", value = "学校ID", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<List<Map<String, Object>>> selectBySchoolId(int schoolId,int pageIndex,int size){
		return biz.selectBySchoolId(schoolId,pageIndex,size);
	}
	
	@PostMapping("/selectBySchoolIdCount")
	@ApiOperation(value ="查询学校的课程总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "schoolId", value = "学校ID", required = false, dataType = "int")
	})
	public ResultBean<Integer> selectBySchoolIdCount(int schoolId){
		return biz.selectBySchoolIdCount(schoolId);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端，根据id查询视听课详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "课程id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "int")
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userid){
		return biz.selectById(id,userid);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端，查询视听课列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sources", value = "课程来源", required = false, dataType = "String"),
		@ApiImplicitParam(name = "prices", value = "价格区间（-分隔）", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hours", value = "课时(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "isspelling", value = "是否接受拼客", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "days", value = "时间区间", required = false, dataType = "int"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "score", value = "排序方式 1默认  2人气 3价格", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
	})
	public ResultBean<List<Map<String, Object>>> getList(CourseSeeing cs,String hours,String prices,Integer days,int score,@RequestParam(value="keyWord",defaultValue="")String keyWord,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,int size){
		return biz.getList(cs,hours, prices, days, score, keyWord, pageIndex, size);
	}
	
	@PostMapping("/getSpellingList")
	public ResultBean<List<Map<String, Object>>> getSpellingList(int pageIndex,int size,String keyWord){
		return biz.getSpellingList(pageIndex,size,keyWord);
	}
	
	@PostMapping("/getSpellingListCount")
	public ResultBean<Integer> getSpellingListCount(){
		return biz.getSpellingListCount();
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端PC，查询视听课列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sources", value = "课程来源", required = false, dataType = "String"),
		@ApiImplicitParam(name = "prices", value = "价格区间（-分隔）", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hours", value = "课时(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "isspelling", value = "是否接受拼客", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "days", value = "时间区间", required = false, dataType = "int"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String")
	})
	public ResultBean<Integer> getListCount(CourseSeeing cs,String hours,String prices,Integer days,@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(cs, hours, prices, days, keyWord);
	}
}

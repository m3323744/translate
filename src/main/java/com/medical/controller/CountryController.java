package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Country;
import com.medical.entity.ResultBean;
import com.medical.service.CountryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/country")
@Api(description="国家")
public class CountryController {
	@Autowired
	private CountryService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value ="国家列表", notes="")
	public ResultBean<List<Country>> getList(){
		return biz.getList();
	}
}

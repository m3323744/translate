package com.medical.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CourseOffline;
import com.medical.entity.ResultBean;
import com.medical.service.CourseOfflineService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/courseoffline")
@Api(description="线下课")
public class CourseOfflineController {
	@Autowired
	private CourseOfflineService biz;
	
	@PostMapping("/addCourseSeeing")
	@ApiOperation(value ="发布线下课", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "封面", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "课程名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "attendtime", value = "上课时间", required = false, dataType = "datetime"),
		@ApiImplicitParam(name = "address", value = "上课地点", required = false, dataType = "String"),
		@ApiImplicitParam(name = "teacher", value = "授课教师", required = false, dataType = "String"),
		@ApiImplicitParam(name = "totalprice", value = "课程总价", required = false, dataType = "float"),
		@ApiImplicitParam(name = "hour", value = "课时", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sources", value = "课程来源", required = false, dataType = "String"),
		@ApiImplicitParam(name = "introduceimages", value = "课程介绍图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "schoolid", value = "学校id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "simple", value = "分节id，逗号分隔", required = false, dataType = "String"),
		@ApiImplicitParam(name = "merchantid", value = "商家用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addCourseSeeing(CourseOffline co,String simple){
		co.setTime(new Date());
		return biz.addCourseSeeing(co, simple);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端，根据id查询线下课详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "课程id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "发布人id", required = false, dataType = "int")
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userId){
		return biz.selectById(id,userId);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端，查询线下课列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sources", value = "课程来源", required = false, dataType = "String"),
		@ApiImplicitParam(name = "prices", value = "价格区间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hours", value = "课时", required = false, dataType = "String"),
		@ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "地址", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "score", value = "排序方式  1人气 2价格3时间", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
	})
	public ResultBean<List<Map<String, Object>>> getList(CourseOffline cs,String hours,String prices,String startTime,String endTime,int score,@RequestParam(value="keyWord",defaultValue="")String keyWord,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,int size){
		return biz.getList(cs,hours, prices, startTime, endTime, score, keyWord, pageIndex, size);

	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端PC，查询线下课列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sources", value = "课程来源", required = false, dataType = "String"),
		@ApiImplicitParam(name = "prices", value = "价格区间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hours", value = "课时", required = false, dataType = "String"),
		@ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "地址", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String")
	})
	public ResultBean<Integer> getListCount(CourseOffline cs,String hours,String prices,String startTime,String endTime,@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(cs, hours, prices, startTime, endTime, keyWord);
	}
}

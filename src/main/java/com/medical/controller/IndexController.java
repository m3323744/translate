package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.InterpreterService;
import com.medical.service.JobService;
import com.medical.service.TeacherService;
import com.medical.util.RandomUtil;
import com.medical.util.SmsUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@Api(description="首页")
@RestController
@RequestMapping("/index")
public class IndexController {
	@Autowired
	private InterpreterService interpreterBiz;
	@Autowired
	private TeacherService teacherBiz;
	@Autowired
	private JobService jobService;
	
	@PostMapping("/findHotInterpreters")
	@ApiOperation(value ="热门翻译", notes="")
	public ResultBean<List<Map<String, Object>>> findHotInterpreters(){
		return interpreterBiz.findHotInterpreters();
	}
	
	@PostMapping("/findHotTeachers")
	@ApiOperation(value ="热门老师", notes="")
	public ResultBean<List<Map<String, Object>>> findHotTeachers(){
		return teacherBiz.findHotTeachers();
	}
	
	@PostMapping("/findHotJob")
	@ApiOperation(value ="推荐职位", notes="")
	public ResultBean<List<Map<String, Object>>> findHotJobs(){
		return jobService.findHotJobs();
	}

	@PostMapping("/sms")
	@ApiOperation(value ="发短信", notes="")
	@ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "String")
	public ResultBean<String> sms(String phone) {
		String rand=RandomUtil.rand();
		SmsUtil.sms(phone, rand);
		return new ResultBean<String>(rand);
	}
}

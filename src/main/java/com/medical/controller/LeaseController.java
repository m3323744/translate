package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Lease;
import com.medical.entity.ResultBean;
import com.medical.service.LeaseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/lease")
@Api(description="租赁操作")
public class LeaseController {
	@Autowired
	private LeaseService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value="获得租赁设备列表")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = true, dataType = "int"),
		@ApiImplicitParam(name = "type", value = "租赁类别id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = true, dataType = "int"),
	})
	public ResultBean<List<Lease>> getList(int pageIndex,int type,int size){
		return biz.getList(pageIndex, type,size);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value="租赁设备详情")
	@ApiImplicitParam(name = "id", value = "租赁设备id", required = true, dataType = "int")
	public ResultBean<Map<String, Object>> selectById(int id){
		return biz.selectById(id);
	}
}


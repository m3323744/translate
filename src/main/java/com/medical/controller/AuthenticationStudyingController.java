package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.AuthenticationStudying;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationStudyingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/authenStudying")
@Api(description="留学经历管理")
public class AuthenticationStudyingController {
	@Autowired
	private AuthenticationStudyingService biz;
	
	@PostMapping("/addAuthenticationStudying")
	@ApiOperation(value ="添加认证留学经历，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "country", value = "留学国家", required = true, dataType = "datetime"),
		@ApiImplicitParam(name = "school", value = "留学学校", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "major", value = "专业", required = true, dataType = "String"),
		@ApiImplicitParam(name = "begintime", value = "开始时间", required = true, dataType = "String"),
		@ApiImplicitParam(name = "endtime", value = "结束时间", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<Integer> addAuthenticationStudying(@ModelAttribute AuthenticationStudying ats){
		return biz.addStudying(ats);
	}
	
	@PostMapping("/getDateils")
	@ApiOperation(value ="认证留学经历详情", notes="")
	@ApiImplicitParam(name = "id", value = "认证留学经历id", required = true, dataType = "int")
	public ResultBean<AuthenticationStudying> getDateils(int id){
		return biz.getDateils(id);
	}
	
	@ApiOperation(value ="添加留学经历详情列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<List<AuthenticationStudying>> getList(int authenticationid,int isteacher){
		return biz.getList(authenticationid,isteacher);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="添加留学经历详情列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "id逗号分隔", required = false, dataType = "String")
	public ResultBean<List<AuthenticationStudying>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
	
	@PostMapping("/selectByUserId")
	@ApiOperation(value ="根据用户id获取留学经验列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "type", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<List<AuthenticationStudying>> selectByUserId(int type,int userId){
		return biz.selectByUserId(type, userId);
	}
	
	@PostMapping("/delStudying")
	@ApiOperation(value ="删除留学经历", notes="")
	@ApiImplicitParam(name = "id", value = "认证留学经历id", required = true, dataType = "int")
	public ResultBean<Integer> delStudying(int id){
		return biz.delStudying(id);
	}
	
	@PostMapping("/updateAuthenticationStudying")
	@ApiOperation(value ="添加认证留学经历，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "认证留学经历id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "country", value = "留学国家", required = true, dataType = "datetime"),
		@ApiImplicitParam(name = "school", value = "留学学校", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "major", value = "专业", required = true, dataType = "String"),
		@ApiImplicitParam(name = "begintime", value = "开始时间", required = true, dataType = "String"),
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "endtime", value = "结束时间", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<Integer> updateAuthenticationStudying(@ModelAttribute AuthenticationStudying ats){
		return biz.updateAuthenticationStudying(ats);
	}
}

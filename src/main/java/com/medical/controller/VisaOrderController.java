package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.VisaOrder;
import com.medical.service.VisaOrderSerivce;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/visaOrder")
@Api(description="签证订单")
public class VisaOrderController {
	@Autowired
	private VisaOrderSerivce biz;
	
	@PostMapping("/addVisaOrder")
	@ApiOperation(value ="添加签证订单", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "visaid", value = "签证id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "departdate", value = "出发日期", required = false, dataType = "date"),
		@ApiImplicitParam(name = "name", value = "申请人姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "age", value = "年龄段", required = false, dataType = "String"),
		@ApiImplicitParam(name = "clienttype", value = "客户类型", required = false, dataType = "String"),
		@ApiImplicitParam(name = "passportnum", value = "护照号码", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "ordernumber", value = "订单号", required = false, dataType = "String"),
		@ApiImplicitParam(name = "paytype", value = "支付方式", required = false, dataType = "int"),
		@ApiImplicitParam(name = "totalprice", value = "总价", required = false, dataType = "float"),
		@ApiImplicitParam(name = "merchantid", value = "商家用户id", required = false, dataType = "int")
	})
	public ResultBean<VisaOrder> addVisaOrder(VisaOrder vo){
		return biz.addVisaOrder(vo);
	}
}

package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.AuthenticationSchool;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationSchoolService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/authenschool")
@Api(description="认证教育经历")
public class AuthenticationSchoolController {
	@Autowired
	private AuthenticationSchoolService biz;
	
	@PostMapping("/addAuthenticationSchool")
	@ApiOperation(value ="添加认证毕业院校，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "graduationdate", value = "毕业时间", required = true, dataType = "datetime"),
		@ApiImplicitParam(name = "school", value = "毕业学校", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "major", value = "专业", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<Integer> addAuthenticationSchool(@ModelAttribute AuthenticationSchool acs){
		return biz.addSchool(acs);
	}
	
	@PostMapping("/getDateils")
	@ApiOperation(value ="认证毕业院校详情", notes="")
	@ApiImplicitParam(name = "id", value = "认证毕业院校id", required = true, dataType = "int")
	public ResultBean<AuthenticationSchool> getDateils(int id){
		return biz.getDateils(id);
	}
	
	@ApiOperation(value ="添加毕业院校详情列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<List<AuthenticationSchool>> getList(int authenticationid,int isteacher){
		return biz.getList(authenticationid,isteacher);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="添加毕业院校详情列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "认证证书id,逗号分隔", required = true, dataType = "String")
	public ResultBean<List<AuthenticationSchool>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
	
	@PostMapping("/delSchool")
	@ApiOperation(value ="删除毕业院校", notes="")
	@ApiImplicitParam(name = "id", value = "认证毕业院校id", required = true, dataType = "int")
	public ResultBean<Integer> delSchool(int id){
		return biz.delSchool(id);
	}
	
	@PostMapping("/updateAuthenticationSchool")
	@ApiOperation(value ="添加认证毕业院校，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "认证毕业院校id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "graduationdate", value = "毕业时间", required = true, dataType = "datetime"),
		@ApiImplicitParam(name = "school", value = "毕业学校", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "major", value = "专业", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<Integer> updateAuthenticationSchool(@ModelAttribute AuthenticationSchool acs){
		return biz.updateAuthenticationSchool(acs);
	}
}

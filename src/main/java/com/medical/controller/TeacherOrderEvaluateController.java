package com.medical.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.TeacherOrderEvaluateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/teacherOrderEvaluate")
@Api(description="老师订单评价操作")
public class TeacherOrderEvaluateController {
	@Autowired
	private TeacherOrderEvaluateService biz;
	
	@PostMapping("/selectByOrderId")
	@ApiOperation(value="获取订单评价，订单id方式获取", notes="")
	@ApiImplicitParams({
	@ApiImplicitParam(name = "orderId", value = "订单id", required = true, dataType = "int",paramType="query"),
	@ApiImplicitParam(name = "type", value = "评价类型", required = true, dataType = "int",paramType="query")
	})
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId,int type){
		return biz.selectByOrderId(orderId,type);
	}
	
	@PostMapping("/addEvaluate")
	@ApiOperation(value="添加评价", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "score", value = "评论分", required = true, dataType = "double",paramType="query"),
		@ApiImplicitParam(name = "orderid", value = "订单id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "content", value = "评论内容", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "courseid", value = "课程id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "type", value = "评价类型 1老师 2译员 3课程 4签证", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "courseType", value = "课程类型 1视听课 2直播课 3线下课", required = true, dataType = "int",paramType="query")
	})
	public ResultBean<Integer> addEvaluate(Float score,int orderid,int userid,String content,int courseType,int type,int courseid){
		return biz.addEvaluate(score,orderid, userid,content,courseType,type,courseid);
	}
	
	@PostMapping("/selectByTeacherId")
	@ApiOperation(value="获取订单评价，教师id 方式获取", notes="")
	@ApiImplicitParam(name = "teacherId", value = "老师id", required = true, dataType = "int",paramType="query")
	public ResultBean<Map<String, Object>> selectByTeacherId(int teacherId){
		return biz.selectByTeacherId(teacherId);
	}
}

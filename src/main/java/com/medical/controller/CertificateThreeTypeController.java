package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CertificateThreeType;
import com.medical.entity.ResultBean;
import com.medical.service.CertificateThreeTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("certificateThreeType")
@Api(description="证书三级类别")
public class CertificateThreeTypeController {
	@Autowired
	private CertificateThreeTypeService biz;
	
	@PostMapping("/getByTwoId")
	@ApiOperation(value ="获取三级证书类别", notes="")
	@ApiImplicitParam(name = "twotypeId", value = "二级证书id", required = true, dataType = "int")
	public ResultBean<List<CertificateThreeType>> getByTwoId(int twotypeId){
		return biz.getByTwoId(twotypeId);
	}
}

package com.medical.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.ResultBean;
import com.medical.entity.TeacherAuthenticationcredit;
import com.medical.service.TeacherAuthenticationcreditService;
import com.medical.util.uploadUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/teachercredit")
@Api(description="老师信增材料管理")
public class TeacherAuthenticationcreditController {
	@Autowired
	private TeacherAuthenticationcreditService biz;
	
	@PostMapping("/addTeacherAuthenticationcredit")
	@ApiOperation(value ="添加增信材料，成功返回添加的id", notes="")
	@ApiImplicitParam(name = "file", value = "增信文件", required = true, dataType = "file")
	public ResultBean<TeacherAuthenticationcredit> addTeacherAuthenticationcredit( @RequestParam(value="file",required=true)MultipartFile file,HttpServletRequest request){
		String url=uploadUtil.uploadYa(request, file);
		TeacherAuthenticationcredit tac=new TeacherAuthenticationcredit();
		tac.setImages(url);
		return biz.addTeacherAuthenticationcredit(tac);
	}
	
	@PostMapping("/updateTeacherAuthenticationcredit")
	@ApiOperation(value ="更新增信材料", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "file", value = "增信文件", required = true, dataType = "file"),
		@ApiImplicitParam(name = "id", value = "增信文件id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "authenticationid", value = "老师认证id", required = true, dataType = "int")
	})
	
	public ResultBean<TeacherAuthenticationcredit> updateTeacherAuthenticationcredit(@RequestParam(value="file",required=true)MultipartFile file,int id,int authenticationid,HttpServletRequest request){
		String url=uploadUtil.uploadYa(request, file);
		TeacherAuthenticationcredit tac=new TeacherAuthenticationcredit();
		tac.setImages(url);
		tac.setId(id);
		tac.setAuthenticationid(authenticationid);
		return biz.addTeacherAuthenticationcredit(tac);
	}
	
	@ApiOperation(value ="添加增信材料列表", notes="")
	@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int")
	public ResultBean<List<TeacherAuthenticationcredit>> getList(int authenticationid){
		return biz.getList(authenticationid);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="添加增信材料列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "认证证书id,逗号分隔", required = true, dataType = "String")
	public ResultBean<List<TeacherAuthenticationcredit>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
}

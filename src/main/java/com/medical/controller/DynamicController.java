package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Dynamic;
import com.medical.entity.DynamicComment;
import com.medical.entity.ResultBean;
import com.medical.service.DynamicService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dynamic")
@Api(description="社区相关")
public class DynamicController {
	@Autowired
	private DynamicService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value="用户端查询社区动态列表",notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<List<Map<String, Object>>> getList(int pageIndex,int size,int userId){
		return biz.getList(pageIndex, size,userId);
	}
	
	@PostMapping("/getListByCount")
	@ApiOperation(value="用户端查询社区动态列表条数",notes="")
	public ResultBean<Integer> getListByCount(int userId){
		return biz.getListByCount();
	}
	
	@PostMapping("/getdetailById")
	@ApiOperation(value="查询动态详情",notes="")
	@ApiImplicitParam(name = "id", value = "动态id", required = false, dataType = "int")
	public ResultBean<Map<String, Object>> getdetailById(int id){
		return biz.getdetailById(id);
	}
	
	@PostMapping("/delDynamic")
	@ApiOperation(value="删除动态",notes="")
	@ApiImplicitParam(name = "id", value = "动态id", required = false, dataType = "int")
	public ResultBean<Integer> delDynamic(int id){
		return biz.delDynamic(id);
	}
	
	@PostMapping("/addDynamic")
	@ApiOperation(value="添加动态",notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "image", value = "图片路径", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "content", value = "动态内容", required = false, dataType = "String")
	})
	public ResultBean<Dynamic> addDynamic(Dynamic dy){
		return biz.addDynamic(dy);
	}
	
	@PostMapping("/likeDynamic")
	@ApiOperation(value="动态点赞",notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "动态id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> likeDynamic(int id,int userid){
		return biz.likeDynamic(id, userid);
	}
	
	@PostMapping("/evaluateDynamic")
	@ApiOperation(value="评论动态",notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "dynamicid", value = "动态id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "content", value = "评论内容", required = false, dataType = "String")
	})
	public ResultBean<Integer> evaluateDynamic(DynamicComment dc){
		return biz.evaluateDynamic(dc);
	}
}

package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.PlatformInformation;
import com.medical.entity.ResultBean;
import com.medical.service.PlatformInformationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/platformInformation")
@Api(description="平台须知")
public class PlatformInformationController {
	@Autowired
	private PlatformInformationService biz;
	
	@PostMapping("/getByType")
	@ApiOperation(value ="按类型查列表", notes="")
	@ApiImplicitParam(name = "type", value = "类别：1笔译服务(需求方条款) 2笔译服务(译员条款) 3国际商务 4会议会展注册条款 5口译服务(需求方条款) 6口译服务(译员条款) 7老师须知条款(需求方) 8鸟语网信息发布规范 9评价规则 10签证服务 11学生须知条款(购买方) 12隐私条款 13用户条款(总条款)", required = false, dataType = "int")
	public ResultBean<PlatformInformation> getByType(int type){
		return biz.getByType(type);
	}
}

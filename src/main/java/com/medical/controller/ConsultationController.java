package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.ConsultationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 咨询controller（国际商务）
 * @author 76449
 *
 */
@RestController
@RequestMapping("/consultation")
@Api(description="咨询相关")
public class ConsultationController {
	@Autowired
	private ConsultationService consulationService;

	/**
	 * 查询类型 列表
	 * @param type 咨询类型 
	 * @return
	 */
	@PostMapping("/findtypeList")
	@ApiOperation(value ="查询类型 列表", notes="")
	@ApiImplicitParam(name = "type", value = "查询类别  1投资 2财税 3法律", required = false, dataType = "int")
	public ResultBean<List<Map<String,Object>>> consultationType(Integer type){
		List<Map<String,Object>> typeList=consulationService.findTypeList(type);
		return new ResultBean<>(typeList);
	}
	/**
	 * 查询内容
	 * @param contentType 文章类型
	 * @return
	 */
	@PostMapping("/fingContent")
	@ApiOperation(value ="查询类型内容", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "typeId", value = "查询一级类别", required = false, dataType = "int"),
		@ApiImplicitParam(name = "twotypeId", value = "查询二级类别", required = false, dataType = "int")
	})
	
	public ResultBean<Map<String,Object>> findContent(Integer typeId,Integer twotypeId){
		Map<String,Object> map=consulationService.findContentByType(typeId,twotypeId);
		return new ResultBean<>(map);
	}
}

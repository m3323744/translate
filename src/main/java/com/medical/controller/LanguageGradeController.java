package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.LanguageGrade;
import com.medical.entity.ResultBean;
import com.medical.service.LanguageGradeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/languageGrade")
@Api(description="语言等级")
public class LanguageGradeController {
	@Autowired
	private LanguageGradeService biz;
	
	@PostMapping("/getByLanguageId")
	@ApiOperation(value ="查询语种对应等级", notes="")
	@ApiImplicitParam(name = "languageId", value = "语种id", required = false, dataType = "int")
	public ResultBean<List<LanguageGrade>> getByLanguageId(int languageId){
		return biz.getByLanguageId(languageId);
	}
}

package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.School;
import com.medical.service.SchoolService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/school")
@Api(description="学校认证")
public class SchoolController {
	@Autowired
	private SchoolService biz;
	
	@PostMapping("/addSchool")
	@ApiOperation(value ="添加学校", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "学校封面", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "学校名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languageid", value = "开设语种id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hauptstudiumid", value = "课程阶段id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "地点", required = false, dataType = "String"),
		@ApiImplicitParam(name = "introduce", value = "介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "longitude", value = "经度", required = false, dataType = "String"),
		@ApiImplicitParam(name = "latitude", value = "纬度", required = false, dataType = "String")
	})
	public ResultBean<Integer> addSchool(School school){
		
		return biz.addSchool(school);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端查找学校列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "lanuageId", value = "开设语种id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hauptstudiumid", value = "课程阶段id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "地点", required = false, dataType = "String"),
		@ApiImplicitParam(name = "longitude", value = "经度", required = true, dataType = "double"),
		@ApiImplicitParam(name = "latitude", value = "纬度", required = true, dataType = "double"),
		@ApiImplicitParam(name = "score", value = "排序方式(1时间 2人气3距离 )", required = true, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = true, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = true, dataType = "int"),
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = true, dataType = "String")
	})
	public ResultBean<List<Map<String, Object>>> getList(String address,Integer lanuageId,String hauptstudiumid,int score,double longitude,double latitude,int pageIndex,int size,@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getList(address, lanuageId, hauptstudiumid, score,longitude,latitude,pageIndex,size,keyWord);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端PC查找学校列表总页数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "lanuageId", value = "开设语种id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hauptstudiumid", value = "课程阶段id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "地点", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyword", value = "关键字", required = true, dataType = "String")
	})
	public ResultBean<Integer> getListCount(String address,Integer lanuageId,String hauptstudiumid,@RequestParam(value="keyWord",defaultValue="")String keyword){
		return biz.getListCount(address, lanuageId, hauptstudiumid, keyword);
	}
	
	@PostMapping("/getByUserid")
	@ApiOperation(value ="翻译端查找学校id", notes="")
	@ApiImplicitParam(name = "userId", value = "发布人id", required = false, dataType = "int")
	public ResultBean<School> getByUserid(int userId){
		return biz.getByUserid(userId);
	}
	
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端找学校详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "用户找学校id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "登录人用户id", required = false, dataType = "int"),
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userId){
		return biz.selectById(id, userId);
	}
}

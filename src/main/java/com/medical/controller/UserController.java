package com.medical.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.UserService;
import com.medical.util.uploadUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/user")
@Api(description="用户管理")
public class UserController {
	@Autowired
	private UserService biz;
	
	@ApiOperation(value ="微信注册方法", notes="")
	@RequestMapping(value="/wxRegister",method=RequestMethod.POST)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openid", value = "微信", required = true, dataType = "String"),
		@ApiImplicitParam(name = "nickname", value = "微信昵称", required = true, dataType = "String"),
		@ApiImplicitParam(name = "portrait", value = "微信头像", required = true, dataType = "String")
	})
	public ResultBean<User> wxRegister(String openid, String nickname, String portrait){
		return biz.wxRegister(openid, nickname, portrait);
	}
	
	@RequestMapping(value="/upload",method=RequestMethod.POST)
	@ApiOperation(value ="上传方法,更换头像", notes="")
	@ApiImplicitParam(name = "file", value = "头像", required = true, dataType = "file")
	public ResultBean<String> upload(HttpServletRequest request,@RequestParam(value="file",required=true)MultipartFile file){
		try {
			String url=uploadUtil.uploadYa(request, file);
			return new ResultBean<String>(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResultBean<>(e);
		}
		
	}
	
	@RequestMapping(value="/updateUser",method=RequestMethod.POST)
	@ApiOperation(value ="更新用户信息", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "birthday", value = "生日", required = false, dataType = "String"),
		@ApiImplicitParam(name = "portrait", value = "头像", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "电话", required = false, dataType = "String"),
		@ApiImplicitParam(name = "email", value = "邮箱", required = false, dataType = "String"),
		@ApiImplicitParam(name = "wechat", value = "微信号", required = false, dataType = "String"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "age", value = "年龄", required = false, dataType = "int")
	})
	public ResultBean<User> updateUser(@ModelAttribute User user){
		return biz.updateUser(user);
	}
	
	@PostMapping("/userDateils")
	@ApiOperation(value ="用户详情", notes="")
	@ApiImplicitParam(name = "id", value = "用户id", required = true, dataType = "int")
	public ResultBean<Map<String, Object>> userDateils(int id){
		return biz.userDateils(id);
	}
	
	
	 
	 
	@PostMapping("/getOpenId")
	@ApiOperation(value ="用户openid", notes="")
	@ApiImplicitParam(name = "openId", value = "用户openId", required = true, dataType = "String")
	public ResultBean<User> getOpenId(String openId){
		return biz.getOpenId(openId);
	}
	
	@ApiOperation(value ="更换手机号", notes="")
	@RequestMapping(value="/updatePhone",method=RequestMethod.POST)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "String")
	})
	public ResultBean<User> updatePhone(int userId,String phone){
		return biz.updatePhone(userId, phone);
	}
	
	@PostMapping("/userDateilsByPhone")
	@ApiOperation(value ="手机登录", notes="")
	@ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "String")
	public ResultBean<User> userDateilsByPhone(String phone){
		return biz.userDateilsByPhone(phone);
	}
}

package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Opinion;
import com.medical.entity.ResultBean;
import com.medical.service.OpinionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/opinion")
@Api(description="意见管理")
public class OpinionController {
	@Autowired
	private OpinionService biz;
	
	@PostMapping("/addOpinion")
	@ApiOperation(value ="添加意见", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "title", value = "标题", required = true, dataType = "String"),
		@ApiImplicitParam(name = "centont", value = "意见", required = true, dataType = "String"),
		@ApiImplicitParam(name = "imageone", value = "图片1", required = true, dataType = "String"),
		@ApiImplicitParam(name = "imagetwo", value = "图片2", required = true, dataType = "String"),
		@ApiImplicitParam(name = "imagethree", value = "图片3", required = true, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = true, dataType = "String")
	})
	public ResultBean<Integer> addOpinion(Opinion op){
		return biz.addOpinion(op);
	}
}

package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.Wanted;
import com.medical.entity.Wantedcertificate;
import com.medical.service.WantedService;
import com.medical.service.WantedcertificateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/wanted")
@Api(description="求职操作表")
public class WantedController {
	@Autowired
	private WantedService biz;
	
	@Autowired
	private WantedcertificateService wBiz;
	
	@PostMapping("/addWanted")
	@ApiOperation(value="添加求职信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "title", value = "求职标题", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "求职人", required = false, dataType = "String"),
		@ApiImplicitParam(name = "city", value = "所在城市", required = false, dataType = "String"),
		@ApiImplicitParam(name = "salary", value = "要求工资", required = false, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "age", value = "年龄", required = false, dataType = "int"),
		@ApiImplicitParam(name = "school", value = "毕业学校", required = false, dataType = "String"),
		@ApiImplicitParam(name = "work", value = "工作经验", required = false, dataType = "String"),
		@ApiImplicitParam(name = "email", value = "联系邮箱", required = false, dataType = "String"),
		@ApiImplicitParam(name = "description", value = "经验描述", required = false, dataType = "String"),
		@ApiImplicitParam(name = "exhibition", value = "风采展示", required = false, dataType = "String"),
		@ApiImplicitParam(name = "images", value = "本人照片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "position", value = "应聘职位", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "degree", value = "学位", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "wantedcertificates", value = "证书id(逗号分隔)", required = false, dataType = "String")
	})
	public ResultBean<Integer> addWanted(Wanted wanted,String wantedcertificates){
		return biz.addWanted(wanted, wantedcertificates);
	}
	
	@PostMapping("/addWantedcertificate")
	@ApiOperation(value="求职证书添加")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "time", value = "有效日期", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "证书名字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "type", value = "证书分类", required = false, dataType = "String"),
		@ApiImplicitParam(name = "certnumber", value = "证书号", required = false, dataType = "String"),
		@ApiImplicitParam(name = "perpetual", value = "是否永久有效", required = true, dataType = "boolean")
	})
	public ResultBean<Integer> addWantedcertificate(Wantedcertificate wantedcertificate){
		return wBiz.addWantedcertificate(wantedcertificate);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value="获取求职列表")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<Map<String, Object>> getList(@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,@RequestParam(value="keyWord",defaultValue="")String keyWord,@RequestParam(value="size",defaultValue="10")int size){
		return biz.getList(pageIndex, keyWord,size);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value="获取求职详情")
	@ApiImplicitParam(name = "id", value = "求职id", required = false, dataType = "int")
	public ResultBean<Map<String, Object>> selectById(int id){
		return biz.selectById(id);
	}
	
	@PostMapping("/selectByUserId")
	@ApiOperation(value="用户端，获取我的求职列表")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<List<Wanted>> selectByUserId(int userId,int pageIndex,int size){
		return biz.selectByUserId(userId,pageIndex,size);
	}
	
	@PostMapping("/selectByUserIdCount")
	@ApiOperation(value="PC用户端，获取我的求职列表总条数")
	@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int")
	public ResultBean<Integer> selectByUserIdCount(int userId){
		return biz.selectByUserIdCount(userId);
	}
	
	
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="添加认证证书详情列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "认证证书id,逗号分隔", required = true, dataType = "String")
	public ResultBean<List<Wantedcertificate>> selectByIds(String ids){
		return wBiz.selectByIds(ids);
	}
	
	@PostMapping("/delWantedcertificate")
	@ApiOperation(value="删除证书")
	@ApiImplicitParam(name = "id", value = "证书id", required = false, dataType = "int")
	public ResultBean<Integer> delWantedcertificate(int id){
		return wBiz.delWantedcertificate(id);
	}
	
	@PostMapping("/update")
	@ApiOperation(value="求职证书更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "证书id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "time", value = "有效日期", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "证书名字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "type", value = "证书分类", required = false, dataType = "String"),
		@ApiImplicitParam(name = "certnumber", value = "证书号", required = false, dataType = "String"),
		@ApiImplicitParam(name = "perpetual", value = "是否永久有效", required = true, dataType = "boolean")
	})
	public ResultBean<Wantedcertificate> update(Wantedcertificate wantedcertificate){
		return wBiz.update(wantedcertificate);
	}
	
	@PostMapping("/updateWanted")
	@ApiOperation(value="更新求职信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "求职id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "title", value = "求职标题", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "求职人", required = false, dataType = "String"),
		@ApiImplicitParam(name = "city", value = "所在城市", required = false, dataType = "String"),
		@ApiImplicitParam(name = "salary", value = "要求工资", required = false, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "age", value = "年龄", required = false, dataType = "int"),
		@ApiImplicitParam(name = "school", value = "毕业学校", required = false, dataType = "String"),
		@ApiImplicitParam(name = "work", value = "工作经验", required = false, dataType = "String"),
		@ApiImplicitParam(name = "email", value = "联系邮箱", required = false, dataType = "String"),
		@ApiImplicitParam(name = "description", value = "经验描述", required = false, dataType = "String"),
		@ApiImplicitParam(name = "exhibition", value = "风采展示", required = false, dataType = "String"),
		@ApiImplicitParam(name = "images", value = "本人照片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "position", value = "应聘职位", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "degree", value = "学位", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "wantedcertificates", value = "证书id(逗号分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "time", value = "发布时间", required = true, dataType = "date")
	})
	public ResultBean<Integer> updateWanted(Wanted wanted,String wantedcertificates){
		return biz.updateWanted(wanted, wantedcertificates);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value="用户端PC，获取我的求职列表总条数")
	@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String")
	public ResultBean<Integer> getListCount(@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(keyWord);
	}
}

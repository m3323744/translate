package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.AuthenticationCash;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationCashService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/authenticationCash")
@Api(description="保证金操作")
public class AuthenticationCashController {
	@Autowired
	private AuthenticationCashService biz;
	
	@PostMapping("/addAuthenticationCash")
	@ApiOperation(value ="缴纳保证金", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "money", value = "保证金金额", required = true, dataType = "double"),
		@ApiImplicitParam(name = "states", value = "缴纳状态 1缴纳 2未缴纳", required = true, dataType = "int"),
		@ApiImplicitParam(name = "paytype", value = "支付方式 1微信 2支付宝 3余额", required = true, dataType = "int"),
		@ApiImplicitParam(name = "ordernumber", value = "订单号", required = true, dataType = "String"),
	})
	public ResultBean<Integer> addAuthenticationCash(AuthenticationCash cash){
		return biz.addAuthenticationCash(cash);
	}
	
	@PostMapping("/refund")
	@ApiOperation(value ="退保证金", notes="")
	@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int")
	public ResultBean<Integer> refund(int userid){
		return biz.refund(userid);
	}
}

package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.InterpreterAuthenticationTerritory;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterAuthenticationTerritoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/interpreterAuthenticationTerritory")
@Api(description="译员擅长领域")
public class InterpreterAuthenticationTerritoryController {
	@Autowired
	private InterpreterAuthenticationTerritoryService biz;
	
	@PostMapping("/addInterpreterAuthenticationTerritory")
	@ApiOperation(value ="认证译员擅长领域", notes="")
	@ApiImplicitParam(name = "territory", value = "擅长领域", required = true, dataType = "String")
	public ResultBean<Integer> addInterpreterAuthenticationTerritory(InterpreterAuthenticationTerritory iat){
		return biz.addInterpreterAuthenticationTerritory(iat);
	}
	
	@PostMapping("/getDateils")
	@ApiOperation(value ="译员擅长领域详情", notes="")
	@ApiImplicitParam(name = "id", value = "译员擅长领域id", required = true, dataType = "int")
	public ResultBean<InterpreterAuthenticationTerritory> getDateils(int id){
		return biz.getDateils(id);
	}
	
	@ApiOperation(value ="译员擅长领域详情列表", notes="")
	@ApiImplicitParam(name = "interpreterid", value = "认证id", required = true, dataType = "int")
	public ResultBean<List<InterpreterAuthenticationTerritory>> getList(int interpreterid){
		return biz.getList(interpreterid);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="译员擅长领域详情列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "id逗号分隔", required = false, dataType = "String")
	public ResultBean<List<InterpreterAuthenticationTerritory>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
	
	@PostMapping("/getListByUserid")
	@ApiOperation(value ="翻译端，译员擅长领域列表，根据用户id获取", notes="")
	@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int")
	public ResultBean<List<InterpreterAuthenticationTerritory>> getListByUserid(int userId){
		return biz.getListByUserid(userId);
	}
	
	@PostMapping("/delTerritory")
	@ApiOperation(value ="删除擅长领域", notes="")
	@ApiImplicitParam(name = "id", value = "译员擅长领域id", required = true, dataType = "int")
	public ResultBean<Integer> delTerritory(int id){
		return biz.delTerritory(id);
	}
	
	@PostMapping("/updateInterpreterAuthenticationTerritory")
	@ApiOperation(value ="认证译员擅长领域", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "译员擅长领域id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "territory", value = "擅长领域", required = true, dataType = "String"),
		@ApiImplicitParam(name = "interpreterid", value = "认证译员id", required = true, dataType = "int")
	})
	
	public ResultBean<Integer> updateInterpreterAuthenticationTerritory(InterpreterAuthenticationTerritory iat){
		return biz.updateInterpreterAuthenticationTerritory(iat);
	}
}

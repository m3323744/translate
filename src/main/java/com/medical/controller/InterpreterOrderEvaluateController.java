package com.medical.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.InterpreterOrderEvaluate;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterOrderEvaluateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/interpreterOrderEvaluate")
@Api(description="译员订单评价操作")
public class InterpreterOrderEvaluateController {
	@Autowired
	private InterpreterOrderEvaluateService biz;
	
	@PostMapping("/selectByOrderId")
	@ApiOperation(value="获取订单评价，订单id方式获取", notes="")
	@ApiImplicitParam(name = "orderId", value = "订单id", required = true, dataType = "int",paramType="query")
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId){
		return biz.selectByOrderId(orderId);
	}
	
	@PostMapping("/addEvaluate")
	@ApiOperation(value="添加评价", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "score", value = "评论分", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "interpreterid", value = "译员id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "content", value = "评论内容", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int",paramType="query")
	})
	public ResultBean<Integer> addEvaluate(InterpreterOrderEvaluate eva){
		return biz.addEvaluate(eva);
	}
	
	@PostMapping("/selectByTeacherId")
	@ApiOperation(value="获取订单评价，翻译id方式获取", notes="")
	@ApiImplicitParam(name = "interpreterId", value = "翻译id", required = true, dataType = "int",paramType="query")
	public ResultBean<Map<String, Object>> selectByTeacherId(int interpreterId){
		return biz.selectByInterpreterId(interpreterId);
	}
}

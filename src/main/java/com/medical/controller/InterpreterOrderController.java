package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.InterpreterOrder;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterOrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/interpreterOrder")
@Api(description="译员订单操作")
public class InterpreterOrderController {
	@Autowired
	private InterpreterOrderService biz;
	
	@PostMapping("/addInterpreterOrder")
	@ApiOperation(value ="添加译员订单", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "num", value = "购买数量", required = false, dataType = "int"),
		@ApiImplicitParam(name = "valuationtype", value = "计价方式", required = false, dataType = "int"),
		@ApiImplicitParam(name = "address", value = "服务地点", required = false, dataType = "String"),
		@ApiImplicitParam(name = "starttime", value = "开始时间", required = false, dataType = "date"),
		@ApiImplicitParam(name = "endtime", value = "结束时间", required = false, dataType = "date"),
		@ApiImplicitParam(name = "phone", value = "电话", required = false, dataType = "String"),
		@ApiImplicitParam(name = "remark", value = "备注", required = false, dataType = "String"),
		@ApiImplicitParam(name = "totalprice", value = "总价", required = false, dataType = "float"),
		@ApiImplicitParam(name = "interpreterid", value = "译员id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "travellingtype", value = "差旅费方式", required = false, dataType = "int"),
		@ApiImplicitParam(name = "paytype", value = "支付方式", required = false, dataType = "int"),
		@ApiImplicitParam(name = "ordernumber", value = "订单号（时间戳生成）", required = false, dataType = "String"),
		@ApiImplicitParam(name = "merchantid", value = "商家userid", required = false, dataType = "int")
	})
	public ResultBean<InterpreterOrder> addInterpreterOrder(InterpreterOrder inter){
		return biz.addInterpreterOrder(inter);
	}
}

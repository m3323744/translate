package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Conference;
import com.medical.entity.ResultBean;
import com.medical.service.ConferenceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/conference")
@Api(description="会议相关")
public class ConferenceController {
	@Autowired
	private ConferenceService biz;
	
	@PostMapping("/addConference")
	@ApiOperation(value ="发布会议", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "展示图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "synopsis", value = "简介", required = false, dataType = "String"),
		@ApiImplicitParam(name = "price", value = "价格", required = false, dataType = "float"),
		@ApiImplicitParam(name = "time", value = "时间", required = false, dataType = "date"),
		@ApiImplicitParam(name = "host", value = "举办地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sponsor", value = "主办方", required = false, dataType = "String"),
		@ApiImplicitParam(name = "contractor", value = "承办方", required = false, dataType = "String"),
		@ApiImplicitParam(name = "apply", value = "报名条件", required = false, dataType = "String"),
		@ApiImplicitParam(name = "introduceimage", value = "会议介绍图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "num", value = "参会人数", required = false, dataType = "int"),
		@ApiImplicitParam(name = "isapply", value = "是否同意报名参会", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addConference(Conference cf){
		return biz.addConference(cf);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端，会议列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "score", value = "排序方式 1价格 2时间3默认", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<List<Conference>> getList(@RequestParam(value="keyWord",defaultValue="")String keyWord,int pageIndex,int score,int size){
		return biz.getList(keyWord, pageIndex, score,size);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端PC，会议列表总条数", notes="")
	@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String")
	public ResultBean<Integer> getListCount(@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(keyWord);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端，会议详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "会议id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userid){
		return biz.selectById(id,userid);
	}
	
	@PostMapping("/delConference")
	@ApiOperation(value ="用户端，会议删除", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "会议id", required = false, dataType = "int")
	})
	public ResultBean<Integer> delConference(int id){
		return biz.delConference(id);
	}
}

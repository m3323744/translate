package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CourseOrder;
import com.medical.entity.ResultBean;
import com.medical.service.CourseOrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/courseOrder")
@Api(description="课程订单")
public class CourseOrderController {
	@Autowired
	private CourseOrderService biz;
	
	@PostMapping("/addCourseOrder")
	@ApiOperation(value ="添加课程订单", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "name", value = "购买姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "email", value = "邮箱", required = false, dataType = "String"),
		@ApiImplicitParam(name = "courseid", value = "课程id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "lessonsid", value = "分节id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "isall", value = "是否全买 ", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "phone", value = "联系电话", required = false, dataType = "String"),
		@ApiImplicitParam(name = "remark", value = "备注", required = false, dataType = "String"),
		@ApiImplicitParam(name = "totalprice", value = "金额", required = false, dataType = "float"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "paytype", value = "支付方式 1微信支付 2支付宝 3余额支付", required = false, dataType = "int"),
		@ApiImplicitParam(name = "ordernumber", value = "订单号", required = false, dataType = "String"),
		@ApiImplicitParam(name = "coursetype", value = "课程类型 1视听课 2直播课 3线下课", required = false, dataType = "int"),
		@ApiImplicitParam(name = "merchantid", value = "商家userid", required = false, dataType = "int")
	})
	public ResultBean<CourseOrder> addCourseOrder(CourseOrder co){
		return biz.addCourseOrder(co);
	}
}

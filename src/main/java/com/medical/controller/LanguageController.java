package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Language;
import com.medical.entity.ResultBean;
import com.medical.service.LanguageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/language")
@Api(description="语种管理")
public class LanguageController {

	@Autowired
	private LanguageService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value ="语种列表", notes="")
	public ResultBean<List<Language>> getList(){
		return biz.getList();
	}
}

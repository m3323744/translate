package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.Slideshow;
import com.medical.service.SlideshowService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/show")
@Api(description="轮播图")
public class SlideshowController {
	@Autowired
	private SlideshowService biz;
	
	@PostMapping("/getList")
	@ApiOperation("轮播图列表")
	@ApiImplicitParam(name = "type", value = "1=翻译端 2用户端", required = false, dataType = "int")
	public ResultBean<List<Slideshow>> getList(int type){
		return biz.getList(type);
	}
}

package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.Visa;
import com.medical.service.VisaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/visa")
@Api(description="签证相关")
public class VisaController {
	@Autowired
	private VisaService biz;
	
	@PostMapping("/addVisa")
	@ApiOperation(value ="添加签证信息", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "area", value = "区域", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "country", value = "国家", required = false, dataType = "String"),
		@ApiImplicitParam(name = "isinterview", value = "是否面试", required = false, dataType = "String"),
		@ApiImplicitParam(name = "visatype", value = "签证类型", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sendaddress", value = "送签地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "company", value = "签证公司", required = false, dataType = "String"),
		@ApiImplicitParam(name = "images", value = "展示图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "price", value = "价格", required = false, dataType = "float"),
		@ApiImplicitParam(name = "arrivalsnum", value = "入境次数", required = false, dataType = "String"),
		@ApiImplicitParam(name = "duration", value = "办理时长", required = false, dataType = "String"),
		@ApiImplicitParam(name = "scope", value = "受理范围", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "validity", value = "有效期", required = false, dataType = "String"),
		@ApiImplicitParam(name = "remainum", value = "可停留天数", required = false, dataType = "int"),
		@ApiImplicitParam(name = "material", value = "所需材料", required = false, dataType = "String"),
		@ApiImplicitParam(name = "matters", value = "其他注意事项", required = false, dataType = "String"),
		@ApiImplicitParam(name = "flow", value = "办理流程", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "服务名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addVisa(Visa vi){
		return biz.addVisa(vi);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端，查询签证列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "area", value = "区域名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "country", value = "国家名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "isinterview", value = "是否面签", required = false, dataType = "String"),
		@ApiImplicitParam(name = "visatype", value = "签证类型", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sendaddress", value = "送签地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "score", value = "排序方式  1人气 2价格", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
	})
	public ResultBean<List<Map<String, Object>>> getList(Visa vi,@RequestParam(value="keyWord",defaultValue="")String keyWord,int score,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,int size){
		return biz.getList(vi, keyWord, score, pageIndex, size);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端Pc，查询签证列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "area", value = "区域名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "country", value = "国家名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "isinterview", value = "是否面签", required = false, dataType = "String"),
		@ApiImplicitParam(name = "visatype", value = "签证类型", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sendaddress", value = "送签地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String")
	})
	public ResultBean<Integer> getListCount(Visa vi,String keyWord){
		return biz.getListCount(vi, keyWord);
	}
	
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端找签证详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "签证id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "登录人用户id", required = false, dataType = "int"),
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userId){
		return biz.selectById(id, userId);
	}
}

package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.AuthenticationLanguage;
import com.medical.entity.ResultBean;
import com.medical.service.AuthenticationLanuageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/authenLanuage")
@Api(description="认证语种管理")
public class AuthenticationLanuageController {
	@Autowired
	private AuthenticationLanuageService biz;
	
	@PostMapping("/addAuthenticationLanuage")
	@ApiOperation(value ="添加认证语种，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "语种id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "grade", value = "语言等级", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "证书编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<Integer> Lanuage(@ModelAttribute AuthenticationLanguage acl){
		return biz.addLanuage(acl);
	}
	
	@PostMapping("/getDateils")
	@ApiOperation(value ="认证语种详情", notes="")
	@ApiImplicitParam(name = "id", value = "认证语种id", required = true, dataType = "int")
	public ResultBean<AuthenticationLanguage> getDateils(int id){
		return biz.getDateils(id);
	}
	
	@ApiOperation(value ="添加认证语言详情列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<List<AuthenticationLanguage>> getList(int authenticationid,int isteacher){
		return biz.getList(authenticationid,isteacher);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="添加认证语言详情列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "id逗号分隔", required = false, dataType = "String")
	public ResultBean<List<Map<String, Object>>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
	
	@PostMapping("/delLanguage")
	@ApiOperation(value ="删除认证语言", notes="")
	@ApiImplicitParam(name = "id", value = "认证语种id", required = true, dataType = "int")
	public ResultBean<Integer> delLanguage(int id){
		return biz.delLanguage(id);
	}
	
	@PostMapping("/updateAuthenticationLanuage")
	@ApiOperation(value ="更新认证语种，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "认证语种id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "languageid", value = "语种id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "grade", value = "语言等级", required = true, dataType = "String"),
		@ApiImplicitParam(name = "authenticationid", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "certificate", value = "证书编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "isteacher", value = "是否老师 1老师 2译员", required = true, dataType = "int")
	})
	public ResultBean<Integer> updateAuthenticationLanuage(@ModelAttribute AuthenticationLanguage acl){
		return biz.updateAuthenticationLanuage(acl);
	}
}

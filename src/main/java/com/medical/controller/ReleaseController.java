package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.ReleaseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/release")
@Api(description="发布管理")
public class ReleaseController {
	@Autowired
	private ReleaseService biz;
	
	@PostMapping("/CancelRelease")
	@ApiOperation(value ="取消发布", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "type", value = "1 老师 2译员 3直播课 4线下课 5视听课 6签证 7会议 8会展9学校", required = false, dataType = "int"),
		@ApiImplicitParam(name = "id", value = "发布id", required = false, dataType = "int")
	})
	public ResultBean<Integer> CancelRelease(int id,int type){
		return biz.CancelRelease(id, type);
	}
	
	@PostMapping("/getByUserId")
	@ApiOperation(value ="根据用户id获取收藏列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "type", value = "1 老师 2学校 3课程 4翻译 5会议会展 6签证", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<List<Map<String, Object>>> getByUserId(int userId,int type){
		return biz.getByUserId(userId, type);
	}
}

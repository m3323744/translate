package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CourseSimple;
import com.medical.entity.ResultBean;
import com.medical.service.CourseSimpleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/courseSimple")
@Api(description="课程分节")
public class CourseSimpleController {
	@Autowired
	private CourseSimpleService biz;
	
	@PostMapping("/addCourseSimple")
	@ApiOperation(value ="添加课程分节信息", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "name", value = "分节名称", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "price", value = "单节价格", required = false, dataType = "String"),
		@ApiImplicitParam(name = "courseid", value = "课程id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "coursetype", value = "课程类型(1视听课、2线下课)", required = false, dataType = "String")
	})
	public ResultBean<CourseSimple> addCourseSimple(CourseSimple cs){
		return biz.addCourseSimple(cs);
	}
	
	@PostMapping("/selectByIds")
	@ApiOperation(value ="查询课程分节信息列表，id分隔形式", notes="")
	@ApiImplicitParam(name = "ids", value = "id逗号分隔", required = false, dataType = "String")
	public ResultBean<List<CourseSimple>> selectByIds(String ids){
		return biz.selectByIds(ids);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value ="查询课程分节信息", notes="")
	@ApiImplicitParam(name = "id", value = "id", required = false, dataType = "String")
	public ResultBean<Map<String, Object>> selectByIds(Integer id){
		return biz.selectById(id);
	}
}

package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ConventionApply;
import com.medical.entity.ResultBean;
import com.medical.service.ConventionApplyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/conventionApply")
@Api(description="会展报名")
public class ConventionApplyController {
	@Autowired
	private ConventionApplyService biz;
	
	@PostMapping("/addConventionApply")
	@ApiOperation(value="会展报名", notes="")
    @ApiImplicitParams({
		@ApiImplicitParam(name = "conventionid", value = "会展id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "name", value = "报名人姓名", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "phone", value = "报名人电话", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "type", value = "1公司 2个人", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "address", value = "地址", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "email", value = "邮箱", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "website", value = "官网", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "company", value = "公司名称", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "duty", value = "报名人职务", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "totalprice", value = "总价", required = true, dataType = "double",paramType="query"),
		@ApiImplicitParam(name = "companytype", value = "企业类型", required = true, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "ordernumber", value = "订单号 时间生成", required = false, dataType = "string")
    })
	public ResultBean<ConventionApply> addConventionApply(ConventionApply ca){
		return biz.addConventionApply(ca);
	}
}

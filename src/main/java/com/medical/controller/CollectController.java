package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Collect;
import com.medical.entity.ResultBean;
import com.medical.service.CollectService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/collect")
public class CollectController {
	@Autowired
	private CollectService biz;
	
	@PostMapping("/addCollect")
	public ResultBean<Integer> addCollect(Collect coll){
		return biz.addCollect(coll);
	}
	
	
	@PostMapping("/delCollect")
	public ResultBean<Integer> delCollect(Collect coll){
		return biz.delCollect(coll);
	}
	
	@PostMapping("/getList")
	@ApiOperation("获取收藏列表")
	public ResultBean<List<Map<String, Object>>> getList(Collect coll,int pageIndex,int size){
		return biz.getList(coll,pageIndex,size);
	}
	
	
	@PostMapping("/getListCount")
	@ApiOperation("PC获取收藏列表总条数")
	public ResultBean<Integer> getListCount(Collect coll){
		return biz.getListCount(coll);
	}
}

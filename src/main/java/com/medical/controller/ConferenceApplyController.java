package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ConferenceApply;
import com.medical.entity.ResultBean;
import com.medical.service.ConferenceApplyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/conferenceApply")
@Api(description="会议报名")
public class ConferenceApplyController {
	@Autowired
	private ConferenceApplyService biz;
	
	@PostMapping("/addConferenceApply")
	@ApiOperation(value ="会议报名", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "conferenceid", value = "会议id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "name", value = "姓名", required = false, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "手机", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "companytype", value = "企业类型", required = false, dataType = "String"),
		@ApiImplicitParam(name = "time", value = "时间", required = false, dataType = "date"),
		@ApiImplicitParam(name = "type", value = "身份类型 1个人 2企业", required = false, dataType = "int"),
		@ApiImplicitParam(name = "company", value = "企业名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "详细地址", required = false, dataType = "String"),
		@ApiImplicitParam(name = "email", value = "报名数量", required = false, dataType = "String"),
		@ApiImplicitParam(name = "num", value = "年龄", required = false, dataType = "int"),
		@ApiImplicitParam(name = "totalprice", value = "总价", required = false, dataType = "double"),
		@ApiImplicitParam(name = "ordernumber", value = "订单号 时间生成", required = false, dataType = "string")
	})
	public ResultBean<ConferenceApply> addConferenceApply(ConferenceApply ca){
		return biz.addConferenceApply(ca);
	}

}

package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.TerritoryType;
import com.medical.service.TerritoryTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/territorytype")
@Api(description="行业领域列表")
public class TerritoryTypeController {
	@Autowired
	private TerritoryTypeService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value="行业领域列表查询",notes="")
	public ResultBean<List<TerritoryType>> getList(){
		return biz.getList();
	}
}

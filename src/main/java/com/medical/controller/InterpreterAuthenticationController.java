package com.medical.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.InterpreterAuthentication;
import com.medical.entity.ResultBean;
import com.medical.service.InterpreterAuthenticationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/interpreterAuthentication")
@Api(description="译员认证管理")
public class InterpreterAuthenticationController {
	@Autowired
	private InterpreterAuthenticationService biz;
	
	@PostMapping("/addInterpreterAuthentication")
	@ApiOperation(value ="添加认证译员，成功返回添加的id", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sex", value = "性别", required = true, dataType = "boolean"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "最高学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "birthdate", value = "出生日期", required = true, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "家庭住址", required = true, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificatetype", value = "证件类型", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcard", value = "证件编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "lanuage", value = "语言id，用添加语种认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "认证证书id，用添加证书认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "school", value = "认证教育经历id，用添加教育经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "认证留学经历id，用添加留学经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "territory", value = "擅长领域id，用添加擅长领域返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardfront", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardreverse", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "interpretertype", value = "译员类型(2口译1笔译)", required = true, dataType = "int"),
		@ApiImplicitParam(name = "certificatetype", value = "证件类型", required = true, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "电话", required = true, dataType = "String"),
	})
	public ResultBean<Integer> addInterpreterAuthentication(@ModelAttribute InterpreterAuthentication ia,String lanuage,String certificate,String school,String studying,String territory){
		return biz.addInterpreterAuthentication(ia, lanuage, certificate, school, studying, territory);
	}
	
	@PostMapping("/interpreterAuthenticationDateils")
	@ApiOperation(value ="认证译员详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "typeId", value = "类型id(1笔译2口译)", required = true, dataType = "int")
	})
	public ResultBean<Map<String, Object>> interpreterAuthenticationDateils(int userId,int typeId){
		return biz.interpreterAuthenticationDateils(userId,typeId);
	}
	
	@PostMapping("/updateInterpreterAuthentication")
	@ApiOperation(value ="修改认证译员，成功返回添加的id(没有修改的话，原样传回来)", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "认证id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "sex", value = "性别", required = true, dataType = "boolean"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = true, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "最高学历", required = true, dataType = "String"),
		@ApiImplicitParam(name = "birthdate", value = "出生日期", required = true, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "家庭住址", required = true, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String"),
		@ApiImplicitParam(name = "interpretertype", value = "译员类型(2口译1笔译)", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcard", value = "证件编号", required = true, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "lanuage", value = "语言id，用添加语种认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "认证证书id，用添加证书认证返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "school", value = "认证教育经历id，用添加教育经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "认证留学经历id，用添加留学经历返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "territory", value = "擅长领域id，用添加擅长领域返回的id加逗号分隔拼成这个字符串", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardfront", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "idcardreverse", value = "手持证件正面照路径", required = true, dataType = "String"),
		@ApiImplicitParam(name = "certificatetype", value = "证件类型", required = true, dataType = "String")
	})
	public ResultBean<Integer> updateInterpreterAuthentication(@ModelAttribute InterpreterAuthentication ia,String lanuage,String certificate,String school,String studying,String territory){
		return biz.updateInterpreterAuthentication(ia, lanuage, certificate, school, studying, territory);
	}
}

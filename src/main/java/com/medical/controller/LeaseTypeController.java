package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.LeaseType;
import com.medical.entity.ResultBean;
import com.medical.service.LeaseTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/leasetype")
@Api(description="租赁类型")
public class LeaseTypeController {
	@Autowired
	private LeaseTypeService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value="租赁类型列表")
	public ResultBean<List<LeaseType>> getList(){
		return biz.getList();
	}
}

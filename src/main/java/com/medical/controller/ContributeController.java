package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Contribute;
import com.medical.entity.ResultBean;
import com.medical.service.ContributeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/contribute")
@Api(description="用户投稿")
public class ContributeController {
	@Autowired
	private ContributeService biz;
	
	@PostMapping("/addContribute")
	@ApiOperation(value ="用户端投稿", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addContribute(Contribute con){
		return biz.addContribute(con);
	}
}

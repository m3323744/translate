package com.medical.controller;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.medical.entity.CourseStreaming;
import com.medical.entity.ResultBean;
import com.medical.service.CourseStreamingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/courseStreaming")
@Api(description="直播课")
public class CourseStreamingController {
	@Autowired
	private CourseStreamingService biz;
	
	@PostMapping("/addCourseStreaming")
	@ApiOperation(value ="添加直播课", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "课程封面", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "课程名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "teacher", value = "主讲教师", required = false, dataType = "String"),
		@ApiImplicitParam(name = "streamingtime", value = "直播时间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "totalprice", value = "课程总价格", required = false, dataType = "float"),
		@ApiImplicitParam(name = "introduceimages", value = "课程介绍图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "schoolid", value = "学校id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "merchantid", value = "商家用户id", required = false, dataType = "int")
	})
	public ResultBean<Integer> addCourseStreaming(CourseStreaming cs){
		return biz.addCourseStreaming(cs);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端，根据id直播课详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "课程id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userId", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userId){
		return biz.selectById(id,userId);
	}
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端，查询直播课列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "prices", value = "价格区间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "score", value = "排序方式  1人气 2价格 3时间", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
	})
	public ResultBean<List<Map<String, Object>>> getList(Integer languageid,Integer stageid,String prices,int score,@RequestParam(value="keyWord",defaultValue="")String keyWord,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,int size){
		return biz.getList(languageid, stageid, prices, score, keyWord, pageIndex, size);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端PC，查询直播课列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "languageid", value = "课程语种id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "stageid", value = "课程阶段id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "prices", value = "价格区间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "搜索关键字", required = false, dataType = "String")
	})
	public ResultBean<Integer> getListCount(Integer languageid,Integer stageid,String prices,@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(languageid, stageid, prices, keyWord);
	}
}

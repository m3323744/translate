package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.News;
import com.medical.entity.ResultBean;
import com.medical.service.NewsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/news")
@Api(description="新闻查询")
public class NewsController {

	@Autowired
	private NewsService biz;
	
	@PostMapping("/getList")
	public ResultBean<News> getList(int type){
		return biz.getList(type);
	}
	
	@PostMapping("/getNews")
	@ApiOperation("获取聚焦全球列表")
	public ResultBean<List<News>> getNews(int page,int size){
		return biz.getNews(page, size);
	}
	
	@PostMapping("/getById")
	@ApiOperation("获取聚焦全球详情")
	public ResultBean<News> getById(int id){
		return biz.getById(id);
	}
	
	@PostMapping("/selectByKey")
	@ApiOperation("根据关键字查询聚焦全球列表")
	public ResultBean<List<News>> selectByKey(String key,int page, int size){
		return biz.selectByKey(key, page, size);
	}
}

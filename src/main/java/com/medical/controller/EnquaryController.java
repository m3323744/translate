package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Enquary;
import com.medical.entity.ResultBean;
import com.medical.service.EnquaryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/enquary")
@Api(description="留言管理")
public class EnquaryController {
	@Autowired
	private EnquaryService biz;
	
	@PostMapping("/addEnquary")
	@ApiOperation(value ="添加留言", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int"),
		@ApiImplicitParam(name = "content", value = "留言内容", required = true, dataType = "string"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = true, dataType = "string"),
		@ApiImplicitParam(name = "type", value = "1国际商务 2租赁设备", required = true, dataType = "int")
	})
	public ResultBean<Integer> addEnquary(Enquary enq){
		return biz.addEnquary(enq);
	}
}

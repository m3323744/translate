package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.Teacher;
import com.medical.service.TeacherService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/teacher")
@Api(description="发布老师")
public class TeacherController {
	@Autowired
	private TeacherService biz;
	
	@PostMapping("/addTeacher")
	@ApiOperation(value ="发布老师信息", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "展示图片", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "head", value = "头像", required = false, dataType = "String"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "所在地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "teaching", value = "教学年限", required = false, dataType = "String"),
		@ApiImplicitParam(name = "price", value = "价格", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "languageid", value = "语种id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languagegrade", value = "语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hauptstudiumid", value = "课程阶段", required = false, dataType = "String"),
		@ApiImplicitParam(name = "experience", value = "教学经验", required = false, dataType = "String"),
		@ApiImplicitParam(name = "make", value = "自我介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imageone", value = "风采展示1", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagetwo", value = "风采展示2", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagethree", value = "风采展示3", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imageoneexperience", value = "风采展示1介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagetwoexperience", value = "风采展示2介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "imagethreeexperience", value = "风采展示3介绍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "longitude", value = "经度", required = false, dataType = "String"),
		@ApiImplicitParam(name = "latitude", value = "纬度", required = false, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "所持证书", required = false, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "留学经验id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean")
	})
	public ResultBean<Integer> addTeacher(Teacher tea){
		return biz.addTeacher(tea);
	}
	
	@PostMapping("/selectByTeacher")
	@ApiOperation(value ="用户端，搜索老师信息列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "minprice", value = "最小价格", required = false, dataType = "double"),
		@ApiImplicitParam(name = "maxprice", value = "最大价格", required = false, dataType = "double"),
		@ApiImplicitParam(name = "languageid", value = "语种id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languagegrade", value = "语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "所在地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hauptstudiumid", value = "课程阶段", required = false, dataType = "String"),
		@ApiImplicitParam(name = "ages", value = "年龄(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "留学经验id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "teachings", value = "教学年限(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "所持证书", required = false, dataType = "String"),
		@ApiImplicitParam(name = "pageIndex", value = "页码(默认0)", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int"),
		@ApiImplicitParam(name = "longitude", value = "经度", required = false, dataType = "double"),
		@ApiImplicitParam(name = "latitude", value = "纬度", required = false, dataType = "double"),
		@ApiImplicitParam(name = "score", value = "排序方式(1人气 2价格 3距离)", required = false, dataType = "int"),
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String"),
		
	})
	public ResultBean<List<Map<String, Object>>> selectByTeacher(Teacher tea,String ages,String teachings,Double minprice,Double maxprice,int score,String nationality,@RequestParam(value="pageIndex",defaultValue="0")Integer pageIndex,int size,Double longitude,Double latitude,@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.selectByTeacher(tea,ages,teachings,minprice, maxprice,score, nationality,pageIndex,size,longitude,latitude,keyWord);
	}
	
	@PostMapping("/selectByTeacherId")
	@ApiOperation(value ="用户端找教师详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "用户找教师id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "登录人用户id", required = false, dataType = "int"),
	})
	
	public ResultBean<Map<String, Object>> selectByTeacherId(int id,int userid){
		return biz.selectByTeacherId(id,userid);
	}
	
	@PostMapping("/selectByTeacherCount")
	@ApiOperation(value ="用户端PC，搜索老师信息列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "minprice", value = "最小价格", required = false, dataType = "double"),
		@ApiImplicitParam(name = "maxprice", value = "最大价格", required = false, dataType = "double"),
		@ApiImplicitParam(name = "languageid", value = "语种id", required = false, dataType = "String"),
		@ApiImplicitParam(name = "languagegrade", value = "语言等级", required = false, dataType = "String"),
		@ApiImplicitParam(name = "address", value = "所在地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "education", value = "学历", required = false, dataType = "String"),
		@ApiImplicitParam(name = "nationality", value = "国籍", required = false, dataType = "String"),
		@ApiImplicitParam(name = "hauptstudiumid", value = "课程阶段", required = false, dataType = "String"),
		@ApiImplicitParam(name = "ages", value = "年龄(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "studying", value = "留学经验id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "teachings", value = "教学年限(-分隔)", required = false, dataType = "String"),
		@ApiImplicitParam(name = "certificate", value = "所持证书", required = false, dataType = "String"),
		@ApiImplicitParam(name = "keyword", value = "关键字", required = false, dataType = "String")
		
	})
	public ResultBean<Integer> selectByTeacherCount(Teacher tea,String ages,String teachings,Double minprice,Double maxprice,String nationality,@RequestParam(value="keyWord",defaultValue="")String keyword){
		return biz.selectByTeacherCount(tea, ages, teachings, minprice, maxprice, nationality, keyword);
	}
}

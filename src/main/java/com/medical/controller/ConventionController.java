package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Convention;
import com.medical.entity.ResultBean;
import com.medical.service.ConventionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/convention")
@Api(description="会展相关")
public class ConventionController {
	@Autowired
	private ConventionService biz;
	
	@PostMapping("/addConvention")
	@ApiOperation(value ="添加会展信息", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "images", value = "展示图片", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String"),
		@ApiImplicitParam(name = "synopsis", value = "简介", required = false, dataType = "String"),
		@ApiImplicitParam(name = "price", value = "价格", required = false, dataType = "String"),
		@ApiImplicitParam(name = "time", value = "时间", required = false, dataType = "String"),
		@ApiImplicitParam(name = "host", value = "举办地", required = false, dataType = "String"),
		@ApiImplicitParam(name = "sponsor", value = "主办方", required = false, dataType = "String"),
		@ApiImplicitParam(name = "contractor", value = "承办方", required = false, dataType = "boolean"),
		@ApiImplicitParam(name = "apply", value = "报名条件", required = false, dataType = "String"),
		@ApiImplicitParam(name = "introduceimage", value = "人满数量", required = false, dataType = "String"),
		@ApiImplicitParam(name = "num", value = "人满数量", required = false, dataType = "String"),
		@ApiImplicitParam(name = "isapply", value = "是否同意报名参展", required = false, dataType = "String"),
		@ApiImplicitParam(name = "phone", value = "联系方式", required = false, dataType = "String"),
		@ApiImplicitParam(name = "userid", value = "发布人id", required = false, dataType = "String")
	})
	public ResultBean<Integer> addConvention(Convention co){
		return biz.addConvention(co);
	}
	
	
	@PostMapping("/getList")
	@ApiOperation(value ="用户端，会展列表", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "score", value = "排序方式 1价格 2时间3默认", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<List<Convention>> getList(@RequestParam(value="keyWord",defaultValue="")String keyWord,int pageIndex,int score,int size){
		return biz.getList(keyWord, pageIndex, score,size);
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value ="用户端PC，会展列表总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "keyWord", value = "关键字", required = false, dataType = "String"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "score", value = "排序方式 1价格 2时间3默认", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<Integer> getListCount(@RequestParam(value="keyWord",defaultValue="")String keyWord){
		return biz.getListCount(keyWord);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value ="用户端，会展详情", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "会展id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int")
	})
	public ResultBean<Map<String, Object>> selectById(int id,int userid){
		return biz.selectById(id,userid);
	}
	
	//用户端 我报名的会议列表
	@PostMapping("/getByMyUserId")
	@ApiOperation(value ="用户端，我报名的会展会议列表查询", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = false, dataType = "int"),
		@ApiImplicitParam(name = "size", value = "每页条数", required = false, dataType = "int")
	})
	public ResultBean<List<Map<String, Object>>> getByMyUserId(int userId,int pageIndex,int size){
		return biz.getByMyUserId(userId, pageIndex, size);
	}
	
	@PostMapping("/delConvention")
	@ApiOperation(value ="用户端，会展删除", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "会展id", required = false, dataType = "int")
	})
	public ResultBean<Integer> delConvention(int id){
		return biz.delConvention(id);
	}
}

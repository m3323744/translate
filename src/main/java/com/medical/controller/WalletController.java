package com.medical.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Consumption;
import com.medical.entity.IntegralConsume;
import com.medical.entity.MyException;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.ConsumptionService;
import com.medical.service.IntegralConsumeService;
import com.medical.service.UserService;
import com.medical.util.WxPayUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("/wallet")
@Api(description="钱包相关")
public class WalletController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private ConsumptionService consumptionService;
	@Autowired
	private IntegralConsumeService integralConsumeService;
	
	
	/**
	 * 我的钱包
	 * @param userId
	 * @return 余额，积分
	 */
	@PostMapping("/myWallet")
	@ApiOperation(value="我的钱包", notes="")
	@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query")
	public ResultBean<Map<String,Object>> myWallet(@RequestParam("userId")String userId){
		Map<String,Object>  user=userService.findBelance(userId);
		return new ResultBean<>(user);
	}
	/**
	 * 充值
	 * @return
	 */
	@PostMapping("/recharge")
	@ApiOperation(value="充值", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "money", value = "钱数", required = false, dataType = "double",paramType="query")
    })
	public ResultBean<User> recharge(int userId,Double money){
		
		return userService.recharge(userId, money);
	}
	
	/**
	 * 提现
	 * @return
	 */
	@PostMapping("/withdrawals")
	@ApiOperation(value="提现", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "money", value = "钱数", required = false, dataType = "double",paramType="query"),
		@ApiImplicitParam(name = "openid", value = "用户id", required = true, dataType = "String",paramType="query"),
    })
	public ResultBean<User> withdrawals(HttpServletRequest request, HttpServletResponse response,int userId,Double money,String openid){
		Boolean pan=WxPayUtil.transferPay(request, response, openid, money.toString());
		if(pan) {
			return userService.withdrawals(request, response, userId, money);
		}else {
			return new ResultBean<>(new MyException("微信发起提现失败"));
		}
	}
	
	/**
	 * 我的钱包 余额详细
	 * @param consumption
	 * @return
	 */
	@PostMapping("/myBalanceInfo")
	@ApiOperation(value="我的钱包 余额详细", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "minTime", value = "查询时间段", required = false, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "maxTime", value = "查询时间段", required = false, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = true, dataType = "int",paramType="query")
    })
	public ResultBean<List<Consumption>> myBalanceInfo(int userId,String minTime,String maxTime,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex){
		List<Consumption> consumptions=consumptionService.findConsumptions(userId,minTime,maxTime,pageIndex);
		return new ResultBean<List<Consumption>>(consumptions);
	}
	
	@PostMapping("/myBalanceInfoCount")
	@ApiOperation(value="我的钱包PC 余额详细总条数", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "minTime", value = "查询时间段", required = false, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "maxTime", value = "查询时间段", required = false, dataType = "String",paramType="query"),
		@ApiImplicitParam(name = "pageIndex", value = "页码", required = true, dataType = "int",paramType="query")
    })
	public ResultBean<Integer> myBalanceInfoCount(int userId,String minTime,String maxTime){
		int count=consumptionService.findConsumptionsCount(userId, minTime, maxTime);
		return new ResultBean<Integer>(count);
	}
	
	/**
	 * 积分明细
	 * @param consumption
	 * @param userId
	 * @return
	 */
	@PostMapping("/myIntegralInfo")
	@ApiOperation(value="积分明细", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "page", value = "页码", required = true, dataType = "int",paramType="query")
	})
	public ResultBean<List<IntegralConsume>> myIntegralInfo(@RequestParam(value="page",defaultValue="1")int page,@RequestParam("userId")int userId){
		//Page page=new Page<IntegralConsume>(1,5);
		
		List<IntegralConsume> consumptions=integralConsumeService.findByPage(page,userId);
		return new ResultBean<List<IntegralConsume>>(consumptions);
	}

	@PostMapping("/myIntegralInfoCount")
	@ApiOperation(value="积分明细PC总页数", notes="")
	@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int",paramType="query")
	public ResultBean<Integer> myIntegralInfoCount(@RequestParam("userId")int userId){
		//Page page=new Page<IntegralConsume>(1,5);
		int count=integralConsumeService.findByPageCount(userId);
		return new ResultBean<Integer>(count);
	}
}

package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CountryArea;
import com.medical.entity.ResultBean;
import com.medical.service.CountryAreaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/countryArea")
@Api(description="国家地区")
public class CountryAreaController {
	@Autowired
	private CountryAreaService biz;
	
	@PostMapping("/getListByCountryId")
	@ApiOperation(value ="地区列表", notes="")
	@ApiImplicitParam(name = "countryId", value = "国家id", required = true, dataType = "int")
	public ResultBean<List<CountryArea>> getListByCountryId(int countryId){
		return biz.getListByCountryId(countryId);
	}
}

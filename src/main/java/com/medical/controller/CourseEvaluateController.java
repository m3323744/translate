package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CourseEvaluate;
import com.medical.entity.ResultBean;
import com.medical.service.CourseEvaluateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/courseEvaluate")
@Api(description="课程评价操作")
public class CourseEvaluateController {
	@Autowired
	private CourseEvaluateService biz;
	
	@PostMapping("/selectByOrderId")
	@ApiOperation(value="获取订单评价，订单id方式获取", notes="")
	@ApiImplicitParam(name = "orderId", value = "订单id", required = true, dataType = "int",paramType="query")
	public ResultBean<Map<String, Object>> selectByOrderId(int orderId){
		return biz.selectByOrderId(orderId);
	}
	
	@PostMapping("/addEvaluate")
	@ApiOperation(value="添加评价", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "score", value = "评论分", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "courseid", value = "课程id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "content", value = "评论内容", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "coursetype", value = "课程类型 1视听课 2直播课 3线下课", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "courseorderid", value = "课程订单id", required = true, dataType = "int",paramType="query")
	})
	public ResultBean<Integer> addEvaluate(CourseEvaluate eva){
		return biz.addEvaluate(eva);
	}
	
	@PostMapping("/selectByCourseId")
	@ApiOperation(value="获取订单评价，课程id方式获取", notes="")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "courseId", value = "课程id", required = true, dataType = "int",paramType="query"),
		@ApiImplicitParam(name = "type", value = "3=线下课  1=视听课 2=直播课", required = true, dataType = "int",paramType="query")
	})
	public ResultBean<Map<String, Object>> selectByCourseId(int courseId,int type){   //type 1=线下课  2=视听课 3=直播课
		return biz.selectByInterpreterId(courseId, type);
	}
	
	@PostMapping("/selectBySchoolId")
	@ApiOperation(value="获取订单评价，学校id方式获取", notes="")
	@ApiImplicitParam(name = "schoolId", value = "学校id", required = true, dataType = "int",paramType="query")
	public ResultBean<List<Map<String, Object>>> selectBySchoolId(int schoolId){   //type 1=线下课  2=视听课 3=直播课
		return biz.selectBySchoolId(schoolId);
	}
}

package com.medical.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.IntegralCommodity;
import com.medical.entity.IntegralConsume;
import com.medical.entity.IntegralConversion;
import com.medical.entity.ResultBean;
import com.medical.service.IntegralCommodityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/integralCommodity")
@Api(description="积分商城")
public class IntegralCommodityController {
	@Autowired
	private IntegralCommodityService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value="积分商城列表")
	public ResultBean<List<IntegralCommodity>> getList(){
		return biz.getList();
	}
	
	@PostMapping("/getListCount")
	@ApiOperation(value="PC获取积分商城列表")
	public ResultBean<Map<String, Object>> getListCount(int pageIndex,int size){
		return biz.getListCount(pageIndex,size);
	}
	
	@PostMapping("/exchange")
	@ApiOperation(value="兑换商品")
	public ResultBean<Integer>  exchange(@ModelAttribute IntegralConversion ic){
		return biz.exchange(ic);
	}
	
	@PostMapping("/sign")
	@ApiOperation(value="每日签到")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "int"),
		@ApiImplicitParam(name = "integral", value = "获得积分", required = false, dataType = "int"),
		@ApiImplicitParam(name = "time", value = "时间", required = false, dataType = "date"),
	})
	public ResultBean<Integer> sign(IntegralConsume ic){
		return biz.sign(ic);
	}
	
	@PostMapping("/selectById")
	@ApiOperation(value="商品详情")
	@ApiImplicitParam(name = "id", value = "商品id", required = false, dataType = "int")
	public ResultBean<IntegralCommodity> selectById(int id){
		return biz.selectById(id);
	}
}

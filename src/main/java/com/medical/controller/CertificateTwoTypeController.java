package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CertificateTwoType;
import com.medical.entity.ResultBean;
import com.medical.service.CertificateTwoTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/certificateTwoType")
@Api(description="证书二级类别查询")
public class CertificateTwoTypeController {
	@Autowired
	private CertificateTwoTypeService biz;
	
	@PostMapping("/getList")
	@ApiOperation(value ="获得二级证书列表", notes="")
	public ResultBean<List<CertificateTwoType>> getList(){
		return biz.getList();
	}
}

package com.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.CommonProblem;
import com.medical.service.CommonProblemService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/commonProblem")
@Api(description="常见问题")
public class CommonProblemController {
	@Autowired
	private CommonProblemService biz;
	
	@PostMapping("/getOne")
	@ApiOperation(value ="常见问题", notes="")
	public CommonProblem getOne() {
		return biz.getOne();
	}
}

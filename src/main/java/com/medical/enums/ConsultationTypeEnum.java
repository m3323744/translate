package com.medical.enums;

public enum ConsultationTypeEnum {

	INVESTMENT(1),
	FINANCE(2),
	LAW(3);
	private Integer code;
	ConsultationTypeEnum(Integer code) {
		this.code=code;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	
}

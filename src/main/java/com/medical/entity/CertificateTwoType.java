package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_certificate_two_type")
public class CertificateTwoType extends Model<CertificateTwoType> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1197230466845170250L;

	@TableId
	private Integer id;

    private Boolean isyu;

    private String twotype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsyu() {
        return isyu;
    }

    public void setIsyu(Boolean isyu) {
        this.isyu = isyu;
    }

    public String getTwotype() {
        return twotype;
    }

    public void setTwotype(String twotype) {
        this.twotype = twotype == null ? null : twotype.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
    * chat 实体类
    *  sujunyuan
    */ 

@TableName("t_chat")
public class Chat extends Model<Chat>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1126946644840268961L;
	@TableId
	private int id;
	private int fromUserId;
	private int toUserId;
	private String content;
	private String type;
	private String time;
	private int read;
	private int showTime;
	public void setId(int id){
	this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setFromUserId(int fromUserId){
	this.fromUserId=fromUserId;
	}
	public int getFromUserId(){
		return fromUserId;
	}
	public void setToUserId(int toUserId){
	this.toUserId=toUserId;
	}
	public int getToUserId(){
		return toUserId;
	}
	public void setContent(String content){
	this.content=content;
	}
	public String getContent(){
		return content;
	}
	public void setType(String type){
	this.type=type;
	}
	public String getType(){
		return type;
	}
	public void setTime(String time){
	this.time=time;
	}
	public String getTime(){
		return time;
	}
	public void setRead(int read){
	this.read=read;
	}
	public int getRead(){
		return read;
	}
	public void setShowTime(int showTime){
	this.showTime=showTime;
	}
	public int getShowTime(){
		return showTime;
	}
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}


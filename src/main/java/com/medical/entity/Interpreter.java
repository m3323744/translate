package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_interpreter")
public class Interpreter extends Model<Interpreter> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 335172294347481610L;

	@TableId
    private Integer id;

    private String images;

    private String head;

    private Integer translatelanguage;

    private Integer intolanguage;

    private String translategrade;

    private String intograde;

    private String name;

    private String address;

    private String education;

    private Integer age;

    private Integer territoryid;

    private Float hourprice;

    private Float minuteprice;

    private Integer experienceid;

    private Integer certificateid;

    private String phone;

    private String experience;

    private String make;

    private String imageone;

    private String imagetwo;

    private String imagethree;

    private String imageoneexplain;

    private String imagetwoexplain;

    private String imagethreeexplain;

    private Integer interpretertype;
    
    private Boolean sex;
    
    private Integer userid;
    
    private Date time;
    
    private String longitude;
    
    private String latitude;
    
    private Float wordprice;
    
    private Integer freeage;
    
    private Integer states;
    
    private Boolean release;

    public Boolean getRelease() {
		return release;
	}

	public void setRelease(Boolean release) {
		this.release = release;
	}

	public Integer getStates() {
		return states;
	}

	public void setStates(Integer states) {
		this.states = states;
	}

    public Integer getFreeage() {
		return freeage;
	}

	public void setFreeage(Integer freeage) {
		this.freeage = freeage;
	}

	public Float getWordprice() {
		return wordprice;
	}

	public void setWordprice(Float wordprice) {
		this.wordprice = wordprice;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images == null ? null : images.trim();
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head == null ? null : head.trim();
    }

    public Integer getTranslatelanguage() {
        return translatelanguage;
    }

    public void setTranslatelanguage(Integer translatelanguage) {
        this.translatelanguage = translatelanguage;
    }

    public Integer getIntolanguage() {
        return intolanguage;
    }

    public void setIntolanguage(Integer intolanguage) {
        this.intolanguage = intolanguage;
    }

    public String getTranslategrade() {
        return translategrade;
    }

    public void setTranslategrade(String translategrade) {
        this.translategrade = translategrade == null ? null : translategrade.trim();
    }

    public String getIntograde() {
        return intograde;
    }

    public void setIntograde(String intograde) {
        this.intograde = intograde == null ? null : intograde.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education == null ? null : education.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getTerritoryid() {
        return territoryid;
    }

    public void setTerritoryid(Integer territoryid) {
        this.territoryid = territoryid;
    }

    public Float getHourprice() {
        return hourprice;
    }

    public void setHourprice(Float hourprice) {
        this.hourprice = hourprice;
    }

    public Float getMinuteprice() {
        return minuteprice;
    }

    public void setMinuteprice(Float minuteprice) {
        this.minuteprice = minuteprice;
    }

    public Integer getExperienceid() {
        return experienceid;
    }

    public void setExperienceid(Integer experienceid) {
        this.experienceid = experienceid;
    }

    public Integer getCertificateid() {
        return certificateid;
    }

    public void setCertificateid(Integer certificateid) {
        this.certificateid = certificateid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience == null ? null : experience.trim();
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make == null ? null : make.trim();
    }

    public String getImageone() {
        return imageone;
    }

    public void setImageone(String imageone) {
        this.imageone = imageone == null ? null : imageone.trim();
    }

    public String getImagetwo() {
        return imagetwo;
    }

    public void setImagetwo(String imagetwo) {
        this.imagetwo = imagetwo == null ? null : imagetwo.trim();
    }

    public String getImagethree() {
        return imagethree;
    }

    public void setImagethree(String imagethree) {
        this.imagethree = imagethree == null ? null : imagethree.trim();
    }

    public String getImageoneexplain() {
        return imageoneexplain;
    }

    public void setImageoneexplain(String imageoneexplain) {
        this.imageoneexplain = imageoneexplain == null ? null : imageoneexplain.trim();
    }

    public String getImagetwoexplain() {
        return imagetwoexplain;
    }

    public void setImagetwoexplain(String imagetwoexplain) {
        this.imagetwoexplain = imagetwoexplain == null ? null : imagetwoexplain.trim();
    }

    public String getImagethreeexplain() {
        return imagethreeexplain;
    }

    public void setImagethreeexplain(String imagethreeexplain) {
        this.imagethreeexplain = imagethreeexplain == null ? null : imagethreeexplain.trim();
    }

    public Integer getInterpretertype() {
        return interpretertype;
    }

    public void setInterpretertype(Integer interpretertype) {
        this.interpretertype = interpretertype;
    }

    
	

	public Boolean getSex() {
		return sex;
	}

	public void setSex(Boolean sex) {
		this.sex = sex;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
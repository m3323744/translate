package com.medical.entity;

import java.util.Date;

public class Messages {
    private Integer id;

    private Integer senduserid;

    private Integer receptionuserid;

    private String message;

    private Date time;

    private Boolean isread;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSenduserid() {
        return senduserid;
    }

    public void setSenduserid(Integer senduserid) {
        this.senduserid = senduserid;
    }

    public Integer getReceptionuserid() {
        return receptionuserid;
    }

    public void setReceptionuserid(Integer receptionuserid) {
        this.receptionuserid = receptionuserid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Boolean getIsread() {
        return isread;
    }

    public void setIsread(Boolean isread) {
        this.isread = isread;
    }
}
package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_authentication_certificate")
public class AuthenticationCertificate extends Model<AuthenticationCertificate> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7942288724298086725L;
	@TableId
	private Integer id;

    private Integer authenticationid;

    private String number;

    private Integer twoid;

    private String time;

    private Integer isteacher;
    
    private Integer threeid;
    
    private Boolean perpetual;
    
    

    public Boolean getPerpetual() {
		return perpetual;
	}

	public void setPerpetual(Boolean perpetual) {
		this.perpetual = perpetual;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthenticationid() {
        return authenticationid;
    }

    public void setAuthenticationid(Integer authenticationid) {
        this.authenticationid = authenticationid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    
    
    public Integer getTwoid() {
		return twoid;
	}

	public void setTwoid(Integer twoid) {
		this.twoid = twoid;
	}

	public Integer getThreeid() {
		return threeid;
	}

	public void setThreeid(Integer threeid) {
		this.threeid = threeid;
	}

	public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    public Integer getIsteacher() {
        return isteacher;
    }

    public void setIsteacher(Integer isteacher) {
        this.isteacher = isteacher;
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
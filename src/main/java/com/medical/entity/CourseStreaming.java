package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_course_streaming")
public class CourseStreaming extends Model<CourseStreaming> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5699958463692647773L;

	@TableId
	private Integer id;

    private String images;

    private String name;

    private Integer languageid;

    private Integer stageid;

    private String teacher;

    private Date streamingtime;

    private Float totalprice;

    private String introduceimages;

    private Integer schoolid;

    private Date time;
    
    private Integer userid;
    
    private Integer states;
    
    private Integer merchantid;
    
    private Boolean release;

    public Boolean getRelease() {
		return release;
	}

	public void setRelease(Boolean release) {
		this.release = release;
	}

	public Integer getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(Integer merchantid) {
		this.merchantid = merchantid;
	}

    public Integer getStates() {
		return states;
	}

	public void setStates(Integer states) {
		this.states = states;
	}

    public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images == null ? null : images.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getLanguageid() {
        return languageid;
    }

    public void setLanguageid(Integer languageid) {
        this.languageid = languageid;
    }

    public Integer getStageid() {
        return stageid;
    }

    public void setStageid(Integer stageid) {
        this.stageid = stageid;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher == null ? null : teacher.trim();
    }

    public Date getStreamingtime() {
        return streamingtime;
    }

    public void setStreamingtime(Date streamingtime) {
        this.streamingtime = streamingtime;
    }

    public Float getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Float totalprice) {
        this.totalprice = totalprice;
    }

    public String getIntroduceimages() {
        return introduceimages;
    }

    public void setIntroduceimages(String introduceimages) {
        this.introduceimages = introduceimages == null ? null : introduceimages.trim();
    }

    public Integer getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Integer schoolid) {
        this.schoolid = schoolid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
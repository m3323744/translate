package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_interpreter_authentication_territory")
public class InterpreterAuthenticationTerritory extends Model<InterpreterAuthenticationTerritory> {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1184398234706714081L;

	@TableId
	private Integer id;

    private Integer interpreterid;

    private String territory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInterpreterid() {
        return interpreterid;
    }

    public void setInterpreterid(Integer interpreterid) {
        this.interpreterid = interpreterid;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory == null ? null : territory.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
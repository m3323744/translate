package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_authentication_language")
public class AuthenticationLanguage extends Model<AuthenticationLanguage> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3757894207013965910L;
	
	@TableId
	private Integer id;

    private Integer languageid;

    private String grade;

    private Integer authenticationid;

    private String certificate;

    private Integer isteacher;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLanguageid() {
        return languageid;
    }

    public void setLanguageid(Integer languageid) {
        this.languageid = languageid;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade == null ? null : grade.trim();
    }

    public Integer getAuthenticationid() {
        return authenticationid;
    }

    public void setAuthenticationid(Integer authenticationid) {
        this.authenticationid = authenticationid;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate == null ? null : certificate.trim();
    }

    public Integer getIsteacher() {
        return isteacher;
    }

    public void setIsteacher(Integer isteacher) {
        this.isteacher = isteacher;
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_enquary")
public class Enquary extends Model<Enquary> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6316042554722529315L;
	
	@TableId
	private int id;
	
	private String content;
	
	private String phone;
	
	private int userid;
	
	private int type;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	
	public Enquary(int id, String content, String phone, int userid, int type) {
		super();
		this.id = id;
		this.content = content;
		this.phone = phone;
		this.userid = userid;
		this.type = type;
	}
	public Enquary() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
	

}

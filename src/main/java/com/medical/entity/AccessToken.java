package com.medical.entity;

/**
 * 
 * @author ange
 * @time 2017年9月11日 上午9:42:49
 */

public class AccessToken {
	private String access_token;
	private Integer expires_in;
	private long startTime=0;
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public Integer getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
}

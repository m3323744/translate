package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_interpreter_order")
public class InterpreterOrder extends Model<InterpreterOrder> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -919830893622660855L;

	@TableId
    private Integer id;

    private Integer num;

    private Integer valuationtype;

    private String address;

    private Date starttime;

    private Date endtime;

    private String phone;

    private String remark;

    private Float totalprice;

    private Integer interpreterid;

    private Integer userid;

    private Integer travellingtype;

    private Integer states;

    private Integer paytype;

    private Date time;

    private String ordernumber;
    
    private Integer merchantid;
    
    

    public Integer getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(Integer merchantid) {
		this.merchantid = merchantid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getValuationtype() {
        return valuationtype;
    }

    public void setValuationtype(Integer valuationtype) {
        this.valuationtype = valuationtype;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Float getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Float totalprice) {
        this.totalprice = totalprice;
    }

    public Integer getInterpreterid() {
        return interpreterid;
    }

    public void setInterpreterid(Integer interpreterid) {
        this.interpreterid = interpreterid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getTravellingtype() {
        return travellingtype;
    }

    public void setTravellingtype(Integer travellingtype) {
        this.travellingtype = travellingtype;
    }

    public Integer getStates() {
        return states;
    }

    public void setStates(Integer states) {
        this.states = states;
    }

    public Integer getPaytype() {
        return paytype;
    }

    public void setPaytype(Integer paytype) {
        this.paytype = paytype;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber == null ? null : ordernumber.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_dynamic_like")
public class DynamicLike extends Model<DynamicLike> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2028012687969067199L;
	
	@TableId
	private Integer id;
	private Integer userid;
	private Integer dynamicid;
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public Integer getDynamicid() {
		return dynamicid;
	}
	public void setDynamicid(Integer dynamicid) {
		this.dynamicid = dynamicid;
	}

}

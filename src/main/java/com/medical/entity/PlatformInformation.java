package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_platform_information")
public class PlatformInformation extends Model<PlatformInformation> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4880689488052086091L;
	
	@TableId
	private int id;
	
	private int type;
	
	private String content;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public PlatformInformation(int id, int type, String content) {
		super();
		this.id = id;
		this.type = type;
		this.content = content;
	}
	public PlatformInformation() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}

}

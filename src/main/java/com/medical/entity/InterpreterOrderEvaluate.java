package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_interpreter_order_evaluate")
public class InterpreterOrderEvaluate extends Model<InterpreterOrderEvaluate> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1060002282962602020L;

	@TableId
    private Integer id;

    private Float score;

    private Integer interpreterid;

    private String content;

    private Date time;

    private Integer userid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public Integer getInterpreterid() {
        return interpreterid;
    }

    public void setInterpreterid(Integer interpreterid) {
        this.interpreterid = interpreterid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
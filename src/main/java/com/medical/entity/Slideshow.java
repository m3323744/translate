package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_slideshow")
public class Slideshow extends Model<Slideshow> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 824843944722857345L;

	@TableId
    private Integer id;

    private String name;

    private String url;

    private String image;
    
    private String pcurl;
    
    private Integer isfan;
    
    

    public String getPcurl() {
		return pcurl;
	}

	public void setPcurl(String pcurl) {
		this.pcurl = pcurl;
	}

	public Integer getIsfan() {
		return isfan;
	}

	public void setIsfan(Integer isfan) {
		this.isfan = isfan;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_hauptstudium")
public class Hauptstudium extends Model<Hauptstudium> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6887623822524645815L;

	@TableId
    private Integer id;

    private String hauptstudium;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHauptstudium() {
        return hauptstudium;
    }

    public void setHauptstudium(String hauptstudium) {
        this.hauptstudium = hauptstudium == null ? null : hauptstudium.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
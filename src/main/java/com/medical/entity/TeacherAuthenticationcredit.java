package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_teacher_authentication_credit")
public class TeacherAuthenticationcredit extends Model<TeacherAuthenticationcredit> {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1880774907589312232L;

	@TableId
	private Integer id;

    private Integer authenticationid;

    private String images;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthenticationid() {
        return authenticationid;
    }

    public void setAuthenticationid(Integer authenticationid) {
        this.authenticationid = authenticationid;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images == null ? null : images.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
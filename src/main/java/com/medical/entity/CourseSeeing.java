package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_course_seeing")
public class CourseSeeing extends Model<CourseSeeing> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8707871175865820143L;

	@TableId
    private Integer id;

    private String images;

    private String name;

    private Integer languageid;

    private Integer stageid;

    private String sources;

    private Float totalprice;

    private Integer hour;

    private Boolean isspelling;

    private Integer num;

    private Float spellingprice;

    private String introduceimage;

    private Integer schoolid;

    private Date time;
    
    private Integer userid;
    
    private Integer states;
    
    private Integer merchantid;
    
    private Boolean buystates;
    
    private Boolean release;

    public Boolean getRelease() {
		return release;
	}

	public void setRelease(Boolean release) {
		this.release = release;
	}

	public Boolean getBuystates() {
		return buystates;
	}

	public void setBuystates(Boolean buystates) {
		this.buystates = buystates;
	}

	public Integer getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(Integer merchantid) {
		this.merchantid = merchantid;
	}

    public Integer getStates() {
		return states;
	}

	public void setStates(Integer states) {
		this.states = states;
	}

    public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images == null ? null : images.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getLanguageid() {
        return languageid;
    }

    public void setLanguageid(Integer languageid) {
        this.languageid = languageid;
    }

    public Integer getStageid() {
        return stageid;
    }

    public void setStageid(Integer stageid) {
        this.stageid = stageid;
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources == null ? null : sources.trim();
    }

    public Float getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Float totalprice) {
        this.totalprice = totalprice;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Boolean getIsspelling() {
        return isspelling;
    }

    public void setIsspelling(Boolean isspelling) {
        this.isspelling = isspelling;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Float getSpellingprice() {
        return spellingprice;
    }

    public void setSpellingprice(Float spellingprice) {
        this.spellingprice = spellingprice;
    }

    public String getIntroduceimage() {
        return introduceimage;
    }

    public void setIntroduceimage(String introduceimage) {
        this.introduceimage = introduceimage == null ? null : introduceimage.trim();
    }

    public Integer getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Integer schoolid) {
        this.schoolid = schoolid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
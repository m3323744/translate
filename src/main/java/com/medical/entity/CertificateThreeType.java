package com.medical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_certificate_three_type")
public class CertificateThreeType extends Model<CertificateThreeType> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6927560075973305425L;

	@TableId
    private Integer id;

    private Integer twotypeid;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTwotypeid() {
        return twotypeid;
    }

    public void setTwotypeid(Integer twotypeid) {
        this.twotypeid = twotypeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}
}
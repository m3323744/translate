package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_enterprise")
public class Enterprise extends Model<Enterprise> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5734973612252376685L;

	@TableId
    private Integer id;

    private String address;

    private String city;

    private String introduce;

    private String creditcode;

    private String enhancement;

    private String legalname;

    private String legalhome;

    private String writtenname;

    private String writtenphone;

    private String idcardfront;

    private String idcardreverse;

    private Integer states;

    private String name;
    
    private Integer userid;
    
    private Date time;
    
    

    public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce == null ? null : introduce.trim();
    }

    public String getCreditcode() {
        return creditcode;
    }

    public void setCreditcode(String creditcode) {
        this.creditcode = creditcode == null ? null : creditcode.trim();
    }

    public String getEnhancement() {
        return enhancement;
    }

    public void setEnhancement(String enhancement) {
        this.enhancement = enhancement == null ? null : enhancement.trim();
    }

    public String getLegalname() {
        return legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname == null ? null : legalname.trim();
    }

    public String getLegalhome() {
        return legalhome;
    }

    public void setLegalhome(String legalhome) {
        this.legalhome = legalhome == null ? null : legalhome.trim();
    }

    public String getWrittenname() {
        return writtenname;
    }

    public void setWrittenname(String writtenname) {
        this.writtenname = writtenname == null ? null : writtenname.trim();
    }

    public String getWrittenphone() {
        return writtenphone;
    }

    public void setWrittenphone(String writtenphone) {
        this.writtenphone = writtenphone == null ? null : writtenphone.trim();
    }

    public String getIdcardfront() {
        return idcardfront;
    }

    public void setIdcardfront(String idcardfront) {
        this.idcardfront = idcardfront == null ? null : idcardfront.trim();
    }

    public String getIdcardreverse() {
        return idcardreverse;
    }

    public void setIdcardreverse(String idcardreverse) {
        this.idcardreverse = idcardreverse == null ? null : idcardreverse.trim();
    }

    public Integer getStates() {
        return states;
    }

    public void setStates(Integer states) {
        this.states = states;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    
	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_teacher")
public class Teacher extends Model<Teacher> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5504030680907235771L;

	@TableId
    private Integer id;

    private String images;

    private Integer userid;

    private String head;

    private String name;

    private String address;

    private String education;

    private String teaching;

    private Float price;

    private Integer languageid;

    private String languagegrade;

    private String hauptstudiumid;

    private String experience;

    private String make;

    private String imageone;

    private String imagetwo;

    private String imagethree;

    private String imageoneexperience;

    private String imagetwoexperience;

    private String imagethreeexperience;

    private String phone;
    
    private String longitude;
    
    private String latitude;
    
    private String certificate;
    
    private Date time;
    
    private int studying;
    
    private Boolean sex;
    
    private Integer states;
    
    private Boolean release;

    public Boolean getRelease() {
		return release;
	}

	public void setRelease(Boolean release) {
		this.release = release;
	}

	public Integer getStates() {
		return states;
	}

	public void setStates(Integer states) {
		this.states = states;
	}

    public int getStudying() {
		return studying;
	}

	public void setStudying(int studying) {
		this.studying = studying;
	}

	public Boolean getSex() {
		return sex;
	}

	public void setSex(Boolean sex) {
		this.sex = sex;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images == null ? null : images.trim();
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head == null ? null : head.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education == null ? null : education.trim();
    }

    public String getTeaching() {
        return teaching;
    }

    public void setTeaching(String teaching) {
        this.teaching = teaching == null ? null : teaching.trim();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getLanguageid() {
        return languageid;
    }

    public void setLanguageid(Integer languageid) {
        this.languageid = languageid;
    }

    public String getLanguagegrade() {
        return languagegrade;
    }

    public void setLanguagegrade(String languagegrade) {
        this.languagegrade = languagegrade == null ? null : languagegrade.trim();
    }

    public String getHauptstudiumid() {
        return hauptstudiumid;
    }

    public void setHauptstudiumid(String hauptstudiumid) {
        this.hauptstudiumid = hauptstudiumid == null ? null : hauptstudiumid.trim();
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience == null ? null : experience.trim();
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make == null ? null : make.trim();
    }

    public String getImageone() {
        return imageone;
    }

    public void setImageone(String imageone) {
        this.imageone = imageone == null ? null : imageone.trim();
    }

    public String getImagetwo() {
        return imagetwo;
    }

    public void setImagetwo(String imagetwo) {
        this.imagetwo = imagetwo == null ? null : imagetwo.trim();
    }

    public String getImagethree() {
        return imagethree;
    }

    public void setImagethree(String imagethree) {
        this.imagethree = imagethree == null ? null : imagethree.trim();
    }

    public String getImageoneexperience() {
        return imageoneexperience;
    }

    public void setImageoneexperience(String imageoneexperience) {
        this.imageoneexperience = imageoneexperience == null ? null : imageoneexperience.trim();
    }

    public String getImagetwoexperience() {
        return imagetwoexperience;
    }

    public void setImagetwoexperience(String imagetwoexperience) {
        this.imagetwoexperience = imagetwoexperience == null ? null : imagetwoexperience.trim();
    }

    public String getImagethreeexperience() {
        return imagethreeexperience;
    }

    public void setImagethreeexperience(String imagethreeexperience) {
        this.imagethreeexperience = imagethreeexperience == null ? null : imagethreeexperience.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}